<?php

return [
    'message_confirm_reserve'               => 'Su reserva en *business* esta pendiente de confirmación para el día *day* a las *hour*, el código de reserva es *code*',
    'message_confirm_reserve_res'           => '[business,day,hour,code]',
    'message_confirm_reserve_des'           => 'Mensaje que se le envía al cliente cuando la reserva queda pendiente de confirmación',

    'message_autoconfirm_reserve'           => 'Su reserva en *business* fue agendada y confirmada para el día *day* a las *hour*, el código de reserva es *code*',
    'message_autoconfirm_reserve_res'       => '[business,day,hour,code]',
    'message_autoconfirm_reserve_des'       => 'Mensaje que se le envía al cliente cuando la reserva se confirma automáticamente',
    
    'message_confirm_task'                  => 'Su reserva en *business* fue confirmada para el día *day* a las *hour*, el código de reserva es *code*',
    'message_confirm_task_res'              => '[business,day,hour,code]',
    'message_confirm_task_des'              => 'Mensaje que se le envía al cliente cuando confirmamos la reserva manualmente',

    'message_refuse_task'                   => 'Su reserva en *business* del día *day* a las *hour* fue rechazada, comentario del negocio: *comment*',
    'message_refuse_task_res'               => '[business,day,hour,comment]',
    'message_refuse_task_des'               => 'Mensaje que se le envía al cliente cuando rechazamos la reserva',

    'message_remember_task'                 => 'Le recordamos que el día de mañana tiene una reserva en *business* a la hora *hour*, si no asistirá por favor ingresar a *url* para cancelar.',
    'message_remember_task_res'             => '[business,hour,url]',
    'message_remember_task_des'             => 'Recordatorio que se le envía al cliente un día antes.',

    'message_modify_task'                   => 'Su reserva en *business* fue modificada para el día *day* a las *hour*',
    'message_modify_task_res'               => '[business,day,hour]',
    'message_modify_task_des'               => 'Mensaje que se le envía al cliente cuando se modifica la reserva',

    'message_delete_task'                   => 'Su reserva en *business* para el día *day* a las *hour* fue cancelada por la empresa, comentario del negocio: *comment*',
    'message_delete_task_res'               => '[business,day,hour,comment]',
    'message_delete_task_des'               => 'Mensaje que se le envía al cliente cuando se elimina la reserva',

    'message_birthday'                      => 'El equipo de *business* le desea un muy feliz cumpleaños.',
    'message_birthday_res'                  => '[business]',
    'message_birthday_des'                  => 'Recordatorio de cumpleaños que se le envía al cliente',

    'message_waiting_list'                  => 'La empresa *business* tiene un horario disponible el dia *date* con *user*',
    'message_waiting_list_res'              => '[business,date,user]',
    'message_waiting_list_des'              => 'Mensaje que se le envia al cliente que esta en lista de espera',
];
