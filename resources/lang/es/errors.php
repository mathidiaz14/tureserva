<?php

return [

    'open'                          => 'No se puedo abrir el registro',
    'update'                        => 'No se puedo actualizar el registro',
    'delete'                        => 'No se puedo eliminar el registro',

    'not_user'                      => 'El usuario no está registrado en la plataforma',
    'repeat_user'                   => 'Este email ya está registrado en nuestro sistema',
    'repeat_code'                   => 'El nombre corto ya existe, debe seleccionar otro',
    
    'password_reset'                => 'Error con el reseteo de contraseña, realice el proceso nuevamente.',
    'password_invitation'           => 'Error con el código de invitación, el administrador debe enviar una nueva invitación',
    'password_not_match'            => 'Las contraseñas no coinciden',
    'password_not_length'           => 'La contraseña debe tener al menos 8 caracteres',
    
    'pay_not_check'                 => 'El pago no se pudo comprobar',
    'pay_not_process'               => 'No pudimos procesar tu pago, prueba nuevamente',
    'pay_pending'                   => 'El pago está pendiente de aprobación, el plan será actualizado cuando podamos aprobarlo.',
    'pay_pending'                   => 'Su pago aún está pendiente.',
    'not_business'                  => 'La empresa no existe',
    'user_disabled'                 => 'El usuario no está habilitado para acceder al sistema, comuniquese con el administrador de su negocio.',
    'plan_many_employees'           => 'Usted tiene :count empleados registrados, debe selecciónar un plan mayor',
    'delete_logo'                   => 'No se pudo eliminar el logo',
    'mercadopago_connect'           => 'No se pudo conectar con MercadoPago, inténtelo nuevamente',
    'not_hour_reservation'          => 'La hora que intenta reservar está ocupada, por favor intenta nuevamente.',
    'not_reservation'               => 'No se encontró la reserva, corrobore el código ingresado',
    'upgrade_plan'                  => 'Usted tiene :count empleados registrados, debe seleccionar un plan mayor',
    'not_upgrade'                   => 'El plan no se pudo actualizar, prueba con otro medio de pago.',
    
    'limit_user_plan1'              => 'Con tu plan solo puedes tener 1 empleado, puedes actualizar tu plan cuando quieras en la sección "Actualizar plan"',
    'limit_user_plan2'              => 'Con tu plan solo puedes tener 3 empleados, puedes actualizar tu plan cuando quieras en la sección "Actualizar plan"',

    'limit_calendar_plan1'          => 'Con tu plan solo puedes tener 30 citas, puedes actualizar tu plan cuando quieras en la sección "Actualizar plan"',
    'limit_calendar_plan2'          => 'Con tu plan solo puedes tener 100 citas, puedes actualizar tu plan cuando quieras en la sección "Actualizar plan"',

    'limit_service_plan1'          => 'Con tu plan solo puedes tener 3 servicios, puedes actualizar tu plan cuando quieras en la sección "Actualizar plan"',
    'limit_service_plan2'          => 'Con tu plan solo puedes tener 5 servicios, puedes actualizar tu plan cuando quieras en la sección "Actualizar plan"',

    'registry_null'                 => 'No se encontro ningún registro',
    'client_create_error'           => 'Debe completar los campos obligatorios: Nombre y Teléfono.',
    'not_more_data'                 => 'No hay ningun dato sobre esta reserva',
    'code_not_exist'                => 'El codigo ingresado no existe',
];
