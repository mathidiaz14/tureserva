<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- SEO Meta Tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
	<meta property="og:site_name" content="" /> <!-- website name -->
	<meta property="og:site" content="" /> <!-- website link -->
	<meta property="og:title" content=""/> <!-- title shown in the actual shared post -->
	<meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
	<meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
	<meta property="og:url" content="" /> <!-- where do you want your post to link to -->
	<meta property="og:type" content="article" />

    <!-- Website Title -->
    <title>{{env('APP_NAME')}}</title>
    
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
    <link href="{{asset('landing/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('landing/css/fontawesome-all.css')}}" rel="stylesheet">
    <link href="{{asset('landing/css/swiper.css')}}" rel="stylesheet">
	<link href="{{asset('landing/css/magnific-popup.css')}}" rel="stylesheet">
	<link href="{{asset('landing/css/styles.css')}}" rel="stylesheet">
	
	<!-- Favicon  -->
    <link rel="icon" href="{{asset('images/icon.png')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Facebook Pixel Code -->
    <script>!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','https://connect.facebook.net/en_US/fbevents.js'); fbq('init', '367680415432301'); fbq('track', 'PageView');
    </script>
    
    <noscript> 
        <img height="1" width="1" src="https://www.facebook.com/tr?id=367680415432301&ev=PageView&noscript=1"/>
    </noscript>
    
    <!-- End Facebook Pixel Code -->
</head>
<body data-spy="scroll" data-target=".fixed-top">
    
    <!-- Preloader -->
	<div class="spinner-wrapper">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <!-- end of preloader -->
    

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
        <!-- Text Logo - Use this if you don't have a graphic logo -->
        <a class="navbar-brand logo-text page-scroll" href="{{url('/')}}">
            <img src="{{asset('images/icon.png')}}" alt="" width="50px" class="mr-2">
            {{env('APP_NAME')}}
        </a>
        
        <!-- Mobile Menu Toggle Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-awesome fas fa-bars"></span>
            <span class="navbar-toggler-awesome fas fa-times"></span>
        </button>
        <!-- end of mobile menu toggle button -->

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#header">Inicio <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#services">Ventajas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#pricing">Precios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#request">Más info.</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link page-scroll" href="#contact">Contacto</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link page-scroll" href="{{url('cancelar')}}">Cancelar reserva</a>
                </li>
                @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link page-scroll" href="{{url('home')}}">Dashboard</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('register')}}">Registrarse</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('login')}}">Iniciar Sesión</a>
                    </li>
                @endif
            </ul>
            <span class="nav-item social-icons">
                <span class="fa-stack">
                    <a href="#your-link">
                        <i class="fas fa-circle fa-stack-2x facebook"></i>
                        <i class="fab fa-facebook-f fa-stack-1x"></i>
                    </a>
                </span>
                <span class="fa-stack">
                    <a href="#your-link">
                        <i class="fas fa-circle fa-stack-2x twitter"></i>
                        <i class="fab fa-twitter fa-stack-1x"></i>
                    </a>
                </span>
            </span>
        </div>
    </nav> <!-- end of navbar -->
    <!-- end of navigation -->


    <!-- Header -->
    <header id="header" class="header">
        <div class="header-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="text-container">
                            <h1><span class="turquoise">Menos trabajo administrativo,</span> <br> más clientes.</h1>
                            <p class="p-large">
                                Tus clientes podrán reservar cita contigo las 24 horas del día a través de tu página web, tus perfiles de Instagram o Facebook, o incluso a través de la búsqueda de Google. Con el calendario online de <b>{{env('APP_NAME')}}</b>, tendrás siempre a mano toda la información necesaria para las citas con tus clientes, incluso cuando no estás físicamente en la empresa.
                            </p>
                            <a class="btn-solid-lg page-scroll" href="#services">Ver mas</a>
                        </div> <!-- end of text-container -->
                    </div> <!-- end of col -->
                    <div class="col-lg-6">
                        <div class="image-container">
                            <img class="img-fluid" src="{{asset('landing/images/header-teamwork.svg')}}" alt="alternative">
                        </div> <!-- end of image-container -->
                    </div> <!-- end of col -->
                </div> <!-- end of row -->
            </div> <!-- end of container -->
        </div> <!-- end of header-content -->
    </header> <!-- end of header -->
    <!-- end of header -->


    <!-- Customers -->
    <div class="slider-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h5>Pensado para ti...</h5>
                    
                    <!-- Image Slider -->
                    <div class="slider-container">
                        <div class="swiper-container image-slider">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Peluquerías
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Centros de estética
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Centros de salud
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Estudios jurídicos
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Couching
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Peluquería canina
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Entrenadores personales
                                        </h4>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Talleres
                                        </h4>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Centros deportivos
                                        </h4>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Fisioterapeutas
                                        </h4>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Instaladores
                                        </h4>
                                    </div>
                                </div>


                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Estudios de tatuajes
                                        </h4>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="image-container">
                                        <h4 style="color:#83a6cc;">
                                            Profesores online
                                        </h4>
                                    </div>
                                </div>
                            </div> <!-- end of swiper-wrapper -->
                        </div> <!-- end of swiper container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of image slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-1 -->
    <!-- end of customers -->


    <!-- Services -->
    <div id="services" class="cards-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-5">
                    <h2>Ventajas para ti</h2>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Card -->
                    <div class="card">
                        <i class="far fa-calendar-alt fa-4x" style="color:#83a6cc;"></i>
                        <br><br><br>
                        <div class="card-body">
                            <h5 class="card-title">
                                Consigue mas clientes y evita los tiempos de inactividad
                            </h5>
                        </div>
                    </div>
                    <!-- end of card -->

                    <!-- Card -->
                    <div class="card">
                        <i class="fa fa-money-bill fa-4x" style="color:#83a6cc;"></i>
                        <br><br><br>
                        <div class="card-body">
                            <h5 class="card-title">
                                Recibe pagos a través de MercadoPago 
                            </h5>
                        </div>
                    </div>
                    <!-- end of card -->

                    <!-- Card -->
                    <div class="card">
                        <i class="far fa-file fa-4x" style="color:#83a6cc;"></i>
                        <br><br><br>
                        <div class="card-body">
                            <h5 class="card-title">
                                Dedica menos tiempo a tareas administrativas
                            </h5>
                        </div>
                    </div>
                    <!-- end of card -->
                    
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->

        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-5">
                    <h2>Ventajas para tus clientes</h2>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Card -->
                    <div class="card">
                        <i class="far fa-clock fa-4x" style="color:#83a6cc;"></i>
                        <br><br><br>
                        <div class="card-body">
                            <h5 class="card-title">
                                Tus clientes podrán reservar sus citas con solo un par de clics.
                            </h5>
                        </div>
                    </div>
                    <!-- end of card -->

                    <!-- Card -->
                    <div class="card">
                        <i class="far fa-calendar-check fa-4x" style="color:#83a6cc;"></i>
                        <br><br><br>
                        <div class="card-body">
                            <h5 class="card-title">
                                Ofrece a tus clientes la posibilidad de hacer sus reservas las 24 horas del día sin importar dónde estén. 
                            </h5>
                        </div>
                    </div>
                    <!-- end of card -->

                    <!-- Card -->
                    <div class="card">
                        <i class="far fa-bell fa-4x" style="color:#83a6cc;"></i>
                        <br><br><br>
                        <div class="card-body">
                            <h5 class="card-title">
                                Ayuda a tus clientes a no olvidar sus citas
                            </h5>
                        </div>
                    </div>
                    <!-- end of card -->
                    
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-1 -->
    <!-- end of services -->


    <!-- Details 1 -->
    <div class="basic-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>
                            Menos citas perdidas
                        </h2>
                        <p>
                            Nuestra plataforma les enviará a tus clientes un correo electrónico o un mensaje de texto automático para recordarles que tienen una cita contigo. Los recordatorios automáticos, además de ser muy útiles para tus clientes, te ayudarán a reducir el número de clientes que no se presenta y, por tanto, aumentarán tus ingresos.
                        </p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="{{asset('landing/images/undraw_completed_tasks_vs6q.svg')}}" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-1 -->
    <!-- end of details 1 -->

    
    <!-- Details 2 -->
    <div class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="{{asset('landing/images/details-2-office-team-work.svg')}}" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>
                            Todas las citas en tu agenda, todos los clientes en tu base de datos
                        </h2>
                        <p>
                            Las citas que reserven tus clientes se registrarán de forma automática en la agenda online, mientras que en la base de datos se creará automáticamente un perfil digital para cada cliente. Esto te ahorrará mucho tiempo en la gestión de clientes y citas.
                        </p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of details 2 -->

    <!-- Details 2 -->
    <div class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>
                            Posibilidad de cobro online
                        </h2>
                        <p>
                            Con {{env('APP_NAME')}}, tus clientes podrán pagar sus citas directamente online. Ello te ayudará a evitar pérdidas de ingresos, sobre todo en el caso de servicios con precios elevados. Además, también es una forma de satisfacer la creciente demanda de opciones de pago digitales.
                        </p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="{{asset('landing/images/details-3-pay.svg')}}" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of details 2 -->

    <!-- Details 2 -->
    <div class="basic-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="{{asset('landing/images/undraw_mobile_interface_re_1vv9.svg')}}" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>
                            Tu propia APP
                        </h2>
                        <p>
                            Podras tener tu propia aplicación movil para que tus clientes la descarguen y te tengan siempre a la mano. Con ella podras estar en contacto con los clientes mas habituales y enviarle promociones especificas.
                        </p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of basic-2 -->
    <!-- end of details 2 -->

    <!-- Pricing -->
    <div id="pricing" class="cards-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>¡Tienes 30 días de prueba!</h2>
                    <p class="p-heading p-large">
                        
                    </p>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-12">

                    <!-- Card-->
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Inicial</div>
                            <div class="card-subtitle">Pequeños negocios</div>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="currency">$</span>
                                <span class="value">{{env('PLAN1')}}</span>
                                <span class="currency">/mes</span>
                            </div>
                            <hr class="cell-divide-hr">
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        1 Empleado
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        3 Servicios
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Agenda online
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        30 Citas / Mes
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        30 Recordatorios / Mes
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i>
                                    <div class="media-body">
                                        Tu propia web
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i>
                                    <div class="media-body">
                                        Lista de espera
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-times"></i>
                                    <div class="media-body">
                                        Soporte ilimitado
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end of card -->
                    <!-- end of card -->

                    <!-- Card-->
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Avanzado</div>
                            <div class="card-subtitle">Negocios en crecimiento</div>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="currency">$</span>
                                <span class="value">{{env('PLAN2')}}</span>
                                <span class="currency">/mes</span>
                            </div>
                            <hr class="cell-divide-hr">
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        3 Empleados
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        5 Servicios
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Agenda y reserva online
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        100 Citas / Mes
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        100 Recordatorios / Mes
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Tu propia web
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Lista de espera
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Soporte avanzado
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end of card -->

                     <!-- Card-->
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title">Premium</div>
                            <div class="card-subtitle">Negocios despegados</div>
                            <hr class="cell-divide-hr">
                            <div class="price">
                                <span class="currency">$</span>
                                <span class="value">{{env('PLAN3')}}</span>
                                <span class="currency">/mes</span>
                            </div>
                            <hr class="cell-divide-hr">
                            <ul class="list-unstyled li-space-lg">
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Empleados ilimitados
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Servicios ilimitados
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Agenda y reserva online
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Citas ilimitadas
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Recordatorios ilimitados
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Tu propia web
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Lista de espera
                                    </div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-check"></i>
                                    <div class="media-body">
                                        Soporte ilimitado
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end of card -->
                    <!-- end of card -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of cards-2 -->
    <!-- end of pricing -->

    <!-- Request -->
    <div id="request" class="form-1">
        <div class="container">
            <div class="row">
                @if(session()->has('form_advice'))
                    <div class="col-12 alert alert-success text-center pt-4">
                        <p>
                            {{session('form_advice')}}  
                            {{session()->forget('form_advice')}}
                        </p>
                    </div>
                @endif
                <div class="col-lg-6">
                    <div class="text-container">
                        <h2>Asesoramiento personalizado:</h2>
                        <p>Escribenos y te enviaremos toda la información que necesites.</p>
                        <ul class="list-unstyled li-space-lg">
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">
                                    <strong class="blue">Conoce todas las funciones del producto</strong>
                                </div>
                            </li>
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">
                                    <strong class="blue">Diseño personalizado para tu negocio</strong>
                                </div>
                            </li>
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">
                                    <strong class="blue">Tu propio dominio .uy</strong>
                                </div>
                            </li>
                            <li class="media">
                                <i class="fas fa-check"></i>
                                <div class="media-body">
                                    <strong class="blue">Asesoramiento individualizado para ti y tu equipo</strong>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <!-- Request Form -->
                    <div class="form-container">
                        <form action="{{url('contact_root')}}" method="post" data-toggle="validator" data-focus="false">
                            @csrf
                            <input type="hidden" name="contact" value="advice">
                            <div class="form-group">
                                <input type="text" class="form-control-input" id="name" name="name" required>
                                <label class="label-control" for="name">Nombre completo</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control-input" id="email" name="email" required>
                                <label class="label-control" for="email">Email</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control-input" id="phone" name="phone" required>
                                <label class="label-control" for="phone">Telefono</label>
                                <div class="help-block with-errors"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control-submit-button">Enviar</button>
                            </div>
                        </form>
                    </div> <!-- end of form-container -->
                    <!-- end of request form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-1 -->
    <!-- end of request -->


    <!-- Testimonials -->
    <div class="slider-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="image-container">
                        <img class="img-fluid" src="{{asset('landing/images/testimonials-2-men-talking.svg')}}" alt="alternative">
                    </div> <!-- end of image-container -->
                </div> <!-- end of col -->
                <div class="col-lg-6">
                    <h2>Testimonios</h2>

                    <!-- Card Slider -->
                    <div class="slider-container">
                        <div class="swiper-container card-slider">
                            <div class="swiper-wrapper">
                                
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('landing/images/testimonial-1.svg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">Queremos ofrecer a nuestros clientes el mejor servicio siempre, lo cual para nosotros también implica la facilidad para reservar citas. Gracias a {{env('APP_NAME')}}, nuestros clientes pueden reservar con nosotros las 24 horas del día los 7 días de la semana desde nuestra página web, Google o Instagram</p>
                                        </div>
                                    </div>
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('landing/images/testimonial-2.svg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">¡Con el software {{env('APP_NAME')}} nos ahorramos mucho dinero y recursos! {{env('APP_NAME')}} realiza las tareas administrativas relacionadas con la gestión de citas y clientes, de forma que nosotros tenemos más tiempo para centrarnos en nuestros clientes y otro tipo de tareas más rentables</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
        
                                <!-- Slide -->
                                <div class="swiper-slide">
                                    <div class="card">
                                        <img class="card-image" src="{{asset('landing/images/testimonial-3.svg')}}" alt="alternative">
                                        <div class="card-body">
                                            <p class="testimonial-text">{{env('APP_NAME')}} me ahorra muchísimo tiempo. Si no lo usara, tendría que contratar a alguien para que se encargara de las tareas administrativas</p>
                                        </div>
                                    </div>        
                                </div> <!-- end of swiper-slide -->
                                <!-- end of slide -->
                               
                            </div> <!-- end of swiper-wrapper -->
        
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <!-- end of add arrows -->
        
                        </div> <!-- end of swiper-container -->
                    </div> <!-- end of slider-container -->
                    <!-- end of card slider -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of slider-2 -->
    <!-- end of testimonials -->


    <!--
    <div id="about" class="basic-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>About The Team</h2>
                    <p class="p-heading p-large">Meat our team of specialized marketers and business developers which will help you research new products and launch them in new emerging markets</p>
                </div> 
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="team-member">
                        <div class="image-wrapper">
                            <img class="img-fluid" src="images/team-member-1.svg" alt="alternative">
                        </div> 
                        <p class="p-large"><strong>Lacy Whitelong</strong></p>
                        <p class="job-title">Business Developer</p>
                        <span class="social-icons">
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x facebook"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                                </a>
                            </span>
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x twitter"></i>
                                    <i class="fab fa-twitter fa-stack-1x"></i>
                                </a>
                            </span>
                        </span> 
                    </div> 
                    
                    <div class="team-member">
                        <div class="image-wrapper">
                            <img class="img-fluid" src="images/team-member-2.svg" alt="alternative">
                        </div>
                        <p class="p-large"><strong>Chris Brown</strong></p>
                        <p class="job-title">Online Marketer</p>
                        <span class="social-icons">
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x facebook"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                                </a>
                            </span>
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x twitter"></i>
                                    <i class="fab fa-twitter fa-stack-1x"></i>
                                </a>
                            </span>
                        </span> 
                    </div> 

                    <div class="team-member">
                        <div class="image-wrapper">
                            <img class="img-fluid" src="images/team-member-3.svg" alt="alternative">
                        </div>
                        <p class="p-large"><strong>Sheila Zimerman</strong></p>
                        <p class="job-title">Software Engineer</p>
                        <span class="social-icons">
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x facebook"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                                </a>
                            </span>
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x twitter"></i>
                                    <i class="fab fa-twitter fa-stack-1x"></i>
                                </a>
                            </span>
                        </span> 
                    </div> 
                    <div class="team-member">
                        <div class="image-wrapper">
                            <img class="img-fluid" src="images/team-member-4.svg" alt="alternative">
                        </div> 
                        <p class="p-large"><strong>Mary Villalonga</strong></p>
                        <p class="job-title">Product Manager</p>
                        <span class="social-icons">
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x facebook"></i>
                                    <i class="fab fa-facebook-f fa-stack-1x"></i>
                                </a>
                            </span>
                            <span class="fa-stack">
                                <a href="#your-link">
                                    <i class="fas fa-circle fa-stack-2x twitter"></i>
                                    <i class="fab fa-twitter fa-stack-1x"></i>
                                </a>
                            </span>
                        </span> 
                    </div> 
                </div>
            </div> 
        </div> 
    </div> 
-->
    

    <!-- Contact -->
    <div id="contact" class="form-2">
        <div class="container">
            <div class="row">
                @if(session()->has('form'))
                    <div class="col-12 alert alert-success text-center pt-4 mb-3">
                        <p>
                            {{session('form')}}  
                            {{session()->forget('form')}}
                        </p>
                    </div>
                @endif
                <div class="col-lg-12">
                    <h2>Información de contacto</h2>
                    <ul class="list-unstyled li-space-lg">
                        <li><i class="fas fa-phone"></i><a class="turquoise" href="tel:00598098481910">+598 098 481 910 </a></li>
                        <li><i class="fas fa-envelope"></i><a class="turquoise" href="{{env('MAIL_FROM_ADDRESS')}}">{{env('MAIL_FROM_ADDRESS')}}</a></li>
                    </ul>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    
                    <!-- Contact Form -->
                    <form action="{{url('contact_root')}}" method="post" data-toggle="validator" data-focus="false">
                        @csrf
                        <input type="hidden" name="contact" value="contact">
                        <div class="form-group">
                            <input type="text" class="form-control-input" id="cname" required>
                            <label class="label-control" for="cname">Nombre</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control-input" id="cemail" required>
                            <label class="label-control" for="cemail">Email</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control-textarea" id="cmessage" required></textarea>
                            <label class="label-control" for="cmessage">Mensaje</label>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control-submit-button">Enviar mensaje</button>
                        </div>
                        <div class="form-message">
                            <div id="cmsgSubmit" class="h3 text-center hidden"></div>
                        </div>
                    </form>
                    <!-- end of contact form -->

                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of form-2 -->
    <!-- end of contact -->


    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">by <a href="https://mathiasdiaz.uy">Mathias Díaz.uy</a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright --> 
    <!-- end of copyright -->
    
    	
    <!-- Scripts -->
    <script src="{{asset('landing/js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
    <script src="{{asset('landing/js/popper.min.js')}}"></script> <!-- Popper tooltip library for Bootstrap -->
    <script src="{{asset('landing/js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework -->
    <script src="{{asset('landing/js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
    <script src="{{asset('landing/js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders -->
    <script src="{{asset('landing/js/jquery.magnific-popup.js')}}"></script> <!-- Magnific Popup for lightboxes -->
    <script src="{{asset('landing/js/validator.min.js')}}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
    <script src="{{asset('landing/js/scripts.js')}}"></script> <!-- Custom scripts -->

    @if(true)
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-1VT242W818"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-1VT242W818');
        </script>

        <script type="text/javascript">

            function callbackThen(response)
            {
                console.log(response.status);
                response.json().then(function(data)
                {
                    console.log(data);
                });
            }

            function callbackCatch(error)
            {
                console.error('Error:', error)
            }

        </script>
        

        {!! htmlScriptTagJsApi([

            'callback_then' => 'callbackThen',

            'callback_catch' => 'callbackCatch'

        ]) !!}
    @endif

</body>
</html>