@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">
				@if($data['user']->gender == "male")
					Bienvenido {{$data['user']->name}}
				@elseif($data['user']->gender == "female")
					Bienvenida {{$data['user']->name}}
				@else
					Bienvenide {{$data['user']->name}}
				@endif
				<br><br>
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<p>Estamos muy contentos de que formes parte de nuestra plataforma, ahora solo necesitamos que confirmes tu correo electrónico haciendo click en el siguiente botón.</p>
				<br /><br /><br />
				<a class="btn" style="color: white; padding:20px;" href="{{ url('verify', $confirmation_code) }}">Verificar correo</a>
				<br /><br /><br />
				<small style="font-size: 10px;">Si el enlace no funciona, copie y pegue la siguiente url en el navegador: {{ url('verify', $confirmation_code) }}</small>
				<p>Muchas gracias por elegirnos</p>
			</td>
		</tr>
	</table>
@endsection