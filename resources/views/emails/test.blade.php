@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Prueba del administrador de {{config('app.name')}}<br><br></td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dignissimos sunt quaerat accusantium facere laudantium quas ratione voluptatibus nesciunt, dolor commodi nostrum, illum, officiis tenetur ullam amet unde eveniet corrupti corporis.</p>
				<br />
			</td>
		</tr>
	</table>
@endsection