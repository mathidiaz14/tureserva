@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">
				Hay un horario disponible para ti
				<br><br>
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<p>
					La empresa {{$data['business']->name}} tiene un horario disponible el dia {{$data['date']}} con {{$data['user']->name}}
				</p>
				<br />
				<br>
				<a class="btn" style="color: white; padding:20px;" href="{{ url($business->code) }}">Ingresa a reservar</a>
				<br>
				<br>
			</td>
		</tr>
	</table>
@endsection