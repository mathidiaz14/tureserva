@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Danos tu opinión<br><br></td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<p>
					Danos tu opinión sobre la cita realizada el dia <b>{{$data['task']->start->format('d/m/Y')}}</b> a la hora <b>{{$data['task']->start->format('H:i')}}</b> 
							
					@if($task->users->first() != null)
						con <b>{{$data['task']->users->first()->name}}</b>
					@endif

					en <b>{{$data['task']->business->name}}</b>
				</p>
				<br><br>
				<a class="btn" style="color: white; padding:20px;" href="{{ url('web/opinion', $task->code) }}">Danos tu opinión</a>
				<br /><br />
				<small style="font-size: 10px;">Si el enlace no funciona, copie y pegue la siguiente url en el navegador: {{ url('web/opinion', $task->code) }}</small>
				<br />
			</td>
		</tr>
	</table>
@endsection