@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 30px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Reserva web aprobada</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style="font-size: 20px; line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				<p>Esta es una confirmación de que su cita fue registrada exitosamente.
				<br /><br />
				<p>Los datos de la reserva son: </p>
				<br />
				<table>
					<tbody>
						<tr>
							<td>Negocio:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['business']}}</b></td>
						</tr>
						<tr>
							<td>Servicio:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['service']}}</b></td>
						</tr>
						<tr>
							<td>Empleado:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['user']}}</b></td>
						</tr>
						<tr>
							<td>Fecha</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['date']}}</b></td>
						</tr>
						<tr>
							<td>Hora:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['hour']}}</b></td>
						</tr>
						<tr>
							<td>Nombre:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['name']}}</b></td>
						</tr>
						<tr>
							<td>Teléfono:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['phone']}}</b></td>
						</tr>
						<tr>
							<td>Email:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['email']}}</b></td>
						</tr>
						<tr>
							<td>Comentario:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['comment']}}</b></td>
						</tr>
						<tr>
							<td>Código de reserva:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['code']}}</b></td>
						</tr>
						<tr>
							<td>Estado:</td>
							<td style="min-width: 20px;"></td>
							<td><b>{{$data['status']}}</b></td>
						</tr>
					</tbody>
				</table>
				<br />
				<p>Muchas gracias por elegirnos</p>
			</td>
		</tr>
	</table>
@endsection