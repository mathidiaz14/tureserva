@extends('email.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Su pago fue rechazado<br><br></td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<p>Lamentablemente no pudimos aprobar tu pago, es necesario que intentes con otro medio de pago para asi poder utilizar la plataforma</p>
				<br />
				<p>Estamos a las ordenes por cualquier consulta.</p>
			</td>
		</tr>
	</table>
@endsection