@extends('email.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Su pago fue aprobado<br><br></td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<p>Ya puedes iniciar sesion con los datos que colocaste al momento de registrarte</p>
				<br />
				<p>Muchas gracias por elegirnos</p>
			</td>
		</tr>
	</table>
@endsection