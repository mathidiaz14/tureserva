@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">
				@if($data['user']->gender == "male")
					Bienvenido {{$data['user']->name}}
				@elseif($data['user']->gender == "female")
					Bienvenida {{$data['user']->name}}
				@else
					Bienvenide {{$data['user']->name}}
				@endif
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style="font-size: 20px; line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				<p>La empresa {{$data['business']}} te envio una invitación para que ingreses al sistema, haz click en el siguiente enlace para colocar una contraseña</p>
				<br /><br /><br />
				<a class="btn" style="color: white; padding:20px;" href="{{ url('invitation', $invitation_code) }}">Registrar contraseña</a>
				<br /><br /><br />
				<small style="font-size: 10px;">Si el enlace no funciona, copie y pegue la siguiente url en el navegador: {{ url('invitation', $invitation_code) }}</small>
				<p>Muchas gracias por elegirnos</p>
			</td>
		</tr>
	</table>
@endsection