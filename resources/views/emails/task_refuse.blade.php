@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 40px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Reserva web rechazada</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style="font-size: 20px; line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				<p>Lamentamos informarle que el negocio "{{$data['business']}}" no aprobó la cita que habías realizado para el día {{$data['date']}} a la hora {{$data['hour']}}, dejaron el siguiente comentario para ti:</p>
				<br />
				<b>"{{$data['comment']}}"</b>
				<br />
				<br />
				<p>Muchas gracias por elegirnos</p>
			</td>
		</tr>
	</table>
@endsection