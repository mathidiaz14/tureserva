@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 40px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Respuesta a tu consulta</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style="font-size: 20px; line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				
				<p>La empresa {{$data['business']}} respondio a tu consulta</p><br>
		        <br><hr><br>
		        {{$data['body']}}
		        <br><hr><br>
		        <br><br>Saludos y muy buenos días le desea el equipo de {{config('app.name')}}

			</td>
		</tr>
	</table>
@endsection