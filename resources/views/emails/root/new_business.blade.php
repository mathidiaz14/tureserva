@extends('emails.master')

@section('content')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">Nueva empresa registrada</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style="font-size: 20px; line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				<table>
					<tr>
						<td>Nombre:</td>
						<td>{{$data['name']}}</td>
					</tr>
					<tr>
						<td>Email:</td>
						<td>{{$data['email']}}</td>
					</tr>
					<tr>
						<td>Teléfono:</td>
						<td>{{$data['phone']}}</td>
					</tr>
					<tr>
						<td>Nombre usuario:</td>
						<td>{{$data['user']}}</td>
					</tr>
					<tr>
						<td>API:</td>
						<td>{{$data['api']}}</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
@endsection