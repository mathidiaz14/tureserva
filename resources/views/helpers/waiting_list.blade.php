<!-- Button trigger modal -->
<a class="link_waiting_list" style="cursor: pointer;" data-bs-toggle="modal" data-bs-target="#waitingModal">
  {{trans('web.waiting_list')}}
</a>

<!-- Modal -->
<div class="modal fade" id="waitingModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content text-start">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
        	{{trans('web.waiting_title')}}
        </h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="{{url('web/waiting')}}" class="form-horizontal" method="post">
        	@csrf
        	<input type="hidden" name="business" value="{{$service->business->id}}">
        	<input type="hidden" name="service" value="{{$service->id}}">
        	<input type="hidden" name="user" value="{{$user == 'nobody' ? 'nobody' : $user->id}}">
        	
        	<div class="row">
	        	<div class="col-12">
	        		<div class="form-group mb-2">
		        		<label for="">{{trans('web.day_select')}}</label>
		        		
			    			<div class="row mt-2">
			    				@for($i=1; $i<$service->business->reservation->days_reserve; $i++)
			        				@php
				        				$day 	= Carbon\Carbon::now()->addDay($i);
				        				$name = strtolower(date('l', strtotime($day)));

				        				$flag = true;

				                if($user == "nobody")
				                {
				                	if($service->business->days->where('day', $name)->first()->status == "off") 
					                	$flag = false;

				                }else
				                {
				                	if(($user->days->where('day', $name)->first()->status == "off") or ($user->business->days->where('day', $name)->first()->status == "off")) 
					                	$flag = false;
				                }

				                foreach($business->absences->where('user_id', null) as $absence) 
		                    {
		                        if($day->betweenIncluded($absence->start, $absence->end->addDay()))
		                            $flag = false;
		                    }

				        			@endphp
				        			
				        			@if($flag)
					        			<div class="col-6 col-md-4 col-lg-3">
					        				<div class="form-check">
												  	<input class="form-check-input" type="checkbox" name="{{$day->format('d-m-Y')}}" id="{{$day->format('l')}}_{{$day->format('d/m/Y')}}">
												  	<label class="form-check-label" for="{{$day->format('l')}}_{{$day->format('d/m/Y')}}">
												    	<b>
												    		{{day_name($day->format('l'))}} {{$day->format('d/m')}}
												    	</b>
												  	</label>
													</div>
					        			</div>
					        		@endif
			        			@endfor
			    			</div>
		        	</div>
	        	</div>
	        	<div class="col-6">
	        		<div class="form-group mb-2">
		        		<label for="">{{trans('web.hour_start')}}</label>
		        		<div class="input-group mb-3">
							<span class="input-group-text" id="basic-addon1">
								<i class="fa fa-clock"></i>
							</span>
						  	<select name="start" class="form-control">
						    	@php
									$hour = \Carbon\Carbon::today();
						    	@endphp

						    	@for($i = 0; $i < (1440 / 10); $i++)
						    		@if($hour->format('H:i') == "09:00")
						    			<option value="{{$hour->format('H:i')}}" selected>{{$hour->format('H:i')}}</option>
						    		@else
						    			<option value="{{$hour->format('H:i')}}">{{$hour->format('H:i')}}</option>
						    		@endif

						    		@php
										$hour->addMinutes(10);
						    		@endphp
						    	@endfor
						    </select>
						</div>
		        	</div>
	        	</div>
	        	<div class="col-6">
	        		<div class="form-group mb-2">
		        		<label for="">{{trans('web.hour_end')}}</label>
		        		<div class="input-group mb-3">
								<span class="input-group-text" id="basic-addon1">
									<i class="fa fa-clock"></i>
								</span>
						  	<select name="end" class="form-control">
						    	@php
									$hour = \Carbon\Carbon::today();
						    	@endphp

						    	@for($i = 0; $i < (1440 / 10); $i++)
						    		@if($hour->format('H:i') == "18:00")
						    			<option value="{{$hour->format('H:i')}}" selected>{{$hour->format('H:i')}}</option>
						    		@else
						    			<option value="{{$hour->format('H:i')}}">{{$hour->format('H:i')}}</option>
						    		@endif

						    		@php
										$hour->addMinutes(10);
						    		@endphp
						    	@endfor
						    </select>
						</div>
		        	</div>
	        	</div>
	        	<div class="col-12 mb-2">
	        		<div class="form-group">
	        			<label for="">{{trans('web.name')}}</label>
	        			<input type="text" class="form-control" name="name" placeholder="{{trans('web.name_write')}}" required>
	        		</div>
	        	</div>
	        	<div class="col-12 col-md-6 mb-2">
	        		<div class="form-group">
	        			<label for="">{{trans('web.phone')}}</label>
	        			<input type="text" class="form-control" name="phone" placeholder="{{trans('web.phone_write')}}" required>
	        		</div>
	        	</div>
	        	<div class="col-12 col-md-6 mb-2">
	        		<div class="form-group">
	        			<label for="">{{trans('web.email')}}</label>
	        			<input type="text" class="form-control" name="email" placeholder="{{trans('web.email_write')}}">
	        		</div>
	        	</div>
	        	<div class="col-12 mb-2">
	        		<hr>
	        		<div class="form-group text-end">
		        		<button class="btn btn-primary disabled btn_waiting">
		        			<i class="fa fa-paper-plane"></i>
		        			{{trans('web.send')}}
		        		</button>
		        	</div>
	        	</div>
        	</div>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
	$(document).ready(function()
	{
		$('.form-check-input').on('change', function()
		{
			var count = false;
			$('.form-check-input').each(function()
			{
				if($(this).is(':checked'))
					count = true;
			});

			if(count)
				$('.btn_waiting').removeClass('disabled');	
			else
				$('.btn_waiting').addClass('disabled');
			
		});
	});
</script>