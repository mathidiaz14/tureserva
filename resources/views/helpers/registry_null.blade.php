<div class="col-12 text-center text-secondary">
	<br><br>
	<i class="fa fa-exclamation-triangle fa-4x"></i>
	<br><br>
	<p>{{__('errors.registry_null')}}</p>
	<br><br>
</div>