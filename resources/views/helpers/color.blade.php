@php
   if($value == null)
      $value = "#00d8ff";
@endphp

<div class="input-group">
   <div class="input-group-prepend">
      <span class="input-group-text">
         <i class="fas fa-paint-brush"></i>
      </span>
   </div>
   <input type="color" name="{{$name}}" id="{{$name}}" class="form-control" value="{{$value}}">
</div>