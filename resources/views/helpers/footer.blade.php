<script>
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    var device;

    if( isMobile.Android())
        device = "Android";
    else if( isMobile.BlackBerry()) 
        device = "BlackBerry";
    else if( isMobile.iOS()) 
        device = "iOS";
    else if( isMobile.Opera()) 
        device = "Opera";
    else if( isMobile.Windows()) 
        device = "Windows Phone";
    else
        device = "PC";

    $.get('https://json.geoiplookup.io/', function(res)
    { 
        ip              = res.ip;
        country         = res.country_name;
        city            = res.city;
        url             = window.location.pathname;
    
    }).done(function(){
        
        if (url == '/')
            url = 'home';

        $.post("{{url('web/visit')}}",
        {
            _token:     $('meta[name=csrf-token]').attr('content'),
            business:   '{{$business->id}}',
            ip:         ip,
            url:        url,
            city:       city,
            country:    country,
            device:     device,
        })
    });
</script>   