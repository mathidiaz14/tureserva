@php
if(empty($class))
    $class = "";
@endphp

<div class="onoffswitch {{$class}}">
    <input type="checkbox" name="{{$name}}" class="onoffswitch-checkbox" id="{{$name}}" tabindex="0" @if($status == "on") checked @endif>
    <label class="onoffswitch-label" for="{{$name}}"></label>
</div>