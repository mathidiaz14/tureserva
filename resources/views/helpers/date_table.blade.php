<style>
	.btn_left, .btn_right
	{
		position: absolute;
		top: 0;
		margin-bottom: 10px;
		width: 50%;
	}

	.btn_left i, .btn_right i
	{
		width: 30%;
	}

	.btn_left
	{
		left: 0;
	}

	.btn_right
	{
		text-align: right;
		left: 50%;
	}

	.table_column
	{
		margin-top: 30px;
	}

</style>
<div class="row" style="position: relative;">
	
	<div class="btn_left disabled" attr-id="null">
		<i class="fa fa-chevron-left fa-2x"></i>
	</div>

	<div class="col-12">
		@php 
			$i 			= 1;
			$weeks 		= 1;

			$days_view  = 7; 

			if($agent->isMobile())
				$days_view  = 4; 

		@endphp

		@foreach($dates as $date)

			@if($i == 1)
				<div class="row week" id="week_{{$weeks}}" @if(!$loop->first) style="display:none;" @endif>
					<div class="col-12 text-center">
						<h4>
							<b id="week_start_{{$weeks}}"></b> - <b id="week_end_{{$weeks}}"></b>
						</h4>
					</div>
			@endif

			<div class="col table_column {{$loop->iteration % 2 == 0 ? 'pair' : 'odd'}}">
				<div class="row table_title">
					<div class="col text-center">
						<b>
							{{$date['day']}}
							<br>
							{{$date['date_display']}}
						</b>
					</div>
				</div>

				@php
					$day_name   = strtolower(date('l', strtotime(\Carbon\Carbon::create($date['date']))));
					$start 		= \Carbon\Carbon::create($date['date'].' '.check_day_start_week($service->business)->format('H:i'));
					$end 		= \Carbon\Carbon::create($date['date'].' '.check_day_end_week($service->business)->format('H:i'));
				@endphp

				@while($start->lt($end) > 0)
					
					@if((search_in_array_date($date['hours'], $start->format('H:i'))))
						<div class="row hour active" attr-date="{{$date['date']}}" attr-hour="{{$start->format('H:i')}}">
							<div class="col text-center">
								{{$start->format('H:i')}}
							</div>
						</div>
					@else
						<div class="row hour disabled">
							<div class="col text-center">
								{{$start->format('H:i')}}
							</div>
						</div>
					@endif

					@php
						$start->addMinutes($business->reservation->interval);
					@endphp

				@endwhile

			</div>

			@if(($i == $days_view) or (($i < $days_view) and ($loop->last)))
				</div>
			@endif

			@php
				if($i == $days_view)
				{
					$i = 1;
					$weeks ++;
				}
				else
				{
					$i++;
				}
			@endphp

		@endforeach
	</div>
	
	<div class='btn_right @if($weeks == 1) disabled @endif' 
		@if($weeks == 1) 
			attr-id="null" 
		@else 
			attr-id="1" 
		@endif
	>
		<i class="fa fa-chevron-right fa-2x"></i>
	</div>
</div>
	
<script>
	$(document).ready(function()
	{
		//Botón que cambia la semana a una siguiente
		$('.btn_right').click(function()
		{
			var total 	= parseFloat("{{$weeks}}") - 1;
			var id 		= parseFloat($(this).attr('attr-id'));
			var next 	= parseFloat($(this).attr('attr-id')) + 1;

			if(!$(this).hasClass('disabled'))
			{
				$('.btn_left').removeClass('disabled');
				$('.btn_left').attr('attr-id', next);

				$('#week_'+id).hide();
				$('#week_'+next).fadeIn();		

				if(next < total)
				{
					$(this).attr('attr-id', next);
				}
				else
				{
					$(this).addClass('disabled');
					$(this).attr('attr-id', 'null');
				}
			}

		});

		//Botón que cambia la semana a una anterior
		$('.btn_left').click(function()
		{
			var id 		= parseFloat($(this).attr('attr-id'));
			var next 	= parseFloat($(this).attr('attr-id')) - 1;

			if(!$(this).hasClass('disabled'))
			{
				$('.btn_right').removeClass('disabled');
				$('.btn_right').attr('attr-id', next);

				$('#week_'+id).hide();
				$('#week_'+next).fadeIn();		

				if(next > 1)
				{
					$(this).attr('attr-id', next);
				}
				else
				{
					$(this).addClass('disabled');
					$(this).attr('attr-id', 'null');
				}
			}

		});

		//Lineas que colocan las fechas de inicio y fin de la semana por jquery
		@php
			$weeks 	= 1;
			$i 		= 1;
			$days 	= 0;
		@endphp

		@foreach($dates as $date)
			@if($i == 1)
				$('#week_start_{{$weeks}}').html("{{$date['date_display']}}");
			@endif

			@if(($i == $days_view) or (($i < $days_view) and ($loop->last)))
				$('#week_end_{{$weeks}}').html("{{$date['date_display']}}");

				@php
					$i = 1;
					$weeks ++;
					$days ++;
				@endphp
			@else
				@php			
					$i++;
					$days ++;
				@endphp
			@endif
		@endforeach
	});
</script>