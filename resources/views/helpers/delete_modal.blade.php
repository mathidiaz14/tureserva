<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_{{$delete_id}}">
	<i class="fa fa-trash"></i>
</button>
<!-- Modal -->

<div class="modal fade deleteModal" id="delete_{{$delete_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-center" role="document">
		<div class="modal-content">
			<div class="modal-header bg-red-color">
				<div class="col-12 text-right">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
			</div>
			<div class="modal-body text-center">
				<div class="row">
					<div class="col-12 text-secondary">
						<br>
						<i class="fa fa-times-circle fa-4x"></i>
						<br><br>
						<h3>{{__('message.delete_item')}}</h3>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-6">
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal">
							{{__('message.no')}}
						</button>
					</div>
					<div class="col-6">
						<form action="{{$delete_route}}" method="POST">
	                        {{ csrf_field() }}
	                        <input type="hidden" name="_method" value="DELETE">    
	                        <button type="submit" class="btn btn-danger btn-block">   
	                            {{__('message.yes')}}
	                        </button>
	                    </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).on("keydown", function(e) 
	{
		var code = e.keyCode || e.which;
		if(code == 13) 
		{
			$('.deleteModal').each(function()
			{
				if($(this).hasClass('show'))
				{
					var modal = $(this).attr('id');
					console.log($("#"+modal+" form").submit());
				}
			});
		}
	});
</script>