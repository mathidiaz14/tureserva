@php
	$range = array();
	foreach($data->days->where('day', $day) as $d)
	{
		$hour 	= \Carbon\Carbon::today();
		$start 	= \Carbon\Carbon::create($hour->format('Y-m-d').' '.$d->start);
		$end 	= \Carbon\Carbon::create($hour->format('Y-m-d').' '.$d->end);

		do
		{
			array_push($range, $start->format('H:i'));

			$start->addMinutes($min);

		}while($start->lt($end));
	}
@endphp

<div class="input-group">
   	<div class="input-group-prepend">
      	<span class="input-group-text"><i class="fa fa-clock"></i></span>
   	</div>
	<select name="{{$name}}" id="{{$name}}" class="form-control {{$class}}" @if($status == 'off') readonly @endif>
	 	@for($i = 0; $i < (1440 / $min); $i++)
	 		
	 		@if(array_search($hour->format('H:i'), $range))
				<option value="{{$hour->format('H:i')}}" disabled style="background:#d5d5d5;">
					{{$hour->format('H:i')}}
				</option>
	 		@else
	 			@if($hour->format('H:i') == $value)
		 			<option value="{{$hour->format('H:i')}}" selected>
		 				{{$hour->format('H:i')}}
		 			</option>
		 		@else
		 			<option value="{{$hour->format('H:i')}}">
		 				{{$hour->format('H:i')}}
		 			</option>
		 		@endif
	 		@endif

	 		@php
				$hour->addMinutes($min);
	 		@endphp
	 	@endfor
	</select>
</div>