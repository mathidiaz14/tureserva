<div class="input-group">
   <div class="input-group-prepend">
      <span class="input-group-text"><i class="fa fa-clock"></i></span>
   </div>
   <select name="{{$name}}" id="{{$name}}" class="form-control" @if($status != null) readonly @endif>
      @php
         $hour = 00;
      @endphp

      @for($i = 0; $i < (1440 / $min); $i++)
      	@if($hour != '0')
            @if($hour == $value)
      			<option value="{{$hour}}" selected>{{$hour}}</option>
      		@else
      			<option value="{{$hour}}">{{$hour}}</option>
      		@endif
         @endif

         @php
      	  $hour += $min;
      	@endphp
      @endfor
   </select>
</div>