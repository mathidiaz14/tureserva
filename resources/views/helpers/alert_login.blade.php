@if(session()->has('error'))
	<div class="alert alert-danger" role="alert">
	  	{!!session('error')!!}
		{{session()->forget('error')}}
	</div>
@endif
@if(session()->has('success'))
	<div class="alert alert-success" role="alert">
	  	{!!session('success')!!}
		{{session()->forget('success')}}
	</div>
@endif
@if(session()->has('warning'))
	<div class="alert alert-warning" role="alert">
	  	{!!session('warning')!!}
		{{session()->forget('warning')}}
	</div>
@endif