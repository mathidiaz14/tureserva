
<div class="input-group">
   <div class="input-group-prepend">
      <span class="input-group-text"><i class="fa fa-clock"></i></span>
   </div>
   <select name="{{$name}}" id="{{$name}}" class="form-control {{$class}}" @if($status == 'off') readonly @endif>
    	@php
			$hour 	= \Carbon\Carbon::today();
    	@endphp

    	@for($i = 0; $i < (1440 / 5); $i++)
			@if(($start->lessThanOrEqualTo($hour)) and ($end->greaterThanOrEqualTo($hour)))
				@if($hour->format('H:i') == $value)
	    			<option value="{{$hour->format('H:i')}}" selected>
	    				{{$hour->format('H:i')}}
	    			</option>
	    		@else
	    			<option value="{{$hour->format('H:i')}}">
	    				{{$hour->format('H:i')}}
	    			</option>
	    		@endif
	    	@else
	    		<option value="{{$hour->format('H:i')}}" disabled style="background:#d5d5d5;">
	    			{{$hour->format('H:i')}}
	    		</option>
			@endif

    		@php
				$hour->addMinutes(5);
    		@endphp
    	@endfor
   </select>
</div>