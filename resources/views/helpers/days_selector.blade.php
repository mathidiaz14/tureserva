<div class="input-group">
   <div class="input-group-prepend">
      <span class="input-group-text"><i class="fa fa-calendar"></i></span>
   </div>
   <select name="{{$name}}" id="{{$name}}" class="form-control" @if($status != null) readonly @endif>
      @for($i = 1; $i <= $limit; $i++)
         @if($i == $value)
            <option value="{{$i}}" selected>{{$i}}</option>
         @else
            <option value="{{$i}}">{{$i}}</option>
         @endif
      @endfor
    </select>
</div>