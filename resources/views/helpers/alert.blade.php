<div class="tureserva_notification_alert col-12 col-md-6 offset-md-3">
    <div class="row body">
        <div class="col-2 icon"></div>
        <div class="col-9 content"></div>
        <div class="col-1">
            <a class="close">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>

<script>
	var timeOutId = "";

        $('.tureserva_notification_alert .close').click(function()
        {
            $('.tureserva_notification_alert').fadeOut();
        });

        $('.tureserva_notification_alert').hover(function()
        {
            clearTimeout(timeOutId);
            $('.tureserva_notification_alert .close').fadeIn();
        });

        $('.tureserva_notification_alert').on('mouseleave', function()
        {
            timeOutId = setTimeout(function(){$('.tureserva_notification_alert').fadeOut();}, 3000);
            $('.tureserva_notification_alert .close').hide();
        });

        function tureserva_notification_message ($content, $type) 
        {
            $('.tureserva_notification_alert .content').html("<p>"+$content+"</p>");

            if($type == "success")
            {
                $('.tureserva_notification_alert .body').removeClass('danger');
                $('.tureserva_notification_alert .body').removeClass('warning');
                $('.tureserva_notification_alert .body').removeClass('info');
                $('.tureserva_notification_alert .body').addClass('success');

                $('.tureserva_notification_alert .icon').html('<i class="fa fa-check"></i>');
            }

            if($type == "warning")
            {
                $('.tureserva_notification_alert .body').removeClass('danger');
                $('.tureserva_notification_alert .body').removeClass('success');
                $('.tureserva_notification_alert .body').removeClass('info');
                $('.tureserva_notification_alert .body').addClass('warning');

                $('.tureserva_notification_alert .icon').html('<i class="fa fa-exclamation"></i>');
            }

            if($type == "danger")
            {
                $('.tureserva_notification_alert .body').removeClass('success');
                $('.tureserva_notification_alert .body').removeClass('warning');
                $('.tureserva_notification_alert .body').removeClass('info');
                $('.tureserva_notification_alert .body').addClass('danger');

                $('.tureserva_notification_alert .icon').html('<i class="fa fa-times"></i>');
            }

            if($type == "info")
            {
                $('.tureserva_notification_alert .body').removeClass('success');
                $('.tureserva_notification_alert .body').removeClass('warning');
                $('.tureserva_notification_alert .body').removeClass('danger');
                $('.tureserva_notification_alert .body').addClass('info');

                $('.tureserva_notification_alert .icon').html('<i class="fa fa-exclamation"></i>');
            }

            $('.tureserva_notification_alert').fadeIn(500);

            timeOutId = setTimeout(function(){$('.tureserva_notification_alert').fadeOut();}, 6000);
        };

        @if(session('error'))
            tureserva_notification_message("{{session('error')}}", "danger");
            {{session()->forget('error')}}
        @endif

        @if(session('success'))
            tureserva_notification_message("{{session('success')}}", "success");
            {{session()->forget('success')}}
        @endif

        @if(session('warning'))
            tureserva_notification_message("{{session('warning')}}", "warning");
            {{session()->forget('warning')}}
        @endif

        @if(session('alert_info'))
            tureserva_notification_message("{{session('alert_info')}}", "info");
            {{session()->forget('alert_info')}}
        @endif
</script>