@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="{{ url('login') }}">
                @csrf
                <input type="hidden" name="redirect" value="{{$request->redirect}}">
                <span class="login100-form-title p-b-43">
                    <a href="{{url('/')}}" class="">
                        <img src="{{asset('images/icon.png')}}" width="80px" alt="">
                    </a>
                    <br><br>
                    ¡Hola!
                    <br>
                    <p>Que bueno verte otra vez</p>
                </span>
                
                <div class="col text-center">
                    @include('helpers.alert_login')
                </div>
                
                
                <div class="wrap-input100 validate-input ">
                    <input id="email" type="email" class="input100" name="email" value="{{ old('email') }}" required autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                    
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input id="password" type="password" class="input100" name="password" required>

                    <span class="focus-input100"></span>
                    <span class="label-input100">Password</span>
                </div>

                <div class="flex-sb-m w-full p-t-3 p-b-32">
                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember">
                        <label class="label-checkbox100" for="ckb1">
                            Recordarme
                        </label>
                    </div>

                    <div>
                        <a href="{{ url('password/request') }}" class="txt1">
                            Olvide mi contraseña :(
                        </a>
                    </div>
                </div>
                <hr>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Iniciar Sesión
                    </button>
                </div>
                <br>
                <div class="container-login100-form-btn">   
                    <a href="{{url('register')}}" class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
                        Registrarse
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection
