@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST">
                <div class="col text-center">
                    @include('helpers.alert')
                </div>
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br><br>
                    <h1>Cuenta deshabilitada</h1>
                    <br>
                    <br>
                    <p>Tu cuenta se encuentra deshabilitada, comunicate con el equipo de {{env('APP_NAME')}} a travez del email <a href="mailto:admin@{{env('APP_NAME')}}">admin@{{env('APP_NAME')}}</a> para tener mas detalles del problema.</p>
                    <br>
                </span>
                <br><br>
                <hr>
                <div class="container-login100-form-btn">   
                	<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                        @csrf

	                    <button class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
	                    	Cerrar sesión
	                    </button>
                    </form>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection