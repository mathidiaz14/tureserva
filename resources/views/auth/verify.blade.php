@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-form validate-form" method="POST">
                <div class="col text-center">
                    @include('helpers.alert_login')
                </div>
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br><br>
                    <h1>¡Hola!</h1>
                    <br>
                    <br>
                    <p>¡Estamos muy contentos de tenerte por aquí! Por seguridad, es necesario que confirmes tu correo electronico con el email que se te envio a tu casilla de correo.</p>
                    <br>
                    <p>La confirmación se envió al correo <b>{{Auth::user()->email}}</b>, ingresa al enlace que contiene el email para confirmar</p>
                </span>
                <hr>
                <div class="container-login100-form-btn">   
                    <a href="{{url('home')}}" class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
                        Ir al inicio
                    </a>
                </div>
                <br>
                <div class="container-login100-form-btn">   
                    <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                        @csrf

                        <button class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
                            Cerrar sesión
                        </button>
                    </form>
                </div>
                <br>
                <a href="{{url('send/verify')}}">
                    Reenviar email
                </a>
            </div>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection