@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="{{ url('password/request') }}">
                @csrf
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br>
                    Resetear contraseña
                    <br>
                    <p>Danos tu email para enviarte una confirmación</p>
                </span>
                
                @include('helpers.alert')
                <div class="wrap-input100 validate-input ">
                    <input id="email" type="email" class="input100" name="email" value="{{ old('email') }}" required autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                </div>
                <hr>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Enviar link
                    </button>
                </div>
                <br>
                <a href="{{url('login')}}">Ir al login</a>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  
@endsection