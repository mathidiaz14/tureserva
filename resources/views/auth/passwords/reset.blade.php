@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="{{ url('password/email', $user->invitation_code) }}">
                @csrf
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br>
                    ¡Hola!
                    <br><br>
                    <p>Solo queda poner la contraseña para volver a utilizar la plataforma</p>
                </span>
                
                @include('helpers.alert')

                 @if ($errors->has('password'))
                    <small style="color:red;">
                        {{ $errors->first('password') }}
                    </small>
                @endif


                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input id="password" type="password" class="input100" name="password" required autofocus="">

                    <span class="focus-input100"></span>
                    <span class="label-input100">Contraseña</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input id="password" type="password" class="input100" name="password_confirmation" required>

                    <span class="focus-input100"></span>
                    <span class="label-input100">Confirmar Contraseña</span>
                </div>
                <br><hr>
                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Comenzar ahora
                    </button>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection
