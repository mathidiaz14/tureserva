@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="{{ url('register') }}" style="padding: 55px 55px 55px 55px;">
                @csrf
                <span class="login100-form-title p-b-43">
                    <a href="{{url('/')}}">
                        <img src="{{asset('images/icon.png')}}" width="80px" alt="">
                    </a>
                    <br><br>
                    ¡Comienza ya! 
                    <p>Registrate, es muy facil</p>
                </span>

                <div class="col text-center">
                    @include('helpers.alert_login')
                </div>

                <div class="wrap-input100 validate-input ">
                    <input id="business_name" type="text" class="input100" name="business_name" value="{{ old('business_name') }}" required autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Nombre del negocio</span>
                </div>
                
                <div class="wrap-input100 validate-input ">
                    <input id="name" type="text" class="input100" name="name" value="{{ old('name') }}" required autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Tu nombre</span>
                </div>

                <div class="wrap-input100 validate-input ">
                    <input id="email" type="email" class="input100" name="email" value="{{ old('email') }}" required autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Email</span>
                </div>

                <div class="wrap-input100 validate-input ">
                    <input id="phone" type="phone" class="input100" name="phone" value="{{ old('phone') }}" autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Telefono</span>    
                </div>
                
                <div class="wrap-input100 validate-input ">
                    <select name="gender" id="gender" class="input100 has-val" style="border:none;">
                        <option value="female" selected>Mujer</option>
                        <option value="male">Hombre</option>
                        <option value="other">Otros</option>
                    </select>

                    <span class="focus-input100"></span>
                        <span class="label-input100">Genero</span>
                </div>

                <hr>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input id="password" type="password" class="input100" name="password" required>

                    <span class="focus-input100"></span>
                    <span class="label-input100">Contraseña</span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Password is required">
                    <input id="password_confirmation" type="password" class="input100" name="password_confirmation" required>

                    <span class="focus-input100"></span>
                    <span class="label-input100">Confirmar Contraseña</span>
                </div>

                <hr>

                <div class="container-login100-form-btn">
                    <button id="register-button" dusk="register-button" class="login100-form-btn" onclick="$('.loading').fadeIn();">
                        Continuar
                    </button>
                </div>
                <br>
                <a href="{{url('login')}}">Ir al login</a>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  
@endsection
