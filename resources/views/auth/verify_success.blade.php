@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST">
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br><br>
                    @if(Auth::user()->gender == "male")
                        ¡Bienvenido {{Auth::user()->name}}!
                    @elseif(Auth::user()->gender == "female")
                        ¡Bienvenida {{Auth::user()->name}}!
                    @else
                        ¡Bienvenide {{Auth::user()->name}}!
                    @endif
                    <br>
                    <br>
                    <p>Tu email fue confirmado y puedes empezar a utilizar la plataforma, si tienes algún problema no dudes en contactarnos haciendo click en el botón de ayuda que se encuentra en el menú de usuario (extremo superior derecho).</p>
                    <br>
                    <p>Saludos y buena jornada, el equipo de {{config('app.name')}}.</p>
                </span>
                <br><br>
                <hr>
                <div class="container-login100-form-btn">   
                    <a href="{{url('home')}}" class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
                        Ir al panel de control
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection