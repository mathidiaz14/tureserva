@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form">
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br>
                    ¡Hola!
                    <br><br>
                    <p>Te han invitado a formar parte de {{config('app.name')}}, pero lamentablementa la invitación solo dura 48 horas, es necesario que te envien otra invitación.</p>
                    <br>
                    <p>Lamentamos las molestias, el equipo de {{config('app.name')}}</p>
                    <hr>
                </span>
                <div class="container-login100-form-btn">   
                    <a href="{{url('/')}}" class="login100-form-btn" style="background: none;border: solid 2px #00E890;color: #00E890;">
                        Volver al inicio
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection
