@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST">
                <span class="login100-form-title p-b-43">
                    <img src="{{asset('images/icon.png')}}" width="100px" alt="">
                    <br><br>
                    ¡Error!
                    <br>
                    <br>
                    <p>Ocurrio un error con la verificación de tu correo, por favor, solicita una nueva confirmación o comunicate con un administrador.</p>
                    <br>
                    <p>Saludos y buena jornada, el equipo de {{config('app.name')}}.</p>
                </span>
                <br><br>
                <div class="container-login100-form-btn">   
                    <a href="{{url('login')}}" class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
                        Ir al login
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection