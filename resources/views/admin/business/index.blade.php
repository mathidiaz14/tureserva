@extends('layouts.dashboard', ['menu' => 'business'])

@section('title')
    {{__('message.business')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{__('message.business_data')}}</h3>
            </div>
            <div class="card-body text-right">
                <form action="{{url('business', $business->id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
          			@csrf
          			<input type="hidden" name="_method" value="PATCH">
          			<div class="form-group row">
	                  	<label for="" class="control-label col-12 col-md-2">{{__('message.logo')}}</label>
	                  	<div class="col-12 col-md-9 offset-md-3">
		                    <div class="row">
		                    	<div class="col-6 text-center">
			                    	@if($business->logo == null)
			                      		<div class="logo_null">
			                      			<h3><b>{{LogoLetter($business->name)}}</b></h3>
			                      		</div>
			                    	@else
			                      		<img src="{{asset($business->logo)}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
			                    	@endif
			                    </div>
			                    <div class="col-6">
			                      	@if($business->logo == null)
										<a class="btn btn-danger btn-block disabled">
				                        	<i class="fa fa-trash"></i>
				                        	{{__('message.delete_logo')}}
				                      	</a>
			                      	@else
										<a href="{{url('business/delete/logo', $business->id)}}" class="btn btn-danger btn-block">
				                        	<i class="fa fa-trash"></i>
				                        	{{__('message.delete_logo')}}
				                      	</a>
			                      	@endif
			                      	<br>
			                      	<div class="upload-btn-wrapper" style="width: 100%;">
									  	<button class="btn btn-info btn-block" style="margin-bottom: 0;">
									  		<i class="fa fa-arrow-up"></i>
									  		{{__('message.upload_file')}}
									  	</button>
									  	<input type="file" name="logo" />
									</div>
			                    </div>
		                    </div>
	                  	</div>
	                </div>
          			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-2">{{__('message.name')}}</label>
	      				<div class="col-12 col-md-9">
	      					<input type="text" name="name" class="form-control" value="{{$business->name}}" required>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-2">{{__('message.url')}}</label>
	      				<div class="col-12 col-md-9">

							<div class="input-group">
							   <div class="input-group-prepend">
							      <span class="input-group-text">
							      		<b>{{env('APP_URL')}}/</b>
							      </span>
							   </div>
	      						<input type="text" name="code" id="code" class="form-control" value="{{$business->code}}" maxlength="20" required>
							</div>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-2">{{__('message.email')}}</label>
	      				<div class="col-12 col-md-9">
	      					<input type="email" name="email" class="form-control" value="{{$business->email}}" required>
	      				</div>
	      			</div>
	                <div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-2">{{__('message.phone')}}</label>
	      				<div class="col-12 col-md-9">
	      					<input type="phone" name="phone" class="form-control" value="{{$business->phone}}" placeholder="{{__('message.phone')}}">
	      				</div>
	      			</div>
	                <div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-2">{{__('message.address')}}</label>
	      				<div class="col-12 col-md-9">
	      					<input type="address" name="address" class="form-control" value="{{$business->address}}" placeholder="{{__('message.address')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-2">{{__('message.rut')}}</label>
	      				<div class="col-12 col-md-9">
	      					<input type="number" name="rut" class="form-control" value="{{$business->rut}}" placeholder="{{__('message.rut')}}">
	      				</div>
	      			</div>
	                
	      			<div class="col-12">
	      				<div class="col-md-10 offset-md-1">
	      					<div id="hour_table">
			      				@include('admin.business.hour_table')
			      			</div>
	      				</div>
	      			</div>
	                <div class="form-group text-right">
	      				<hr>
      					<button class="btn btn-info">
          					<i class="fa fa-save pr-3"></i>
          					{{__('message.save')}}
          				</button>
          			</div>
          		</form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#code').keypress(function(tecla) 
			{
	            if(((tecla.charCode >= 48) && (tecla.charCode <= 57)) || ((tecla.charCode >= 97) && (tecla.charCode <= 122)))
	            	return true;
            
                $('#code').val($('#code').val() + '_'); 
                return false;

	        });
		});
	</script>
@endsection
