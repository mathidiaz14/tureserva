@extends('layouts.dashboard', ['menu' => 'incident'])

@section('title')
    {{__('message.incident')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.incident')}}</h3>
                    </div>
                </div>
            </div>
            <div class="card-body">
            	<div class="col-12 col-md-8 offset-md-2">
	                <div class="form-horizontal">
	                	<div class="row">
	                		<div class="form-group col-12 col-md-6">
		                		<label for="">{{__('message.date')}}</label>
		                		<input type="text" class="form-control" readonly value="{{$incident->created_at->format('d/m/Y H:i')}}">
		                	</div>
		                	<div class="form-group col-12 col-md-6">
		                		<label for="">{{__('message.user')}}</label>
		                		<input type="text" class="form-control" readonly value="{{$incident->user != null ? $incident->user->name : $incident->user_id}}">
		                	</div>
	                	</div>
	                	<div class="row">
	                		<div class="form-group col-12 col-md-6">
		                		<label for="">{{__('message.status')}}</label>
		                		<input type="text" class="form-control" readonly value="{{$incident->status}}">
		                	</div>
		                	<div class="form-group col-12 col-md-6">
		                		<label for="">{{__('message.url')}}</label>
		                		<input type="text" class="form-control" readonly value="{{$incident->url}}">
		                	</div>
	                	</div>
	                	<div class="row">
	                		<div class="form-group col-12 col-md-6">
	                			<label for="">{{__('message.screen')}}</label>
	                			<br>
	                			@if($incident->screen != null)
	                				<a href="{{url('incident/screen', $incident->id)}}" class="btn btn-info" target="_blank">
		                				<i class="fa fa-desktop"></i>
		                				{{__('message.view_screen')}}
		                			</a>		
	                			@else
	                				<a class="btn btn-info disabled">
	                					<i class="fa fa-desktop"></i>
		                				{{__('message.view_screen')}}
	                				</a>
	                			@endif
	                		</div>
	                		<div class="form-group col-12 col-md-6">
	                			<label for="">{{__('message.attach')}}</label>
	                			<br>
	                			@if($incident->attached != null)
	                				<a href="{{url('incident/attach', $incident->id)}}" class="btn btn-dark">
	                					<i class="fa fa-paperclip"></i>
		                				{{__('message.view_attach')}}
		                			</a>	
	                			@else
	                				<a class="btn btn-dark disabled">
	                					<i class="fa fa-paperclip"></i>
		                				{{__('message.view_attach')}}
		                			</a>	
	                			@endif
	                		</div>
	                	</div>	
	                	<div class="row">
                			<div class="form-group col-12">
                				<label for="">{{__('message.content')}}</label>
                				<textarea name="" id="" cols="30" rows="4" class="form-control" readonly>{{$incident->content}}</textarea>
                			</div>
                		</div>
	                </div>	
            	</div>
            </div>
        </div>
    </div>

    <div class="col-12">
    	<div class="card collapsed-card">
    		<div class="card-header">
    			<h3 class="card-title">
    				{{__('message.messages')}}
    				@if($incident->messages->count() == 1)
    					<span class="badge badge-info">{{$incident->messages->count()}} Mensaje nuevo</span>
    				@elseif($incident->messages->count() > 1)
    					<span class="badge badge-info">{{$incident->messages->count()}} Mensajes nuevos</span>
    				@endif
    			</h3>
    			<div class="card-tools">
					<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
            			<i class="fas fa-plus"></i>
          			</button>
				</div>
    		</div>
    		<div class="card-body">
    			@foreach($incident->messages as $message)
    				
    			@endforeach
    		</div>
    	</div>
    </div>	
</div>
@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
