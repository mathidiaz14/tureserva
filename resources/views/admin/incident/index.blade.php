@extends('layouts.dashboard', ['menu' => 'incident'])

@section('title')
    {{__('message.incidents')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.incidents')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($incidents->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                            	<th>
                            		#
                            	</th>
                            	<th>
                            		{{__('message.status')}}
                            	</th>
                            	<th>
                            		{{__('message.date')}}
                            	</th>
                            	<th>
                            		{{__('message.view')}}
                            	</th>
                            	<th>
                            		{{__('message.delete')}}
                            	</th>
                            </tr>		
                            @foreach($incidents as $incident)
                            	<tr>
                            		<td>
                            			#{{$incident->id}}
                            		</td>
                            		<td>
                            			@if($incident->status == "pending")
                            				<span class="badge badge-warning">{{__('message.pending')}}</span>
                            			@elseif($incident->status == "enabled")
                            				<span class="badge badge-success">{{__('message.enabled')}}</span>
                            			@else
                            				<span class="badge badge-info">{{__('message.closed')}}</span>
                            			@endif
                            		</td>
                            		<td>
                            			{{$incident->created_at->format('d/m/Y H:i')}}
                            		</td>
                            		<td>
                            			<a href="{{url('incident', $incident->id)}}" class="btn btn-success">
                            				<i class="fa fa-eye"></i>
                            			</a>
                            		</td>
                            		<td>
                            			@include('helpers.delete_modal', ['delete_id' => $incident->id, 'delete_route' => route('incident.destroy', $incident->id)])
                            		</td>
                            	</tr>	
                            @endforeach
                        </table> 
                    </div>
                    @include('helpers.paginate', ['link' => $incidents])
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    
@endsection
