@extends('layouts.dashboard', ['menu' => 'client'])

@section('title')
    {{__('message.clients')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.clients')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('client/create')}}" class="btn btn-info">
                    		<i class="fa fa-plus"></i>
                    		{{__('message.new_client')}}
                    	</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($clients->count() == 0)
                	@include('helpers.registry_null')
                @else
                	<div class="table table-responsive">
	                	<table class="table table-striped">
	                		<tr>
	                			<th>{{__('message.name')}}</th>
	                			<th>{{__('message.email')}}</th>
	                			<th>{{__('message.phone')}}</th>
	                			<th>{{__('message.view')}}</th>
	                			<th>{{__('message.edit')}}</th>
	                			<th>{{__('message.delete')}}</th>
	                		</tr>
	                		@foreach($clients as $client)
								<tr>
									<td>{{$client->name}}</td>
									<td>{{$client->email == null ? "--" : $client->email}}</td>
									<td>{{$client->phone == null ? "--" : $client->phone}}</td>
									<td>
										<a href="{{url('client', $client->id)}}" class="btn btn-success">
											<i class="fa fa-eye"></i>
										</a>
									</td>
									<td>
										<a href="{{url('client', $client->id)}}/edit" class="btn btn-primary">
											<i class="fa fa-edit"></i>
										</a>
									</td>
									<td>
										@if(user()->type == "admin")
                                        	@include('helpers.delete_modal', ['delete_id' => $client->id, 'delete_route' => url('client', $client->id)])
                                        @else
                                            <a  class="btn btn-danger disabled">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        @endif
										
									</td>
								</tr>
	                		@endforeach
	                	</table>
	                </div>
                    @include('helpers.paginate', ['link' => $clients])
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
