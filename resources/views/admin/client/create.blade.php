@extends('layouts.dashboard', ['menu' => 'client_create'])

@section('title')
	{{__('message.client')}}
@endsection

@section('content')

<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.create_client')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{URL::Previous()}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="card-body text-right">
          		<form action="{{url('client')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
          			@csrf
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.name')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="name" class="form-control" value="{{old('name')}}" required autofocus placeholder="{{__('message.name')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.address')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="address" class="form-control" value="{{old('address')}}" placeholder="{{__('message.address')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.phone')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="phone" class="form-control" value="{{old('phone')}}" placeholder="{{__('message.phone')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.email')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="{{__('message.email')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.birthday')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="date" name="birthday" class="form-control" value="{{old('birthday')}}" placeholder="{{__('message.birthday')}}">
	      				</div>
	      			</div>
					<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.description')}}</label>
	      				<div class="col-12 col-md-7">
	      					<textarea name="description" id="" class="form-control"  placeholder="{{__('message.description')}}"></textarea>
	      				</div>
	      			</div>
	      			<hr>
          			<div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					{{__('message.save')}}
	          				</button>
          				</div>
          			</div>
          		</form>
          	</div>
        </div>
  	</div>
</div>
@endsection