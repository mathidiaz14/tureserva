@extends('layouts.dashboard', ['menu' => 'client'])

@section('title')
	{{__('message.client')}}
@endsection

@section('content')
	<div class="row">
		<div class="col-12">
	      	<div class="card">
	        	<div class="card-header">
	          		<div class="row">
	          			<div class="col-6">
	          				<h3 class="card-title">{{$client->name}}</h3>
	          			</div>
	          			<div class="col-6 text-right">
	          				<a href="{{url('client')}}" class="btn btn-default">
	          					<i class="fa fa-chevron-left"></i>
	          					{{__('message.back')}}
	          				</a>
	          			</div>
	          		</div>
	          		<div class="clearfix"></div>
	        	</div>
	        	<div class="card-body">
	          		<form class="form-horizontal" enctype="multipart/form-data">
		      			<div class="form-group row text-right">
		      				<label for="" class="control-label col-12 col-md-3">{{__('message.name')}}</label>
		      				<div class="col-12 col-md-7">
		      					<input type="text" name="name" class="form-control" value="{{$client->name}}" readonly="">
		      				</div>
		      			</div>
		      			<div class="form-group row text-right">
		      				<label for="" class="control-label col-12 col-md-3">{{__('message.address')}}</label>
		      				<div class="col-12 col-md-7">
		      					<input type="text" name="address" class="form-control" value="{{$client->address}}" readonly="">
		      				</div>
		      			</div>
		      			<div class="form-group row text-right">
		      				<label for="" class="control-label col-12 col-md-3">{{__('message.phone')}}</label>
		      				<div class="col-12 col-md-7">
		      					<input type="text" name="phone" class="form-control" value="{{$client->phone}}" readonly="">
		      				</div>
		      			</div>
		      			<div class="form-group row text-right">
		      				<label for="" class="control-label col-12 col-md-3">{{__('message.email')}}</label>
		      				<div class="col-12 col-md-7">
		      					<input type="email" name="email" class="form-control" value="{{$client->email}}" readonly="">
		      				</div>
		      			</div>
		      			<div class="form-group row text-right">
		      				<label for="" class="control-label col-12 col-md-3">{{__('message.birthday')}}</label>
		      				<div class="col-12 col-md-7">
		      					<input type="date" name="birthday" class="form-control" value="{{$client->birthday != null ? $client->birthday->format('Y-m-d') : null}}" readonly="">
		      				</div>
		      			</div>
								<div class="form-group row text-right">
		      				<label for="" class="control-label col-12 col-md-3">{{__('message.description')}}</label>
		      				<div class="col-12 col-md-7">
		      					<textarea name="description" id="" class="form-control" readonly="">{{$client->description}}</textarea>
		      				</div>
		      			</div>
	          		</form>
	          	</div>
	        </div>
	  	</div>
	</div>

	<div class="row">
		<div class="col-6 col-md-3">
			<div class="small-box bg-info">
	            <div class="inner">
	                <h3>
	                    {{count($client->tasks)}}
	                </h3>
	                <p>
	                	{{__('message.tasks_total')}}
	                </p>
	            </div>
	            <div class="icon">
	                <i class="fa fa-calendar"></i>
	            </div>
	            <a href="#" class="small-box-footer">
	            </a>
	        </div>
		</div>

		<div class="col-6 col-md-3">
			<div class="small-box bg-success">
	            <div class="inner">
	                <h3>
	                    {{count($client->tasks->where('online', 'yes'))}}
	                </h3>
	                <p>
	                	{{__('message.tasks_online')}}
	                </p>
	            </div>
	            <div class="icon">
	                <i class="fa fa-calendar-plus"></i>
	            </div>
	            <a href="#" class="small-box-footer">
	            </a>
	        </div>
		</div>

		<div class="col-6 col-md-3">
			<div class="small-box bg-warning">
	            <div class="inner">
	                <h3>
	                  	{{count($client->tasks->where('status', 'cancel'))}}
	                </h3>
	                <p>
	                	{{__('message.tasks_cancel')}}
	                </p>
	            </div>
	            <div class="icon">
	                <i class="fa fa-calendar-minus"></i>
	            </div>
	            <a href="#" class="small-box-footer">
	            </a>
	        </div>
		</div>

		<div class="col-6 col-md-3">
			<div class="small-box bg-danger">
	            <div class="inner">
	                <h3>
	                  	{{count($client->tasks->where('status', 'faild'))}}
	                </h3>
	                <p>
	                	{{__('message.tasks_failed')}}
	                </p>
	            </div>
	            <div class="icon">
	                <i class="fa fa-calendar-times"></i>
	            </div>
	            <a href="#" class="small-box-footer">
	            </a>
	        </div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
	  	<div class="card collapsed-card">
	    	<div class="card-header">
  				<h3 class="card-title">{{__('message.tasks_details')}}</h3>
  				<div class="card-tools">
  					<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
	            		<i class="fas fa-plus"></i>
	          		</button>
  				</div>
	    	</div>
	    	<div class="card-body">
	      		<div class="row">
	    				@if($client->tasks->count() == 0)
	    					@include('helpers.registry_null')
	    				@else
	      				<div class="col-12">
	      					<div class="table table-responsive">
		      					<table class="table table-striped">
		      						<tr>
		      							<th>{{__('message.service')}}</th>
		      							<th>{{__('message.employee')}}</th>
		      							<th>{{__('message.resources')}}</th>
		      							<th>{{__('message.comment')}}</th>
		      							<th>{{__('message.schedule')}}</th>
		      							<th>{{__('message.status')}}</th>
		      						</tr>
		      						@foreach($client->tasks as $task)
												<tr>
													<td>{{$task->service_name}}</td>
													<td>
														@if($task->users()->first() != null)
															@foreach($task->users as $user)
																{{$user->name}}
																@if(!$loop->last)
																	<br>
																@endif
															@endforeach
														@else
															--
														@endif
													</td>
													<td>
														@if($task->resources()->first() != null)
															@foreach($task->resources as $resource)
																{{$resource->name}}
																@if(!$loop->last)
																	<br>
																@endif
															@endforeach
														@else
															--
														@endif
													</td>
													<td>{{$task->comment == null ? "--" : $task->comment}}</td>
													<td>{{$task->start->format('d/m/Y H:i')}} - {{$task->end->format('H:i')}}</td>
													<td>
														@switch($task->status)
															@case('enabled')
																<span class="badge" style="width: 100%; background:#36b74c;">Completada</span>
															@break
															@case('faild')
																<span class="badge" style="width: 100%; background:#b73636;">Fallida</span>
															@break
															@case('cancel')
																<span class="badge" style="width: 100%; background:#b76c36;">Cancelada</span>
															@break
															@case('pending')
																<span class="badge" style="width: 100%; background:#367bb7;">Pendiente</span>
															@break
														@endswitch
													</td>
												</tr>
		      						@endforeach
		      					</table>
		      				</div>
	      				</div>
	    				@endif
	      		</div>
	      	</div>
	    </div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="card collapsed-card">
				<div class="card-header">
					<h4 class="card-title">
						{{__('message.notes_for_client')}}
					</h4>
					<div class="card-tools">
  						<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
	            			<i class="fas fa-plus"></i>
	          			</button>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-12">
							<form action="{{url('note/client')}}" class="form-horizontal" method="post">
								@csrf
								<input type="hidden" name="client" value="{{$client->id}}">
								<div class="form-group">
									<label for="">{{__('message.add_note')}}</label>
									<textarea name="note" id="" cols="30" rows="3" class="form-control" required></textarea>
								</div>	
								<div class="form-group text-right">
									<button class="btn btn-info">
										<i class="fa fa-save"></i>
										{{__('message.save')}}
									</button>	
								</div>
							</form>
						</div>
					</div>
					<hr>
					<div class="row">
						@foreach($client->notes as $note)
							<div class="col-12 client_note">
								<div class="row">
									<div class="col-10">
										<p><b>{{$note->created_at->format('d/m/Y')}}</b> - {{$note->user->name}}</p>
									</div>
									<div class="col-2 text-right">
										@include('helpers.delete_modal', ['delete_id' => $note->id, 'delete_route' => url('note/client', $note->id)])
									</div>
									<div class="col-12">
										<p>{{$note->note}}</p>
									</div>
								</div>
							</div>
						@endforeach
					</div>	
				</div>
			</div>
		</div>
	</div>
@endsection