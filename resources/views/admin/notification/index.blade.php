@extends('layouts.dashboard', ['menu' => 'notification'])

@section('title')
    {{__('message.notifications')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.notifications')}}
                </h3>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($notifications->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('message.title')}}</th>
                                <th>{{__('message.body')}}</th>
                                <th>{{__('message.delete')}}</th>
                            </tr>
                            @foreach($notifications as $notification)
                                <tr>
                                    <td>{{$notification->title}}</td>
                                    <td>{{$notification->body}}</td>
                                    <td>
                                        @if(user()->type == "admin")
                                            @include('helpers.delete_modal', ['delete_id' => $notification->id, 'delete_route' => route('notification.destroy', $notification->id)])
                                        @else
                                            <a  class="btn btn-danger disabled">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    @include('helpers.paginate', ['link' => $notifications])
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
