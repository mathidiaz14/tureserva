@extends('layouts.dashboard', ['menu' => 'absence'])

@section('title')
	{{__('message.absences')}}
@endsection

@section('content')

<div class="row">
	<div class="col-12 col-md-8 offset-md-2">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.edit_period')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('absence')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
        	</div>
        	
      		<form action="{{url('absence', $absence->id)}}" method="post">
            	@csrf
            	@method('PATCH')
                <div class="card-body">
                    <div class="row">
                    	<div class="col-12 col-md-6">
                            <div class="form-group">
                            	<label for="">{{__('message.starts')}}</label>
                            	<input type="datetime-local" class="form-control" name="start" required="" value="{{$absence->start}}">
                            </div>
                    	</div>
                    	<div class="col-12 col-md-6">
                            <div class="form-group">
                            	<label for="">{{__('message.ends')}}</label>
                            	<input type="datetime-local" class="form-control" name="end" required="" value="{{$absence->end}}">
                            </div>
                    	</div>
                    </div>
                    <div class="row">
                    	<div class="col-12 col-md-6">
                    		<div class="form-group">
                            	<label for="">{{__('message.type')}}</label>
                            	<select name="model" id="model" class="form-control">
                            		@if($absence->model == "App\Models\Business")
                            			<option value="App\Models\Business" selected>{{__('message.business')}}</option>
                            		@else
                            			<option value="App\Models\Business">{{__('message.business')}}</option>
                            		@endif
                            		
                            		@if($absence->model == "App\Models\User")
                            			<option value="App\Models\User" selected>{{__('message.user')}}</option>
                            		@else
                            			<option value="App\Models\User">{{__('message.user')}}</option>
                            		@endif
                            		
                            		@if($absence->model == "App\Models\Service")
                            			<option value="App\Models\Service" selected>{{__('message.service')}}</option>
                            		@else
                            			<option value="App\Models\Service">{{__('message.service')}}</option>
                            		@endif

                            		@if($absence->model == "App\Models\Resource")
                            			<option value="App\Models\Resource" selected>{{__('message.resource')}}</option>
                            		@else
                            			<option value="App\Models\Resource">{{__('message.resource')}}</option>
                            		@endif
                            	</select>
                            </div>
                    	</div>
                    	<div class="col-12 col-md-6">
                    		<div class="form-group">
                    			<label for="model_id">ID</label>
                    			
                    			<select name="modal_id" id="model_id_business" class="form-control model_id"
                    			@if($absence->model == "App\Models\Business") style="display:block;" @else style="display:none;" @endif>
                    				<option value="{{business()->id}}">{{business()->name}}</option>
                    			</select>

                    			<select name="" id="model_id_users" class="form-control model_id"
                    			@if($absence->model == "App\Models\User") style="display:block;" @else style="display:none;" @endif>
                    				@foreach(business()->users as $user)
                    					@if(($absence->model == "App\Models\User") and ($user->id == $absence->model_id))
                    						<option value="{{$user->id}}" selected>{{$user->name}}</option>
                    					@else
                    						<option value="{{$user->id}}">{{$user->name}}</option>
                    					@endif
                    				@endforeach
                    			</select>

                    			<select name="" id="model_id_services" class="form-control model_id" @if($absence->model == "App\Models\Service") style="display:block;" @else style="display:none;" @endif>
                    				@foreach(business()->services as $service)
                    					@if(($absence->model == "App\Models\Service") and ($service->id == $absence->model_id))
                    						<option value="{{$service->id}}" selected>{{$service->name}}</option>
                    					@else
                    						<option value="{{$service->id}}">{{$service->name}}</option>
                    					@endif
                    				@endforeach
                    			</select>

                    			<select name="" id="model_id_resources" class="form-control model_id" @if($absence->model == "App\Models\Resource") style="display:block;" @else style="display:none;" @endif>
                    				@foreach(business()->resources as $resource)
                    					@if(($absence->model == "App\Models\Resource") and ($resource->id == $absence->model_id))
                    						<option value="{{$resource->id}}" selected>{{$resource->name}}</option>
                    					@else
                    						<option value="{{$resource->id}}">{{$resource->name}}</option>
                    					@endif
                    				@endforeach
                    			</select>
                    		</div>
                    	</div>
                    </div>	</div>
                <div class="card-footer">
                	<div class="col-12 text-right">
                		<button class="btn btn-info">
                			<i class="fa fa-save"></i>
                			{{__('message.send')}}
                		</button>
                	</div>
                </div>
            </form>
        </div>
  	</div>
</div>
@endsection


@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#model').on('change', function()
			{
				var model = $('#model').find(":selected").val();

				if(model == "App\\Models\\Business")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_business').attr('name', 'model_id');
					$('#model_id_business').show();

				}else if(model == "App\\Models\\User")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_users').attr('name', 'model_id');
					$('#model_id_users').show();

				}else if(model == "App\\Models\\Service")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_services').attr('name', 'model_id');
					$('#model_id_services').show();

				}else if(model == "App\\Models\\Resource")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_resources').attr('name', 'model_id');
					$('#model_id_resources').show();
				}
			});
		});
	</script>
@endsection
