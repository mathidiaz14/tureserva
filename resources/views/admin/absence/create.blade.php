@extends('layouts.dashboard', ['menu' => 'absence_create'])

@section('title')
	{{__('message.absences')}}
@endsection

@section('content')

<div class="row">
	<div class="col-12 col-md-8 offset-md-2">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.new_period')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('absence')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
        	</div>
        	
      		<form action="{{url('absence')}}" method="post">
            	@csrf
                <div class="card-body">
                    <div class="row">
                    	<div class="col-12 col-md-6">
                            <div class="form-group">
                            	<label for="">{{__('message.starts')}}</label>
                            	<input type="datetime-local" class="form-control" name="start" required="" value="">
                            </div>
                    	</div>
                    	<div class="col-12 col-md-6">
                            <div class="form-group">
                            	<label for="">{{__('message.ends')}}</label>
                            	<input type="datetime-local" class="form-control" name="end" required="" value="">
                            </div>
                    	</div>
                    </div>
                    <div class="row">
                    	<div class="col-12 col-md-6">
                    		<div class="form-group">
                            	<label for="">{{__('message.type')}}</label>
                            	<select name="model" id="model" class="form-control">
                            		<option value="App\Models\Business">{{__('message.business')}}</option>
                            		<option value="App\Models\User">{{__('message.users')}}</option>
                            		<option value="App\Models\Service">{{__('message.services')}}</option>
                            		<option value="App\Models\Resource">{{__('message.resources')}}</option>
                            	</select>
                            </div>
                    	</div>
                    	<div class="col-12 col-md-6">
                    		<div class="form-group">
                    			<label for="model_id">ID</label>
                    			<select name="modal_id" id="model_id_business" class="form-control model_id" disabled>
                    				<option value="{{business()->id}}">{{business()->name}}</option>
                    			</select>

                    			<select name="" id="model_id_users" class="form-control model_id" style="display: none;">
                    				@foreach(business()->users as $user)
                    					<option value="{{$user->id}}">{{$user->name}}</option>
                    				@endforeach
                    			</select>

                    			<select name="" id="model_id_services" class="form-control model_id" style="display: none;">
                    				@foreach(business()->services as $service)
                    					<option value="{{$service->id}}">{{$service->name}}</option>
                    				@endforeach
                    			</select>

                    			<select name="" id="model_id_resources" class="form-control model_id" style="display: none;">
                    				@foreach(business()->resources as $resource)
                    					<option value="{{$resource->id}}">{{$resource->name}}</option>
                    				@endforeach
                    			</select>
                    		</div>
                    	</div>
                    </div>	</div>
                <div class="card-footer">
                	<div class="col-12 text-right">
                		<button class="btn btn-info">
                			<i class="fa fa-save"></i>
                			{{__('message.send')}}
                		</button>
                	</div>
                </div>
            </form>
        </div>
  	</div>
</div>
@endsection


@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#model').on('change', function()
			{
				var model = $('#model').find(":selected").val();

				if(model == "App\\Models\\Business")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_business').attr('name', 'model_id');
					$('#model_id_business').show();

				}else if(model == "App\\Models\\User")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_users').attr('name', 'model_id');
					$('#model_id_users').show();

				}else if(model == "App\\Models\\Service")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_services').attr('name', 'model_id');
					$('#model_id_services').show();

				}else if(model == "App\\Models\\Resource")
				{
					$('.model_id').attr('name', '');
					$('.model_id').hide();
					$('#model_id_resources').attr('name', 'model_id');
					$('#model_id_resources').show();
				}
			});
		});
	</script>
@endsection
