@extends('layouts.dashboard', ['menu' => 'absence'])

@section('title')
    {{__('message.absences')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.absence_period')}}</h3>
                    </div>
                    <div class="col-6 text-right">
						<a href="{{url('absence/create')}}" class="btn btn-info">
							<i class="fa fa-plus"></i>
                            {{__('message.new_period')}}
						</a>    	
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($absences->count() == 0)
                	@include('helpers.registry_null')
                @else
                	<div class="table table-responsive">
	                	<table class="table table-striped">
	                		<tr>
	                			<th>{{__('message.starts')}}</th>
	                			<th>{{__('message.ends')}}</th>
	                			<th>{{__('message.model')}}</th>
	                			<th></th>
	                			<th></th>
	                			<th></th>
	                		</tr>
	                		@foreach($absences as $absence)
								<tr>
									<td>{{$absence->start->format('d/m/Y H:i')}}</td>
									<td>{{$absence->end->format('d/m/Y H:i')}}</td>
									<td>
										@switch($absence->model)
											@case('App\Models\Business')
												{{__('message.business')}}
											@break

											@case('App\Models\User')
												{{__('message.user')}}
											@break

											@case('App\Models\Service')
												{{__('message.service')}}
											@break

											@case('App\Models\Resource')
												{{__('message.resource')}}
											@break
										@endswitch
									</td>
									<td>
										@switch($absence->model)
											@case('App\Models\Business')
												--
											@break

											@case('App\Models\User')
												{{\App\Models\User::find($absence->model_id)->name}}
											@break

											@case('App\Models\Service')
												{{\App\Models\Service::find($absence->model_id)->name}}
											@break

											@case('App\Models\Resource')
												{{\App\Models\Resource::find($absence->model_id)->name}}
											@break
										@endswitch
									</td>
									<td>
										<a href="{{route('absence.edit', $absence->id)}}" class="btn btn-info">
											<i class="fa fa-edit"></i>
										</a>
									</td>
									<td>
										@include('helpers.delete_modal', ['delete_id' => $absence->id, 'delete_route' => route('absence.destroy', $absence->id)])
									</td>
								</tr>
	                		@endforeach
	                	</table>
	                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
