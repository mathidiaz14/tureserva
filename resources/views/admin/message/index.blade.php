@extends('layouts.dashboard', ['menu' => 'message'])

@section('title')
    {{__('message.messages')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.messages')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    	
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">

                @foreach($messages as $message)
                    <div class="row">
                        <div class="col-12">
                            <label>{{$message->description}}</label>
                            <textarea id="{{$message->key}}" name="{{$message->key}}" id="" cols="30" rows="2" class="form-control textarea" attr-restrictions="{{$message->restrictions}}">{{$message->value}}</textarea>
                            <small>
                                Los campos que esten dentro de * son necesarios, estos son:  
                                @foreach(convert_restrictions($message->restrictions) as $restriction)
                                    <b>*{{$restriction}}*</b>
                                    @if(!$loop->last)
                                        , 
                                    @endif
                                @endforeach
                            </small>
                            @if(!$loop->last)
                                <hr>
                            @endif
                        </div>
                    </div>
                @endforeach   
                <hr>
                <div class="row">
                    <div class="col-12 text-right">
                        <a class="btn btn-info btn_save_messages">
                            <i class="fa fa-save"></i>
                            {{__('message.save')}}
                        </a>
                    </div>
                </div>      
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('.btn_save_messages').click(function()
            {
                $('.loading').fadeIn();
                $('.textarea').each(function(message)
                {
                    var id = $(this).attr('id');

                    $.ajax(
                    {
                        type:'POST',
                        url:"{{url('check/message')}}",
                        data:{
                            _token          : "{{csrf_token()}}",
                            key             : id,
                            message         : $(this).val(),
                            restrictions    : $(this).attr('attr-restrictions'),
                        },
                    }).done(function(result)
                    {
                        if(result == "no")
                        {
                            $('#'+id).removeClass("border border-success");
                            $('#'+id).addClass("border border-danger");
                            tureserva_notification_message('Algun campo no cumple con los codigos de restricciónes', 'danger');
                        }
                        else
                        {
                            $('#'+id).removeClass("border border-danger");
                            $('#'+id).addClass("border border-success");
                        }
                    });
                });
                
                $('.loading').fadeOut();
                
            });
        });
    </script>
@endsection
