@php 
    $now    = \Carbon\Carbon::now();
    $month  = \Carbon\Carbon::now()->startOfMonth();
@endphp

<div class="row mt-4">
    <div class="col-6 col-md-3">
        <div class="small-box bg-info">
            <div class="inner">
                @php 
                    $star = 0;
                    foreach(business()->opinions as $opinion)
                        $star += $opinion->star;
                    
                    if(count(business()->opinions) == 0)
                        $star = 0;
                    else
                        $star = round($star/count(business()->opinions), 1);
                @endphp 

                <h3>
                    {{$star}} / 5
                </h3>
                <p>{{count(business()->opinions)}} Opiniones</p>
            </div>
            <div class="icon">
                <i class="fa fa-star"></i>
            </div>
            <a class="small-box-footer">
                Valoración
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>
                    {{count(business()->tasks->where('start', '>=', $month)->where('status', '!=', 'pending'))}}
                </h3>
                <p>
                    Total de citas
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar"></i>
            </div>
            <a class="small-box-footer">
                Mes actual
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>
                    {{count(business()->tasks->where('start', '>=', $month)->where('status', 'faild'))}}
                </h3>
                <p>
                    Citas fallidas
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar-minus"></i>
            </div>
            <a class="small-box-footer">
                Mes actual
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>
                    {{count(business()->tasks->where('start', '>=', $month)->where('status', 'cancel'))}}
                </h3>
                <p>
                    Citas canceladas
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar-times"></i>
            </div>
            <a class="small-box-footer">
                Mes actual
            </a>
        </div>
    </div>
</div>