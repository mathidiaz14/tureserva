<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    <i class="fa fa-list"></i>
                    {{__('message.waiting_list_title')}}
                </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    @if(business()->waitings->count() == 0)
                        @include('helpers.registry_null')
                    @endif

                    @foreach(business()->waitings as $waiting)
                        <div class="col-12 mb-3 callout callout-success">
                            <p>Cliente: <b>{{$waiting->name}} - {{$waiting->phone}}</b> <br>
                            Servicio: <b>{{$waiting->service->name}}</b> <br>
                            Empleado: <b>{{$waiting->user->name}}</b> <br>
                            Horarios: 
                            @foreach($waiting->days as $day)
                                <b>{{$day->date}} de {{$waiting->start}} - {{$waiting->end}}</b>
                                @if(!$loop->last)
                                ,
                                @endif
                            @endforeach
                            </p>    
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>