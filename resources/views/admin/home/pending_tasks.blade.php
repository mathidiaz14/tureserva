<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">
                            <i class="fa fa-calendar"></i> {{__('message.pending_tasks')}}
                        </h3>
                    </div>
                    <div class="col-6 text-right">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    @if(count($tasks) == 0)
                        @include('helpers.registry_null')
                    @endif

                    @foreach($tasks as $task)
                        <div class="col-12 callout callout-info">
                            <div class="row">
                                <div class="col-12 col-md-9">
                                    <div class="row">
                                        <div class="col-4">
                                            <p>{{__('message.reservation_code')}}: </p>
                                        </div>
                                        <div class="col-8">
                                            <b>{{$task->code}}</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <p>{{__('message.date')}}: </p>
                                        </div>
                                        <div class="col-8">
                                            <b>{{$task->start->format('d/m/Y H:i')}} - {{$task->end->format('H:i')}}</b>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <p>{{__('message.client')}}: </p>
                                        </div>
                                        <div class="col-8">
                                            <a href="{{url('client', $task->client)}}">
                                                <b>{{$task->client->name}} - {{$task->client->phone}} - {{$task->client->email}}</b>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <p>{{__('message.services')}}: </p>
                                        </div>
                                        <div class="col-8">
                                            @foreach($task->services as $service)
                                                <b>{{$service->name}} <small>({{$service->duration}} min)</small></b> 
                                                @if(!$loop->last)
                                                    - 
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4">
                                            <p>{{__('message.employees')}}: </p>
                                        </div>
                                        <div class="col-8">
                                            @foreach($task->users as $user)
                                                <b>{{$user->name}}</b> 
                                                @if(!$loop->last)
                                                    - 
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    
                                    @if($task->description != "")
                                        <div class="row">
                                            <div class="col-4">
                                                <p>{{__('message.description')}}: </p>
                                            </div>
                                            <div class="col-8">
                                                <b>{{$task->description}}</b>
                                            </div>
                                        </div>
                                    @endif
                                    
                                </div>
                                <div class="col-12 col-md-3 mt-4">
                                    <div class="row">
                                        <div class="col-12 mb-3">
                                            <!-- Boton de aceptar -->
                                            <a class="btn btn-success btn-block" onclick="event.preventDefault(); document.getElementById('check-form').submit();" style="text-decoration: none; color:white;">
                                                <i class="fa fa-check"></i>
                                                {{__('message.approve')}}
                                            </a>

                                            <form id="check-form" action="{{url('accept/calendar')}}" method="POST" style="display: none;">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$task->id}}">
                                            </form>
                                        </div>
                                        <div class="col-12">
                                             <!-- Boton de rechazar -->
                                                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#refuseModal_{{$task->id}}">
                                                    <i class="fa fa-times"></i>
                                                    {{__('message.refuse')}}
                                                </button>

                                                <!-- Modal de rechazar -->
                                                <div class="modal fade" id="refuseModal_{{$task->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-center" role="document">
                                                        <div class="modal-content" style="color:#73879C;">
                                                            <div class="modal-header">
                                                                <div class="col-6 text-left">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Rechazar cita</h5>        
                                                                </div>
                                                                <div class="col-6">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>  
                                                                </div>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{url('refuse/calendar')}}" method="post" class="form-horizontal">
                                                                    @csrf
                                                                    <input type="hidden" name="id" value="{{$task->id}}">
                                                                    <div class="form-group text-left">
                                                                        <label for="" class="form-label">Dejale un comentario a tu cliente</label>
                                                                        <textarea name="comment" cols="30" rows="3" class="form-control" id="comment_{{$task->id}}" required=""></textarea>
                                                                    </div>
                                                                    <br>
                                                                    <div class="form-group text-right">
                                                                        <button class="btn btn-primary" style="width: 200px;">
                                                                            <i class="fa fa-paper-plane"></i>
                                                                            Enviar
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    @foreach($tasks as $task)
        $('#refuseModal_{{$task->id}}').on('shown.bs.modal', function () {
            $('#comment_{{$task->id}}').trigger('focus')
        });
    @endforeach
</script>