<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">
                            <i class="fa fa-sticky-note"></i> 
                            {{__('message.notes')}}
                        </h3>
                    </div>
                    <div class="col-6 text-right">
                        <!-- Agregar nota -->
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add_notes">
                            <i class="fa fa-plus"></i>
                            Agregar nota
                        </button>

                        <!-- Modal de rechazar -->
                        <div class="modal fade" id="add_notes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-center" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="col-6 text-left">
                                            <h5 class="modal-title" id="exampleModalLabel">Agregar nota</h5>        
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>  
                                        </div>
                                    </div>
                                    <div class="modal-body text-left">
                                        <form action="{{url('note')}}" method="post" class="form-horizontal">
                                            @csrf
                                            <div class="form-group">
                                                <label for="">Nota</label>
                                                <textarea name="note" id="note" cols="30" rows="3" class="form-control"></textarea>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label>
                                                    <input type="checkbox" class="js-switch" name="business"/> ¿Mostrar a todos los usuarios de la empresa?
                                                </label>
                                            </div>
                                            <div class="form-group text-right">
                                                <hr>
                                                <button class="btn btn-primary">
                                                    <i class="fa fa-save"></i>
                                                    Guardar
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <div class="row">
                    @if(user()->notes->count() == 0)
                        @include('helpers.registry_null')
                    @endif
                    
                    @foreach(user()->notes as $note)
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="note" style="background:#83a6cc; color:#333333;">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6">
                                                <small>Creada el {{$note->created_at->format('d/m/Y H:i')}}</small>        
                                            </div>
                                            <div class="col-6">
                                                @if($note->business_id != null)
                                                    <small>Creada por {{$note->user->name}}</small> <br>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12" style="padding:10px;">
                                        <b>{{$note->text}}</b>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-6 text-center">
                                                <!-- Agregar nota -->
                                                <button type="button" class="btn btn-default btn-block" data-toggle="modal" data-target="#edit_note_{{$note->id}}">
                                                    <i class="fa fa-edit"></i>
                                                    {{__('message.edit')}}
                                                </button>

                                                <!-- Modal de rechazar -->
                                                <div class="modal fade" id="edit_note_{{$note->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-center" role="document">
                                                        <div class="modal-content" style="color:#73879C;">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Editar nota</h5>        
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>  
                                                            </div>
                                                            <div class="modal-body text-left">
                                                                <form action="{{route('note.update', $note->id)}}" method="post" class="form-horizontal">
                                                                    @csrf
                                                                    <input type="hidden" name="_method" value="patch">
                                                                    <div class="form-group">
                                                                        <label for="" class="control-label">Nota</label>
                                                                        <textarea name="note" id="note" cols="30" rows="3" class="form-control">{{$note->text}}</textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>
                                                                            <input type="checkbox" class="js-switch" name="business"
                                                                            @if($note->business_id != null)
                                                                                checked
                                                                            @endif
                                                                            /> ¿Mostrar a todos los usuarios?
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-group text-right">
                                                                        <button class="btn btn-primary">
                                                                            <i class="fa fa-save"></i>
                                                                            Guardar
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-6 text-center">
                                                
                                                <button type="button" class="btn-default btn btn-block " data-toggle="modal" data-target="#delete_{{$note->id}}">
                                                    <i class="fa fa-trash"></i>
                                                    {{__('message.delete')}}
                                                </button>
                            
                                                <div class="modal fade" id="delete_{{$note->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog modal-center" role="document">
                                                        <div class="modal-content" style="color:#73879C;">
                                                            <div class="modal-header bg-red-color">
                                                                <div class="col-12 text-right">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                </div>
                                                            </div>
                                                            <div class="modal-body text-center">
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <i class="fa fa-times-circle fa-4x"></i>
                                                                        <br>
                                                                        <h2>¿Desea eliminar el item?</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">No</button>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <form action="{{route('note.destroy', $note->id)}}" method="POST">
                                                                            {{ csrf_field() }}
                                                                            <input type="hidden" name="_method" value="DELETE">    
                                                                            <button type="submit" class="btn btn-danger btn-block">   
                                                                                Si
                                                                            </button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#add_notes').on('shown.bs.modal', function () {
        $('#note').trigger('focus')
    });
</script>