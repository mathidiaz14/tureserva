@extends('layouts.dashboard', ['menu' => 'mercadopago'])

@section('title')
    {{__('message.mercadopago')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">
                            {{__('message.mercadopago')}}
                        </h3>
                    </div>
                    <div class="col-6 text-right">
                    </div>
                </div>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-12 col-md-10 offset-md-1 text-center">
                        <img src="{{asset('images/mercadopago.png')}}" alt="" width="400px">
                        <br><br>
                        <p>{{__('message.mercadopago_info')}}</p>
                        <br>
                        @if(Auth::user()->business->mp_state == null)
                            <a 
                            href="https://auth.mercadopago.com.uy/authorization?client_id={{env('MP_CLIENT_ID')}}&response_type=code&platform_id=mp&redirect_uri={{env('MP_REDIRECT_URI')}}" class="btn btn-info px-5 btn-lg">
                                    <i class="fa fa-plug"></i>
                                    {{__('message.connect')}}
                            </a>
                        @else
                            <a href="{{url('admin/mercadopago/disconect')}}" class="btn btn-danger px-5 btn-lg">
                                <i class="fa fa-times"></i>
                                <i class="fa fa-plug"></i>
                                {{__('message.disconnect')}}
                            </a>
                            <br><br>

                            <form action="{{url('mercadopago')}}" method="post" class="form-horizontal">
                                @csrf
                                <hr>
                                <h5>{{__('message.mercadopago_config')}}</h5>
                                <hr>
                                <div class="form-group row">
                                    <div class="col text-right">
                                        <label for="status">
                                            {{__('message.mercadopago_enabled')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.switch', ['name' => 'status', 'status' => business()->return_status])
                                    </div>  
                                </div>

                                <div id="return_select" @if(business()->return_status == null) style="display:none;" @endif>
                                    <div class="form-group row">
                                        <div class="col text-right">
                                            <label for="">
                                                {{__('message.in_cancellation')}}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <select name="return" id="return" class="form-control">
                                                @if(business()->return_option == "partial")
                                                    <option value="no">{{__('message.no_return')}}</option>
                                                    <option value="all">{{__('message.return_all')}}</option>
                                                    <option value="partial" attr-data="part" selected="">{{__('message.return_partial')}}</option>
                                                @elseif(business()->return_option == "all")
                                                    <option value="no">{{__('message.no_return')}}</option>
                                                    <option value="all" selected="">{{__('message.return_all')}}</option>
                                                    <option value="partial" attr-data="part">{{__('message.return_partial')}}</option>
                                                @else
                                                    <option value="no" selected="">{{__('message.no_return')}}</option>
                                                    <option value="all">{{__('message.return_all')}}</option>
                                                    <option value="partial" attr-data="part">{{__('message.return_partial')}}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row" id="return_part" @if(business()->return_option == "partial") style="display:block;" @else style="display:none;" @endif>
                                        <div class="col text-right">
                                            <label for="">
                                                {{__('message.return_porcentage')}}
                                            </label>
                                        </div>
                                        <div class="col">
                                            <input type="number" id="return_percentage" class="form-control" name="return_percentage" placeholder="{{__('message.return_porcentage')}}" value="{{business()->return_percentage}}">
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group text-right">
                                    <button class="btn btn-info">
                                        <i class="fa fa-save"></i>
                                        {{__('message.save')}}
                                    </button>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {   
            $('#return_percentage').on('change', function()
            {
                if($(this).val() > 100)
                    $(this).val('100');
                else if($(this).val() <= 0)
                    $(this).val('1');
            });

            $('#status').click(function()
            {
                if($(this).prop('checked') == false)
                {
                    $('#return_select').fadeOut();
                }else
                {
                    $('#return_select').fadeIn();
                }
            });

            $('#return').on('change', function()
            {
                if($('#return :selected').attr('attr-data') == "part")
                {
                    $('#return_part').fadeIn();
                }else{
                    $('#return_part').fadeOut();
                }
            });
        });
    </script>
@endsection
