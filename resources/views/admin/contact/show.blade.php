@extends('layouts.dashboard', ["menu" => 'contact'])

@section('title')
    {{__('message.contact')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.contact')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('contact')}}" class="btn btn-default">
                    		<i class="fa fa-chevron-left"></i>
                    		Atras
                    	</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('contact.update', $message->id)}}" method="post" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12 col-md">
                                <label for="" class="control-label">
                                    {{__('message.name')}}
                                </label>
                                <input type="text" class="form-control" value="{{$message->name}}" readonly>
                            </div>
                            <div class="col-12 col-md">
                                <label for="" class="control-label">
                                    {{__('message.email')}}
                                </label>
                                <input type="text" class="form-control" value="{{$message->email}}" readonly>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col">
                                <label for="" class="control-label">
                                    {{__('message.message')}}
                                </label>
                                <textarea name="" id="" cols="30" rows="10" class="form-control" readonly>{{$message->message}}</textarea>
                            </div>
                        </div>
                    </div>
                    
                    @if($message->childrens->count() > 0)
                        <hr>
                        @foreach($message->childrens as $children)
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <b>{{$children->created_at->format('d/m/Y H:i')}}</b>
                                    </div>
                                    <div class="col-sm-12">
                                        <p>{{$children->message}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <hr>  
                    <div class="form-group">
                        <label for="" class="control-label">{{__('message.answer')}}</label>
                        <textarea name="message" id="message" rows="5" class="form-control" placeholder="Escriba aquí su respuesta"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <hr>
                        <button class="btn btn-primary">
                            <i class="fa fa-send"></i>
                            {{__('message.send')}}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        
    </script>
@endsection
