@extends('layouts.dashboard', ['menu' => 'contact'])

@section('title')
    {{__('message.contact')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.contact')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($messages->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('message.name')}}</th>
                                <th>{{__('message.email')}}</th>
                                <th>{{__('message.message')}}</th>
                                <th>{{__('message.status')}}</th>
                                <th>{{__('message.view')}}</th>
                                <th>{{__('message.delete')}}</th>
                            </tr>
                            @foreach($messages->where('status', '!=', 'answer')->sortBy('created_at') as $message)
                                <tr>
                                    <td><p>{{$message->name}}</p></td>
                                    <td><p>{{$message->email}}</p></td>
                                    <td><p>{{$message->message}}</p></td>
                                    <td>
                                        <p>
                                            @if($message->status == "new")
                                                {{__('message.new')}}
                                            @elseif($message->status == "answered")
                                                {{__('message.answered')}}
                                            @elseif($message->status == "pending")
                                                {{__('message.pending')}}
                                            @elseif($message->status == "enabled")
                                                {{__('message.enabled')}}
                                            @elseif($message->status == "read")
                                                {{__('message.read')}}
                                            @endif
                                        </p>
                                    </td>
                                    <td>
                                        <a href="{{url('contact', $message->id)}}" class="btn btn-primary">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                    <td>
                                        @if(user()->type == "admin")
                                            @include('helpers.delete_modal', ['delete_id' => $message->id, 'delete_route' => route('contact.destroy', $message->id)])
                                        @else
                                            <a class="btn btn-danger disabled">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table> 
                    </div>
                    @include('helpers.paginate', ['link' => $messages])
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        @foreach($messages as $message)
            $('#response_modal_{{$message->id}}').on('shown.bs.modal', function () {
                $('#message').trigger('focus')
            });
        @endforeach
    </script>
@endsection
