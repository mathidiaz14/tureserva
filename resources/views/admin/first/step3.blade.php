@extends('layouts.dashboard', ['menu' => ''])

@section('title')
    {{__('message.first_steps')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<div class="row">
            		<div class="col-12 text-center">
            			<h4>
            				{{__('message.add_services')}}
            			</h4>
            		</div>
            	</div>	
      			<hr>
      			<div class="row">
      				<div class="col-12 col-md-8 offset-md-2">
                        <form action="{{url('service')}}" method="POST" class="form-horizontal">
		          			@csrf
		          			<input type="hidden" name="online" value="on">
			      			<div class="row">
			      				<div class="col-12 col-md-6">
			      					<div class="form-group">
					      				<label for="" class="control-label">{{__('message.name')}}</label>
				      					<input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('message.name')}}" required autofocus>
					      			</div>
			      				</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
					      				<label for="" class="control-label">{{__('message.description')}}</label>
				      					<input type="text" name="description" class="form-control" placeholder="{{__('message.description')}}">
					      			</div>
					      		</div>
					      		<div class="col-12 col-md-6">
					      			<div class="form-group">
					      				<label for="" class="control-label">{{__('message.duration')}} <small>[{{__('message.minutes')}}]</small></label>
			                        	@include('helpers.min_selector', ['name' => 'duration', 'value' => '30', 'status' => null, 'min' => '5'])
					      			</div>
					      		</div>
					      		<div class="col-12 col-md-6">
					      			<div class="form-group">
					      				<label for="" class="control-label">{{__('message.price')}} <small>[$]</small></label>
					      				<div class="input-group">
										   <div class="input-group-prepend">
										      	<span class="input-group-text">
										      		<i class="fa fa-dollar-sign"></i>
										      	</span>
										   </div>
				      						<input type="number" name="price" class="form-control" value="{{old('price')}}" placeholder="{{__('message.price')}}">
										</div>
					      			</div>
					      		</div>
			      			</div>
			      			<div class="form-group">
			      				<hr>
			      				<label for="" class="control-label">{{__('message.users_services')}}</label>
	                        	<hr>
	                        	<div class="row">
	                        		@foreach(Auth::user()->business->users->where('online', 'on') as $user)
			      						<div class="col-12 col-md-4">
			      							<div class="checkbox">
												<label for="user_{{$user->id}}">
													{{$user->name}}
					                            </label>
					                            @include('helpers.switch', ['name' => 'user_'.$user->id, 'status' => null])
				                        	</div>
			      						</div>
		                        	@endforeach
	                        	</div>	    
			      			</div>
			      			<hr>
		          			<div class="form-group text-right">
	          					<button class="btn btn-primary">
		          					<i class="fa fa-save"></i>
		          					{{__('message.save')}}
		          				</button>
		          			</div>
		          		</form>
      				</div>
      			</div>
                <hr>
                <div class="row">
                    <div class="col-12 col-md-8 offset-md-2">
                        @if(business()->services->count() == 0)
                            @include('helpers.registry_null')
                        @else
                        	<label for="">{{__('message.services')}}</label>
                            <div class="table table-responsive">
                                <table class="table table-stripped">
                                    @foreach(business()->services as $service)
                                        <tr>
                                            <td>
                                                {{$service->name}}
                                            </td>
                                            <td>
                                                {{$service->description == null ? "--" : $service->description}}
                                            </td>
                                            <td>
                                                {{$service->duration}} min
                                            </td>
                                            <td>
                                                ${{$service->price}}
                                            </td>
                                            <td>
                                                @include('helpers.delete_modal', ['delete_id' => $service->id, 'delete_route' => url('service', $service->id)])
                                            </td>
                                        </tr>  
                                    @endforeach
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
      			<hr>
                <div class="form-group text-right">
  					<a href="{{url('first/finish')}}" class="btn btn-dark">
      					{{__('message.finish')}}
      				</a>
      			</div>
            </div>
        </div>
    </div>
</div>
@endsection