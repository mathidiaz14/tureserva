@extends('layouts.dashboard', ['menu' => ''])

@section('title')
    {{__('message.first_steps')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<div class="row">
            		<div class="col-12 text-center">
            			<h4>
            				@if(user()->gender == "male")
								{{__('message.welcome')}}
							@elseif(user()->gender == "female")
								{{__('message.welcome1')}}
							@else
								{{__('message.welcome2')}}
							@endif
							{!!__('message.step1_welcome')!!}
            				
            				<br><br>
            				{{__('message.change_business_hours')}}
            			</h4>
            		</div>
            	</div>	
            	<hr>
	      		<div class="row">
      				<div class="col-12 col-lg-9 offset-lg-1">
      					<div id="hour_table">
		      				@include('admin.business.hour_table')
		      			</div>	
      				</div>
      			</div>	
      			<hr>
                <div class="form-group text-right">
  					<a href="{{url('first/step2')}}" class="btn btn-dark">
      					{{__('message.next')}}
      					<i class="fa fa-angle-right"></i>
      				</a>
      			</div>
            </div>
        </div>
    </div>
</div>
@endsection