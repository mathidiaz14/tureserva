@extends('layouts.dashboard', ['menu' => ''])

@section('title')
    {{__('message.first_steps')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<div class="row">
            		<div class="col-12 text-center">
            			<h4>
            				{{__('message.add_users')}}
            			</h4>
            		</div>
            	</div>	
      			<hr>
      			<div class="row">
      				<div class="col-12 col-md-8 offset-md-2">
                        <form action="{{url('user')}}" method="post" class="form-horizontal">
                            @csrf
                            <div class="row">
                                <input type="hidden" name="online" value="on">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="">{{__('message.name')}} *</label>
                                        <input type="text" class="form-control" name="name" required="" placeholder="{{__('message.name')}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="">{{__('message.email')}}</label>
                                        <input type="email" class="form-control" name="email" placeholder="{{__('message.email')}}">
                                    </div>
                                </div>
                                
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="" class="control-label">{{__('message.phone')}}</label>
                                        <input type="phone" name="phone" class="form-control" value="{{old('phone')}}" placeholder="{{__('message.phone')}}">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="">{{__('message.color')}}</label>
                                        @include('helpers.color', ['name' => "color", 'value' => "", 'user' => Auth::user()])
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-save"></i>
                                            {{__('message.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
      				</div>
      			</div>
                <hr>
                <div class="row">
                    <div class="col-12 col-md-8 offset-md-2">
                        @if(business()->users->count() == 1)
                            @include('helpers.registry_null')
                        @else
                            <label for="">{{__('message.users')}}</label>
                            <div class="table table-responsive">
                                <table class="table table-stripped">
                                    @foreach(business()->users->where('id', '!=', user()->id) as $user)
                                        <tr>
                                            <td>
                                                <div class="flag-color" style="background: {{$user->color}};"></div>
                                            </td>
                                            <td>
                                                {{$user->name}}
                                            </td>
                                            <td>
                                                {{$user->email == null ? "--" : $user->email}}
                                            </td>
                                            <td>
                                                {{$user->phone == null ? "--" : $user->phone}}
                                            </td>
                                            <td>
                                                @include('helpers.delete_modal', ['delete_id' => $user->id, 'delete_route' => url('user', $user->id)])
                                            </td>
                                        </tr>  
                                    @endforeach
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
      			<hr>
                <div class="form-group text-right">
  					<a href="{{url('first/step3')}}" class="btn btn-dark">
      					{{__('message.next')}}
      					<i class="fa fa-angle-right"></i>
      				</a>
      			</div>
            </div>
        </div>
    </div>
</div>
@endsection