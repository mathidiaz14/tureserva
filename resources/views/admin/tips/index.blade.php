@extends('layouts.dashboard', ['menu' => 'tips'])

@section('title')
    {{__('message.tips')}}
@endsection

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.tips')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('tips/create')}}" class="btn btn-info">
                            <i class="fa fa-plus"></i>   
                            {{__('message.new_tip')}}
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body ">
            	@if($tips->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('message.image')}}</th>
                                <th>{{__('message.title')}}</th>
                                <th>{{__('message.body')}}</th>
                                <th>{{__('message.edit')}}</th>
                                <th>{{__('message.delete')}}</th>
                            </tr>
                            @foreach($tips as $tip)
                                <tr>
                                    <td>
                                        <img src="{{$tip->image}}" alt="" width="100px">
                                    </td>
                                    <td>{{$tip->title}}</td>                              
                                    <td>{{substr($tip->body, 0, 100)}}...</td>
                                    <td>
                                        <a href="{{route('tips.edit', $tip->id)}}" class="btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        @include('helpers.delete_modal', ['delete_id' => $tip->id, 'delete_route' => route('tips.destroy', $tip->id)])
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
