@extends('layouts.dashboard', ['menu' => 'tips'])

@section('title')
	{{__('message.tip')}}
@endsection

@section('content')

<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.new_tip')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('tips')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
        	</div>
        	<div class="card-body">
          		<form action="{{url('tips')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
          			@csrf
          			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.image')}}</label>
	      				<div class="col-12 col-md-3">
	      					<div class="upload-btn-wrapper" style="width: 100%;">
	                          <button class="btn btn-info btn-block" style="margin-bottom: 0;">
	                            <i class="fa fa-arrow-up"></i>
	                            {{__('message.upload_file')}}
	                          </button>
	                          <input type="file" name="image" />
	                        </div>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.title')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="title" class="form-control" placeholder="{{__('message.title')}}" required autofocus>
	      				</div>
	      			</div>
					<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.body')}}</label>
	      				<div class="col-12 col-md-7">
	      					<textarea name="body" id="" cols="30" rows="10" class="form-control" placeholder="{{__('message.body')}}"></textarea>
	      				</div>
	      			</div>
	      			<hr>
          			<div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					{{__('message.save')}}
	          				</button>
          				</div>
          			</div>
          		</form>
          	</div>
        </div>
  	</div>
</div>
@endsection