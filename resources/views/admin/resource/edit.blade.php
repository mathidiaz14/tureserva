@extends('layouts.dashboard', ['menu' => 'resource'])

@section('title')
	{{__('message.resource')}}
@endsection

@section('content')

<form action="{{route('resource.update', $resource->id)}}" method="POST" enctype="multipart/form-data">
	@csrf
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2">
	      	<div class="card">
	        	<div class="card-header">
	          		<div class="row">
	          			<div class="col-6">
	          				<h3 class="card-title">{{__('message.edit_resource')}}</h3>
	          			</div>
	          			<div class="col-6 text-right">
	          				<a href="{{url('resource')}}" class="btn btn-default">
	          					<i class="fa fa-chevron-left"></i>
	          					{{__('message.back')}}
	          				</a>
	          			</div>
	          		</div>
	          		<div class="clearfix"></div>
	        	</div>
	        	<div class="card-body">
          			<input type="hidden" name="_method" value="PATCH">
	      			<div class="form-group">
	      				<label for="" class="control-label">{{__('message.name')}}</label>
						<input type="text" name="name" class="form-control" value="{{$resource->name}}" required autofocus>	
	      			</div>
	      			<div class="row">
	      				<div class="col-12 col-md-6">
	      					<div class="form-group">
			      				<label for="" class="control-label">{{__('message.type')}}</label>
		      					<select name="type" id="" class="form-control">
		      						 @if($resource->type == "equipment")
		                                <option value="equipment" selected>{{__('message.equipament')}}</option>
			      						<option value="room">{{__('message.room')}}</option>
			      						<option value="vehicle">{{__('message.vehicle')}}</option>
			      						<option value="other">{{__('message.other')}}</option>
		                            @elseif($resource->type == "room")
		                                <option value="equipment">{{__('message.equipament')}}</option>
			      						<option value="room" selected="">{{__('message.room')}}</option>
			      						<option value="vehicle">{{__('message.vehicle')}}</option>
			      						<option value="other">{{__('message.other')}}</option>
		                            @elseif($resource->type == "vehicle")
		                                <option value="equipment">{{__('message.equipament')}}</option>
			      						<option value="room">{{__('message.room')}}</option>
			      						<option value="vehicle" selected="">{{__('message.vehicle')}}</option>
			      						<option value="other">{{__('message.other')}}</option>
		                            @elseif($resource->type == "other")
		                                <option value="equipment">{{__('message.equipament')}}</option>
			      						<option value="room">{{__('message.room')}}</option>
			      						<option value="vehicle">{{__('message.vehicle')}}</option>
			      						<option value="other" selected="">{{__('message.other')}}</option>
		                            @endif
		      					</select>
			      			</div>			
	      				</div>
	      				<div class="col-12 col-md-6">
	      					<div class="form-group">
			      				<label for="" class="control-label">{{__('message.capacity')}}</label>
			      				<input type="number" name="capacity" class="form-control" value="{{$resource->capacity}}" placeholder="1">
			      			</div>	
	      				</div>
	      			</div>
	      			<div class="form-group mt-4">
	      				<hr>
	      				<div class="row">
      						<div class="col-3">
                            	@include('helpers.switch', ['name' => 'business_hour', 'status' => $resource->business_hour])
      						</div>
                            <div class="col-9">
      							<label for="business_hour">{{__('message.business_hours_user')}}</label>
                            </div>
      					</div>
	      			</div>
	          	</div>
	          	<div class="card-footer text-right">
	          		<button class="btn btn-primary">
      					<i class="fa fa-save"></i>
      					{{__('message.save')}}
      				</button>
	          	</div>
	        </div>
	  	</div>

	  	<div class="col-12 col-md-8 offset-md-2">
	  		<div class="card">
	  			<div class="card-header">
	  				<h5 class="card-title">{{__('message.resource_services')}}</h5>
	  			</div>
	  			<div class="card-body">
	  				@if(business()->services->count() == 0)
							@include('helpers.registry_null')
						@endif

                	@foreach(business()->services as $service)
                		<div class="row">
							<div class="col-8">
								<label for="service_{{$service->id}}">
	                              	{{$service->name}}
	                            </label>
							</div>
							<div class="col-4">
								@php
									$flag = "";
								@endphp
								@foreach($resource->services as $resource_service)
									@if($service->id == $resource_service->id)
										@php 
											$flag = "on";
										@endphp
									@endif
								@endforeach

                            	@include('helpers.switch', ['name' => 'service_'.$service->id, 'status' => $flag]) 
							</div>
                		</div>
                	@endforeach  
	  			</div>
	  		</div>
	  	</div>

	  	<div class="col-12 col-md-8 offset-md-2">
	  		<div class="card">
	  			<div class="card-header">
	  				<h5 class="card-title">{{__('message.resource_hours')}}</h5>
	  			</div>
	  			<div class="card-body">
	  				<div class="row" id="hour_table" @if($resource->business_hour == "on") style="display:none;" @endif>
  						@include('admin.resource.hour_table')
	  				</div>
	  			
	  				<div class="row" id="resource_business_hour_message" @if($resource->business_hour != "on") style="display:none;" @endif>
	  					<div class="col-12 text-center text-secondary">
							<br><br>
							<i class="fa fa-exclamation-circle fa-4x"></i>
							<br><br>
							<p>{{__('message.resource_business_hour_message')}}</p>
							<br><br>
						</div>
	  				</div>
		  			
	  			</div>
	  		</div>
	  	</div>
	</div>
</form>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#business_hour').on('change', function()
			{
				if(!$(this).is(':checked'))
				{
					$('#resource_business_hour_message').fadeOut();
					$('#hour_table').fadeIn();
				}
				else
				{
					$('#hour_table').fadeOut();
					$('#resource_business_hour_message').fadeIn();
				}
			});
		});
	</script>
@endsection
