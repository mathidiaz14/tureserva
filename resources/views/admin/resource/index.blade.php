@extends('layouts.dashboard', ['menu' => 'resource'])

@section('title')
    {{__('message.resources')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.resources')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{url('resource/create')}}" class="btn btn-info">
                            <i class="fa fa-plus"></i>
                            {{__('message.new_resource')}}
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($resources->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('message.name')}}</th>
                                <th>{{__('message.type')}}</th>
                                <th>{{__('message.capacity')}}</th>
                                <th>{{__('message.edit')}}</th>
                                <th>{{__('message.delete')}}</th>
                            </tr>
                            @foreach($resources as $resource)
                                <tr>
                                    <td>{{$resource->name}}</td>
                                    <td>
                                        @if($resource->type == "equipment")
                                            {{__('message.equipament')}}
                                        @elseif($resource->type == "room")
                                            {{__('message.room')}}
                                        @elseif($resource->type == "vehicle")
                                            {{__('message.vehicle')}}
                                        @elseif($resource->type == "other")
                                            {{__('message.other')}}
                                        @endif
                                    </td>
                                    <td>{{$resource->capacity == null ? '--' : $resource->capacity}}</td>
                                    <td>
                                        <a href="{{route('resource.edit', $resource->id)}}" class="btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        @include('helpers.delete_modal', ['delete_id' => $resource->id, 'delete_route' => route('resource.destroy', $resource->id)])
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
