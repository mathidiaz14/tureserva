@extends('layouts.dashboard', ['menu' => 'resource_create'])

@section('title')
	{{__('message.resource')}}
@endsection

@section('content')
<form action="{{url('resource')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
@csrf
	<div class="row">
		<div class="col-12 col-md-8 offset-md-2">
	      	<div class="card">
	        	<div class="card-header">
	          		<div class="row">
	          			<div class="col-6">
	          				<h3 class="card-title">{{__('message.new_resource')}}</h3>
	          			</div>
	          			<div class="col-6 text-right">
	          				<a href="{{url('resource')}}" class="btn btn-default">
	          					<i class="fa fa-chevron-left"></i>
	          					{{__('message.back')}}
	          				</a>
	          			</div>
	          		</div>
	        	</div>
	        	<div class="card-body">
	      			<div class="form-group">
	      				<label for="" class="control-label">{{__('message.name')}}</label>
      					<input type="text" name="name" class="form-control" value="{{old('name')}}" placeholder="{{__('message.name')}}" required autofocus>
	      			</div>
	      			<div class="row">
	      				<div class="col-12 col-md-6">
	      					<div class="form-group">
			      				<label for="" class="control-label">{{__('message.type')}}</label>
		      					<select name="type" id="" class="form-control">
		      						<option value="equipment">{{__('message.equipament')}}</option>
		      						<option value="room">{{__('message.room')}}</option>
		      						<option value="vehicle">{{__('message.vehicle')}}</option>
		      						<option value="other">{{__('message.other')}}</option>
		      					</select>
			      			</div>	
	      				</div>
	      				<div class="col-12 col-md-6">
	      					<div class="form-group">
			      				<label for="" class="control-label">{{__('message.capacity')}}</label>
		      					<input type="number" name="capacity" class="form-control" value="1">
			      			</div>	
	      				</div>
	      			</div>
	          	</div>
	          	<div class="card-footer text-right">
	          		<div class="form-group">
      					<button class="btn btn-primary">
          					<i class="fa fa-save"></i>
          					{{__('message.save')}}
          				</button>
          			</div>
	          	</div>
	        </div>
	  	</div>

	  	<div class="col-12 col-md-8 offset-md-2">
	  		<div class="card">
	  			<div class="card-header">
	  				<h5 class="card-title">{{__('message.resource_services')}}</h5>
	  			</div>
	  			<div class="card-body">
					@if(business()->services->count() == 0)
						@include('helpers.registry_null')
					@endif
                	@foreach(business()->services as $service)
                		<div class="row">
							<div class="col-8">
								<label for="service_{{$service->id}}">
	                              	{{$service->name}}
	                            </label>
							</div>
							<div class="col-4">
                            	@include('helpers.switch', ['name' => 'service_'.$service->id, 'status' => ""]) 
							</div>
                		</div>
                	@endforeach    
	  			</div>
	  		</div>
	  	</div>
	</div>
</form>
@endsection