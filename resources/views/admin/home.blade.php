@extends('layouts.dashboard', ["menu" => 'home'])

@section('title')
    Inicio
@endsection

@section('content')

    @include('admin.home.numbers')

    @include('admin.home.pending_tasks')

    @include('admin.home.waiting_list')

    @include('admin.home.notes')

@endsection
