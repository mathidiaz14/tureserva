@extends('layouts.dashboard', ['menu' => 'theme'])

@section('title')
    {{__('message.themes')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{__('message.themes')}}</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    @php
                        $path   = resource_path()."/views/landings/";
                        $dir    = opendir($path);
                    @endphp
                    
                    @while ($folder = readdir($dir))
                        @if( $folder != "." && $folder != "..")
                            @php
                                $body = null;
                                if(file_exists($path.$folder."/info.xml"))
                                    $body = simplexml_load_file($path.$folder."/info.xml");

                            @endphp

                            @if($body != null)
                                @if(($body->status == 'public') or ($body->status == business()->id))
                                    <div class="col-12 col-md-4" style="margin-bottom: 1em;">
                                        <div class="btn_modal" data-toggle="modal" data-target="#modal_{{str_replace(' ', '', $folder)}}">
                                            <img src="{{asset('landings/'.$folder.'/screenshot.png')}}" alt="" width="100%" >
                                            <p>
                                                <b>{{$body->name}}</b> 

                                                @if($folder == business()->folder) 
                                                    <small class="modal_status">Actual</small> 
                                                @endif 
                                            </p>
                                            <i class="fa fa-link"></i>
                                            <div class="background"></div>
                                        </div>
                                        <!-- Modal -->

                                        <div class="modal fade deleteModal" id="modal_{{str_replace(' ', '', $folder)}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <div class="col-6">
                                                            <h5>{{$body->name}}</h5>
                                                        </div>
                                                        <div class="col-6 text-right">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        </div>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-12 col-md-8">
                                                                <img src="{{asset('landings/'.$folder.'/screenshot.png')}}" alt="" width="100%" >
                                                            </div>
                                                            <div class="col-12 col-md-4">
                                                                <h4>
                                                                    <a href="{{$body->uri}}">
                                                                        {{$body->name}}
                                                                    </a>
                                                                    <small>{{$body->version}}</small>
                                                                </h4>
                                                                <hr>
                                                                <p>
                                                                    <a href="{{$body->author_uri}}" target="_blank">
                                                                        {{$body->author}}
                                                                    </a>
                                                                </p>
                                                                <p></p>
                                                                <p>{{$body->description}}</p>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        @if($folder == business()->folder) 
                                                            <a class="btn btn-info disabled">
                                                                <i class="fa fa-check"></i>
                                                                {{__('message.installed')}}
                                                            </a>    
                                                        @else
                                                            <a href="{{url('theme', str_replace(' ', '', $folder))}}" class="btn btn-info">
                                                                <i class="fas fa-cloud-upload-alt"></i>
                                                                {{__('message.install')}}
                                                            </a>
                                                        @endif 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif

                        @endif
                    @endwhile
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('.btn_modal').mouseenter(function()
            {
                $(this).children('.fa-link').fadeIn(100);
                $(this).children('.background').fadeIn(100);
            });

            $('.btn_modal').mouseleave(function()
            {
                $('.fa-link').fadeOut(100);
                $('.background').fadeOut(100);
            });
        });
    </script>
@endsection
