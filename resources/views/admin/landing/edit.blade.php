@extends('layouts.dashboard', ['menu' => 'theme_edit'])

@section('title')
    {{__('message.themes')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.theme_edit')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#reinstallModal">
                            <i class="fas fa-cloud-upload-alt"></i>
                            {{__('message.reinstall')}}
                        </button>
                        <!-- Modal -->

                        <div class="modal fade deleteModal" id="reinstallModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-center" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <div class="col-12 text-right">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        </div>
                                    </div>
                                    <div class="modal-body text-center">
                                        <div class="row text-secondary">
                                            <div class="col-12 text-center">
                                                <i class="fas fa-cloud-upload-alt fa-4x"></i>
                                                <br><br>
                                                <h3>{{__('message.reinstall_theme')}}</h3>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-6">
                                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">
                                                    {{__('message.no')}}
                                                </button>
                                            </div>
                                            <div class="col-6">
                                                <form action="{{route('theme.update', 1)}}" method="POST">
                                                    @csrf
                                                    @method('PATCH')
                                                    <button type="submit" class="btn btn-info btn-block">   
                                                        {{__('message.yes')}}
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if(business()->landings->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <form action="{{url('theme')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="table table-responsive">
                                <table class="table table-stripped">
                                    @foreach(business()->landings as $landing)
                                        <tr>
                                            <td>
                                                <label for="{{$landing->key}}">
                                                    {{$landing->title}}
                                                </label>
                                            </td>
                                            @if($landing->type == "image")
                                                <td>
                                                    @if($landing->value == null)
                                                        <img src="{{asset('images/null.jpg')}}" alt="" width="100px">
                                                    @else
                                                        <img src="{{asset($landing->value)}}" alt="" width="100px">
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="upload-btn-wrapper" style="width: 100%;">
                                                        <button class="btn btn-info" style="margin-bottom: 0; min-width: 240px;">
                                                            <i class="fa fa-arrow-up"></i>
                                                            {{__('message.upload_file')}}
                                                        </button>
                                                        <input type="file" name="{{$landing->key}}" />
                                                    </div>
                                                    <div style="width:100%;">
                                                        @if($landing->value != null)
                                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_modal_{{$landing->id}}" style="min-width: 240px;">
                                                                <i class="fa fa-trash"></i>
                                                                {{__('message.delete_file')}}
                                                            </button>
                                                            <!-- Modal -->

                                                            <div class="modal fade deleteModal" id="delete_modal_{{$landing->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header bg-red-color">
                                                                            <div class="col-12 text-right">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <div class="col-12 text-center">
                                                                                <i class="fa fa-times-circle fa-4x"></i>
                                                                                <br><br>
                                                                            </div>
                                                                            <h3>{{__('message.delete_file')}}</h3>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <div class="col-6">
                                                                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">
                                                                                    {{__('message.no')}}
                                                                                </button>
                                                                            </div>
                                                                            <div class="col-6">
                                                                                <a class="btn btn-danger btn-block" href="{{url('theme/delete', $landing->id)}}">
                                                                                    {{__('message.yes')}}
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        @else
                                                            <a class="btn btn-danger disabled" style="min-width: 240px;">
                                                                <i class="fa fa-trash"></i>
                                                                {{__('message.delete_file')}}
                                                            </a>
                                                        @endif 
                                                    </div>
                                                </td>
                                            @elseif($landing->type == "color")
                                                <td></td>
                                                <td>
                                                    <div class="input-group">
                                                       <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="fa fa-paint-brush"></i>
                                                            </span>
                                                       </div>
                                                       <input type="color" name="{{$landing->key}}" value="{{$landing->value}}" class="form-control" style="max-width:200px;">
                                                    </div>
                                                </td>
                                            @elseif($landing->type == "string")
                                                <td></td>
                                                <td>
                                                    <input type="text" class="form-control" name="{{$landing->key}}" value="{{$landing->value}}" placeholder="{{$landing->title}}">
                                                </td>
                                            @elseif($landing->type == "text")
                                                <td></td>
                                                <td>
                                                    <textarea name="{{$landing->key}}" id="" cols="30" rows="3" class="form-control" placeholder="{{$landing->title}}">{{$landing->value}}</textarea>
                                                </td>
                                            @elseif($landing->type == "checkbox")
                                                <td></td>
                                                <td>
                                                    @include('helpers.switch', ['name' => $landing->key, 'status' => $landing->value])
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>  
                        <hr>
                        <div class="row">
                            <div class="col-12 text-right">
                                <button class="btn btn-info">
                                    <i class="fa fa-save"></i>
                                    {{__('message.save')}}
                                </button>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    
@endsection
