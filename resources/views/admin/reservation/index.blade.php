@extends('layouts.dashboard', ['menu' => 'reservation'])

@section('title')
    {{__('message.reservation_config')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.reservation_config')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                      <a href="{{url($reservation->business->code)}}" target="_blank" class="btn btn-success">
                        <i class="fa fa-eye"></i>
                        {{__('message.view_web')}}
                      </a>
                    </div>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="card-body">
                <form action="{{route('reservation.update', $reservation->id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
        			@csrf
        			<input type="hidden" name="_method" value="PATCH">
    	      		<div class="form-group row">
                  	     <div class="col-12 col-md-8 offset-md-2">
                            <div class="row">
                                <div class="col">
                                    <label for="">
                                        {{__('message.reservation_url')}}
                                    </label>
                                </div>
                                <div class="col">
                                    <b><a href="{{url($reservation->business->code)}}" target="_blank">{{url($reservation->business->code)}}</a></b>
                                </div>   
                            </div>  
                        	<div class="row">
                                <div class="col">
                                    <label>
                                    	{{__('message.reservation_enabled')}}
                                    </label>
                                </div>
                                <div class="col">
                                    @include('helpers.switch', ['name' => 'access', 'status' => $reservation->access ])
                                </div>   
                            </div>  
                  	     </div>
                    </div>
  	      			<div class="reservation_info" @if($reservation->access == null) style="display:none;"@endif>
                        <hr>
                        <div class="form-group row">
                            
                            <div class="col-12 col-md-8 offset-md-2">
                                <div class="row">
                                    <div class="col">
                                        <label for="auto">
                                            {{__('message.task_auto')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.switch', ['name' => 'auto', 'status' => $reservation->auto ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="today">
                                            {{__('message.task_today')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.switch', ['name' => 'today', 'status' => $reservation->today ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="service_price">
                                            {{__('message.service_price')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.switch', ['name' => 'service_price', 'status' => $reservation->service_price ])
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col">
                                        <label for="nobody">
                                            {{__('message.nobody')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.switch', ['name' => 'nobody', 'status' => $reservation->nobody ])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label for="table_hours">
                                            {{__('message.table_hours')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.switch', ['name' => 'table_hours', 'status' => $reservation->table_hours ])
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col">
                                        <label for="">
                                          {{__('message.anticipation_days_reservation')}}
                                        </label>
                                        
                                    </div>
                                    <div class="col">
                                        @include('helpers.days_selector', ['name' => 'days_reserve', 'value' => $reservation->days_reserve , 'status' => null, 'limit' => '365'])
                                    </div>
                                </div>
                                
                                <div class="row mb-2">
                                    <div class="col">
                                        <label for="">
                                          {{__('message.anticipation_days_cancellation')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        @include('helpers.days_selector', ['name' => 'days_cancel', 'value' => $reservation->days_cancel , 'status' => null, 'limit' => $reservation->days_reserve])
                                    </div>
                                </div>

                                <div class="row mb-2">
                                    <div class="col">
                                        <label for="">
                                          {{__('message.interval_reservation')}}
                                        </label>
                                    </div>
                                    <div class="col">
                                        <div class="input-group">
                                           <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="fa fa-clock"></i></span>
                                           </div>
                                            <select name="interval" id="interval" class="form-control">
                                                @if($reservation->interval == "10")
                                                    <option value="10" selected>10</option>
                                                @else
                                                    <option value="10">10</option>
                                                @endif
                                                
                                                @if($reservation->interval == "15")
                                                    <option value="15" selected>15</option>
                                                @else
                                                    <option value="15">15</option>
                                                @endif

                                                @if($reservation->interval == "20")
                                                    <option value="20" selected>20</option>
                                                @else
                                                    <option value="20">20</option>
                                                @endif

                                                @if($reservation->interval == "30")
                                                    <option value="30" selected>30</option>
                                                @else
                                                    <option value="30">30</option>
                                                @endif

                                                @if($reservation->interval == "60")
                                                    <option value="60" selected>60</option>
                                                @else
                                                    <option value="60">60</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <hr>
                <div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					{{__('message.save')}}
	          				</button>
          				</div>
          			</div>
          		</form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
    {
      $('#access').on('change', function()
      {
        if($(this).prop('checked') == true)
          $('.reservation_info').fadeIn();
        else
          $('.reservation_info').fadeOut();
      }); 
    });
	</script>
@endsection
