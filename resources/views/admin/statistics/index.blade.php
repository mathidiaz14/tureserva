@extends('layouts.dashboard', ['menu' => 'statistics'])

@section('title')
    {{__('message.stadistics')}}
@endsection

@section('content')
<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.tasks_total')}}
                </h3>
            </div>
            <div class="card-body">
                <div class="chart">
                    <canvas id="totalTasks" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.tasks_failed')}}
                </h3>
            </div>
            <div class="card-body">
                <div class="chart">
                    <canvas id="faildTasks" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.tasks_cancel')}}
                </h3>
            </div>
            <div class="card-body">
                <div class="chart">
                    <canvas id="cancelTasks" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{__('message.clients_stadistics')}}</h3>
            </div>
            <div class="card-body text-center">
                <div class="col-12">
                    <canvas id="clientChart" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.tasks_for_user')}}
                </h3>
            </div>
            <div class="card-body text-center">
                <div class="col-12">
                    <canvas id="clientTaskChart" class="chartjs-render-monitor"></canvas>
                </div>  
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.visits_mounth')}}
                </h3>
            </div>
            <div class="card-body text-center">
                <div class="col-12">
                    <canvas id="visitsMounthChart" class="chartjs-render-monitor"></canvas>
                </div>  
            </div>
        </div>
    </div>
     <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.visits_daily')}}
                </h3>
            </div>
            <div class="card-body text-center">
                <div class="col-12">
                    <canvas id="visitsDailyChart" class="chartjs-render-monitor"></canvas>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
    <script src="{{asset('dashboard/plugins/chart.js/Chart.min.js')}}"></script>
    <script>
        //Citas totales
        var areaChartCanvas = $('#totalTasks').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 6; $i >= 0; $i --)
                            "{{month_name(\Carbon\Carbon::now()->subMonth($i)->format('M'))}}",
                        @endfor
          ],
          datasets: [
            {
              label               : 'Citas totales',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : [
                    @for($i = 6; $i >= 0; $i --)
                        {{Auth::user()->business->tasks
                                                ->whereBetween('start', [
                                                    \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                                    \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()]
                                                )->count()}},
                    @endfor
              ]
            },
            {
              label               : 'Reservas Online',
              backgroundColor     : 'rgba(123, 15, 140, 0.9)',
              borderColor         : 'rgba(123, 15, 140, 0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(123, 15, 140, 1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(123, 15, 140, 1)',
              data                : [
                    @for($i = 6; $i >= 0; $i --)
                        {{Auth::user()->business->tasks
                                                ->where('online', 'yes')
                                                ->whereBetween('start', [
                                                    \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                                    \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()]
                                                )->count()}},
                    @endfor
              ]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        });

        
        //Citas fallidas
        var areaChartCanvas = $('#faildTasks').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 6; $i >= 0; $i --)
                            "{{month_name(\Carbon\Carbon::now()->subMonth($i)->format('M'))}}",
                        @endfor
          ],
          datasets: [
            {
              label               : 'Citas fallidas',
              backgroundColor     : 'rgba(38, 185, 154, .9)',
              borderColor         : 'rgba(38, 185, 154,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(38, 185, 154,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(38, 185, 154,1)',
              data                : [
                    @for($i = 6; $i >= 0; $i --)
                        {{Auth::user()->business->tasks
                                                ->where('status', 'faild')
                                                ->whereBetween('start', [
                                                    \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                                    \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()]
                                                )->count()}},
                    @endfor
              ]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        });

        //Citas canceladas
        var areaChartCanvas = $('#cancelTasks').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 6; $i >= 0; $i --)
                            "{{month_name(\Carbon\Carbon::now()->subMonth($i)->format('M'))}}",
                        @endfor
          ],
          datasets: [
            {
              label               : 'Citas canceladas',
              backgroundColor     : 'rgba(211, 10, 10, .9)',
              borderColor         : 'rgba(211, 10, 10,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(211, 10, 10,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(211, 10, 10,1)',
              data                : [
                    @for($i = 6; $i >= 0; $i --)
                        {{Auth::user()->business->tasks
                                                ->where('status', 'cancel')
                                                ->whereBetween('start', [
                                                    \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                                    \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()]
                                                )->count()}},
                    @endfor
              ]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        });

        //Clientes
        var areaChartCanvas = $('#clientChart').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 6; $i >= 0; $i --)
                            "{{month_name(\Carbon\Carbon::now()->subMonth($i)->format('M'))}}",
                        @endfor
          ],
          datasets: [
            {
              label               : 'Clientes por mes',
              backgroundColor     : 'rgba(38, 185, 154, .9)',
              borderColor         : 'rgba(38, 185, 154,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(38, 185, 154,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(38, 185, 154,1)',
              data                : [
                    @for($i = 6; $i >= 0; $i --)
                        {{Auth::user()->business->clients
                                        ->whereBetween('created_at', [
                                                \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                                \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()])
                                        ->count()}},
                    @endfor
              ]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        });

        //Citas fallidas
        var areaChartCanvas = $('#clientTaskChart').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 6; $i >= 0; $i --)
                            "{{month_name(\Carbon\Carbon::now()->subMonth($i)->format('M'))}}",
                        @endfor
          ],
          datasets: [
            @foreach(business()->users as $user)
                {
                  label               : '{{$user->name}}',
                  backgroundColor     : '{{$user->color}}b3',
                  borderColor         : '{{$user->color}}',
                  pointRadius          : false,
                  pointColor          : '#3b8bba',
                  pointStrokeColor    : '{{$user->color}}',
                  pointHighlightFill  : '#fff',
                  pointHighlightStroke: '{{$user->color}}',
                  data                : [
                        @for($i = 6; $i >= 0; $i --)
                            {{$user->tasks
                                    ->whereBetween('start', [
                                            \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                            \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()])
                                    ->count()}},
                        @endfor
                  ]
                },
            @endforeach
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: true
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        }); 

        //Visitas mensuales
        var areaChartCanvas = $('#visitsMounthChart').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 6; $i >= 0; $i --)
                            "{{month_name(\Carbon\Carbon::now()->subMonth($i)->format('M'))}}",
                        @endfor
          ],
          datasets: [
            {
              label               : 'Visitas Mensuales',
              backgroundColor     : 'rgba(255,212,0,0.9)',
              borderColor         : 'rgba(255,212,0,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(255,212,0,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(255,212,0,1)',
              data                : [
                    @for($i = 6; $i >= 0; $i --)
                        {{Auth::user()->business->visits
                                                ->whereBetween('created_at', [
                                                    \Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), 
                                                    \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()]
                                                )->count()}},
                    @endfor
              ]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        });

        //Visitas diarias
        var areaChartCanvas = $('#visitsDailyChart').get(0).getContext('2d')

        var areaChartData = {
          labels  : [
                        @for($i = 30; $i >= 0; $i --)
                            "{{\Carbon\Carbon::now()->subDays($i)->format('d/m')}}",
                        @endfor
          ],
          datasets: [
            {
              label               : 'Visitas Mensuales',
              backgroundColor     : 'rgba(255,212,0,0.9)',
              borderColor         : 'rgba(255,212,0,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(255,212,0,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(255,212,0,1)',
              data                : [
                    @for($i = 30; $i >= 0; $i --)
                        {{Auth::user()->business->visits
                                                ->whereBetween('created_at', [
                                                    \Carbon\Carbon::now()->subDays($i)->startOfDay(), 
                                                    \Carbon\Carbon::now()->subDays($i)->endOfDay()]
                                                )->count()}},
                    @endfor
              ]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        });
</script>
@endsection
