@extends('layouts.dashboard', ['menu' => 'billing'])

@section('title')
    {{__('message.billings')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.billings')}}
                </h3>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($billings->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('message.number')}}</th>
                                <th>{{__('message.date')}}</th>
                                <th>{{__('message.description')}}</th>
                                <th>{{__('message.period')}}</th>
                                <th>{{__('message.payment_method')}}</th>
                                <th>{{__('message.card')}}</th>
                                <th>{{__('message.subtotal')}}</th>
                                <th>{{__('message.total')}}</th>
                            </tr>
                            @foreach($billings as $billing)
                                <tr>
                                    <td>#{{$billing->number}}</td>
                                    <td>{{$billing->date->format('d/m/Y')}}</td>
                                    <td>{{$billing->description}}</td>
                                    <td>{{$billing->period}}</td>
                                    <td>{{$billing->payment_method}}</td>
                                    <td>···· ···· ···· {{$billing->last_four_numbers}}</td>
                                    <td>${{$billing->subtotal}}</td>
                                    <td>${{$billing->total}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    @include('helpers.paginate', ['link' => $billings])
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
