@extends('layouts.dashboard', ['menu' => 'service'])

@section('title')
	{{__('message.service')}}
@endsection

@section('content')

	<form action="{{url('service', $service->id)}}" method="POST" class="form-horizontal">
		@csrf
		@method('PATCH')

		<div class="row">
			<div class="col-12 col-md-8 offset-md-2">
		      	<div class="card">
		        	<div class="card-header">
		          		<div class="row">
		          			<div class="col-6">
		          				<h3 class="card-title">
		          					{{__('message.edit_service')}}
		          				</h3>
		          			</div>
		          			<div class="col-6 text-right">
		          				<a href="{{url('service')}}" class="btn btn-default">
		          					<i class="fa fa-chevron-left"></i>
		          					{{__('message.back')}}
		          				</a>
		          			</div>
		          		</div>
		          		<div class="clearfix"></div>
		        	</div>
		        	<div class="card-body">
		      			<div class="form-group">
		      				<label for="" class="control-label">{{__('message.name')}}</label>
	      					<input type="text" name="name" class="form-control" value="{{$service->name}}" required autofocus>		
		      			</div>

						<div class="form-group">
		      				<label for="" class="control-label">{{__('message.description')}}</label>
	      					<textarea name="description" id="" class="form-control">{{$service->description}}</textarea>	
		      			</div>

		      			<div class="row">
		      				<div class="col-12 col-md-6">
		      					<div class="form-group">
				      				<label for="" class="control-label">{{__('message.duration')}} <small>[{{__('message.minutes')}}]</small></label>
		                        	@include('helpers.min_selector', ['name' => 'duration', 'value' => $service->duration, 'status' => null, 'min' => '5'])	
				      			</div>			
		      				</div>
		      				<div class="col-12 col-md-6">
		      					<div class="form-group">
				      				<label for="" class="control-label">{{__('message.price')}} <small>[$]</small></label>
									<div class="input-group">
									   	<div class="input-group-prepend">
									      	<span class="input-group-text"><i class="fa fa-dollar-sign"></i></span>
									   	</div>
			      						<input type="number" name="price" class="form-control" value="{{$service->price}}">
									</div>
				      			</div>		
		      				</div>
		      			</div>
		      			
		      			<div class="form-group mt-4">
		      				<hr>
		      				<div class="row">
	      						<div class="col-3">
									@include('helpers.switch', ['name' => 'online', 'status' => $service->online])
	      						</div>
	      						<div class="col-9">
			      					<label for="online">{{__('message.online_reservation_info')}}</label>
	      						</div>
	      					</div>
	      					<div class="row">
	      						<div class="col-3">
	                            	@include('helpers.switch', ['name' => 'business_hour', 'status' => $service->business_hour])
	      						</div>
	                            <div class="col-9">
	      							<label for="business_hour">{{__('message.business_hours_user')}}</label>
	                            </div>
	      					</div>
		      			</div>
	          		</div>
	          		<div class="card-footer">
	          			<div class="row">
	          				<div class="col-12 text-right">
	          					<button class="btn btn-primary">
		          					<i class="fa fa-save"></i>
		          					{{__('message.save')}}
		          				</button>
	          				</div>
	          			</div>
	          		</div>
		        </div>
		  	</div>

		  	<div class="col-12 col-md-8 offset-md-2">
		  		<div class="card">
		  			<div class="card-header">
		  				<h5 class="card-title">{{__('message.users_services')}}</h5>
		  			</div>
		  			<div class="card-body">
		  				@if(business()->users->count() == 0)
  							@include('helpers.registry_null')
  						@endif
                    	@foreach(business()->users as $user)
                    		<div class="row">
								<div class="col-8">
									<label for="user_{{$user->id}}">
		                              	{{$user->name}}
		                            </label>
								</div>
								<div class="col-4">
									@php
										$flag = "";
									@endphp
									@foreach($service->users as $service_user)
										@if($user->id == $service_user->id)
											@php 
												$flag = "on";
											@endphp
										@endif
									@endforeach

	                            	@include('helpers.switch', ['name' => 'user_'.$user->id, 'status' => $flag]) 
								</div>
                    		</div>
                    	@endforeach
		  			</div>
		  		</div>
		  	</div>

		  	<div class="col-12 col-md-8 offset-md-2">
		  		<div class="card">
		  			<div class="card-header">
		  				<h5 class="card-title">{{__('message.service_resources')}}</h5>
		  			</div>
		  			<div class="card-body">
		  				@if(business()->resources->count() == 0)
  							@include('helpers.registry_null')
  						@endif

                    	@foreach(business()->resources as $resource)
                    		<div class="row">
								<div class="col-8">
									<label for="resource_{{$resource->id}}">
		                              	{{$resource->name}}
		                            </label>
								</div>
								<div class="col-4">
									@php
										$flag = "";
									@endphp
									@foreach($service->resources as $service_resource)
										@if($resource->id == $service_resource->id)
											@php 
												$flag = "on";
											@endphp
										@endif
									@endforeach

	                            	@include('helpers.switch', ['name' => 'resource_'.$resource->id, 'status' => $flag]) 
								</div>
                    		</div>
                    	@endforeach  
		  			</div>
		  		</div>
		  	</div>

		  	<div class="col-12 col-md-8 offset-md-2">
		  		<div class="card">
		  			<div class="card-header">
		  				<h5 class="card-title">{{__('message.service_hours')}}</h5>
		  			</div>
		  			<div class="card-body">
		  				
			  				<div class="row" id="hour_table" @if($service->business_hour == "on") style="display:none;" @endif>
		  						@include('admin.service.hour_table')
			  				</div>
			  			
			  				<div class="row" id="service_business_hour_message" @if($service->business_hour != "on") style="display:none;" @endif>
			  					<div class="col-12 text-center text-secondary">
									<br><br>
									<i class="fa fa-exclamation-circle fa-4x"></i>
									<br><br>
									<p>{{__('message.service_business_hour_message')}}</p>
									<br><br>
								</div>
			  				</div>
			  			
		  			</div>
		  		</div>
		  	</div>
		</div>
  	</form>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#business_hour').on('change', function()
			{
				if(!$(this).is(':checked'))
				{
					$('#service_business_hour_message').fadeOut();
					$('#hour_table').fadeIn();
				}
				else
				{
					$('#hour_table').fadeOut();
					$('#service_business_hour_message').fadeIn();
				}
			});
		});
	</script>
@endsection
