@foreach($days as $day)
	<div class="row" id="day_{{$day->id}}" style="margin-top:5px;">
		<div class="col-12 col-md-2" style="margin-top: 5px;">
			@if($loop->first)
				@php  
					$status = $day->status;
					if($status == NULL)
						$status = "on";
				@endphp
				<p>
					<b>{{day_name($day->day)}}</b>
				</p>
			@endif
		</div>
		<div class="col-6 col-md-3" style="margin-top: 5px;">
			@include('helpers.hour_selector_basic', [
									'class' 	=> $day->day.'_status select_hour', 
									'name' 		=> $day->day.'_start_'.$day->id, 
									'value' 	=> $day->start, 
									'status' 	=> $status, 
									'min' 		=> '5', 
									'start'		=> Carbon\Carbon::today(),
									'end'		=> check_day_end(business(), $day->day),
								])
		</div>
		<div class="col-6 col-md-3" style="margin-top: 5px;">
			@include('helpers.hour_selector_basic', [
									'class' 	=> $day->day.'_status select_hour', 
									'name' 		=> $day->day.'_end_'.$day->id, 
									'value' 	=> $day->end, 
									'status' 	=> $status, 
									'min' 		=> '5', 
									'start'		=> check_day_start(business(), $day->day),
									'end'		=> Carbon\Carbon::today()->addHours(23)->addMinutes(55),
								])
		</div>
		<div class="col-6 col-md-1 mt-2">
			@if($loop->first)
				<button type="button" class="btn btn-block btn-secondary add_hour" data-toggle="modal" data-target="#add_hour_{{$day->day}}">
					<i class="fa fa-plus"></i>
				</button>
				<!-- Modal -->

				<div class="modal fade deleteModal" id="add_hour_{{$day->day}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog modal-center" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<div class="col-10">
									<h6>{{__('message.add_hous_to_day', ['day' => day_name($day->day)])}}</h6>
								</div>
								<div class="col-2 text-right">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>
							</div>
							<div class="modal-body">
								<div class="row">
									<input type="hidden" id="add_day_{{$day->day}}" value="{{$day->day}}">
									<div class="col-6" style="margin-top: 5px;">
										@include('helpers.hour_selector_new', [
																'class' 	=> '', 
																'name' 		=> 'add_start_'.$day->day, 
																'value' 	=> check_day_end_service($service->id, $day->day)->format('H:i'),
																'status' 	=> '', 
																'min' 		=> '5', 
																'day'		=> $day->day,
																'data'		=> $service
															])
									</div>
									<div class="col-6" style="margin-top: 5px;">		
										@include('helpers.hour_selector_new', [
																'class' 	=> '', 
																'name' 		=> 'add_end_'.$day->day, 
																'value' 	=> check_day_end_service($service->id, $day->day)->addMinutes(60)->format('H:i'),
																'status' 	=> '', 
																'min' 		=> '5', 
																'day'		=> $day->day,
																'data'		=> $service
															])
									</div>
									<div class="col-12 mt-2">
										<hr>
										<a class="btn btn-info btn-block btn_add_day_{{$day->day}}">
											<i class="fa fa-plus"></i>
											{{__('message.add')}}
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<script>
					$('.btn_add_day_{{$day->day}}').on("click change", function()
				    {
				    	$.ajax({
						  	url: "{{url('daysService/add')}}",
						  	type: "POST",
				        	dataType: 'json',
						  	data: {
						  		"_token"	: "{{csrf_token()}}",
						  		"service"	: '{{$service->id}}',
							  	"day"		: $('#add_day_{{$day->day}}').val(),
							  	"start"		: $('#add_start_{{$day->day}}').val(),
							  	"end"		: $('#add_end_{{$day->day}}').val(),
						  	},
						  	success: function(response)
						  	{
						  		$('.loading').fadeIn();
						  		$('#add_hour_{{$day->day}}').modal('toggle');
						  		$('#hour_table').load("{{url('service/get/hours', $service->id)}}");
						  		$('.loading').fadeOut();
						  	}
						});
				    });
				</script>
			@else
				<a class="btn btn-block btn-danger btn_delete_day" attr-id="{{$day->id}}">
					<i class="fa fa-trash"></i>
				</a>
			@endif
		</div>
		<div class="col-6 col-md-3 mt-2">
			@if($loop->first)
				@if($status == "on")
					<a class="btn btn-block btn-info btn_check_day" attr-id="{{$day->day}}" attr-number="{{$day->id}}" attr-status="{{$status}}">
						{{__('message.open')}}
					</a>
				@else
					<a class="btn btn-block btn-dark btn_check_day" attr-id="{{$day->day}}" attr-number="{{$day->id}}" attr-status="{{$status}}">
						{{__('message.close')}}
					</a>
				@endif
			@endif
			<input type="checkbox" style="display:none;" name="{{$day->day}}_status_{{$day->id}}"  id="{{$day->day}}"
		  	@if($status != null)
				checked
			@endif />
		</div>
	</div>
@endforeach