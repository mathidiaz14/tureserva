@extends('layouts.dashboard', ['menu' => 'service'])

@section('title')
    {{__('message.services')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.services')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('service/create')}}" class="btn btn-info">
                    		<i class="fa fa-plus"></i>
                    		{{__('message.new_service')}}
                    	</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
            	@if($services->count() == 0)
                    @include('helpers.registry_null')
                @else
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>{{__('message.name')}}</th>
                                <th>{{__('message.description')}}</th>
                                <th>{{__('message.price')}}</th>
                                <th>{{__('message.duration')}}</th>
                                <th>{{__('message.online_reservation')}}</th>
                                <th>{{__('message.edit')}}</th>
                                <th>{{__('message.delete')}}</th>
                            </tr>
                            @foreach($services as $service)
                                <tr>
                                    <td>{{$service->name}}</td>
                                    <td>{{$service->description}}</td>                              
                                    <td>{{$service->price == null ? "--" : "$".$service->price}}</td>
                                    <td>{{$service->duration}} min</td>
                                    <td>
                                        @if($service->online == "on")
                                            {{__('message.yes')}}
                                        @else
                                            {{__('message.no')}}
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('service.edit', $service->id)}}" class="btn btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        @include('helpers.delete_modal', ['delete_id' => $service->id, 'delete_route' => route('service.destroy', $service->id)])
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
