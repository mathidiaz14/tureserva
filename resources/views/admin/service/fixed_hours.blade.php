<style>
	.modal-body{
    max-height: calc(100vh - 200px);
    overflow-y: auto;
}
</style>
<div class="col-12">
	<div class="row">
		@foreach(days_list() as $day)
			<div class="col-12">
				<div class="card">
					<div class="card-header bg-dark">
						<div class="row">
							<div class="col-6">
								<h5 class="card-title">
									{{day_name($day)}}
								</h5>
							</div>
							<div class="col-6 text-right">
								<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#add_hour_{{$day}}" @if(!check_day_status(business(), $day)) disabled @endif onclick="$('.back_modal').show();">
									<i class="fa fa-plus"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="card-body bg-light" id="day_{{$day}}">
						<b>Horarios</b>		
					</div>
				</div>
			</div>

			<div class="modal fade fixedHoursModal text-left" id="add_hour_{{$day}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-center" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<div class="col-10">
								<h6>{{__('message.add_hous_to_day', ['day' => day_name($day)])}}</h6>
							</div>
							<div class="col-2 text-right">
								<button type="button" class="close" aria-label="Close" onClick="$('#add_hour_{{$day}}').modal('hide');"><span aria-hidden="true">&times;</span></button>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-6" style="margin-top: 5px;">
									<label for="">{{__('message.starts')}}</label>
									@include('helpers.hour_selector_service', [
										'class' 	=> '', 
										'name' 		=> 'add_start_'.$day, 
										'value' 	=> check_day_start(business(), $day)->format('H:i'),
										'status' 	=> '', 
										'min' 		=> '5', 
										'day'		=> $day,
										'data'		=> business()
									])
								</div>
								<div class="col-6" style="margin-top: 5px;">	
									<label for="">{{__('message.ends')}}</label>	
									@include('helpers.hour_selector_service', [
										'class' 	=> '', 
										'name' 		=> 'add_end_'.$day, 
										'value' 	=> check_day_end(business(), $day)->format('H:i'),
										'status' 	=> '', 
										'min' 		=> '5', 
										'day'		=> $day,
										'data'		=> business()
									])
								</div>
								<div class="col-12 mt-2">
									<label for="">{{__('message.quotas')}}</label>
									<div class="row">
										<div class="col-6">
											<input type="number" id="add_quotas_{{$day}}" value="10" min="1" class="form-control">
										</div>
										<div class="col-3">
											<a class="btn btn-info btn-block text-white btn_remove_quotas" attr-id="{{$day}}">
												<i class="fa fa-minus"></i>
											</a>
										</div>
										<div class="col-3">
											<a class="btn btn-info btn-block text-white btn_add_quotas" attr-id="{{$day}}">
												<i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
								</div>
								<div class="col-12 mt-2">
									<hr>
									<a class="btn btn-info btn-block btn_add_day" attr-id="{{$day}}">
										<i class="fa fa-plus"></i>
										{{__('message.add')}}
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<script>
				$( "#add_hour_{{$day}}" ).on('hidden.bs.modal', function()
				{
				    $('.back_modal').hide();
				});
			</script>	
		@endforeach
	</div>
	<br>		
</div>

<input type="hidden" name="quantity_fixed_hours" id="quantity_fixed_hours">

<div class="back_modal hide"></div>

<script>
	$('.btn_add_quotas').click(function()
	{
		var day 		= $(this).attr('attr-id');
		var number 		= parseFloat($('#add_quotas_'+day).val());

		$('#add_quotas_'+day).val(number + 1);
	});

	$('.btn_remove_quotas').click(function()
	{
		var day 		= $(this).attr('attr-id');
		var number 		= parseFloat($('#add_quotas_'+day).val());

		if(number > 0)
			$('#add_quotas_'+day).val(number - 1);
	});

	$('.btn_add_day').on("click change", function()
	{
		var day 		= $(this).attr('attr-id');
		var start		= $('#add_start_'+day).val();
	  	var end			= $('#add_end_'+day).val();
	  	var quotas		= $('#add_quotas_'+day).val();

	  	var quantity 	= $('.hours').length;

	  	$('#quantity_fixed_hours').val(quantity);

	  	var text = '<div class="row hours mt-2" id="row_'+quantity+'">';
	  	text += ' <input type="hidden" name="hour_day_'+quantity+'" value="'+day+'">';
	  	text += ' <input type="hidden" name="hour_start_'+quantity+'" value="'+start+'">';
	  	text += ' <input type="hidden" name="hour_end_'+quantity+'" value="'+end+'">';
	  	text += ' <input type="hidden" name="hour_quota_'+quantity+'" value="'+quotas+'">';
		text += '	<div class="col-3 mt-1">';
		text += '		<div class="input-group">';
		text += '		   <div class="input-group-prepend">';
		text += '		      <span class="input-group-text"><i class="fa fa-clock"></i></span>';
		text += '		   </div>';
		text += '		   <input type="text" class="form-control" value="'+start+'" disabled>';
		text += '		</div>';
		text += '	</div>';
		text += '	<div class="col-3 mt-1">';
		text += '		<div class="input-group">';
		text += '		   <div class="input-group-prepend">';
		text += '		      <span class="input-group-text"><i class="fa fa-clock"></i></span>';
		text += '		   </div>';
		text += '		   <input type="text" class="form-control" value="'+end+'" disabled>';
		text += '		</div>';
		text += '	</div>';
		text += '	<div class="col-3 mt-1">';
		text += '		<div class="input-group">';
		text += '		   <div class="input-group-prepend">';
		text += '		      <span class="input-group-text"><i class="fa fa-users"></i></span>';
		text += '		   </div>';
		text += '		   <input type="text" class="form-control" value="'+quotas+'" disabled>';
		text += '		</div>';
		text += '	</div>';
		text += '	<div class="col-3 mt-1">';
		text += '		<a class="btn btn-danger btn-block" onClick="btn_delete_hour(\''+quantity+'\');">';
		text += '			<i class="fa fa-times"></i>';
		text += '		</a>';
		text += '	</div>';
		text += '</div>';
	  	
	  	$('#day_'+day).append(text);

	  	$('#add_hour_'+day).modal('toggle');
	});

	function btn_delete_hour(id)
	{
		$('#row_'+id).fadeIn();
		$('#row_'+id).remove();
	};
</script>