@extends('layouts.dashboard', ['menu' => 'service_create'])

@section('title')
	{{__('message.service')}}
@endsection

@section('content')

	<form action="{{url('service')}}" method="POST" class="form-horizontal">
	@csrf
		<div class="row">
			<div class="col-12 col-md-8 offset-md-2">
		      	<div class="card">
		        	<div class="card-header">
		          		<div class="row">
		          			<div class="col-6">
		          				<h3 class="card-title">
		          					{{__('message.new_service')}}
		          				</h3>
		          			</div>
		          			<div class="col-6 text-right">
		          				<a href="{{url('service')}}" class="btn btn-default">
		          					<i class="fa fa-chevron-left"></i>
		          					{{__('message.back')}}
		          				</a>
		          			</div>
		          		</div>
		          		<div class="clearfix"></div>
		        	</div>
		        	<div class="card-body">
		      			<div class="form-group">
		      				<label for="" class="control-label">{{__('message.name')}}</label>
	      					<input type="text" name="name" class="form-control" placeholder="{{__('message.name')}}" value="" required autofocus>
		      			</div>
						<div class="form-group">
		      				<label for="" class="control-label">{{__('message.description')}}</label>
	      					<textarea name="description" id="" class="form-control" placeholder="{{__('message.description')}}"></textarea>
		      			</div>
		      			<div class="row">
		      				<div class="col-12 col-md-6">
				      			<div class="form-group">
				      				<label for="" class="control-label">{{__('message.duration')}} <small>[{{__('message.minutes')}}]</small></label>
		                        	@include('helpers.min_selector', ['name' => 'duration', 'value' => 30, 'status' => null, 'min' => '5'])
				      			</div>
		      				</div>
		      				<div class="col-12 col-md-6">
				      			<div class="form-group">
				      				<label for="" class="control-label">{{__('message.price')}} <small>[$]</small></label>
									<div class="input-group">
									   	<div class="input-group-prepend">
									      	<span class="input-group-text"><i class="fa fa-dollar-sign"></i></span>
									   	</div>
			      						<input type="number" name="price" class="form-control" value="">
									</div>
				      			</div>
		      				</div>
		      			</div>
		      			
		      			<div class="form-group mt-4">
		      				<hr>
		      				<div class="row">
	      						<div class="col-3">
									@include('helpers.switch', ['name' => 'online', 'status' => "on"])
	      						</div>
	      						<div class="col-9">
			      					<label for="online">{{__('message.online_reservation_info')}}</label>
	      						</div>
	      					</div>
		      			</div>
	          		</div>
	          		<div class="card-footer">
	          			<div class="row">
	          				<div class="col-12 text-right">
	          					<button class="btn btn-primary">
		          					<i class="fa fa-save"></i>
		          					{{__('message.save')}}
		          				</button>
	          				</div>
	          			</div>
	          		</div>
		        </div>
		  	</div>

		  	<div class="col-12 col-md-8 offset-md-2">
		  		<div class="card">
		  			<div class="card-header">
		  				<h5 class="card-title">{{__('message.users_services')}}</h5>
		  			</div>
		  			<div class="card-body">
  						@if(business()->users->count() == 0)
  							@include('helpers.registry_null')
  						@endif
                    	@foreach(business()->users as $user)
                    		<div class="row">
								<div class="col-8">
									<label for="user_{{$user->id}}">
		                              	{{$user->name}}
		                            </label>
								</div>
								<div class="col-4">
	                            	@include('helpers.switch', ['name' => 'user_'.$user->id, 'status' => "on"]) 
								</div>
                    		</div>
                    	@endforeach    
		  			</div>
		  		</div>
		  	</div>

		  	<div class="col-12 col-md-8 offset-md-2">
		  		<div class="card">
		  			<div class="card-header">
		  				<h5 class="card-title">{{__('message.service_resources')}}</h5>
		  			</div>
		  			<div class="card-body">
  						@if(business()->resources->count() == 0)
  							@include('helpers.registry_null')
  						@endif
                    	@foreach(business()->resources as $resource)
                    		<div class="row">
								<div class="col-8">
									<label for="resource_{{$resource->id}}">
		                              	{{$resource->name}}
		                            </label>
								</div>
								<div class="col-4">
	                            	@include('helpers.switch', ['name' => 'resource_'.$resource->id, 'status' => ""]) 
								</div>
                    		</div>
                    	@endforeach    
		  			</div>
		  		</div>
		  	</div>
		</div>
  	</form>
@endsection

@section('scripts')
	<script>
		
	</script>
@endsection