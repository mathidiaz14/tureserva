@extends('layouts.dashboard', ['menu' => 'user'])

@section('title')
	{{__('message.user')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.edit_user')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('user')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="card-body text-right">
          		<form action="{{url('user', $user->id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
          			@csrf
          			<input type="hidden" name="_method" value="PATCH">
	                <div class="form-group row">
	                  	<label for="" class="control-label col-12 col-md-3">{{__('message.avatar')}}</label>
	                  	<div class="col-12 col-md-7">
		                    <div class="row">
		                    	<div class="col-6 text-center">
			                    	@if($user->avatar == null)
			                      		<img src="{{asset('images/profile.png')}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
			                    	@else
			                      		<img src="{{asset($user->avatar)}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
			                    	@endif
			                    </div>
			                    <div class="col-6">
			                      	@if($user->avatar == null)
										<a class="btn btn-danger btn-block disabled">
				                        	<i class="fa fa-trash"></i>
				                        	{{__('message.delete_file')}}
				                      	</a>
			                      	@else
										<a href="{{url('user/delete/avatar', $user->id)}}" class="btn btn-danger btn-block">
				                        	<i class="fa fa-trash"></i>
				                        	{{__('message.delete_file')}}
				                      	</a>
			                      	@endif
			                      	<br>
			                      	<div class="upload-btn-wrapper" style="width: 100%;">
									  	<button class="btn btn-info btn-block" style="margin-bottom: 0;">
									  		<i class="fa fa-arrow-up"></i>
									  		{{__('message.upload_file')}}
									  	</button>
									  	<input type="file" name="avatar" />
									</div>
			                    </div>
		                    </div>
	                  	</div>
	                </div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.name')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="name" class="form-control" value="{{$user->name}}" required placeholder="{{__('message.name')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.email')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="email" name="email" id="email" class="form-control" value="{{$user->email}}" placeholder="{{__('message.email')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.gender')}}</label>
	      				<div class="col-12 col-md-7">
	      					<select name="gender" id="" class="form-control">
	      						@if($user->gender == "female")
		      						<option value="female" selected="">{{__('message.female')}}</option>
		      						<option value="male">{{__('message.male')}}</option>
		      						<option value="other">{{__('message.other')}}</option>
		      					@elseif($user->gender == "male")
		      						<option value="female">{{__('message.female')}}</option>
		      						<option value="male" selected="">{{__('message.male')}}</option>
		      						<option value="other">{{__('message.other')}}</option>
		      					@else
		      						<option value="female">{{__('message.female')}}</option>
		      						<option value="male">{{__('message.male')}}</option>
		      						<option value="other" selected="">{{__('message.other')}}</option>
		      					@endif
	      					</select>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.phone')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="phone" name="phone" class="form-control" value="{{$user->phone}}" placeholder="{{__('message.phone')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.address')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="address" name="address" class="form-control" value="{{$user->address}}" placeholder="{{__('message.address')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.birthday')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="date" name="birthday" class="form-control" value="{{$user->birthday}}" placeholder="{{__('message.birthday')}}">
	      				</div>
	      			</div>
	                <div class="form-group row">
	                	<label for="" class="control-label col-12 col-md-3">
	                		{{__('message.color')}}
	                	</label>
	                	<div class="col-10 col-md-6">
                          	@include('helpers.color', ['name' => "color", 'value' => $user->color, 'user' => Auth::user()])
	                	</div>
	                	<div class="col-12 col-md-1">
	                		<a class="btn btn-block btn-info btn_random_color">
	                			<i class="fa fa-random"></i>
	                		</a>
	                	</div>
	                </div>
                    <hr>
	                <div class="form-group row text-left">
	                	<div class="col-12 col-md-7 offset-md-3">
	                		<label for="business_hour">
	                          	{{__('message.business_hours_user')}}
	                        </label>
	                        @include('helpers.switch', ['name' => 'business_hour', 'status' => $user->business_hour])
	                	</div>	
	                </div>
	                <hr>
	                <div id="hours" @if($user->business_hour == 'on') style="display:none;" @endif>
	                	<div class="row">
		      				<div class="col-12 col-md-8 offset-md-2">
		      					<div id="hour_table">
				      				@include('admin.user.hour_table')
				      			</div>	
		      				</div>
		      			</div>	
	                </div>
          			<hr>
	                <div class="form-group row text-left">
	                  	<div class="col-12 col-md-7 offset-md-3">
	                    	<label for="access">
                              	{{__('message.send_invitation')}}
                            </label>
                            @include('helpers.switch', ['name' => 'access', 'status' => $user->access])
	                  	</div>
	                </div>
	                <div class="form-group row access_disabled" 
		                @if($user->access == null)
							style="display: none;"
						@endif>
	                  <label for="" class="control-label col-12 col-md-3">{{__('message.password')}}</label>
	                  <div class="col-8 col-md-4">
	                    <input type="password" name="password" id="password" class="form-control">
	                  </div>
	                  <div class="col-4 col-md-3">
	                  	<div class="row">
                  			<div class="col-3">
		                  		<a class="btn btn-primary btn-block" id="view_pass">
		                  			<i class="fa fa-eye"></i>
		                  		</a>
		                  	</div>
		                  	<div class="col-9">
		                  		<a class="btn btn-info btn-block" id="btn_pass">
			                  		<i class="fa fa-key"></i>
			                  		{{__('message.generate')}}
			                  	</a>
		                  	</div>
	                  	</div>
	                  </div>
	                </div>
	                <div class="form-group row access_disabled"
		                @if($user->access == null)
							style="display: none;"
						@endif>
	                  <label for="" class="control-label col-12 col-md-3">{{__('message.type')}}</label>
	                  <div class="col-12 col-md-7">
	                    <select name="type" class="form-control">
	                      	@if($user->type == "admin")
		                      	<option value="standard">
		                      		{{__('message.standard')}}
		                      	</option>
		                      	<option value="admin" selected>
		                      		{{__('message.administrator')}}
		                      	</option>
		                    @else
								<option value="standard" selected>
									{{__('message.standard')}}
								</option>
		                      	<option value="admin">
		                      		{{__('message.administrator')}}
		                      	</option>
		                    @endif
	                    </select>
	                  </div>
	                </div>
	                <hr>
	                <div class="form-group row text-left">
	                  	<div class="col-12 col-md-7 offset-md-3">
	                    	<div class="col-12">
	                    		<label for="online">
	                              	{{__('message.online_reservation')}}
	                            </label>
	                            @include('helpers.switch', ['name' => 'online', 'status' => $user->online])
	                    	</div>
		                </div>
		            </div>
		            <div class="form-group row online_reserve" @if($user->online == null) style="display:none;" @endif>
	                  	<label for="" class="control-label col-12 col-md-3">
	                  		{{__('message.what_service_enabled')}}
	                  	</label>
	                  	<div class="col-12 col-md-7 text-left">
	                    	@foreach($user->business->services as $service)
								@if($service->online == "on")
									<div class="col-12 service">
			                    		@php 
			                    			$flag = "";
			                    		@endphp

			                    		@foreach($user->services as $service_user)
											@if($service->id == $service_user->id)
												@php
													$flag = "on";
												@endphp
											@endif
										@endforeach
										<div class="row">
											<div class="col">
					                    		<label for="service_{{$service->id}}">
					                              	{{$service->name}}
					                            </label>
											</div>
											<div class="col">
			                            		@include('helpers.switch', ['name' => 'service_'.$service->id, 'status' => $flag])	
											</div>
										</div>
			                    	</div>
			                    @endif
	                    	@endforeach
	                  	</div>
	                </div>
	                <hr>
          			<div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					{{__('message.save')}}
	          				</button>
          				</div>
          			</div>
          		</form>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('.btn_random_color').click(function()
			{
				$.get("{{url('user/random/color')}}", function(result)
				{
					$('#color').val(result);
				});
			});

			$('#view_pass').click(function()
			{
				if($('#password').get(0).type == "password")
				{
					$('#password').get(0).type = "text";
				}
				else
				{
					$('#password').get(0).type = "password";
				}
			});

			$('#business_hour').click(function()
			{
				if($(this).prop('checked') == true)
				{
					$('#hours').fadeOut('slow');
				}else
				{
					$('#hours').fadeIn('slow');
				}
			});
			
			$('#btn_pass').click(function()
			{
				var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
				var contraseña = "";
				for (i=0; i<10; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));

				$('#password').val(contraseña);
			});

			$('#access').click(function()
			{	
				if($(this).prop('checked') == true)
				{
					$('#email').prop('required', true);
					$('#password').prop('required', true);
					$(".access_disabled").fadeIn();
				}else
				{
					$('#email').prop('required', false);
					$('#password').prop('required', false);
					$(".access_disabled").fadeOut();
				}
			});

			$('.js-switch.service_all').click(function()
			{				
				if($(this).prop('checked') == true)
				{
					$(".service").fadeOut();
				}else
				{
					$(".service").fadeIn();
				}
			});

			$('#online').click(function()
			{				
				if($(this).prop('checked') == true)
				{
					$(".online_reserve").fadeIn();
				}else
				{
					$(".online_reserve").fadeOut();
				}
			});			
		});
	</script>
@endsection