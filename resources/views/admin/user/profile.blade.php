@extends('layouts.dashboard', ['menu' => 'profile'])

@section('title')
	{{__('message.my_profile')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.my_profile')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('/home')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="card-body">
          		<form action="{{url('profile')}}" method="POST" class="form-horizontal text-right" enctype="multipart/form-data">
          			@csrf
	                <div class="form-group row">
	                  	<label for="" class="control-label col-12 col-md-3">
	                  		{{__('message.avatar')}}
	                  	</label>
	                  	<div class="col-12 col-md-7">
		                    <div class="row">
		                    	<div class="col-8 col-md-9 text-center">
			                    	@if(user()->avatar == null)
			                      		<img src="{{asset('images/profile.png')}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
			                    	@else
			                      		<img src="{{asset(user()->avatar)}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
			                    	@endif
			                    </div>
			                    <div class="col-4 col-md-3">
			                      	@if(user()->avatar == null)
										<a class="btn btn-danger btn-block disabled">
				                        	<i class="fa fa-trash"></i>
				                        	{{__('message.delete_file')}}
				                      	</a>
			                      	@else
										<a href="{{url('user/delete/avatar', user()->id)}}" class="btn btn-danger btn-block">
				                        	<i class="fa fa-trash"></i>
				                        	{{__('message.delete_file')}}
				                      	</a>
			                      	@endif
			                      	<br>
			                      	<div class="upload-btn-wrapper" style="width: 100%;">
									  	<button class="btn btn-info btn-block" style="margin-bottom: 0;">
									  		<i class="fa fa-arrow-up"></i>
									  		{{__('message.upload_file')}}
									  	</button>
									  	<input type="file" name="avatar" />
									</div>
			                    </div>
		                    </div>
	                  	</div>
	                </div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">
	      					{{__('message.name')}}
	      				</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="name" class="form-control" value="{{user()->name}}" required autofocus="" placeholder="{{__('message.name')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">
	      					{{__('message.email')}}
	      				</label>
	      				<div class="col-12 col-md-7">
	      					<input type="email" name="email" class="form-control" value="{{user()->email}}" required="">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">
	      					{{__('message.gender')}}
	      				</label>
	      				<div class="col-12 col-md-7">
	      					<select name="gender" id="" class="form-control">
	      						@if(user()->gender == "female")
		      						<option value="female" selected="">{{__('message.female')}}</option>
		      						<option value="male">{{__('message.male')}}</option>
		      						<option value="other">{{__('message.other')}}</option>
		      					@elseif(user()->gender == "male")
		      						<option value="female">{{__('message.female')}}</option>
		      						<option value="male" selected="">{{__('message.male')}}</option>
		      						<option value="other">{{__('message.other')}}</option>
		      					@else
		      						<option value="female">{{__('message.female')}}</option>
		      						<option value="male">{{__('message.male')}}</option>
		      						<option value="other" selected="">{{__('message.other')}}</option>
		      					@endif
	      					</select>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">
	      					{{__('message.phone')}}
	      				</label>
	      				<div class="col-12 col-md-7">
	      					<input type="phone" name="phone" class="form-control" value="{{user()->phone}}" placeholder="{{__('message.phone')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">
	      					{{__('message.address')}}
	      				</label>
	      				<div class="col-12 col-md-7">
	      					<input type="address" name="address" class="form-control" value="{{user()->address}}" placeholder="{{__('message.address')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">
	      					{{__('message.birthday')}}
	      				</label>
	      				<div class="col-12 col-md-7">
	      					<input type="date" name="birthday" class="form-control" value="{{user()->birthday}}">
	      				</div>
	      			</div>
	                <div class="form-group row">
	                	<label for="" class="control-label col-12 col-md-3">
	                		{{__('message.color')}}
	                	</label>
	                	<div class="col-12 col-md-7">
                          	@include('helpers.color', ['name' => "color", 'value' => user()->color, 'user' => user()])
	                	</div>
	                </div>
          			<hr>
	                <div class="form-group row">
	                	<label for="" class="col-12 text-center">
	                		{{__('message.change_password')}}
	                	</label>
	                  	<label for="" class="control-label col-12 col-md-3">
	                  		{{__('message.new_password')}}
	                  	</label>
	                  	<div class="col-12 col-md-7">
	                    	<input type="password" name="password" id="password" class="form-control" placeholder="{{__('message.new_password')}}">
	                  	</div>
	                </div>
	                <div class="form-group row">
	                  	<label for="" class="control-label col-12 col-md-3">
	                  		{{__('message.repeat_password')}}
	                  	</label>
	                  	<div class="col-12 col-md-7">
	                    	<input type="password" name="password_re" id="password" class="form-control" placeholder="{{__('message.repeat_password')}}">
	                  	</div>
	                </div>
	                <hr>
          			<div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					{{__('message.save')}}
	          				</button>
          				</div>
          			</div>
          		</form>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#view_pass').click(function()
			{
				if($('#password').get(0).type == "password")
				{
					$('#password').get(0).type = "text";
				}
				else
				{
					$('#password').get(0).type = "password";
				}
			});
		});
	</script>
@endsection