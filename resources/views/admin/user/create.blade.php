@extends('layouts.dashboard', ['menu' => 'user_create'])

@section('title')
	{{__('message.user')}}
@endsection

@section('content')

<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3 class="card-title">{{__('message.new_user')}}</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('user')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					{{__('message.back')}}
          				</a>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="card-body text-right">
          		<form action="{{url('user')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
          			@csrf
          			<div class="form-group row">
          				<label for="" class="control label col-12 col-md-3">{{__('message.avatar')}}</label>
          				<div class="col-12 col-md-7 text-left">
          					<div class="upload-btn-wrapper" style="width: 100%;">
                                <button class="btn btn-info" style="margin-bottom: 0; min-width: 240px;">
                                    <i class="fa fa-arrow-up"></i>
                                    {{__('message.upload_file')}}
                                </button>
                                <input type="file" name="avatar" />
                            </div>	
          				</div>
          			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.name')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="name" class="form-control" value="{{old('name')}}" required autofocus="" placeholder="{{__('message.name')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">Email</label>
	      				<div class="col-12 col-md-7">
	      					<input type="email" name="email" id="email" class="form-control" value="{{old('email')}}" placeholder="{{__('message.email')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.gender')}}</label>
	      				<div class="col-12 col-md-7">
	      					<select name="gender" id="" class="form-control">
	      						<option value="female">{{__('message.female')}}</option>
	      						<option value="male">{{__('message.male')}}</option>
	      						<option value="other">{{__('message.other')}}</option>
	      					</select>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.phone')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="phone" name="phone" class="form-control" value="{{old('phone')}}" placeholder="{{__('message.phone')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.address')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="address" name="address" class="form-control" value="{{old('address')}}" placeholder="{{__('message.address')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.birthday')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="date" name="birthday" class="form-control" value="{{old('birthday')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.contact_number')}}</label>
	      				<div class="col-12 col-md-7">
	      					<input type="phone" name="contact_number" class="form-control" value="{{old('contact_number')}}" placeholder="{{__('message.contact_number')}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label col-12 col-md-3">{{__('message.comment')}}</label>
	      				<div class="col-12 col-md-7">
	      					<textarea name="coment" id="" cols="30" rows="4" class="form-control" placeholder="{{__('message.comment')}}"></textarea>
	      				</div>
	      			</div>
	      			<div class="form-group row">
	                	<label for="" class="control-label col-12 col-md-3">
	                		{{__('message.color')}}
	                	</label>
	                	<div class="col-10 col-md-6">
                          	@include('helpers.color', ['name' => "color", 'value' => "", 'user' => Auth::user()])
	                	</div>
	                	<div class="col-12 col-md-1">
	                		<a class="btn btn-block btn-info btn_random_color">
	                			<i class="fa fa-random"></i>
	                		</a>
	                	</div>
	                </div>
	                <hr>
	                <div class="form-group row text-left">
	                  	<div class="col-12 col-md-7 offset-md-3">
	                    	<label for="access">
                              	{{__('message.send_invitation')}}
                            </label>
                            @include('helpers.switch', ['name' => 'access', 'status' => ''])
	                  	</div>
	                </div>
	                <div class="form-group row access_disabled"  style="display: none;">
	                  	<label for="" class="control-label col-12 col-md-3">{{__('message.type')}}</label>
	                  	<div class="col-12 col-md-7 text-left">
	                    	<select name="type" class="form-control">
								<option value="standard">{{__('message.standard')}}</option>
								<option value="admin">{{__('message.administrator')}}</option>
	                    	</select>
	                  	</div>
	                  	<div class="col-12 col-md-7 offset-md-3">
	                  		<small>{{__('message.send_invitation_info')}}</small>
	                  	</div>
	                </div>
	                <hr>
	                <div class="form-group row text-left">
	                  	<div class="col-12 col-md-7 offset-md-3">
	                    	<div class="col-12">
	                    		<div class="row">
	                    			<div class="col">
			                    		<label for="online">
			                              	{{__('message.online_reservation_info')}}
			                            </label>  	
	                    			</div>
	                    			<div class="col">
                            			@include('helpers.switch', ['name' => 'online', 'status' => 'on'])
	                    			</div>
	                    		</div>
	                    	</div>
		                </div>
		            </div>
		            <div class="form-group row online_reserve text-left">
	                  	<div class="col-12 col-md-7 offset-md-3">
		                  	<label for="" class="control-label">
		                  		{{__('message.what_service_enabled')}}
		                  	</label>
		                  	<br><br>
		                </div>
	                  	<div class="col-12 col-md-7 offset-md-3">
	                    	<div class="col-12">
	                    		<div class="row">
	                    			<div class="col">
			                    		<label for="service_all">
			                              	{{__('message.all_services')}}
			                            </label>
	                    			</div>
	                    			<div class="col">
                            			@include('helpers.switch', ['name' => 'service_all', 'status' => 'on'])
	                    			</div>
	                    		</div>
	                    	</div>

	                    	@foreach(Auth::user()->business->services as $service)
								@if($service->online == "on")
									<div class="col-12 service" style="display: none;">
			                    		<div class="row">
			                    			<div class="col">
					                    		<label for="service_{{$service->id}}">
					                              	{{$service->name}}
					                            </label>	
			                    			</div>
			                    			<div class="col">
                            					@include('helpers.switch', ['name' => "service_".$service->id, 'status' => ''])
			                    			</div>
			                    		</div>
			                    	</div>
								@endif
	                    	@endforeach
	                  	</div>
	                </div>
	                <hr>
          			<div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					{{__('message.save')}}
	          				</button>
          				</div>
          			</div>
          		</form>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('.btn_random_color').click(function()
			{
				$.get("{{url('user/random/color')}}", function(result)
				{
					$('#color').val(result);
				});
			});

			$('#business_hour').click(function()
			{
				if($(this).prop('checked') == true)
				{
					$('#hours').fadeOut('slow');
				}else
				{
					$('#hours').fadeIn('slow');
				}
			});

			$('#btn_pass').click(function()
			{
				var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ2346789";
				var contraseña = "";
				for (i=0; i<10; i++) contraseña += caracteres.charAt(Math.floor(Math.random()*caracteres.length));

				$('#password').val(contraseña);
			});

			$('.js-switch.date').click(function()
			{
				var id = $(this).attr('id');
				
				if($(this).prop('checked') == true)
				{
					$("#"+id+"_start").attr('readonly', true);
					$("#"+id+"_end").attr('readonly', true);
				}else
				{
					$("#"+id+"_start").attr('readonly', false);
					$("#"+id+"_end").attr('readonly', false);
				}
			});

			$('#access').click(function()
			{	
				if($(this).prop('checked') == true)
				{
					$('#email').prop('required', true);
					$(".access_disabled").fadeIn();
				}else
				{
					$('#email').prop('required', false);
					$(".access_disabled").fadeOut();
				}
			});

			$('#service_all').click(function()
			{				
				if($(this).prop('checked') == true)
				{
					$(".service").fadeOut();
				}else
				{
					$(".service").fadeIn();
				}
			});

			$('#online').click(function()
			{				
				if($(this).prop('checked') == true)
				{
					$(".online_reserve").fadeIn();
				}else
				{
					$(".online_reserve").fadeOut();
				}
			});			
		});
	</script>
@endsection