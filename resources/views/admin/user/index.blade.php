@extends('layouts.dashboard', ['menu' => 'user'])

@section('title')
    {{__('message.users')}}
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">{{__('message.users')}}</h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('user/create')}}" class="btn btn-info">
                    		<i class="fa fa-plus"></i>
                    		{{__('message.new_user')}}
                    	</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
            	<div class="table table-responsive">
                	<table class="table table-striped">
                		<tr>
                			<th>{{__('message.avatar')}}</th>
                			<th>{{__('message.color')}}</th>
                			<th>{{__('message.name')}}</th>
                			<th>{{__('message.email')}}</th>
                			<th>{{__('message.access_login')}}</th>
                			<th>{{__('message.online_reservation')}}</th>
                			<th>{{__('message.edit')}}</th>
                			<th>{{__('message.delete')}}</th>
                		</tr>
                		@foreach($users->where('type', '!=', 'root') as $user)
							<tr>
								<td>
									@if($user->avatar == null)
										<img src="{{asset('images/profile.png')}}" alt="" width="80px" class="img-circle">
									@else
										<img src="{{asset($user->avatar)}}" alt="" width="80px" class="img-circle">
									@endif
								</td>
								<td>
									<div class="flag-color" style="background: {{$user->color}};">
									</div>
								</td>
								<td>{{$user->name}}</td>
								<td>{{$user->email == null ? "--" : $user->email}}</td>
								<td>
									@if($user->access == null)
										<b>{{__('message.no')}}</b>
									@else
										<b>{{__('message.yes')}}</b>
										@if($user->type == "admin")
											<small> - [{{__('message.administrator')}}]</small>
										@else
											<small> - [{{__('message.standard')}}]</small>
										@endif
									@endif
								</td>
								<td>
									{{$user->online == null ? __('message.no') : __('message.yes')}}
								</td>
								<td>
									<a href="{{url('user', $user->id)}}/edit" class="btn btn-primary">
										<i class="fa fa-edit"></i>
									</a>
								</td>
								<td>
									@if($user->id != Auth::user()->id)
										@include('helpers.delete_modal', ['delete_id' => $user->id, 'delete_route' => url('user', $user->id)])
									@else
										<a href="" class="btn btn-danger disabled">
											<i class="fa fa-trash"></i>
										</a>
									@endif
								</td>
							</tr>
                		@endforeach
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection