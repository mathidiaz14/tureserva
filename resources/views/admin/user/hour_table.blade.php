<div class="form-group col-12">
	<div class="row">
		<div class="col-12 col-md-3 text-center"><b>{{__('message.day')}}</b></div>
		<div class="col-6 col-md-3 text-center"><b>{{__('message.start_hour')}}</b></div>
		<div class="col-6 col-md-3 text-center"><b>{{__('message.end_hour')}}</b></div>
		<div class="col-12 col-md-3 text-center"><b>{{__('message.status')}}</b></div>
	</div>
	<hr>
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'monday')])
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'tuesday')])
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'wednesday')])
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'thursday')])
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'friday')])
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'saturday')])
	@include('admin.user.hour_tr', ['days' => $user->days->where('day', 'sunday')])
</div>

<script>
	$(document).ready(function()
	{   
		$('.btn_delete_day').click(function()
	    {
	    	var id = $(this).attr('attr-id');

	    	$.get("{{url('daysUser/delete')}}/"+id, function(result)
	    	{
	    		if(result)
	    			$('#day_'+id).fadeOut();
	    	});
	    });

	    $('.btn_check_day').on("click change", function()
	    {
	    	var id 		= $(this).attr('attr-id');
	    	var status 	= $(this).attr('attr-status');
	    	var number 	= $(this).attr('attr-number');

	    	if(status == 'on')
	    	{
	    		$("."+id+'_status').attr('readonly', true);
	    		$(this).removeClass('btn-info');
	    		$(this).addClass('btn-dark');
	    		$(this).attr('attr-status', "off");
	    		$(this).html('{{__("message.close")}}');
	    		$('#'+id).attr('checked', true);
	    	}
	    	if(status == 'off')
	    	{
	    		$("."+id+'_status').attr('readonly', false);
	    		$(this).removeClass('btn-dark');
	    		$(this).addClass('btn-info');
	    		$(this).attr('attr-status', 'on');
	    		$(this).html('{{__("message.open")}}');
	    		$('#'+id).attr('checked', false);
	    	}

	    	$.ajax({
			  	url: "{{url('user/set/status')}}",
			  	headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
			  	type: "POST",
	        	dataType: 'json',
			  	data: {
				  	"id"		: number,
				  	"status"	: status,
			  	},
			});
	    });

	    $('.select_hour').on('change', function()
	    {
	    	var id 	= $(this).attr('id');
	    	var val = $(this).find('option:selected').text();

	    	$.ajax({
			  	url: "{{url('user/set/hours')}}",
			  	headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')},
			  	type: "POST",
	        	dataType: 'json',
			  	data: {
				  	"id"	: id,
				  	"val"	: val,
			  	},
			});
	    });
	});
</script>