<div class="modal-header" style="margin-bottom: 20px;">
	<div class="col-10">
		<p>
			<b>{{$task->title}}</b>
			<b>({{$task->start->format('d/m/Y')}} de {{$task->start->format('H:i')}} a {{$task->end->format('H:i')}})</b>
		</p>
	</div>
	<div class="col-2 text-right">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  		<span aria-hidden="true">&times;</span>
	</button>
	</div>
</div>
<div class="modal-body">
	@if($task->status != "enabled")
		<div class="row" style="margin-bottom: 20px;">
			<div class="col-12 text-center">
				<p>
					<b style="color:red;">
						@if($task->status == "faild") 
							[Fallida] 
						@elseif($task->status == "cancel") 
							[Cancelada]
						@endif
					</b>
				</p>
			</div>
		</div>
	@endif
	@if(($task->client_name == null) and ($task->services->count() == 0) and ($task->users->count() == 0) and ($task->resources->count() == 0))
		<div class="row text-secondary">
			<div class="col-12 text-center">
				<br>
				<i class="fa fa-exclamation-triangle fa-2x"></i>
				<br><br>
				<p>{{__('errors.not_more_data')}}</p>
				<br>
			</div>
		</div>
	@endif

	@if($task->client_name != null)
		<div class="row">
			<div class="col-12">
				Cliente:
			</div>
			<div class="col-3 text-center py-1">
				<b  class="logo_task" style="background: #00c7ff;">{{LogoLetter($task->client_name)}}</b>
			</div>
			<div class="col-9">
				@if($task->client_id != null)
					<a href="{{url('client', $task->client_id)}}" target="_blank">
						<p>
							<b>{{$task->client_name}} <small><i class="fas fa-external-link-alt"></i></small></b>
						</p>
					</a>
					<p style="margin-left:20px;">
						@if($task->client->phone != null)
							<b>
								<small><i class="fas fa-arrow-right"></i></small> {{$task->client->phone}}<br>
							</b>
						@endif

						@if(($task->client->birthday != null) and ($task->client->birthday->format('m-d') == Carbon\Carbon::now()->format('m-d')))
							<b class="text-success">
								<small><i class="fas fa-arrow-right"></i></small> {{__('message.happy_birthday')}}
							</b>
						@endif
					</p>
				@else
					<p>
						<b>{{$task->client_name}}</b>
					</p>
				@endif
			</div>	
		</div>
	@endif

	@if($task->services->count() > 0)
		<div class="row">
			<div class="col-12">
				Servicios:
			</div>
			@foreach($task->services as $service)
				<div class="col-3 text-center py-1">
					<b class="logo_task" style="background: #ffa100;">{{LogoLetter($service->name)}}</b>
				</div>
				<div class="col-9">
					<p>
						<b>{{$service->name}}</b>
					</p>
				</div>	
			@endforeach
		</div>
	@endif

	
	@if($task->users->count() > 0)
		<div class="row">
			<div class="col-12">
				Empleados:
			</div>
			@foreach($task->users as $user)
					<div class="col-3 text-center py-1" >
						@if($user->avatar == null)
							<b class="logo_task" style="background: {{$user->color}};">
								{{LogoLetter($user->name)}}
							</b>
						@else
							<img src="{{asset($user->avatar)}}" alt="" class="logo_task_user">
						@endif
					</div>
					<div class="col-9">
						<b>{{$user->name}}</b>
					</div>
			@endforeach
		</div>	
	@endif

	@if($task->resources->count() > 0)
		<div class="row">
			<div class="col-12">
				Recursos:
			</div>
			@foreach($task->resources as $resource)
				<div class="col-3 text-center py-1">
					<b class="logo_task" style="background: #ffa100;">{{LogoLetter($resource->name)}}</b>
				</div>
				<div class="col-9">
					<p>
						<b>{{$resource->name}}</b>
					</p>
				</div>	
			@endforeach
		</div>
	@endif
	@if($task->code != null)
		<div class="row">
			<div class="col-12">
				<p>Código reserva: <b>{{$task->code}}</b></p>
			</div>
		</div>
	@endif

	@if($task->status == "pending")
		<div class="row">
			<hr>
            <div class="col-6 text-center">
                <!-- Boton de aceptar -->
                <form id="check-form" action="{{url('accept/calendar')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$task->id}}">
                    <button class="btn btn-success btn-block">
                    	<i class="fa fa-check"></i>
                    	{{__('message.approve')}}
                    </button>
                </form>

            </div>
            <div class="col-6 text-center">
                <!-- Boton de rechazar -->
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#refuseModal_{{$task->id}}">
                    <i class="fa fa-close"></i>
                    {{__('message.refuse')}}
                </button>

                <!-- Modal de rechazar -->
                <div class="modal fade" id="refuseModal_{{$task->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" style="width: 90%;">
                        <div class="modal-content" style="color:#73879C;">
                            <div class="modal-header">
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <h5 class="modal-title" id="exampleModalLabel">Rechazar cita</h5>        
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>  
                                    </div>
                                </div>
                            </div>
                            <div class="modal-body">
                                <form action="{{url('refuse/calendar')}}" method="post" class="form-horizontal">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$task->id}}">
                                    <div class="form-group text-left">
                                        <label for="" class="form-label">Dejale un comentario a tu cliente</label>
                                        <textarea name="comment" cols="30" rows="3" class="form-control" id="comment_{{$task->id}}" required=""></textarea>
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-primary">
                                            <i class="fa fa-paper-plane"></i>
                                            {{__('message.send')}}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
			    $('#refuseModal_{{$task->id}}').on('shown.bs.modal', function () 
			    {
			        $('#comment_{{$task->id}}').trigger('focus')
			    });
			</script>
        </div>
	@endif
	<hr>
	<div class="row">
		<div class="col-6 text-left">
			@if(\Carbon\Carbon::now()->gte($task->end))
				<a class="btn btn-info btn_faild_task" attr-id="{{$task->id}}">
					@if($task->status == "faild")
						<i class="fa fa-check"></i>
						{{__('message.task_confirm')}}
					@else
						<i class="fa fa-times"></i>
						{{__('message.task_failed')}}
					@endif
				</a>
			@endif		
		</div>	
		<div class="col-6 text-right">
			<a class="btn btn-info btn_edit_task" attr-task-id="{{$task->id}}">
				<i class="fa fa-edit"></i>
				{{__('message.edit')}}
			</a>
		</div>
	</div>
</div>