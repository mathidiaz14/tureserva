<div class="modal-header bg-gradient-primary">
	<div class="col-6 text-left">
		<p>
			<b>{{__('message.edit_task')}}</b>
		</p>
	</div>
	<div class="col-6 text-right">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      		<span aria-hidden="true">&times;</span>
    	</button>
	</div>
</div>

<!------------------------------------------------------->
<!-- Modal de editar tarea -->
<!------------------------------------------------------->

<div class="modal-body text-left">
	<div class="row">
		<div class="col-12">
			<!-- Titulo -->
	    	<div class="form-group">
	    		<div class="row">
	    			<div class="col-12">
	        			<label for="" class="control-label">{{__('message.title')}}</label>
						<input id="title" class="form-control" placeholder="{{__('message.title')}}" autofocus="" value="{{$task->title}}">
	    			</div>
	    		</div>
	    	</div>
			<!-- Fin titulo -->

	    	<div class="form-group">
	    		<div class="row">
					
					<!-- Clientes -->
	    			<div class="col-6">
	    				<label for="" class="control-label">{{__('message.client')}}</label>
	    				<input list="clients" id="client_name" class="form-control" placeholder="{{__('message.client_name')}}" value="{{$task->client_name}}">
					    <input type="hidden" name="client" id="client" value="{{$task->client_id}}">
						
						<datalist id="clients" style="width: 100px;">
						 	@foreach(Auth::user()->business->clients->sortBy('name') as $client)
						  		<option value="{{$client->name}}" data-id="{{$client->id}}">
						  		</option>
						  	@endforeach
						</datalist>
	    			</div>
	    		
	    			<!-- Servicios -->
	    			<div class="col-6">
	    				<label for="" class="control-label">{{__('message.services')}}</label>	
	        			<select class="form-control select2" multiple="multiple" id="services" style="width: 100%;">
	        				@foreach($task->services as $service)
	        					<option value="{{$service->name}}">{{$service->name}}</option>
	        				@endforeach
	        			</select>
	    			</div>
	    		</div>
	    	</div>
			<!-- Fin Servicios -->

			<hr>
	    	<div class="form-group">
	    		<div class="row">
					<!-- Usuarios -->
	    			<div class="col-6">
	    				<label for="" class="control-label">{{__('message.employees')}}</label>	
	        			<select class="form-control select2" multiple="multiple" id="employees" style="width: 100%;">
	        				@foreach($task->users as $user)
	        					<option value="{{$user->name}}">{{$user->name}}</option>
	        				@endforeach
	        			</select>
	    			</div>
	    	
	    			<!-- Recursos -->
	    			<div class="col-6">
	    				<label for="" class="control-label">{{__('message.resources')}}</label>	
	        			<select class="form-control select2" multiple="multiple" id="resources" style="width: 100%;">
	        				@foreach($task->resources as $resource)
	        					<option value="{{$resource->name}}">{{$resource->name}}</option>
	        				@endforeach
	        			</select>
	    			</div>
	    		</div>
	    	</div>
	    	
	    	<div class="form-group">
	    		<div class="row">
	    			<div class="col-12">
	    				<label for="" class="control-label">{{__('message.comment')}}</label>	
	    				<textarea name="description" id="description" cols="30" rows="2" class="form-control" placeholder="{{__('message.comment')}}" style="resize: none;">{{$task->comment}}</textarea>
	    			</div>
	    		</div>
	    	</div>
	    	<!-- Enviar mensaje -->
			<div class="form-group">
				<div class="row">
					<div class="col">
						<label for="sms_edit">
							{{__('message.sms_reminder')}}
						</label>	
					</div>
					<div class="col">
						@include('helpers.switch', ['name' => 'sms_edit', 'status' => $task->sms])
					</div>
				</div>
			</div>
			<!-- Fin mensaje -->
	    	<hr>
	    	<div class="form-group">
				<div class="row">
    				<div class="col-6">
    					<label for="" class="control-label">{{__('message.starts')}}</label>
						<div class="row">
							<div class="col-12 mb-2">
		        				<input type="date" class="form-control" name="start_date" id="start_date" value="{{$task->start->format('Y-m-d')}}">
							</div>
							<div class="col-12">
								<input type="time" class="form-control" name="start_hour" id="start_hour" value="{{$task->start->format('H:i')}}">
							</div>
						</div>
					</div>

	    			<div class="col-6">
	    				<label for="" class="control-label">{{__('message.ends')}}</label>	
	        			<div class="row">
	        				<div class="col-12 mb-2">
		        				<input type="date" class="form-control" id="end_date" name="end_date" value="{{$task->end->format('Y-m-d')}}">
		        			</div>
		        			<div class="col-12">
							   <input type="time" class="form-control" name="end_hour" id="end_hour" value="{{$task->end->format('H:i')}}">
		        			</div>
	        			</div>
					</div>
				</div>	
			</div>
			<hr>
			<div class="form-group">
				<div class="row">
					<div class="col-6">
						<a class="btn btn-outline-danger btn-block btn_delete_task" style="color:red;" attr-id="{{$task->id}}" 
							@if((Carbon\Carbon::now()->lt($task->end)) and ($task->client_name != null))
								attr-status="true"
							@else
								attr-status="false"
							@endif
							>
							<i class="fa fa-trash"></i>
							{{__('message.delete')}}
						</a>
					</div>
					<div class="col-6">
						<a class="btn btn_edit btn-block" style="background: #383434; color:white;" attr-id="{{$task->id}}">
							<i class="fa fa-save"></i>
							{{__('message.save')}}
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function()
	{
		$('#modal_edit #employees').select2(
        {
            data: [
            	@foreach(business()->users as $user)
            		"{{$user->name}}",
            	@endforeach
            ],
            tags: true,
            tokenSeparators: [',', ' '],
            language: "es",
            placeholder: "{{__('message.employees')}}"
        });

        //Cargo los empleados que estan en la tarea

    	$('#modal_edit #employees').val([
    		@foreach($task->users as $user)
    			"{{$user->name}}",
    		@endforeach
    		]
		).trigger('change');

        $('#modal_edit #employees').on('select2:opening select2:closing', function( event ) 
        {
		    var $searchfield = $(this).parent().find('.select2-search__field');
		    $searchfield.prop('disabled', true);
		});

        //***********************************************

        $('#modal_edit #resources').select2(
        {
            data: [
            	@foreach(business()->resources as $resource)
            		"{{$resource->name}}",
            	@endforeach
            ],
            tags: true,
            tokenSeparators: [',', ' '],
            language: "es",
            placeholder: "{{__('message.resources')}}"
        });

		$('#modal_edit #resources').on('select2:opening select2:closing', function( event ) 
		{
		    var $searchfield = $(this).parent().find('.select2-search__field');
		    $searchfield.prop('disabled', true);
		});

		//Cargo los recursos que estan en la tarea

    	$('#modal_edit #resources').val([
    		@foreach($task->resources as $resource)
    			"{{$resource->name}}",
    		@endforeach
    		]
		).trigger('change');

		//***********************************************
        
        $('#modal_edit #services').select2(
        {
            data: [
            	@foreach(business()->services as $service)
            		"{{$service->name}}",
            	@endforeach
            ],
            tags: true,
            tokenSeparators: [',', ' '],
            language: "es",
            placeholder: "{{__('message.services')}}"
        });

		$('#modal_edit #services').on('select2:opening select2:closing', function( event ) 
		{
		    var $searchfield = $(this).parent().find('.select2-search__field');
		    $searchfield.prop('disabled', true);
		});

		//Cargo los recursos que estan en la tarea

    	$('#modal_edit #services').val([
    		@foreach($task->services as $service)
    			"{{$service->name}}",
    		@endforeach
    		]
		).trigger('change');
	});
</script>