<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#calendarOptions">
    <i class="fa fa-cogs"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="calendarOptions" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content text-left">
            <div class="modal-header bg-gradient-info">
                <div class="col-9">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <i class="fa fa-cogs mr-2"></i>
                        {{__('message.calendar_options')}}
                    </h5>
                </div>
                <div class="col-3 text-right">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                      <span aria-hidden="true" style="color:red;">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <form action="{{url('calendar_option', Auth::user()->calendar_options->id)}}" method="post" class="form-horizontal">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="" class="control-label">
                                    {{__('message.interval_minutes')}}
                                </label>
                                
                                <select name="slots" id="" class="form-control">
                                    @if(user()->calendar_options->slots == "00:10")
                                        <option value="00:10" selected>10 Min</option>
                                    @else
                                        <option value="00:10">10 Min</option>
                                    @endif

                                    @if(user()->calendar_options->slots == "00:15")
                                        <option value="00:15" selected>15 Min</option>
                                    @else
                                        <option value="00:15">15 Min</option>
                                    @endif

                                    @if(user()->calendar_options->slots == "00:30")
                                        <option value="00:30" selected>30 Min</option>
                                    @else
                                        <option value="00:30">30 Min</option>
                                    @endif

                                    @if(user()->calendar_options->slots == "01:00")
                                        <option value="01:00" selected>60 Min</option>
                                    @else
                                        <option value="01:00">60 Min</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="" class="control-label">Vista por defecto</label>
                                <select name="default_view" id="" class="form-control">
                                    @if(Auth::user()->calendar_options->default_view == "timeGridWeek")
                                        <option value="timeGridWeek" selected="">
                                            {{__('message.week')}}
                                        </option>
                                    @else
                                        <option value="timeGridWeek">   
                                            {{__('message.week')}}
                                        </option>
                                    @endif

                                    @if(Auth::user()->calendar_options->default_view == "timeGridDay")
                                        <option value="timeGridDay" selected="">
                                            {{__('message.day')}}
                                        </option>
                                    @else
                                        <option value="timeGridDay">
                                            {{__('message.day')}}
                                        </option>
                                    @endif
                                    
                                    @if(Auth::user()->calendar_options->default_view == "listWeek")
                                        <option value="listWeek" selected="">
                                            {{__('message.calendar')}} 
                                        </option>
                                    @else
                                        <option value="listWeek">
                                            {{__('message.calendar')}} 
                                        </option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <br>
                                <label for="business_hours">
                                    {{__('message.business_hours')}}
                                </label>
                                
                                @include('helpers.switch', ['name' => 'business_hour', 'status' => user()->calendar_options->business_hours])
                            </div>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                        <div class="col-6 text-left">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <i class="fa fa-chevron-left"></i>
                                {{__('message.back')}}
                            </button>
                        </div>
                        <div class="col-6">
                            <div class="form-group text-right">
                                <button class="btn btn-info">
                                    <i class="fa fa-save"></i>
                                    {{__('message.save')}}
                                </button>
                            </div>
                        </div>
                    </div>  
                </form>
            </div>
        </div>
    </div>
</div>