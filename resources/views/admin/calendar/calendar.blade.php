
<div class="col-12" id='calendar'></div>

<script>

	//Inicia funcion de FullCalendar
	$(document).ready(function()
	{
		var calendarEl = document.getElementById('calendar');

	    var calendar = new FullCalendar.Calendar(calendarEl, {
	    	contentHeight: "auto",
	    	editable: false,
	      	plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ],
	      	header: {
		        left: 'prev,next today',
		        center: 'title',
		        right: 'timeGridWeek,timeGridDay,listWeek'
	      	},
	      	
			defaultView: $(window).width() < 765 ? 'timeGridDay':'{{Auth::user()->calendar_options->default_view}}',
			defaultDate: '{{Date::today()->format("Y-m-d")}}',
			locale: 'es',
			navLinks: true,
			selectable: true,
			selectMirror: true,
			slotLabelFormat: [
	        {
	            hour: '2-digit',
	            minute: '2-digit',
	        }],
			slotDuration: @if(Auth::user()->calendar_options->slots == "00:00") '00:05' @else '{{Auth::user()->calendar_options->slots}}' @endif,

	      	@if(Auth::user()->calendar_options->business_hours == "on" )
	      		businessHours: [ 
					
	      			@php
	      				if($user != null)
	      					$data = $user;
	      				else
	      					$data = business();
	      			@endphp

					@foreach($data->days->where('day', 'sunday') as $day)
						{
						daysOfWeek: [ 0 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
					@foreach($data->days->where('day', 'monday') as $day)
						{
						daysOfWeek: [ 1 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
					@foreach($data->days->where('day', 'tuesday') as $day)
						{
						daysOfWeek: [ 2 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
					@foreach($data->days->where('day', 'wednesday') as $day)
						{
						daysOfWeek: [ 3 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
					@foreach($data->days->where('day', 'thursday') as $day)
						{
						daysOfWeek: [ 4 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
					@foreach($data->days->where('day', 'friday') as $day)
						{
						daysOfWeek: [ 5 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
					@foreach($data->days->where('day', 'saturday') as $day)
						{
						daysOfWeek: [ 6 ], 
							@if($day->status != "off")
								startTime: '{{$day->start}}',
								endTime: '{{$day->end}}',
							@else
								startTime: '00:00',
								endTime: '00:00',
							@endif 
						},
					@endforeach
				],
	      	@endif
	      	
	      	select: function(arg) 
	      	{
	      		//Funciones al hacer click en un espacio en blanco del calendario
	      		//Abro el modal de nuevo evento y formateo todos los valores del mismo

	      		var tasks = parseInt("{{business()->tasks->wherebetween('created_at', [\Carbon\Carbon::now()->startOfMonth(), Carbon\Carbon::now()->endOfMonth()])->count()}}");

	      		if(("{{business()->plan}}" == "plan1") && (tasks >= 30))
	      		{
	      			tureserva_notification_message("{{__('errors.limit_calendar_plan1')}}", 'danger');
	      		}else if(("{{business()->plan}}" == "plan2") && (tasks >= 100))
	      		{
	      			tureserva_notification_message("{{__('errors.limit_calendar_plan2')}}", 'danger');
	      		}else
	      		{
	      			$('.loading').fadeIn();

	      			$('#add_event #title').val("");
					$('#add_event #client').val("");
					$('#add_event #client_name').val("");
					$("#add_event #services").val(null).trigger('change');
					$("#add_event #employees").val(null).trigger('change');
					$("#add_event #resources").val(null).trigger('change');
	            	$('#add_event #description').val("");
	            	$('.error_client_data').hide();
	            	$('.success_client_create').hide();
		      		
		      		var start		= arg.startStr.split("T");
		      		var startHour 	= start[1].split(":");

		      		var end			= arg.endStr.split("T");
		      		var endHour 	= end[1].split(":");

		      		$('#add_event #start_date').val(start[0]);
		      		$('#add_event #start_hour').val(startHour[0]+":"+startHour[1]);

		      		$('#add_event #end_date').val(end[0]);
		      		$('#add_event #end_hour').val(endHour[0]+":"+endHour[1]);

					$('.form_add_client').hide();
					$('.form_add_task').show();
			    	$('#add_event').modal({keyboard: true});

			    	$('.loading').fadeOut();
	      		}
	      	},
	      	allDaySlot: false,
	      	nowIndicator: true,
			
			eventLimit: true,
			timeZone: 'UTC-3',
			events: [
				//Cargo todos los eventos del calendario
		        @foreach ($tasks as $task)
		        	{
		        		id: '{{$task->id}}',
		        		@if($task->title != null)
							title: '{{$task->title}}',
						@else
		        			@if($task->client_name != null)
		        				title: '{{$task->client_name}}',
		        			@endif
						@endif
						start: '{{$task->start}}',
						end: '{{$task->end}}',
						status: '{{$task->status}}',
						color: '{{$task->color}}',
						classNames: 'event',
						@if(($task->status == 'faild') or ($task->status == 'cancel'))
							classNames: "event_faild",
						@elseif($task->status == 'pending')
							classNames: "event_pending",
						@endif


			        },
		        @endforeach
	      	],
	      	eventClick:function(info)
	      	{
	      		//Eventos a realizar cuando hago click en un evento guardado

	      		//al dar click en un evento, consulto los datos de la base de datos.
	      		$('#modal_view_content').html("");
	      		
	      		$('.loading').fadeIn();
	      		
	      		$('#modal_view_content').load("{{url('calendar')}}/"+info.event.id, function()
      			{
		      		$('#modal_view').modal({keyboard: true});
		      		$('.loading').fadeOut();
      			});
	      		
	      	},

	    });

    	calendar.render();

    	//Eventos de agregar tarea al calendario

		//autofoco en titulo
		$(document).on('show.bs.modal','#add_event', function () {
			$('#add_event #title').focus();
		});

		$(document).on('change', '#add_event #service_name', function() 
		{
			var val 		= $('#add_event #service_name').val()
		    
		    var duration 	= $('#add_event #services option').filter(function() 
		    {
		        return this.value == val;
		    }).data('duration');

			
			var start 		= $('#add_event #start_hour').val();
			var end 		= moment.utc(start,'hh:mm').add(duration,'minutes').format('HH:mm');

		    var id 			= $('#add_event #services option').filter(function() 
		    {
		        return this.value == val;
		    }).data('id');

			$('#add_event #service').val(id);
			$('#add_event #end_hour').val(end);
		});

		$(document).on('change', '#add_event #client_name', function() 
		{
			var val 	= $('#add_event #client_name').val()
		    var id 		= $('#add_event #clients option').filter(function() 
		    {
		        return this.value == val;
		    }).data('id')

			$('#add_event #client').val(id);
		});

		$('#add_event .btn_save').click(function()
		{
			var sms = null;

			if($('#sms').is(':checked'))
				sms = "on";

			$('.landing').fadeIn();
			$.ajax({
				type:'POST',
				url:"{{url('calendar')}}",
				data:{
					title: 			$('#add_event #title').val(),
					client: 		$('#add_event #client').val(),
					client_name: 	$('#add_event #client_name').val(),
					services: 		$('#add_event #services').val(),
					sms: 			sms,
					employees: 		$('#add_event #employees').val(),
                	resources: 		$('#add_event #resources').val(),
                	description: 	$('#add_event #description').val(),
                	start_date: 	$('#add_event #start_date').val(),
                	start_hour: 	$('#add_event #start_hour').val(),
                	end_date: 		$('#add_event #end_date').val(),
                	end_hour: 		$('#add_event #end_hour').val(),
				},
	        }).done(function(result)
	        {
				$('#add_event').modal('toggle');

				var event = {
					id: result['id'],
					title: result['title'],
					start: result['start'],
					end: result['end'],
					color: result['color'],
					classNames: 'event',
					status: 'enabled',
				};

				calendar.addEvent(event);

	        	tureserva_notification_message("{{__('message.success_task_create')}}", 'success');
				$('.landing').fadeOut();
	        });
    	});
        
        $('#add_event #employees').select2(
        {
            data: [
            	@foreach(business()->users as $user)
            		"{{$user->name}}",
            	@endforeach
            ],
            tags: true,
            tokenSeparators: [',', ' '],
            language: "es",
            placeholder: "{{__('message.employees')}}"
        });

        $('#add_event #employees').on('select2:opening select2:closing', function( event ) 
        {
		    var $searchfield = $(this).parent().find('.select2-search__field');
		    $searchfield.prop('disabled', true);
		});

        $('#add_event #resources').select2(
        {
            data: [
            	@foreach(business()->resources as $resource)
            		"{{$resource->name}}",
            	@endforeach
            ],
            tags: true,
            tokenSeparators: [',', ' '],
            language: "es",
            placeholder: "{{__('message.resources')}}"
        });

		$('#add_event #resources').on('select2:opening select2:closing', function( event ) 
		{
		    var $searchfield = $(this).parent().find('.select2-search__field');
		    $searchfield.prop('disabled', true);
		});

		$('#add_event #services').select2(
        {
            data: [
            	@foreach(business()->services as $service)
            		"{{$service->name}}",
            	@endforeach
            ],
            tags: true,
            tokenSeparators: [',', ' '],
            language: "es",
            placeholder: "{{__('message.services')}}"
        });

        $('#add_event #services').on('select2:opening select2:closing', function( event ) 
        {
		    var $searchfield = $(this).parent().find('.select2-search__field');
		    $searchfield.prop('disabled', true);
		});

		//jquery de agregar cliente

		$('#add_event .btn_new_client').click(function()
		{
			$('.form_add_task').hide();
			$('.form_add_client').fadeIn();
			$('#new_client_name').focus();
		});

		$('.form_add_client .btn_new_client_back').click(function()
		{
			$('.form_add_client').hide();
			$('.form_add_task').fadeIn();
		});

		$('.form_add_client .btn_save_client').click(function()
		{
			if(($('#new_client_name').val() == "") || ($('#new_client_phone').val() == ""))
			{
				tureserva_notification_message("{{__('errors.client_create_error')}}", "danger");
			}else
			{
				$('.loading').fadeIn();

				$.ajax({
					type:'POST',
					url:"{{url('client/create/jquery')}}",
					data:{
						name: $('#new_client_name').val(),
						phone: $('#new_client_phone').val(),
						email: $('#new_client_email').val(),
						address: $('#new_client_address').val(),
						description: $('#new_client_comment').val(),
					},
		        }).done(function(response)
		        {
					$('#add_event #client').val(response);
					$('#add_event #client_name').val($('#new_client_name').val());
					
					$('.loading').fadeOut();
					
					tureserva_notification_message("{{__('message.success_client_create')}}", "success");
					
					$('.form_add_client').hide();
					$('.form_add_task').fadeIn();
		        });
			}
		});
	    
    	//btn de confirmacion al momento de eliminar el evento
		$('.btn_delete_confirmation').click(function()
		{
			$('.loading').fadeIn();
			
			var id = $(this).attr('attr-delete-id');
			
			//elimino el evento de la DB
			$.ajax({
				type:'POST',
				url:"{{url('calendar')}}/"+id,
				data:{
					_method : "DELETE",
					message : $('#message_delete').val(),
				},
	        }).done(function()
	        {
	        	//Muestro un mensaje
				tureserva_notification_message("{{__('message.success_task_delete')}}", "success");

				//cierro los modals
				$('#modal_delete_confirmation').modal('toggle');
				$('#modal_edit').modal('toggle');

				//elimino el evento del calendario
				var event = calendar.getEventById(id);
		        event.remove();

	        	$('.loading').fadeOut();
	        });
		});


		//Eventos de la vista view

		$(document).on('click', '.btn_edit_task', function()
		{
			var id = $(this).attr('attr-task-id');

			$('.loading').fadeIn();

			$('#modal_edit_content').load("{{url('calendar')}}/"+id+"/edit", function()
			{	
				$('#modal_view').modal('toggle');
				$('#modal_edit').modal({keyboard: true});
				$('.loading').fadeOut();
			});

		});

		$(document).on('click', '.btn_faild_task', function()
		{
			$('.loading').fadeIn();

			var id = $(this).attr('attr-id');

			$.get('faild/calendar/'+id, function(result)
			{
				var event = calendar.getEventById(id);
		        event.remove();

		        if(result['status'] == "enabled")
		        	var status = "";
		        else
		        	var status = "event_faild";

		        var event = {
					id: 		result['id'],
					title: 		result['title'],
					start: 		result['start'],
					end: 		result['end'],
					color: 		result['color'],
					classNames: 'event',
					classNames: status,
				};

				calendar.addEvent(event);

		        $('.loading').fadeOut();
		        $('#modal_view').modal('toggle');
			});
		});

		//Eventos del modal edit

		//autofoco en titulo
		$(document).on('show.bs.modal','#modal_edit', function () {
			$('#modal_edit #title').focus();
		});

		$(document).on('change', '#modal_edit #service_name', function() 
		{
			var val 		= $('#modal_edit #service_name').val()
		    
		    var duration 	= $('#modal_edit #services option').filter(function() 
		    {
		        return this.value == val;
		    }).data('duration');

			
			var start 		= $('#modal_edit #start_hour').val();
			var end 		= moment.utc(start,'hh:mm').add(duration,'minutes').format('HH:mm');

		    var id 			= $('#modal_edit #services option').filter(function() 
		    {
		        return this.value == val;
		    }).data('id');

			$('#modal_edit #service').val(id);
			$('#modal_edit #end_hour').val(end);
		});

		$(document).on('change', '#modal_edit #client_name', function() 
		{
			var val 	= $('#modal_edit #client_name').val()
		    var id 		= $('#modal_edit #clients option').filter(function() 
		    {
		        return this.value == val;
		    }).data('id')

			$('#modal_edit #client').val(id);
		});

		$(document).on('click', '#modal_edit .btn_edit', function()
		{
			var start 	= $('#modal_edit_content #start_hour').val().replace(/-/g,'/');
			var end 	= $('#modal_edit_content #end_hour').val().replace(/-/g,'/');

			if(end <= start)
			{
				tureserva_notification_message("La hora final no puede ser mayor que la hora inicial", "danger");
			}else
			{
				var sms = null;
				if($('#modal_edit #sms_edit').is(':checked'))
					sms = "on";

				$('.loading').fadeIn();

				var id = $(this).attr('attr-id');

				$.ajax({
					type:'POST',
					url:"{{url('calendar')}}/"+id,
					data:{
						_method:'PATCH',
						title: 			$('#modal_edit #title').val(),
						client: 		$('#modal_edit #client').val(),
						client_name: 	$('#modal_edit #client_name').val(),
						services: 		$('#modal_edit #services').val(),
						service_name: 	$('#modal_edit #service_name').val(),
						sms: 			sms,
						employees: 		$('#modal_edit #employees').val(),
	                	resources: 		$('#modal_edit #resources').val(),
	                	description: 	$('#modal_edit #description').val(),
	                	start_date: 	$('#modal_edit #start_date').val(),
	                	start_hour: 	$('#modal_edit #start_hour').val(),
	                	end_date: 		$('#modal_edit #end_date').val(),
	                	end_hour: 		$('#modal_edit #end_hour').val(),
					},
		        }).done(function(result)
		        {
		        	tureserva_notification_message("{{__('message.success_task_edit')}}", 'success');
					
					var event = calendar.getEventById(id);
			        event.remove();
					
					if(result['status'] == "enabled")
			        	var status = "";
			        else
			        	var status = "event_faild";

					var event2 = {
						id: result['id'],
						title: result['title'],
						start: result['start'],
						end: result['end'],
						color: result['color'],
						classNames: 'event',
						classNames: status,
					};

					calendar.addEvent(event2);

					$('#modal_edit').modal('toggle');
		        	$('.loading').fadeOut();
					
		        });
		    }
    	});

        $(document).on('click', '.btn_delete_task', function()
        {
        	var id 		= $(this).attr('attr-id');
        	var status 	= $(this).attr('attr-status');

        	if(status == "true")
        		$('#modal_delete_confirmation #delete_message_client').show();
        	else
        		$('#modal_delete_confirmation #delete_message_client').hide();
        	

        	$('#modal_delete_confirmation .btn_delete_confirmation').attr('attr-delete-id', id);
        	$('#modal_delete_confirmation').modal({keyboard:true});
        	$('#modal_delete_confirmation #message_delete').focus();
        });
	});

</script>