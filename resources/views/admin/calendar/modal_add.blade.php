<div class="modal fade" id="add_event" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-gradient-info">
				<p class="modal-title">
					<b>{{__('message.new_task')}}</b>
				</p>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				 	<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body form_add_task text-left">
	      		<div class="row">
					<!-- Titulo -->
        			<div class="col-12">
			        	<div class="form-group">
		        			<label for="" class="control-label">{{__('message.title')}}</label>
        					<input id="title" class="form-control" placeholder="{{__('message.title')}}" autofocus="">
	        			</div>
		        	</div>
					<!-- Fin titulo -->

					<!-- Clientes -->
        			<div class="col-6">
		        		<div class="form-group">
	        				<label for="" class="control-label">{{__('message.client')}}</label>

	        				<input list="clients" id="client_name" class="form-control" placeholder="{{__('message.client_name')}}">
						    <input type="hidden" name="client" id="client">
							
							<datalist id="clients" style="width: 100px;">
							 	@foreach(Auth::user()->business->clients->sortBy('name') as $client)
							  		<option value="{{$client->name}}" data-id="{{$client->id}}">
							  			{{$client->phone != null ? $client->phone."-" : ""}}  {{$client->email}}
							  		</option>
							  	@endforeach
							</datalist>
	        			</div>
	        		</div>

        			<!-- Servicios -->
        			<div class="col-6">
        				<div class="form-group">
	        				<label for="" class="control-label">{{__('message.services')}}</label>

	            			<select class="form-control select2" multiple="multiple" id="services" style="width: 100%;"></select>
			        	</div>
        			</div>

        			<div class="col-12">
						<hr>
        			</div>
					
					<!-- Usuarios -->
        			<div class="col-6">
			        	<div class="form-group">
	        				<label for="" class="control-label">{{__('message.employees')}}</label>

	            			<select class="form-control select2" multiple="multiple" id="employees" style="width: 100%;"></select>
			        	</div>
        			</div>

        			<!-- Recursos -->
        			<div class="col-6">
        				<div class="form-group">
	        				<label for="" class="control-label">{{__('message.resources')}}</label>
	        			
	            			<select class="form-control select2" multiple="multiple" id="resources" style="width: 100%;"></select>
        				</div>
        			</div>
		        	<!-- Fin Recursos -->
			        	
        			<div class="col-12">
		        		<div class="form-group">
	        				<label for="" class="control-label">{{__('message.comment')}}</label>
	        			
	        				<textarea name="description" id="description" cols="30" rows="2" class="form-control" placeholder="{{__('message.comment')}}" style="resize: none;"></textarea>
	        			</div>
		        	</div>
		        	
		        	<!-- Enviar mensaje -->
					<div class="col-6">
						<label for="sms">
							{{__('message.sms_reminder')}}
                        </label>
					</div>
					<div class="col-6">
						@include('helpers.switch', ['name' => 'sms', 'status' => "on"])
					</div>
					<!-- Fin mensaje -->
		        	
		        	<div class="col-12">
						<hr>
        			</div>
					
    				<div class="col-6">
    					<div class="form-group">
	    					<label for="" class="control-label">{{__('message.starts')}}</label>

							<div class="row">
								<div class="col-12 mb-1">
			        				<input type="date" class="form-control" name="start_date" id="start_date" value="">
								</div>
								<div class="col-12">
								   <input type="time" class="form-control" name="start_hour" id="start_hour">
								</div>
							</div>
						</div>
					</div>
					
	    			<div class="col-6">
	    				<div class="form-group">
		    				<label for="" class="control-label">{{__('message.ends')}}</label>	
		    				<div class="row">
		        				<div class="col-12 mb-1">
			        				<input type="date" class="form-control" id="end_date" name="end_date" value="">
			        			</div>
			        			<div class="col-12">
								   <input type="time" class="form-control" name="end_hour" id="end_hour">
			        			</div>
							</div>
	        			</div>
					</div>	
			    	
			    	<div class="col-12">
						<hr>
        			</div>
					
        			<div class="col-6">
	      				<a class="btn btn_new_client btn-default btn-block">
	      					<i class="fa fa-plus"></i>
	      					{{__('message.new_client')}}
	      				</a>
	      			</div>
	      			<div class="col-6 text-right">
		        		<a class="btn btn_save btn-block btn-dark">
		        			<i class="fa fa-save"></i>
		        			{{__('message.save')}}
		        		</a>
		        	</div>
			    </div>
      		</div>

      		@include('admin.calendar.modal_add_client')

		</div>
	</div>
</div>