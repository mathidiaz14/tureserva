<div class="modal fade" id="modal_view" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog modal-center" role="document" >
    	<div class="modal-content" id="modal_view_content">
    		<!-- El contenido del modal view se carga desde jquery llamando a la view modal_view_content.blade.php -->
    	</div>
  	</div>
</div>