<div class="modal fade deleteModal" id="modal_delete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal_delete_background"></div>
	<div class="modal-dialog modal-center" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gradient-danger">
				<div class="col-12 text-right">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
			</div>
			<div class="modal-body text-secondary text-center">
				<div class="row">
					<div class="col-12">
						<i class="fa fa-times-circle fa-4x"></i>
						<h5><b>{{__('message.delete_task')}}</b></h5>
					</div>
					<div class="col-12 mt-2">
						<div class="form-group" id="delete_message_client" style="display:none;">
							<label for="">{{__('message.delete_task_info')}}</label>
							<input type="text" id="message_delete" class="form-control" placeholder="{{__('message.write_comment')}}" max="100">
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-6">
						<button type="button" class="btn btn-default btn-block" data-dismiss="modal">
							<i class="fa fa-chevron-left"></i>
							{{__('message.back')}}
						</button>
					</div>
					<div class="col-6">
						<a class="btn btn-danger btn-block btn_delete_confirmation" attr-delete-id="">
	                        <i class="fa fa-trash"></i>
		                    {{__('message.confirm')}}
		                </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>