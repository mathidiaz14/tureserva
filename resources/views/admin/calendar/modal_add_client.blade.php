<!------------------------------------------------------------>
<!-- Modal de agregar cliente -->
<!------------------------------------------------------------>

<div class="modal-body form_add_client hide">
	<div class="row">
		<div class="col-12">
			<div class="form-group">
				<label for="" class="control-label">{{__('message.name')}} *</label>
				<input type="text" class="form-control" id="new_client_name" placeholder="{{__('message.name')}}">
			</div>
		</div>
	
		<div class="col-6">
			<div class="form-group">
				<label for="" class="control-label">{{__('message.phone')}} *</label>
				<input type="phone" class="form-control" id="new_client_phone" placeholder="{{__('message.phone')}}">
			</div>
		</div>
	
		
		<div class="col-6">
			<div class="form-group">
				<label for="" class="control-label">{{__('message.email')}}</label>
				<input type="email" class="form-control" id="new_client_email" placeholder="{{__('message.email')}}">
			</div>
		</div>
	
	
		<div class="col-6">
			<div class="form-group">
				<label for="" class="control-label">{{__('message.address')}}</label>
				<input type="address" class="form-control" id="new_client_address" placeholder="{{__('message.address')}}">
			</div>
		</div>
	

		
		<div class="col-6">
			<div class="form-group">
				<label for="" class="control-label">{{__('message.comment')}}</label>
				<input type="text" class="form-control" id="new_client_description" placeholder="{{__('message.comment')}}">
			</div>
		</div>
	
		<div class="col-12">
			<small>Los campos marcados con * son requeridos.</small>
			<hr>
		</div>	
	
		<div class="col-6">
				<a class="btn btn-default btn-block btn_new_client_back">
					<i class="fa fa-chevron-left"></i>
					{{__('message.back')}}
				</a>
			</div>
			<div class="col-6 text-right">
    		<a class="btn btn-dark btn-block btn_save_client">
    			<i class="fa fa-save"></i>
    			{{__('message.save')}}
    		</a>
    	</div>
	</div>
</div>