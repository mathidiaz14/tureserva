@extends('layouts.dashboard', ['menu' => 'calendar'])

@section('title')
    {{__('message.calendar')}}
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href='{{asset("fullcalendar/packages/core/main.css")}}' rel='stylesheet' />
	<link href='{{asset("fullcalendar/packages/daygrid/main.css")}}' rel='stylesheet' />
	<link href='{{asset("fullcalendar/packages/timegrid/main.css")}}' rel='stylesheet' />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}">

    <script src='{{asset("fullcalendar/packages/core/main.js")}}'></script>
    <script src='{{asset("fullcalendar/packages/interaction/main.js")}}'></script>
    <script src='{{asset("fullcalendar/packages/daygrid/main.js")}}'></script>
    <script src='{{asset("fullcalendar/packages/timegrid/main.js")}}'></script>
    <script src='{{asset("fullcalendar/packages/core/locales/es.js")}}'></script>
    <script src='{{asset("fullcalendar/packages/list/main.js")}}'></script>
    
    <script src="https://momentjs.com/downloads/moment-with-locales.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>

@endsection

@section('content')
<div class="row"> 
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-4">
                        <h5>
                            @if($user != null)
                                {{$user->name}}
                            @elseif($resource != null)
                                {{$resource->name}}
                            @endif
                        </h5>
                    </div>
                    <div class="col-4 text-center">
                        @include('admin.calendar.modal_add_button')
                    </div>
                    <div class="col-4 text-right">
                        @include('admin.calendar.calendar_options')
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <div id="view_calendar">
                	@include('admin.calendar.calendar')
                </div>

				<div class="modal_add">
					@include('admin.calendar.modal_add')
				</div>
				
                <div class="modal_view">
                    @include('admin.calendar.modal_view')
                </div>
				
                <div class="modal_edit">
					@include('admin.calendar.modal_edit')
				</div>

                <div class="modal_delete_confirmation">
                    @include('admin.calendar.modal_delete_confirmation')
                </div>

                <input type="hidden" id="count" value="{{count(Auth::user()->business->tasks)}}">
			</div>
        </div>
    </div>
    <div class="col-12">
        <div class="card collapsed-card">
            <div class="card-header">
                <h3 class="card-title">
                    {{__('message.calendar_individual')}}
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
                      <i class="fas fa-plus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach(business()->users as $user_name)
                        <div class="col-4 col-md-3 text-center">
                            <a href="{{url('users/calendar', $user_name->id)}}">
                                @if($user_name->avatar == null)
                                    <img src="{{asset('images/profile.png')}}" alt="" class="img-circle elevation-2" width="30%">
                                @else
                                    <img src="{{asset($user_name->avatar)}}" alt="" class="img-circle elevation-2" width="30%">
                                @endif
                                <p style="margin-top:10px;">
                                    <b>{{$user_name->name}}</b>
                                </p>
                            </a>
                        </div>
                    @endforeach

                    @foreach(business()->resources as $resource_name)
                        <div class="col-4 col-md-3 text-center">
                            <a href="{{url('resources/calendar', $resource_name->id)}}">
                                <i class="fa fa-bicycle fa-2x elevation-2"></i>
                                <p style="margin-top:10px;">
                                    <b>{{$resource_name->name}}</b>
                                </p>
                            </a>
                        </div>
                    @endforeach
                </div>        
            </div>
        </div>
    </div> 
</div>

@endsection

@section('scripts')
	<script>

    	//Inicia funcion de FullCalendar
    	$(document).ready(function()
    	{
    		$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });

            //reload();

            function reload()
            {
                $.get('{{url("count/calendar")}}', function(result)
                {
                    if(result > $('#count').val())
                    {
                        $('#count').val(result)   
                        $('#view_calendar').load("{{url('reload/calendar')}}");        
                    }
                });

                setTimeout(reload, 30000);
            }        
		});
    </script>
@endsection
