<a class="btn btn-info d-none d-md-block add_event_button">
    <i class="fa fa-plus"></i>
    {{__('message.add_task')}}
</a>

<a class="btn btn-info d-block d-md-none add_event_button">
    <i class="fa fa-plus"></i>
</a>

<script>
	$(document).ready(function()
	{
		$('.add_event_button').click(function()
		{
			var tasks = parseInt("{{business()->tasks->wherebetween('created_at', [\Carbon\Carbon::now()->startOfMonth(), Carbon\Carbon::now()->endOfMonth()])->count()}}");

			if(("{{business()->plan}}" == "plan1") && (tasks >= 30))
      		{
      			tureserva_notification_message("{{__('errors.limit_calendar_plan1')}}", 'danger');
      		}else if(("{{business()->plan}}" == "plan2") && (tasks >= 100))
      		{
      			tureserva_notification_message("{{__('errors.limit_calendar_plan2')}}", 'danger');
      		}else
      		{
				$('.loading').fadeIn();
				
				$('#add_event').modal('toggle');
				$('#add_event #title').val("");
				$('#add_event #client').val("");
				$('#add_event #client_name').val("");
				$('#add_event #service').val("");
				$('#add_event #service_name').val("");
				$("#add_event #employees").val(null).trigger('change');
				$("#add_event #resources").val(null).trigger('change');
		    	$('#add_event #description').val("");
		    	$('.error_client_data').hide();
		    	$('.success_client_create').hide();

		  		$('#add_event #start_date').val("{{Carbon\Carbon::now()->format('Y-m-d')}}");
		  		$('#add_event #start_hour').val("{{Carbon\Carbon::now()->roundMinutes(10)->format('H:i')}}");

		  		$('#add_event #end_date').val("{{Carbon\Carbon::now()->format('Y-m-d')}}");
		  		$('#add_event #end_hour').val("{{Carbon\Carbon::now()->addMinutes(30)->roundMinutes(10)->format('H:i')}}");

		    	$('.form_add_client').hide();
				$('.form_add_task').show();
		    	$('#add_event').modal({keyboard: true});

		    	$('.loading').fadeOut();
      		}
		});
	});
</script>
