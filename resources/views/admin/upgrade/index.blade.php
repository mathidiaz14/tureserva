@extends('layouts.dashboard', ['menu' => 'upgrade'])

@section('title')
    {{__('message.upgrade_plan')}}
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title">
                                {{__('message.upgrade_plan')}}
                            </h3>
                        </div>
                        <div class="col text-right">
                            
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col plan actual">
                            <h1>{{__('message.current_plan')}}: {{__('message.'.business()->plan)}}</h1>
                            
                            <p>
                                @if(business()->plan != "free")    
                                    {!!__('message.current_plan_info')!!}
                                @else
                                    {!!__('message.not_plan_info', ['date' => business()->created_at->addDays(30)->format('d/m/Y')])!!}
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            @include('admin.upgrade.plans.basic')
                        </div>
                        <div class="col-12 col-md-4">
                            @include('admin.upgrade.plans.advanced')
                        </div>
                        <div class="col-12 col-md-4">
                            @include('admin.upgrade.plans.premium')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
