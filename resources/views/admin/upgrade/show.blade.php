@extends('layouts.dashboard', ['menu' => 'upgrade_details'])

@section('title')
    {{__('message.details_plan')}}
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h3 class="card-title">
                                {{__('message.details_plan')}}
                            </h3>
                        </div>
                        <div class="col text-right">
                            
                        </div>
                    </div>
                </div>
                <div class="card-body text-right">
                    <div class="form-group row">
                        <div class="col-12 col-md-3">
                            <label for="">Plan activo:</label>
                        </div>
                        <div class="col-12 col-md-7">
                            <input type="text" class="form-control" value="{{__('message.'.business()->plan)}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-3">
                            <label for="">ID del plan:</label>
                        </div>
                        <div class="col-12 col-md-7">
                            <input type="text" class="form-control" value="{{$details['id']}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-3">
                            <label for="">Comienzo del plan:</label>
                        </div>
                        <div class="col-12 col-md-7">
                            <input type="text" class="form-control" value="{{Carbon\Carbon::create($details['date_created'])->format('d/m/Y H:i')}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-3">
                            <label for="">Facturación:</label>
                        </div>
                        <div class="col-12 col-md-7">
                            <input type="text" class="form-control" value="{{$details['auto_recurring']['frequency']}} {{$details['auto_recurring']['frequency_type']}}" disabled>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        
                        <div class="col text-center">
                            <a href="{{url('change/card/upgrade')}}" class="btn btn-success">
                                <i class="fa fa-credit-card mr-2"></i>
                                Cambiar tarjeta asociada
                            </a>
                        </div>
                        <div class="col text-center">
                            @include('admin.upgrade.cancel_modal')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
