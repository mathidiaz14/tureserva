<button type="button" class="btn btn-dark btn-block" data-toggle="modal" data-target="#cancel_upgrade">
    <i class="fa fa-times mr-2"></i>
    {{__('message.cancel_upgrade')}}
</button>
<!-- Modal -->

<div class="modal fade deleteModal" id="cancel_upgrade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-center" role="document">
        <div class="modal-content">
            <div class="modal-header bg-dark text-white">
            	<div class="col-11 text-left">
                	
                </div>
                <div class="col-1 text-right">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            </div>
            <div class="modal-body text-center">
                <div class="row">
                    <div class="col-12 text-dark text-center">
                        <i class="fa fa-times-circle fa-4x"></i>
                        <br><br>
                        <h2>
                            {{__('message.cancel_upgrade_info')}}
                        </h2>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-6">
                        <button type="button" class="btn btn-default btn-block" data-dismiss="modal">
                            {{__('message.no')}}
                        </button>
                    </div>
                    <div class="col-6">
                        <form action="{{url('cancel/upgrade')}}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-dark btn-block">   
                                {{__('message.yes')}}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>