@extends('layouts.dashboard', ['menu' => 'upgrade'])

@section('title')
    {{__('message.upgrade_plan')}}
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3 class="card-title">
                            {{__('message.pay')}}
                        </h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('upgrade')}}" class="btn btn-default">
                    		<i class="fa fa-chevron-left"></i>
                    		{{__('message.back')}}
                    	</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                	<div class="col-12 col-lg-6 d-none d-lg-block d-xxl-none">
                		<div class='credit-card'>
                			<div class="chip">
                				<div class="row">
                					<div class="col">
		                				<div class="exterior">
		                					<div class="interior"></div>
		                				</div>
                					</div>
                					<div class="col text-right">
                						<h2 class="issuer_name"></h2>
                					</div>
                				</div>
                			</div>
                			<div class="number">
                				<p><b>···· ···· ····</b> <b class="number_card_text">····</b></p>
                			</div>
                			<div class="name">
                				<p class="name_card_text">Nombre Apellido</p>
                			</div>
                			<div class="fecha">
                				<p class="expiration_card_text">MM/YY</p>
                			</div>
  						</div>
                	</div>
                	<div class="col-12 col-lg-6">
                		<form id="form-checkout" class="form-horizontal">
			                <progress value="0" class="progress-bar hide">Cargando...</progress>
							<div class="row">
								<div class="form-group col-12">
									<label for="">Titular de la tarjeta</label>
									<input class="form-control name_card" type="text" name="cardholderName" id="form-checkout__cardholderName" required autofocus/>
								</div>
								<div class="form-group col-12">
									<label for="">Numero de tarjeta</label>
									<input class="form-control number_card" type="text" name="cardNumber" id="form-checkout__cardNumber" maxlength="16" required />
								</div>
								<div class="form-group col-6">
									<label for="">Fecha de expiración</label>
									<input class="form-control expiration_card" type="text" name="cardExpirationDate" id="form-checkout__cardExpirationDate" maxlength="5" required />
								</div>
								<div class="form-group col-6">
									<label for="">CCV</label>
									<input class="form-control" type="text" name="securityCode" id="form-checkout__securityCode" maxlength="3" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required />
								</div>
                                <div class="form-group col-6 hide">
                                    <label for="">Banco emisor</label>
                                    <select class="form-control issuer" name="issuer" id="form-checkout__issuer" required ></select>
                                </div>
                                <div class="form-group col-6 hide">
                                    <label for="">Cuotas</label>
                                    <select class="form-control" name="installments" id="form-checkout__installments" required ></select>
                                </div>  
								<div class="form-group col-12">
									<label for="">Email</label>
									<input class="form-control" type="email" name="cardholderEmail" id="form-checkout__cardholderEmail" required />
								</div>
								<div class="form-group col-6">
									<label for="">Tipo de documento</label>
									<select class="form-control" name="identificationType" id="form-checkout__identificationType" required ></select>
								</div>
								<div class="form-group col-6">
									<label for="">Documento</label>
									<input class="form-control" type="text" name="identificationNumber" id="form-checkout__identificationNumber" required />
								</div>
								<div class="col-12">
									<hr>
								</div>
								<div class="form-group col-6 offset-6 text-right">
									<button type="submit" id="form-checkout__submit" class="btn btn-info btn-block">
				                   		<i class="fa fa-money-bill"></i>
				                   		Pagar
				                   	</button>
								</div>
							</div>
		                </form>
                	</div>
                </div>	
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	<script src="https://www.mercadopago.com/v2/security.js" view="card"></script>
	<script src="https://sdk.mercadopago.com/js/v2"></script>
    <script>
    	$('.name_card').keyup(function()
    	{	
    		if($(this).val() === "")
    			$('.name_card_text').html("Nombre Apellido");
    		else
    			$('.name_card_text').html($(this).val());


    		$('.issuer_name').html($('.issuer').text().toUpperCase());
    	});

	  	$('.number_card').bind('keyup paste', function(){
		    this.value = this.value.replace(/[^0-9]/g, '');
		});

    	$('.number_card').keyup(function()
    	{
			if($(this).val().length < 13)
			{
				$('.number_card_text').html('····');
			}else if($(this).val().length == 13)
    		{
    			$('.number_card_text').html($(this).val().substr(-1) + '···');
    		}else if($(this).val().length == 14)
    		{
    			$('.number_card_text').html($(this).val().substr(-2) + '··');
    		}else if($(this).val().length == 15)
    		{
    			$('.number_card_text').html($(this).val().substr(-3) + '·');
    		}else if($(this).val().length == 16)
    		{
    			$('.number_card_text').html($(this).val().substr(-4) );
    		}

    		$('.issuer_name').html($('.issuer').text().toUpperCase());
    	});

    	$('.expiration_card').keyup(function()
    	{
    		if($(this).val().length == 0)
    		{
    			$('.expiration_card_text').html('MM/YY');
    		}else if($(this).val().length == 1)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-1) + 'M/YY');
    		}else if($(this).val().length == 2)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-2) + '/YY');
    		}else if($(this).val().length == 4)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-4) + 'Y');
    		}else if($(this).val().length == 5)
    		{
    			$('.expiration_card_text').html($(this).val().substr(-5));
    		}
    		
    		$('.issuer_name').html($('.issuer').text().toUpperCase());
    	});

       const mp = new MercadoPago('{{env("MP_PUBLIC_KEY")}}');
	    const cardForm = mp.cardForm({
	      amount: "{{$amount}}",
	      autoMount: true,
	      form: {
	        id: "form-checkout",
	        cardholderName: {
	          id: "form-checkout__cardholderName",
	          placeholder: "Titular de la tarjeta",
	        },
	        cardholderEmail: {
	          id: "form-checkout__cardholderEmail",
	          placeholder: "E-mail",
	        },
	        cardNumber: {
	          id: "form-checkout__cardNumber",
	          placeholder: "Número de la tarjeta",
	        },
	        cardExpirationDate: {
	          id: "form-checkout__cardExpirationDate",
	          placeholder: "Fecha de vencimiento (MM/YY)",
	        },
	        securityCode: {
	          id: "form-checkout__securityCode",
	          placeholder: "Código de seguridad",
	        },
	        installments: {
	          id: "form-checkout__installments",
	          placeholder: "Cuotas",
	        },
	        identificationType: {
	          id: "form-checkout__identificationType",
	          placeholder: "Tipo de documento",
	        },
	        identificationNumber: {
	          id: "form-checkout__identificationNumber",
	          placeholder: "Número de documento",
	        },
	        issuer: {
	          id: "form-checkout__issuer",
	          placeholder: "Banco emisor",
	        },
	      },
	      callbacks: {
	        onFormMounted: error => {
	          if (error) return console.warn("Form Mounted handling error: ", error);
	        },
	        onSubmit: event => {
	          	event.preventDefault();

	          	const {
	                cardholderName,
		            paymentMethodId: payment_method_id,
		            issuerId: issuer_id,
		            cardholderEmail: email,
		            amount,
		            token,
		            installments,
		            identificationNumber,
		            identificationType,
	          	} = cardForm.getCardFormData();

	          	$.ajax({
	          		type:'POST',
					url:"{{url('change/card/upgrade')}}",
					headers: {
		              "Content-Type": "application/json",
		            },
					data:JSON.stringify({
		                _token:"{{csrf_token()}}",
		              	token,
		              	issuer_id,
		              	payment_method_id,
	                    cardholderName,
		              	transaction_amount: Number(amount),
		              	installments: Number(installments),
		              	description: "{{$plan}}",
		              	payer: {
		                	email,
		                	identification: {
		                  		type: identificationType,
		                  		number: identificationNumber,
		                	},
		              	},
	          		}),
          		 	success: function (result) 
          		 	{
						$('.landing').fadeOut();

				        if(result.status === true)
				        	window.location.replace("{{url('upgrade')}}");
				        else
				        	tureserva_notification_message(result.message,'danger');
				        
			        	$('form input').val("");
			        },
	          	});
	        },
	        onFetching: (resource) => {
	       		
	        }
	      },
	    });
    </script>
@endsection