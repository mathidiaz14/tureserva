<div class="row plan premium">
    <div class="col-12 title">
        @if(business()->plan == 'plan3')
            <div class="ribbon-wrapper ribbon-xl">
                <div class="ribbon bg-danger">
                    Plan actual
                </div>
            </div>
        @endif
        <h1>Premium</h1>
    </div>
    <div class="col-12 details">
        <ul>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Empleados ilimitados
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Tu propia web
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Agenda y reserva online
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Lista de espera
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Citas <b>ilimitadas</b>
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Recordatorios de citas <b>(ilimitados)</b>
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Soporte 24/7
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                <b>Email marketing</b>
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                <b>Tu propia APP Mobile</b>
            </li>
        </ul>
    </div>
    <div class="col-12 price">
        <div class="row text-center">
            <div class="col-6 col-md-12 col-xl-6">
                <h5 class="mt-2">
                    <b>${{env('PLAN3')}}</b><small> / mes</small>
                </h5>
            </div>
            <div class="col-6 col-md-12 col-xl-6">
                @if(business()->plan == "plan3")
                    @include('admin.upgrade.cancel_modal')
                @else
                    <a href="{{url('upgrade', 'plan3')}}" class="btn btn-info btn-block">
                        {{__('message.select')}}
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>