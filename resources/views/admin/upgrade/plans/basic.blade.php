<div class="row plan basic">
    <div class="col-12 title">
        @if(business()->plan == 'plan1')
            <div class="ribbon-wrapper ribbon-lg">
                <div class="ribbon bg-info">
                    Plan actual
                </div>
            </div>
        @endif
        <h1>Básico</h1>
    </div>
    <div class="col-12 details">
        <ul>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                <b>2</b> empleados
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Tu propia web
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Agenda y reserva online
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Lista de espera
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                50 Citas mensuales
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Recordatorios de citas <b>(60 por mes) </b>
            </li>
            <li>
                <i class="fas fa-check text-info mr-2"></i>
                Soporte 24/7
            </li>
            <li>
                <i class="fas fa-times text-gray mr-2"></i>
                Email marketing
            </li>
            <li>
                <i class="fas fa-times text-gray mr-2"></i>
                Tu propia APP Mobile
            </li>
        </ul>
    </div>
    <div class="col-12 price">
        <div class="row text-center">
            <div class="col-6 col-md-12 col-xl-6">
                <h5 class="mt-2">
                    <b>${{env('PLAN1')}}</b><small> / mes</small>
                </h5>
            </div>
            <div class="col-6 col-md-12 col-xl-6">
                @if(business()->plan == "plan1")
                    @include('admin.upgrade.cancel_modal')
                @else
                    <a href="{{url('upgrade', 'plan1')}}" class="btn btn-info btn-block">
                        {{__('message.select')}}
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>