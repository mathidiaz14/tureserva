<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">

<div class="tz-gallery">
	@if(count(business()->medias) == 0)
		@include('helpers.registry_null')
	@else
		<div class="row">
			@foreach(business()->medias as $media)
	            <div id="image_{{$media->id}}" class="col-6 col-md-4 col-lg-3">
				    <div class="row">
				    	<div class="col-12">
				    		<a class="lightbox" href="{{asset($media->url)}}">
			                    <img src="{{asset($media->url)}}" alt="{{$media->alternative}}" style="    margin-bottom: 10px;!important">
			                </a>
				    	</div>
				    	<div class="col-6">
				    		<button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#edit_media_{{$media->id}}">
								<i class="fa fa-list"></i>
							</button>

							<div class="modal fade" id="edit_media_{{$media->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog modal-center modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<div class="col-6 text-left">
												<p>{{__('message.edit_file')}}</p>
											</div>
											<div class="col-6">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true" style="color:red;">&times;</span>
												</button>
											</div>
										</div>
										<div class="modal-body text-left">
											<div class="row">
												<div class="col-12 col-md-6">
													<img src="{{asset($media->url)}}" alt="{{$media->alternative}}" width="100%">
												</div>
												<div class="col-12 col-md-6">
													<form action="{{route('media.update', $media->id)}}" method="post" class="form-horizontal">
														@csrf
														@method('PATCH')
														<div class="form-group">
															<label for="">
																{{__('message.type')}}: {{$media->type}} <br>
																{{__('message.size')}}: {{$media->size}} <br>
																{{__('message.user')}}: {{$media->user->name}}
															</label>
														</div>
														<div class="form-group">
															<label for="">{{__('message.name')}}</label>
															<input type="text" class="form-control" name="name" value="{{$media->name}}">
														</div>
														<div class="form-group">
															<label for="">{{__('message.alternative_text')}}</label>
															<input type="text" class="form-control" name="alternative" value="{{$media->alternative}}">
														</div>
														<div class="form-group text-right">
															<button class="btn btn-info">
																<i class="fa fa-save"></i>
																{{__('message.save')}}
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
				    	</div>
				    	<div class="col-6">
				    		<a class="btn btn-info btn-block check" id="media_{{$media->id}}" attr-id="{{$media->id}}" attr-check="false">
				    			<i class="far fa-square"></i>
				    		</a>
				    	</div>
				    </div>	
	            </div>
			@endforeach
	    </div>
	@endif
</div>

<script>
	$(document).ready(function()
	{
		baguetteBox.run('.tz-gallery');

		var array = [];

		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });

		$('.check').on('click', function()
		{
			var id = $(this).attr('attr-id');

			if($(this).attr('attr-check') == "false")
			{
				$(this).attr('attr-check', 'true');
				$(this).removeClass('btn-info');
				$(this).addClass('btn-success');
				$(this).html('<i class="far fa-check-square"></i>');

				array.push(id);
			}else
			{
				$(this).attr('attr-check', 'false');
				$(this).removeClass('btn-success');
				$(this).addClass('btn-info');
				$(this).html('<i class="far fa-square"></i>');

				var position = $.inArray(id, array);
				array.splice(position, 1);
			}

			var flag = false;

			$('.check').each(function()
			{
				if($(this).attr('attr-check') == "true")
					flag = true;
			});

			if(flag)
				$('.delete_items').removeClass('disabled');	
			else
				$('.delete_items').addClass('disabled');
			
		});

		$('.delete_items').on('click', function()
		{
			
			$.each(array, function(index, value)
			{
				$.ajax({
					type:'POST',
					url:"{{url('media')}}/"+value,
					data:{
						_method: 'DELETE',

					},
		        });

		        $('#image_'+value).remove();
		    });
		
			array = [];
			$('.delete_items').addClass('disabled');

			tureserva_notification_message("Las imagenes se eliminaron correctamente.", "success");
		});
	});
</script>