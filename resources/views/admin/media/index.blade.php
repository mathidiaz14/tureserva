@extends('layouts.dashboard', ['menu' => 'media'])

@section('title')
	{{__('message.multimedia')}}
@endsection

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-4">
          				<h3 class="card-title">{{__('message.multimedia')}}</h3>
          			</div>
          			<div class="col-8 text-right">
          				<a class="btn btn-danger delete_items disabled">
							<i class="fa fa-trash"></i>
							Eliminar items
						</a>
          				<!-- Button trigger modal -->
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#add_media">
							<i class="fa fa-upload"></i>
							{{__('message.upload_file')}}
						</button>
						<!-- Modal -->
						<div class="modal fade" id="add_media" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<div class="col-6 text-left">
											<p>{{__('message.upload_file')}}</p>
										</div>
										<div class="col-6">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" style="color:red;">&times;</span>
											</button>
										</div>
									</div>
									<div class="modal-body text-left">
										<p>{{__('message.drag_files_to_upload')}} <i class="fa fa-arrow-down"></i></p>
						        		<form action="{{url('media')}}" method="post" class="dropzone" id="myAwesomeDropzone">
						        			@csrf
										</form>
									</div>
								</div>
							</div>
						</div>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="card-body">
    			<div id="media_content">
					@include('admin.media.images')
				</div>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
	<script>
		$(document).ready(function()
		{
			Dropzone.options.myAwesomeDropzone = {
			    paramName: "files", 
			    maxFilesize: 10, 
			    success: function () 
			    {
		        	$('#media_content').load("{{url('load/images')}}");
			    }
			};

			
		});
	</script>
@endsection