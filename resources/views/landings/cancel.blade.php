@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" method="POST" action="{{ url('web/cancel') }}">
                @csrf
                <span class="login100-form-title p-b-43">
                    <a href="{{url('/')}}" class="">
                        <img src="{{asset('images/icon.png')}}" width="80px" alt="">
                    </a>
                    <br><br>
                    {{__('web.cancel_question')}}
                    <br>
                    <p>{{__('web.cancel_info')}}</p>
                </span>
                
                <div class="col text-center">
                    @include('helpers.alert_login')
                </div>
                
                
                <div class="wrap-input100 validate-input ">
                    <input id="code" type="text" class="input100" name="code" value="{{ $code }}" required autofocus>

                    <span class="focus-input100"></span>
                        <span class="label-input100">{{__('web.cancel_code')}}</span>
                    
                </div>

                <hr>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Cancelar
                    </button>
                </div>
                <div class="container-login100-form-btn" style="padding-top:10px;">   
                    <a href="{{url('/')}}" class="login100-form-btn" style="background: none;border: solid 2px #00A8D6;color: #00A8D6;">
                        Volver
                    </a>
                </div>
            </form>

            <div class="login100-more" style="background-image: url({{asset('auth/images/bg-01.jpg')}});">
            </div>
        </div>
    </div>
</div>
@endsection
