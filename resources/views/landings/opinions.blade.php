<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>{{config('app.name')}} - {{__('web.cancel_task')}}</title>

	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
	<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">
	<link rel="stylesheet" href="{{asset('fontawesome/css/all.css')}}">
	
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/PrintArea.js')}}"></script>
	<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
	<script src="{{asset('fontawesome/js/all.js')}}"></script>  

  	<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
</head>
<body>
  
  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    	<div class="container">
      		<a class="navbar-brand" href="{{url('/')}}">{{config('app.name')}}</a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon"></span>
  			</button>
      		<div class="collapse navbar-collapse" id="navbarResponsive">
        		<ul class="navbar-nav ml-auto">
	      			
	    		</ul>
      		</div>
    	</div>
  	</nav>

  <!-- Header - set the background image for the header in the line below -->
    <header class="py-5 bg-image-full" style="background-image: url({{asset('images/header.jpg')}});">
	    <img class="img-fluid d-block mx-auto" src="{{asset('images/icon.png')}}" width="200px" height="200px" alt="">
  	</header>
  
  	<section class="py-5 text-center">
			<div class="container">
				<form action="{{url('web/opinion')}}" method="POST" class="form-horizontal">
					@csrf
					<input type="hidden" name="business" value="{{$business->id}}">
					<input type="hidden" name="stars" id="stars">
					<input type="hidden" name="calendar" value="{{$task->id}}">
					
					<div class="form-group">
						<h3>Valoramos mucho tu opinión</h3>
					
						<i class="fa fa-star fa-2x" id="1"></i>
						<i class="fa fa-star fa-2x" id="2"></i>
						<i class="fa fa-star fa-2x" id="3"></i>
						<i class="fa fa-star fa-2x" id="4"></i>
						<i class="fa fa-star fa-2x" id="5"></i>
					
					</div>	
					<br>
					<div class="form-group">
						<p>
							Danos tu opinión sobre la cita realizada el dia <b>{{$task->start->format('d/m/Y')}}</b> a la hora <b>{{$task->start->format('H:i')}}</b> 
							
							@if($task->users->first() != null)
								con <b>{{$task->users->first()->name}}</b>
							@endif

							en <b>{{$business->name}}</b>
								
						</p>
						<br>
					</div>
					<div class="form-group">
						<textarea name="comment" id="" cols="30" rows="4" class="form-control" placeholder="{{__('web.comment')}}"></textarea>
					</div>
					<br>
					<div class="form-group text-end">
						<button class="btn btn-info" disabled>
							<i class="fa fa-paper-plane"></i>
							{{__('web.send')}}
						</button>
					</div>
				</form>
			</div>
		</section>	
	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<a href="{{config('app.url')}}" target="_blank">
				<p class="m-0 text-center text-white">
					<img src="{{asset('images/icon.png')}}" alt="" width="30px">
					{{config('app.name')}}
				</p>
			</a>
		</div>
	</footer>
	<script>
			
			$(document).ready(function()
			{
	    		$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    });    		
		
				$('.fa-star').click(function()
	    		{
	    			if ($(this).attr('id') == "1")
	    			{
	    				$('.fa-star').css('color', 'black');
	    				$(this).css('color', 'yellow');

						var star = "1";

	    			}else if($(this).attr('id') == "2")
	    			{
	    				$('.fa-star').css('color', 'black');
	    				$('#1').css('color', 'yellow');
	    				$(this).css('color', 'yellow');

						var star = "2";

	    			}else if($(this).attr('id') == "3")
	    			{
	    				$('.fa-star').css('color', 'black');
	    				$('#1').css('color', 'yellow');
	    				$('#2').css('color', 'yellow');
	    				$(this).css('color', 'yellow');

						var star = "3";

	    			}else if($(this).attr('id') == "4")
	    			{
	    				$('.fa-star').css('color', 'black');
	    				$('#1').css('color', 'yellow');
	    				$('#2').css('color', 'yellow');
	    				$('#3').css('color', 'yellow');
	    				$(this).css('color', 'yellow');

						var star = "4";

	    			}else if($(this).attr('id') == "5")
	    			{
	    				$('.fa-star').css('color', 'black');
	    				$('#1').css('color', 'yellow');
	    				$('#2').css('color', 'yellow');
	    				$('#3').css('color', 'yellow');
	    				$('#4').css('color', 'yellow');
	    				$(this).css('color', 'yellow');

						var star = "5";
	    			}

	    			$('#stars').val(star);
	    			$('.btn-info').attr('disabled', false);
	    		});
			});
		</script>
</body>

</html>