<nav class="navbar navbar-expand-lg navbar-dark">
	<div class="container-fluid">
  		<a class="navbar-brand" href="{{url('web', $business->code)}}">{{$business->name}}</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
  		<div class="collapse navbar-collapse" id="navbarResponsive">
    		<ul class="navbar-nav ml-auto">
      			<li class="nav-item active">
        			<a class="nav-link" href="{{url($business->code)}}">{{__('web.home')}}
          				<span class="sr-only">(current)</span>
        			</a>
      			</li>

      			@if(landing('tips', $business->id) == "on")
      				<li class="nav-item active">
        				<a class="nav-link" href="{{url($business->code)}}/tips">{{__('web.tips')}}
	          				<span class="sr-only">(current)</span>
	        			</a>
	      			</li>
      			@endif

      			@if(landing('opinions', $business->id) == "on")
      				<li class="nav-item active">
        				<a class="nav-link" href="{{url($business->code)}}/opinions">{{__('web.opinions')}}
	          				<span class="sr-only">(current)</span>
	        			</a>
	      			</li>
      			@endif
		      	
		      	<li class="nav-item">
        			<a class="nav-link" href="{{url('web/cancel')}}">{{__('web.cancel_task')}}
          				<span class="sr-only">(current)</span>
        			</a>
      			</li>
    		</ul>
  		</div>
	</div>
	</nav>