<div class="card rounded-0 border-0">
	<div class="card-header rounded-0" style="padding: 14px; border-bottom: none;">
		<i class="fa fa-list" style="margin-right:2em;"></i> <b>{{__('web.details')}}</b>		
	</div>
	<div class="card-body">
		<div class="row not-details">
			<div class="col-12 text-center py-5 text-secondary	">
				<i class="fa fa-exclamation-circle fa-3x"></i>
				<br><br>
				<p>Aun no hay ningun dato</p>
			</div>
		</div>
		<div class="row details text-secondary" style="display: none;">
			
		</div>
	</div>
	<div class="card-footer details-footer" style="display: none;">
		<div class="row">
			<div class="col-6">
				<p id="timer-content" style="display:none;"><b id="timer"></b> / 3 min</p>
			</div>
			<div class="col-6 text-end">
				<a href="{{url($business->code)}}" class="btn btn-secondary">
					<i class="fa fa-undo" style="margin-right:15px;"></i>
					{{__('message.restart')}}
				</a>
			</div>	
		</div>
	</div>
</div>