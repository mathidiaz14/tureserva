<!-- Services Accordion -->
<div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
        <button id="collapseServicesButton" class="accordion-button" type="button"  data-bs-target="#collapseServices" aria-expanded="true" aria-controls="collapseServices">
            <i class="far fa-clock" style="margin-right:2em;"></i> <b>{{__('message.services')}}</b>
        </button>
    </h2>
    <div id="collapseServices" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#acordionBooking">
        <div class="accordion-body">
            <div class="row my-1">
                <div class="col-12 text-secondary">
                    <p>{{__('web.services')}}</p>
                </div>
            </div>
            <div class="row">
                @foreach($business->services as $service)
                    <div class="col-12 col-md-6">
                        <div id="service_{{$service->id}}" class="row rounded-2 btn-service text-start mx-1 my-3 p-4" attr-id="{{$service->id}}" attr-name="{{$service->name}}">
                            <div class="col-8">
                                <h5>{{$service->name}}</h5>
                                <small>{{$service->description}}</small>
                            </div>
                            <div class="col-4 text-end">
                                <p class="duration">{{$service->duration}} min</p>
                                @if($service->business->reservation->service_price == "on")
                                <p><b>${{$service->price}}</b></p>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="row pt-2">
                <hr>
                <div class="col-12 d-grid gap-2">
                    <a class="btn btn-secondary p-3 btn-service-finish disabled">
                        <i class="fa fa-paper-plane" style="margin-right:15px;"></i>
                        Continuar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $('.btn-service').click(function () {
            if (!$(this).hasClass('inactive')) {
                if ($(this).hasClass('active'))
                    $(this).removeClass('active');
                else
                    $(this).addClass('active');

                $('.loading').fadeIn();

                var services = "";

                $('.btn-service.active').each(function (index, element) {
                    services += $(this).attr('attr-id') + ",";
                });

                if (services.length > 0) {
                    services = "?services=" + services.substring(0, services.length - 1);
                    $('.btn-service-finish').removeClass('disabled');
                } else {
                    services = null;
                    $('.btn-service-finish').addClass('disabled');
                }

                if (services != null) {
                    $.get("{{url('booking/services')}}/" + services, function (response) {
                        $('.btn-service').addClass('inactive');

                        $.each(response, function (index, element) {
                            $('#service_' + element).removeClass('inactive');
                        });

                        $('.btn-service.active').removeClass('inactive');

                    }).done(function () {
                        $('.loading').fadeOut();
                    });
                } else {
                    $('.btn-service.inactive').removeClass('inactive');
                    $('.loading').fadeOut();
                }
            }
        });

        $('.btn-service-finish').click(function () {
            if (!$(this).hasClass('disabled')) {
                $('.loading').fadeIn();

                var services        = "";
                var services_names  = "";

                $('.btn-service.active').each(function (index, element) {
                    services        += $(this).attr('attr-id') + ",";
                    services_names  += $(this).attr('attr-name') + ", ";
                });

                if (services.length > 0) {
                    services        = services.substring(0, services.length - 1);
                    services_names  = services_names.substring(0, services_names.length - 2);

                    $('.btn-service-finish').removeClass('disabled');
                } else {
                    services = null;
                    $('.btn-service-finish').addClass('disabled');
                }

                $('#services').val(services);

                if (services != null) {
                    $.get("{{url('booking/users')}}/?services=" + services, function (response) 
                    {
                        $('.users').html("");

                        var users = "";

                        $.each(response, function (index, element) 
                        {
                            users += "<div class='col-12 col-md-6 col-lg-4'><div id='user_"+element.id+"' class='row rounded-2 btn-user text-start mx-1 my-3 p-4' attr-id='"+element.id+"' attr-name='"+element.name+"'><div class='col-12 text-center'><h5>"+element.name+"</h5></div></div></div>";
                            
                        });
                        

                        @if($business->reservation->nobody == "on")
                            if(response.length > 1)
                            {
                                users += "<div class='col-12 col-md-6 col-lg-4'><div id='user_any' class='row rounded-2 btn-user text-start mx-1 my-3 p-4' attr-id='any' attr-name='{{__('web.nobody')}}'><div class='col-12 text-center'><h5>{{__('web.nobody')}}</h5></div></div></div>";
                            }
                        @endif
                        
                        $('.users').append(users);

                    }).done(function () 
                    {
                        $('#collapseServices').removeClass('show');
                        $('#collapseServicesButton').addClass('collapsed');
                        $('#collapseServicesButton').html('<i class="fa fa-check-circle text-success" style="margin-right:2em;"></i> <b>{{__("message.services")}}:</b> &nbsp;'+services_names);

                        $('#collapseUsers').addClass('show');
                        $('#collapseUsersButton').removeClass('collapsed');
                        $('#collapseUsersButton').attr('aria-expanded', 'true');

                        var details = '<div class="col-12"><p><b>{{__("message.services")}}:</b> '+services_names+'</p></div>';

                        $('.details').append(details);
                        
                        $('.not-details').hide();
                        $('.details').fadeIn();
                        $('.details-footer').fadeIn();

                        $('.loading').fadeOut();
                    });
                }
            }
        });
    })
</script>