const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
const dayNames = ["Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"];

let currentMonth = new Date().getMonth();
let currentYear = new Date().getFullYear();

function renderCalendar(month, year, enabledDates) {
    $("#custom-month").text(monthNames[month]);
    $("#custom-year").text(year);

    const firstDay = (new Date(year, month)).getDay();
    const daysInMonth = 32 - new Date(year, month, 32).getDate();

    let calendar = '<div class="fc-calendar fc-five-rows">';
    calendar += '<div class="fc-head">';
    for (let i = 0; i < dayNames.length; i++) {
        calendar += '<div>' + dayNames[i] + '</div>';
    }
    calendar += '</div><div class="fc-body">';

    let date = 1;
    for (let i = 0; i < 6; i++) {
        calendar += '<div class="fc-row">';
        for (let j = 1; j <= 7; j++) {
            if (i === 0 && j < firstDay) {
                calendar += '<div></div>';
            } else if (date > daysInMonth) {
                break;
            } else {
                const dateString  = `${year}-${String(month + 1).padStart(2, '0')}-${String(date).padStart(2, '0')}`;
                const dateString2 = `${String(date).padStart(2, '0')}/${String(month + 1).padStart(2, '0')}/${year}`;
                const isDisabled = !enabledDates.includes(dateString);
                const today = new Date();
                const isToday = today.getDate() === date && today.getMonth() === month && today.getFullYear() === year;

                var currentDate = $('#date').val();

                calendar += `<div id="day_${dateString}" class="fc-day ${currentDate == dateString ? 'select' : '' } ${isToday ? 'fc-today ' : ''}${isDisabled ? 'disabled' : ''}" attr-date="${dateString}" attr-date-text="${dateString2}"><span class="fc-date">${date}</span><span class="fc-weekday">${dayNames[j - 1]}</span></div>`;
                date++;
            }
        }
        calendar += '</div>';
    }
    calendar += '</div></div>';

    $("#calendar").html(calendar);
}

function nextMonth() {
    currentMonth++;
    if (currentMonth === 12) {
        currentMonth = 0;
        currentYear++;
    }

    var dates = $('#dates').val().split(',');
    renderCalendar(currentMonth, currentYear, dates);
}

function prevMonth() {
    currentMonth--;
    if (currentMonth === -1) {
        currentMonth = 11;
        currentYear--;
    }

    var dates = $('#dates').val().split(',');
    renderCalendar(currentMonth, currentYear, dates);
}