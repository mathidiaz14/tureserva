<!-- SERVICIOS -->
@if($business->services->where('online', 'on')->count() == 0)
	<div class="row">
		<div class="col-12 text-center text-secondary">
			<br><br>
			<i class="fa fa-exclamation-triangle fa-3x"></i>
			<br><br>
			<p>{{trans('web.not_services')}}</p>
		</div>
	</div>
@else
	<div class="row">
	    @foreach($business->services->where('online', 'on') as $service)
			<div class="col-12 col-md-6">
				<div class="btn-service" service_id="{{$service->id}}" service_name="{{$service->name}}">
			      	<div class="row">
			      		<div class="col-12">
			      			<h2 class="title">
			      				<b>{{$service->name}}</b>
			      			</h2>
				      		{{$service->description}}
				      		
		      				<div class="row" style="padding-top: 1em; margin-top: 1em; border-top: solid 1px #68549530;">
		      					<div class="col">
		      						<b>{{$service->duration}} Min.</b>
		      					</div>
		      					<div class="col text-end">
		      						@if($service->business->reservation->service_price == "on") 
										<b>${{$service->price}}</b>
							      	@endif	
		      					</div>
		      				</div>
			      		</div>
			      	</div>
				</div>
			</div>
	    @endforeach
	</div>

	<script>
		$(document).ready(function()
		{
			$('.btn-service').click(function()
			{
				$('.loading').fadeIn();
				$('.service_section').hide();
				$('.timelapse_all').hide();

				var service = $(this).attr('service_id');
				var name 	= $(this).attr('service_name');

				$('.user_section').load("{{url('web/users')}}/"+service, function()
				{
					$('#form #service_id').val(service);
					$('.timelapse_service').html('<b>'+name+'</b>');
					
					$('.timelapse_all').fadeIn();
					$('.timelapse.active').removeClass('active');
					$('.timelapse_user').addClass('active');
					
					$('.user_section').fadeIn();
					$('.timelapse_all').fadeIn();
					$('.loading').fadeOut();
				});
			});
		});
	</script>
@endif
<!-- FIN SERVICIOS -->