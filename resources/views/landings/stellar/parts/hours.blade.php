<section id="hours" class="main special">
	<header class="major">
		<h2>{{__('web.hours')}}</h2>
	</header>
	
	<div class="row">
		<div class="col m-2">
			<div class="table table-responsive">
				<table class="table">
					@foreach(days_list() as $list)
						@if($business->days->where('day', $list)->first()->status != "off")
							<tr>
								<td>
									<b>{{__('message.'.$list)}}: </b>
								</td>
						
								@foreach($business->days->where('day', $list) as $day)
									<td>
										{{$day->start}} - {{$day->end}}
									</td>
								@endforeach
							</tr>	
						@endif
					@endforeach
				</table>
			</div>
		</div>
	</div>
</section>