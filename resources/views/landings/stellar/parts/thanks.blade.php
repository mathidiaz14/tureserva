@if(session()->has('confirmation_code'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-success">
				<p style="margin:0;">{{trans('web.confirm', ['code' => Session('confirmation_code')])}}</p>
				
				{{session()->forget('confirmation_code')}}
			</div>
		</div>
	</div>

@elseif(session()->has('error'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-danger">
				{{session('error')}}
				{{session()->forget('error')}}
			</div>
		</div>
	</div>

@elseif(session()->has('waiting'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-success">
				<p style="margin:0;">{{session('waiting')}}</p>
				
				{{session()->forget('waiting')}}
			</div>
		</div>
	</div>

@elseif(session()->has('opinion'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-success">
				<p style="margin:0;">{{session('opinion')}}</p>
				
				{{session()->forget('opinion')}}
			</div>
		</div>
	</div>
@endif