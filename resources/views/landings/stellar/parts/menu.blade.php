<nav id="nav">
	<ul>
		<li><a href="#intro" class="active">Reserva</a></li>
		<li><a href="#gallery">Galeria</a></li>
		<li><a href="#hours">Horarios</a></li>
		<li><a href="#map">Mapa</a></li>
		<li><a href="{{url($business->code)}}/tips">Consejos</a></li>
		<li><a href="{{url($business->code)}}/opinions">Opiniones</a></li>
		<li><a href="#contact">Contacto</a></li>
	</ul>
</nav>