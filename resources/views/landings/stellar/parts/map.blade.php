
<section id="map" class="main special">
	<header class="major">
		<h2>{{__('web.map')}}</h2>
	</header>
	
	<div class="row">
		<div class="col m-2">
			<iframe width="100%" height="400" src="https://maps.google.com/maps?q={{$business->address}}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
		</div>
	</div>
</section>