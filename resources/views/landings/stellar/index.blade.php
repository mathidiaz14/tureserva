<!DOCTYPE HTML>
<html>
	<head>
		<title>{{$business->name}}</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

		<link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
		
		<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
		
		<link rel="stylesheet" href="{{asset('landings/stellar/assets/css/main.css')}}" />
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<noscript><link rel="stylesheet" href="{{asset('landings/stellar/assets/css/noscript.css')}}" /></noscript>

		@if($business->logo != null)
	    	<link rel="shortcut icon" type="image" href="{{asset($business->logo)}}">
	  	@else
	    	<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
	  	@endif

	  	<style>
	  		body {
				background-color: {{landing('background1', $business->id)}};
				background-image: url("{{asset('images/overlay.png')}}"), 
					-moz-linear-gradient(45deg, 
										{{landing('background1', $business->id)}} 15%, 
										{{landing('background2', $business->id)}} 85%
									);
				background-image: url("{{asset('images/overlay.png')}}"), 
					-webkit-linear-gradient(45deg, 
										{{landing('background1', $business->id)}} 15%, 
										{{landing('background2', $business->id)}} 85%
									);
				background-image: url("{{asset('images/overlay.png')}}"), 
					-ms-linear-gradient(45deg, 
										{{landing('background1', $business->id)}} 15%, 
										{{landing('background2', $business->id)}} 85%
									);
				background-image: url("{{asset('images/overlay.png')}}"), 
					linear-gradient(45deg, 
										{{landing('background1', $business->id)}} 15%, 
										{{landing('background2', $business->id)}} 85%
									);
				color: rgba(255, 255, 255, 0.65);
			}
	  	</style>
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<span class="logo">
							@if($business->logo != null)
								<img src="{{asset($business->logo)}}" alt="" width="100px" class="logo_business" />
							@else
								<img src="{{asset('landings/stellar/images/logo.svg')}}" alt="" />
							@endif
						</span>
						<h1>{{$business->name}}</h1>
						<p>
							{{landing('text_index', $business->id)}}
						</p>
					</header>

				<!-- Nav -->
					@include('landings.stellar.parts.menu')

				<!-- Main -->
					<div id="main">

						<!-- Introduction -->
							<section id="intro" class="main">
								<div class="spotlight">
									<div class="content">
										<header class="major">
											<h2>{{__('web.services')}}</h2>
										</header>

										@include('landings.stellar.parts.thanks')

										<div class="row">
											<div class="col-12 timelapse_all mt-3" style="display:none;">
												<hr>
												<div class="row">
													<div class="col-md col-5 timelapse timelapse_service active">
														<b>{{__('web.service')}}</b>
													</div>
													
													<div class="col-md col-2 pt-2"><hr></div>
													
													<div class="col-md col-5 timelapse timelapse_user">
														<b>{{__('web.user')}}</b>
													</div>
													
													<div class="col-md d-none d-sm-none d-md-block pt-2"><hr></div>
													
													<div class="col-md col-5 timelapse timelapse_date">
														<b>{{__('web.date_hour')}}</b>
													</div>
													
													<div class="col-md col-2 pt-2"><hr></div>
													
													<div class="col-md col-5 timelapse timelapse_finish">
														<b>{{__('web.finish')}}</b>
													</div>
												</div>
												<div class="row">
													<div class="col-12 text-center">
														<p class="time_count_content pt-4" style="display:none;">
															{{trans('web.details_time')}} 
															<b class="time_count"></b>
														</p>
													</div>
												</div>
												<hr>
											</div>
										</div>

										<div class="row">
											<div class="col-12">
												<div class="service_section">
													@include('landings.stellar.service')
												</div>

												<div class="user_section" style="display:none;">
												</div>

												<div class="date_section" style="display:none;">
												</div>

												<div class="form_section" style="display:none;">
													@include('landings.stellar.form')
												</div>

												@include('helpers.loading')
											</div>
										</div>
										
									</div>
								</div>
							</section>

						<!-- Gallery Section -->
							@if(landing('gallery', $business->id) == "on")
								@include('landings.stellar.parts.gallery')
							@endif

						<!-- Hours Section -->
							@if(landing('hours', $business->id) == "on")
								@include('landings.stellar.parts.hours')
							@endif

						<!-- Map Section -->
							@if(landing('map', $business->id) == "on")
								@include('landings.stellar.parts.map')
							@endif

						<!-- Contact Section -->
							@if(landing('contact', $business->id) == "on")
								@include('landings.stellar.parts.contact')
							@endif

					</div>

				<!-- Footer -->
					@include('landings.stellar.parts.footer')

			</div>

		<!-- Scripts -->
			<script src="{{asset('landings/stellar/assets/js/jquery.min.js')}}"></script>
			<script src="{{asset('landings/stellar/assets/js/jquery.scrollex.min.js')}}"></script>
			<script src="{{asset('landings/stellar/assets/js/jquery.scrolly.min.js')}}"></script>
			<script src="{{asset('landings/stellar/assets/js/browser.min.js')}}"></script>
			<script src="{{asset('landings/stellar/assets/js/breakpoints.min.js')}}"></script>
			<script src="{{asset('landings/stellar/assets/js/util.js')}}"></script>
			<script src="{{asset('landings/stellar/assets/js/main.js')}}"></script>
			@include('helpers.footer')

	</body>
</html>