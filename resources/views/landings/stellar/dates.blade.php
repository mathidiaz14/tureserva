<!-- FECHAS -->
<div class="container">
	<div class="row">
		<div class="col">
			<p class="lead">{{trans('web.dates_days', ['days' => $dates->count()])}}</p>
		</div>
		<div class="col text-end">
			@if($business->plan != "plan1")
				@include('helpers.waiting_list')
			@endif
		</div>
	</div>
	
	<div class="row">
		@include('helpers.date_table')
	</div>

</div>
<script>
	$(document).ready(function()
	{
		$('.hour').click(function()
		{
			if($(this).hasClass('active'))
			{
				$('.loading').show();
				$('.timelapse_all').hide();
				
				var date 	= $(this).attr('attr-date');
				var hour 	= $(this).attr('attr-hour');
				
				$('.date_section').hide();
				
				$('.timelapse_date').html('<b>'+date+' '+hour+'</b>');
				$('.timelapse_date').removeClass('active');
				$('.timelapse_finish').addClass('active');
				
				$('#form #date').val(date);
				$('#form #hour').val(hour);
				
				$('.form_section').fadeIn();
				$('.timelapse_all').fadeIn();
				$('.loading').fadeOut();
			}
		});
	});
</script>
<!-- FIN FECHAS -->
<hr>
<div class="row">
	<div class="col">
		<a href="{{url($business->code)}}">
			<i class="fa fa-angle-left"></i>
			Volver a empezar
		</a>
	</div>
</div>