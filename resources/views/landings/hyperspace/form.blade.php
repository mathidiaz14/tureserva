<div class="container">
	<div class="row">
		<div class="col mb-3">
			<h4>{{trans('web.form')}}</h4>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<form id="form" method="post" action="{{url('web')}}" class="form-horizontal">
				@csrf
				<div class="row">
					<input type="hidden" id="service_id" class="service_id" name="service" >
					<input type="hidden" id="user_id" class="user_id" name="user" >
					<input type="hidden" id="date" name="date" >
					<input type="hidden" id="hour" name="hour" >
					<input type="hidden" id="business" name="business" value="{{$business->id}}">

					<div class="col-12 mt-2">
						<div class="form-group">
							<label for="" class="form-label">{{trans('web.name')}} *</label>
							<input type="text" class="form-control" name="name" id="name" required placeholder="Escribe tu nombre">
						</div>
					</div>
					
					<div class="col-12 col-md-6 mt-2">
						<div class="form-group">
							<label for="" class="form-label">{{trans('web.email')}} *</label>
							<input type="email" class="form-control" name="email" id="email" required="" placeholder="Escibe tu email">
						</div>	
					</div>
						
					<div class="col-12 col-md-6 mt-2">
						<div class="form-group">
							<label for="" class="form-label">{{trans('web.phone')}} *</label>
							<input type="phone" class="form-control" name="phone" id="phone" required="" placeholder="Escibe tu telefono">
						</div>
					</div>

					<div class="col-12 mt-2">
						<div class="form-group">
							<label for="" class="form-label">{{trans('web.comment')}}</label>
							<textarea id="description" name="description" cols="30" rows="3" class="form-control" placeholder="¿Quieres agregar algun comentario a la reserva?"></textarea>
						</div>
					</div>
					
					<div class="col-12 mt-2">
						<div class="form-group">
							<small>{{trans('web.required')}}</small>
						</div>
					</div>
					<hr>
					<div class="col-12">
						<div class="form-group text-end">
							<button class="button">
								<i class="fa fa-paper-plane"></i>
								{{trans('web.send')}}
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="row mt-4">
		<div class="col-12">
			<a href="{{url($business->code)}}">
				<i class="fa fa-angle-left"></i>
				Volver a empezar
			</a>
		</div>
	</div>
</div>