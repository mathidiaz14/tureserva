<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{$business->name}}</title>
	
	<link rel="stylesheet" href="{{asset('landings/hyperspace/assets/style.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="{{asset('landings/hyperspace/assets/style.js')}}"></script>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	@if($business->logo != null)
		<link rel="shortcut icon" type="image" href="{{asset($business->logo)}}">
	@else
		<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
	@endif

	<style>
		.navigate
		{
			background: {{landing('sidebar_background', $business->id)}}!important;
		}

		body
		{
			background: {{landing('style1', $business->id)}};	
		}

		.style2
		{
			background: {{landing('style2', $business->id)}};	
		}

		.style3
		{
			background: {{landing('style3', $business->id)}};	
		}

		.style4
		{
			background: {{landing('style4', $business->id)}};	
		}
	</style>

</head>
<body>
	
	@include('helpers.loading')

	@include('landings.hyperspace.parts.menu')

	<div class="container-fluid">
		@include('landings.hyperspace.parts.alerts')
		
		<section class="content style1 intro" id="top">
			<div class="inner">
				<h1>{{$business->name}}</h1>
				<p>{{landing('text_index', $business->id)}}</p>
				<br><br>
				<a href="#one" class="button scrolly">
					Comenzar
				</a>
			</div>
		</section>

		<section class="content style2" id="one">
			<div class="row">
				<div class="col-12">
					<div class="timelapse_all" style="display: none;">
						<div class="row">
							<div class="col-6 col-md-3 text-center">
								<div class="timelapse timelapse_service active">
									Servicio
								</div>
							</div>
							<div class="col-6 col-md-3 text-center">
								<div class="timelapse timelapse_user">
									Usuario
								</div>
							</div>
							<div class="col-6 col-md-3 text-center">
								<div class="timelapse timelapse_date">
									Fecha y hora
								</div>
							</div>
							<div class="col-6 col-md-3 text-center">
								<div class="timelapse timelapse_finish">
									Finalizar
								</div>
							</div>
						</div>
						<div class="time_count_content text-center" style="display:none;">
							{{__('web.details_time')}} 
							<b class="time_count"></b>
							<br>
						</div>
						<hr>
					</div>
				</div>
				<div class="col-12">
					<div class="inner" >
						<div class="service_section">
							@include('landings.hyperspace.services')
						</div>

						<div class="user_section" style="display: none;">
							
						</div>

						<div class="date_section" style="display: none;">
							
						</div>

						<div class="form_section" style="display: none;">
							@include('landings.hyperspace.form')
						</div>
					</div>
				</div>
			</div>
		</section>
		
		@if(landing('gallery', $business->id) == "on")
			@include('landings.hyperspace.parts.gallery')
		@endif

		@if(landing('map', $business->id) == "on")
			@include('landings.hyperspace.parts.maps')
		@endif

		@if(landing('hours', $business->id) == "on")
			@include('landings.hyperspace.parts.hours')
		@endif

		@if(landing('contact', $business->id) == "on")
			@include('landings.hyperspace.parts.contacts')
		@endif
	</div>

	<footer>
		<div class="content-alt style1">
			<div class="inner">
				<p>
					Design by <b class="me-3"><a href="{{url('/')}}">{{env('APP_NAME')}}</a></b> | 

					<b class="ms-3"><a href="{{url('home')}}">Administrar</a></b>
				</p>
			</div>
		</div>
	</footer>
	@include('helpers.footer')
</body>
</html>