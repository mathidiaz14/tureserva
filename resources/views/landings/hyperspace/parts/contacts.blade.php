<section class="content style1" id="contact">
	<div class="inner">
		<div class="row">
			<div class="col-12">
				<h2>{{__('web.contact')}}</h2>
				<br><br>
			</div>
			<div class="col-12 col-md-8 contact_form">
				@include('helpers.alert_login')
				<form action="{{url('web/contact')}}" class="form-horizontal" method="post">
					@csrf
					<input type="hidden" name="business" value="{{$business->id}}">
					<div class="row">
						<div class="col">
							<div class="form-group">
								<div class="row">
									<div class="col-12">
										<h4>{{__('web.name')}}</h4>
									</div>
									<div class="col-12">
										<input type="text" name="name" placeholder="{{__('web.name')}}" required>
									</div>
								</div>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<div class="row">
									<div class="col-12">
										<h4>{{__('web.email')}}</h4>
									</div>
									<div class="col-12">
										<input type="email" name="email" placeholder="{{__('web.email')}}" required>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-4">
						<div class="col-12">
							<h4>Mensaje</h4>
						</div>
						<div class="col-12">
							<textarea name="message" id="" cols="30" rows="8" placeholder="{{__('web.message')}}"></textarea>
						</div>
					</div>	
					<div class="row mt-4">
						<div class="col">
							<button class="button">
								{{__('web.send')}}
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="col-12 col-md-4 contact_info">
				<div class="row">
					<div class="col-12">
						<h3>{{__('web.address')}}</h3>
						<p style="color: #bbbbbb;">{{landing('address', $business->id)}}</p>
					</div>
					<div class="col-12">
						<h3>{{__('web.email')}}</h3>
						<p style="color: #bbbbbb;">{{landing('email', $business->id)}}</p>
					</div>
					<div class="col-12">
						<h3>{{__('web.phone')}}</h3>
						<p style="color: #bbbbbb;">{{landing('phone', $business->id)}}</p>
					</div>
					<div class="col-12">
						<h3>Social</h3>
						<ul class="icons list-inline">
							@if(landing('twitter', $business->id) != null)
								<li class="list-inline-item m-2">
									<a href="{{landing('twitter', $business->id)}}">
										<i class="fab fa-2x fa-twitter"></i>
									</a>
								</li>
							@endif
							@if(landing('facebook', $business->id) != null)
								<li class="list-inline-item m-2">
									<a href="{{landing('facebook', $business->id)}}">
										<i class="fab fa-2x fa-facebook"></i>
									</a>
								</li>
							@endif
							@if(landing('instagram', $business->id) != null)
								<li class="list-inline-item m-2">
									<a href="{{landing('instagram', $business->id)}}">
										<i class="fab fa-2x fa-instagram"></i>
									</a>
								</li>
							@endif
						</ul>
					</li>
					</div>
				</div>
			</div>
		</div>			
	</div>
</section>