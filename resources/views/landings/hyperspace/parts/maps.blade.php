<section class="content style4" id="map">
	<div class="inner">
		<div class="row">
			<div class="col-12">
				<h2>{{__('web.map')}}</h2>
			</div>
			<div class="col-12 p-3">
				<iframe width="100%" height="400" src="https://maps.google.com/maps?q={{$business->address}}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
			</div>
		</div>
	</div>
</section>