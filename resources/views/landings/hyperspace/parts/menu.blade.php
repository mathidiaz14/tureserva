<nav class="navigate">
	<div class="menu">
		<ul>
			<li>
				<a href="#top" class="link active">
					<p>Inicio</p>
				</a>
			</li>
			<li>
				<a href="#one" class="link">
					<p>Reserva</p>
				</a>
			</li>

			@if(landing('gallery', $business->id) == "on")
				<li>
					<a href="#gallery" class="link">
						<p>Galería</p>
					</a>
				</li>
			@endif

			@if(landing('tips', $business->id) == "on")
				<li>
					<a href="{{$business->code}}/tips" class="link">
						<p>Consejos</p>
					</a>
				</li>
			@endif

			@if(landing('map', $business->id) == "on")
				<li>
					<a href="#map" class="link">
						<p>Ubicación</p>
					</a>
				</li>
			@endif

			@if(landing('hours', $business->id) == "on")
				<li>
					<a href="#hours" class="link">
						<p>Horario</p>
					</a>
				</li>
			@endif

			@if(landing('opinions', $business->id) == "on")
				<li>
					<a href="{{$business->code}}/opinions" class="link">
						<p>Opiniones</p>
					</a>
				</li>
			@endif

			@if(landing('contact', $business->id) == "on")
				<li>
					<a href="#contact" class="link">
						<p>Contacto</p>
					</a>
				</li>
			@endif
		</ul>
	</div>	
</nav>