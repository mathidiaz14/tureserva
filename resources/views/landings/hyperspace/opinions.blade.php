<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{$business->name}}</title>
	
	<link rel="stylesheet" href="{{asset('landings/hyperspace/assets/style.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="{{asset('landings/hyperspace/assets/style.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>

	@if($business->logo != null)
		<link rel="shortcut icon" type="image" href="{{asset($business->logo)}}">
	@else
		<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
	@endif

	<style>
		.navigate
		{
			background: {{landing('sidebar_background', $business->id)}}!important;
		}

		body
		{
			background: {{landing('style1', $business->id)}};	
		}

		.style2
		{
			background: {{landing('style2', $business->id)}};	
		}

		.style3
		{
			background: {{landing('style3', $business->id)}};	
		}

		.style4
		{
			background: {{landing('style4', $business->id)}};	
		}
	</style>

</head>
<body>
	
	@include('helpers.loading')

	<header>
		<div class="row">
			<div class="col">
				<h1>{{$business->name}}</h1>
			</div>
			<div class="col text-end">
				<a href="{{url($business->code)}}" class="button">
					Volver
				</a>
			</div>
		</div>
	</header>

	<div class="container-fluid">
		@include('landings.hyperspace.parts.alerts')

		<section class="style2" id="opinions">
			<div class="inner">
				@if($business->opinions->count() > 0)
					<div class="row">
						<div class="col-12 text-center">
							<h2>{{__('web.all_opinions')}}</h2>
						</div>
						<div class="col-12">
							<div class="row">
								@foreach($business->opinions as $opinion)
									<div class="col-12 p-4 mt-3 opinion">
										<div class="row">
											<div class="col">
												<p>{{$opinion->star}} <i class="fa fa-star"></i></p>
											</div>
											<div class="col text-end">
												<p>{{$opinion->created_at->format('d/m/Y')}}</p>
											</div>
										</div>
										{{$opinion->comment}}
									</div>
								@endforeach
							</div>
						</div>
					</div>
				@else
					<div class="row">
						<div class="col-12 text-center p-4">
							<i class="fa fa-exclamation-triangle fa-3x"></i>
							<br><br>
							<p>{{__('web.not_opinions')}}</p>
						</div>
					</div>
				@endif
			</div>
		</section>
	</div>
	<footer>
		<div class="content-alt style1">
			<div class="inner">
				<p>
					Design by <b class="me-3"><a href="{{url('/')}}">{{env('APP_NAME')}}</a></b> | 

					<b class="ms-3"><a href="{{url('home')}}">Administrar</a></b>
				</p>
			</div>
		</div>
	</footer>
</body>
</html>