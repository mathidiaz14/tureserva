@extends('layouts.dashboard')

@section('title')
    Ayuda
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-xs-12">
                        <h4>Chat en linea</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
    					<div id="lhc_status_container_page" ></div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- Coloque esta etiqueta después de la etiqueta &#039;&#039;Live Helper Plugin&#039;&#039;. -->
<script type="text/javascript">
	var LHCChatOptionsPage = {};
	LHCChatOptionsPage.opt = {};
	(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	var referrer = (document.referrer) ? encodeURIComponent(document.referrer.substr(document.referrer.indexOf('://')+1)) : '';
	var location  = (document.location) ? encodeURIComponent(window.location.href.substring(window.location.protocol.length)) : '';
	po.src = 'http://mathiasdiaz.uy/chat/index.php/esp/chat/getstatusembed/(leaveamessage)/true/(department)/1?r='+referrer+'&l='+location;
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>
@endsection

