<!-- Notifications Dropdown Menu -->

@php 
    $notifications  = business()->notifications;
    $news           = business()->notifications->where('status', 'new');
    $pendings       = business()->notifications->where('status', 'pending');
    $enabled        = business()->notifications->where('status', 'enabled');
@endphp

<li id="notifications" class="nav-item dropdown">
	<a id="menu_notification" class="nav-link" data-toggle="dropdown" href="#">
	  	<i class="far fa-bell"></i>
	  	@if(count($pendings) + count($news) > 0)
	  		<span class="badge badge-warning navbar-badge" id="notification_message">
	  			{{count($pendings) + count($news)}}
	  		</span>
		@endif
	</a>
	<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
		<span class="dropdown-item dropdown-header">
			{{$notifications->count()}} Notificaciones
		</span>
		<div class="dropdown-divider"></div>
	  	
		@if($notifications->count() == 0)
		  	<a href="#" class="dropdown-item">
		    	{{__('message.not_notifications')}}
		  	</a>
		@else
			@foreach($notifications->take(8) as $notification)
				<a href="{{url('notification', $notification->id)}}" class="dropdown-item">
					<div class="row">
						<div class="col-12">
							<p>
								{{$notification->title}}
							</p>
						</div>
						<div class="col-6">
							@if (($notification->status == "pending") or ($notification->status == "new"))
				                <span class="badge bg-success">
				                    {{__('message.new')}}
				                </span>
				            @elseif ($notification->status == "enabled")
				                <span class="badge bg-info">
				                    {{__('message.not_reader')}}
				                </span>
				            @endif
						</div>
						<div class="col-6 text-right">
							<span class="float-right text-muted text-sm">
								@if($notification->created_at->diffInMinutes(\Carbon\Carbon::now()) < 60)
			                        {{$notification->created_at->diffInMinutes(\Carbon\Carbon::now())}} Min
			                    @elseif($notification->created_at->diffInHours(\Carbon\Carbon::now()) < 24)
			                        {{$notification->created_at->diffInHours(\Carbon\Carbon::now())}} hor
			                    @elseif($notification->created_at->diffInHours(\Carbon\Carbon::now()) < 48)
			                        {{$notification->created_at->diffInDays(\Carbon\Carbon::now())}} dia
			                    @else
			                        {{$notification->created_at->diffInDays(\Carbon\Carbon::now())}} dias
			                    @endif 
							</span>
						</div>
					</div>


				</a>
        	@endforeach
		@endif

	  	<div class="dropdown-divider"></div>
	  	<a href="{{url('notification')}}" class="dropdown-item dropdown-footer">
	  		{{__('message.view_all')}}
	  	</a>
	</div>
</li>


<script>
    $(document).ready(function()
    {   
        @foreach($news as $new)
            tureserva_notification_message("{{$new->body}}", 'success');

            @php
                $new->status = "pending";
                $new->save();
            @endphp

        @endforeach

        $('#menu_notification').click(function()
        {
            $.get("{{url('read/notification')}}");
            $("#notification_message").fadeOut();
        });
    });
</script>