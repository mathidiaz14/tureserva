<!-- Modal -->
<div class="modal fade" id="modalHelp" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-center" role="document">
      <div class="modal-content">
      <div class="modal-header bg-gradient-info">
          <div class="col-6">
            <h6>
              <i class="fa fa-exclamation-circle"></i>
              Reportar error
            </h6>
          </div>
          <div class="col-6 text-right">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      </div>
      <div class="modal-body">
        <form action="{{url('incident')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <input type="hidden" id="screen" name="screen">
          <div class="row">
            <div class="col">
              <div class="form-group">
                <label for="">Usuario</label>
                <input type="text" class="form-control" value="{{Auth::user()->name}}" readonly="">
              </div>
            </div>
            <div class="col">
              <div class="form-group">
                <label for="">URL</label>
                <input type="text" class="form-control" name="url" value="{{Request::fullUrl()}}" readonly="">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="">Mensaje</label>
            <textarea name="content" rows="3" class="form-control" required="" placeholder="¿Cuál es el error?"></textarea>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <label for="">Captura de pantalla</label>
                  <a class="btn btn-info text-white px-4 btn_capture_screen btn-block">
                    <i class="fa fa-laptop"></i>
                    Capturar
                  </a>
                </div>
              </div>
              <div class="col">
              	<div class="form-group">
              		<label for="">Adjunto</label>
	                <div class="upload-btn-wrapper" style="width: 100%;">
					  	<button class="btn btn-info btn-block" style="margin-bottom: 0;">
					  		<i class="fa fa-arrow-up"></i>
					  		{{__('message.upload_file')}}
					  	</button>
					  	<input type="file" name="attached" />
	              	</div>
				</div>
              </div>
              <div class="col-12">
                <span id="mensaje_screen" class="text-success mt-1" style="display: none;">
                    <i class="fa fa-check-circle fa-2x mx-2"></i>
                    <b>Captura realizada</b>
                  </span>
              </div>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col">
              <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                <i class="fa fa-times"></i>
                Cerrar
              </button>
            </div>
            <div class="col text-right">
              <button class="btn btn-info btn-block">
                <i class="fa fa-paper-plane"></i>
                Enviar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>