<!-- contacts Dropdown Menu -->

@php 
    $contacts       = business()->contacts;
    $news           = business()->contacts->where('status', 'new');
    $pendings       = business()->contacts->where('status', 'pending');
    $enabled        = business()->contacts->where('status', 'enabled');
@endphp

<li id="contacts" class="nav-item dropdown">
  <a id="menu_contact" class="nav-link" data-toggle="dropdown" href="#">
      <i class="far fa-comments"></i>
      @if(count($pendings) + count($news) > 0)
        <span class="badge badge-warning navbar-badge" id="contact_message">
          {{count($pendings) + count($news)}}
        </span>
    @endif
  </a>
  <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
    <span class="dropdown-item dropdown-header">
      {{$contacts->count()}} Mensajes
    </span>
    <div class="dropdown-divider"></div>
      
    @if($contacts->count() == 0)
        <a href="#" class="dropdown-item">
          {{__('message.not_contacts')}}
        </a>
    @else
      @foreach($contacts->take(8) as $contact)
        <a href="{{url('contact', $contact->id)}}" class="dropdown-item">
          <div class="row">
            <div class="col-12">
              <p>
                Contacto desde la web
              </p>
            </div>
            <div class="col-6">
              @if (($contact->status == "pending") or ($contact->status == "new"))
                <span class="badge bg-success">
                    {{__('message.new')}}
                </span>
              @elseif ($contact->status == "enabled")
                <span class="badge bg-info">
                    {{__('message.not_reader')}}
                </span>
              @endif
            </div>
            <div class="col-6 text-right">
              <span class="float-right text-muted text-sm">
                @if($contact->created_at->diffInMinutes(\Carbon\Carbon::now()) < 60)
                    {{$contact->created_at->diffInMinutes(\Carbon\Carbon::now())}} Min
                @elseif($contact->created_at->diffInHours(\Carbon\Carbon::now()) < 24)
                    {{$contact->created_at->diffInHours(\Carbon\Carbon::now())}} hor
                @elseif($contact->created_at->diffInHours(\Carbon\Carbon::now()) < 48)
                    {{$contact->created_at->diffInDays(\Carbon\Carbon::now())}} dia
                @else
                    {{$contact->created_at->diffInDays(\Carbon\Carbon::now())}} dias
                @endif 
              </span>
            </div>
          </div>


        </a>
          @endforeach
    @endif

      <div class="dropdown-divider"></div>
      <a href="{{url('contact')}}" class="dropdown-item dropdown-footer">
        {{__('message.view_all')}}
      </a>
  </div>
</li>


<script>
  $(document).ready(function()
  {
    @foreach($news as $new)
        tureserva_notification_message("Nuevo contacto desde la pagina", 'success');

        @php
            $new->status = "pending";
            $new->save();
        @endphp

    @endforeach

    $('#menu_contact').click(function()
    {
        $.get("{{url('read/contact')}}");
        $("#contact_message").fadeOut();
    });
  });
</script>