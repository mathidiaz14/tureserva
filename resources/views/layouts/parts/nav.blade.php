 <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('/')}}" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      @include('layouts.parts.messages')

      @include('layouts.parts.notifications')

      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>

      <!-- User Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <span>
            <p>{{user()->name}}
            <i class="fa fa-chevron-down ml-1"></i></p>
          </span>
        </a>
        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
          <a href="{{url('profile')}}" class="dropdown-item">
            <i class="fa fa-user"></i>
            Mi perfil
          </a>
          <div class="dropdown-divider"></div>
          <a href="" class="dropdown-item text-secondary" data-toggle="modal" data-target="#modalHelp">
              <i class="fa fa-exclamation-circle"></i>
              Reportar error
          </a>
          <a href="" class="dropdown-item text-secondary" data-toggle="modal" data-target="#modalControl">
              <i class="fa fa-laptop-house"></i>
              Conceder control
          </a>
          <a href="{{url('ayuda')}}" class="dropdown-item text-secondary" target="_blank">
              <i class="fa fa-question"></i>
              Ayuda
          </a>
          <div class="dropdown-divider"></div>
          <a href="" class="dropdown-item" data-toggle="modal" data-target="#modalCerrarSesion">
              <i class="fa fa-sign-out-alt"></i>
              Cerrar Sesión
          </a>
        </div>
      </li>
    </ul>
  </nav>

  <div class="modal fade" id="modalCerrarSesion" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <div class="modal-header bg-gradient-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body text-center text-secondary">
          <p><i class="fa fa-question-circle fa-3x"></i></p>
            <h4>¿Seguro que desea cerrar la sesión?</h4>
            <br>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                  NO
                </button>
              </div>
              <div class="col">
                <form action="{{ url('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-info btn-block">
                        SI
                    </button>
                </form>
              </div>
            </div>
        </div>
      </div>
  </div>
</div>

@include('layouts.parts.incident')

@include('layouts.parts.remote_control')

<script>
  $(document).ready(function()
  {
    $('.btn_capture_screen').click(function()
    {
      $('#modalHelp').hide();
      $('.modal-backdrop').removeClass('show');
      
      var contenido = $('html').html();
      
      $('.modal-backdrop').addClass('show');
      $('#modalHelp').show();
      $('#screen').val(contenido);
      $('#mensaje_screen').fadeIn();
    });

    $('.btn_generate_control_code').click(function()
    {
      $.get("{{url('business/generate/code')}}", function(result)
      {
        $('#code_control').html(result);
      });
    });

  });
</script>
