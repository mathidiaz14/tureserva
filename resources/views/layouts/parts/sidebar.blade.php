<aside class="main-sidebar sidebar-dark-info elevation-4">
    <!-- Brand Logo -->
    <a href="@if(user()->type != 'root'){{url(business()->code)}} @endif" class="brand-link" target="_blank">
      <img src="{{asset('images/icon3.png')}}" alt="{{env('APP_NAME')}}" class="brand-image" style="opacity: .8">
      <span class="brand-text font-weight-light ml-3">{{env('APP_NAME')}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      
      	<div class="user-panel mt-3 pb-3 mb-3 d-flex">
	        @if(user()->avatar != null)
	        	<div class="image">
		          	<img src="{{user()->avatar}}" class="img-circle elevation-2" alt="User Image">
		        </div>
	        @endif
	        <div class="info">
	        	<span class="text-white">@if(Auth::user()->gender == "male")
                    {{__('message.welcome')}}
                @elseif(Auth::user()->gender == "female")
                    {{__('message.welcome1')}}
                @else
                    {{__('message.welcome2')}}
                @endif</span>
	          	<a href="#" class="d-block">{{user()->name}}</a>
	        </div>
      	</div>

      	<!-- Sidebar Menu -->
      	<nav class="mt-2">
        	<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          		<li class="nav-item">
		            <a href="{{url('home')}}" class="nav-link {{$menu == 'home' ? 'active' : ''}}">
		              	<i class="nav-icon fas fa-tachometer-alt"></i>
		              	<p>
		                	Inicio
		              	</p>
		            </a>
          		</li>
	          	<li class="nav-item">
		            <a href="{{url('calendar')}}" class="nav-link {{$menu == 'calendar' ? 'active' : ''}}">
		              	<i class="nav-icon fas fa-calendar-alt"></i>
		              	<p>
		                	Agenda
		              	</p>
		            </a>
	          	</li>
          		<li class="nav-item 
          			@if(
          			($menu == 'client') or 
          			($menu == 'client_create'))
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-user"></i>
          				<p>
                			Clientes
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('client')}}" class="nav-link {{$menu == 'client' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Ver clientes</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('client/create')}}" class="nav-link {{$menu == 'client_create' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Crear cliente</p>
	                		</a>
		              	</li>
		            </ul>
          		</li>
          		<li class="nav-item
          			@if(
          			($menu == 'service') or 
          			($menu == 'service_create'))
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-life-ring"></i>
          				<p>
                			Servicios
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('service')}}" class="nav-link {{$menu == 'service' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Ver servicios</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('service/create')}}" class="nav-link {{$menu == 'service_create' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Crear servicio</p>
	                		</a>
		              	</li>
		            </ul>
          		</li>
          		<li class="nav-item
          			@if(
          			($menu == 'resource') or 
          			($menu == 'resource_create'))
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-bicycle"></i>
          				<p>
                			Recursos
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('resource')}}" class="nav-link {{$menu == 'resource' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Ver recursos</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('resource/create')}}" class="nav-link {{$menu == 'resource_create' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Crear recurso</p>
	                		</a>
		              	</li>
		            </ul>
          		</li>
          		<li class="nav-item
          			@if(
          			($menu == 'user') or 
          			($menu == 'user_create'))
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-users"></i>
          				<p>
                			Empleados
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('user')}}" class="nav-link {{$menu == 'user' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Ver empleados</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('user/create')}}" class="nav-link {{$menu == 'user_create' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Crear empleado</p>
	                		</a>
		              	</li>
		            </ul>
          		</li>
          		<li class="nav-item
          			@if(
          			($menu == 'absence') or 
          			($menu == 'absence_create'))
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-calendar"></i>
          				<p>
                			Ausencias
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('absence')}}" class="nav-link {{$menu == 'absence' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Ver ausencias</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('absence/create')}}" class="nav-link {{$menu == 'absence_create' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Crear ausencia</p>
	                		</a>
		              	</li>
		            </ul>
          		</li>
          		<li class="nav-item
          			@if(($menu == 'business') or
						($menu == 'reservation') or
						($menu == 'mercadopago') or
						($menu == 'statistics') or
						($menu == 'upgrade') or
						($menu == 'notification') or
						($menu == 'message') or
						($menu == 'incident') or
						($menu == 'upgrade_details') or
						($menu == 'contact') 
					)
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-wrench"></i>
          				<p>
                			Administrar
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('business')}}" class="nav-link {{$menu == 'business' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Datos del negocio</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('reservation')}}" class="nav-link {{$menu == 'reservation' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Configurar reserva</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
                            <a href="{{url('mercadopago')}}" class="nav-link {{$menu == 'mercadopago' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>MercadoPago</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('incident')}}" class="nav-link {{$menu == 'incident' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Incidencias</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('statistics')}}" class="nav-link {{$menu == 'statistics' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Estadisticas</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('notification')}}" class="nav-link {{$menu == 'notification' ? 'active' : ''}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Notificaciones</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('contact')}}" class="nav-link {{$menu == 'contact' ? 'active' : ''}}">
								<i class="far fa-circle nav-icon"></i>
								<p>Contacto </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('message')}}" class="nav-link {{$menu == 'message' ? 'active' : ''}}">
								<i class="far fa-circle nav-icon"></i>
								Mensajes
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('upgrade')}}" class="nav-link {{$menu == 'upgrade' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Mejorar plan</p>
                            </a>
                        </li>
                        
                        @if(business()->plan != "free")
	                        <li class="nav-item">
	                            <a href="{{url('details/upgrade')}}" class="nav-link {{$menu == 'upgrade_details' ? 'active' : ''}}">
	                                <i class="far fa-circle nav-icon"></i>
	                                <p>Facturación</p>
	                            </a>
	                        </li>
                        @endif
		            </ul>
          		</li>
          		<li class="nav-item
          			@if(($menu == 'theme_edit') or
						($menu == 'theme') or
						($menu == 'tips') or
						($menu == 'media')
					)
          				menu-open
          			@endif
          		">
            		<a href="#" class="nav-link">
              			<i class="nav-icon fas fa-palette"></i>
          				<p>
                			Apariencia
                			<i class="fas fa-angle-left right"></i>
              			</p>
            		</a>
		            <ul class="nav nav-treeview pl-1">
		              	<li class="nav-item">
		                	<a href="{{url('theme/1/edit')}}" class="nav-link {{$menu == 'theme_edit' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Personalizar visual</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('theme')}}" class="nav-link {{$menu == 'theme' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Cambiar tema visual</p>
	                		</a>
		              	</li>
		              	<li class="nav-item">
		                	<a href="{{url('tips')}}" class="nav-link {{$menu == 'tips' ? 'active' : ''}}">
		                  		<i class="far fa-circle nav-icon"></i>
		                  		<p>Tips para usuarios</p>
	                		</a>
		              	</li>
                        <li class="nav-item mb-3">
                            <a href="{{url('media')}}" class="nav-link {{$menu == 'media' ? 'active' : ''}}">
								<i class="far fa-circle nav-icon"></i>
								Multimedia
                            </a>
                        </li>
		            </ul>
          		</li>
        	</ul>
      	</nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>