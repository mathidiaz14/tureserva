<!-- Modal -->
<div class="modal fade" id="modalControl" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-center" role="document">
      <div class="modal-content">
      <div class="modal-header bg-dark-info">
          <div class="col-6">
            <p>
              <i class="fa fa-laptop-house"></i>
              Conceder control
            </p>
          </div>
          <div class="col-6 text-right">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      </div>
      <div class="modal-body">
        <div class="row text-center">
          <div class="col-12">
            <h1 id="code_control">######</h1>
            <hr>
          </div>
          <div class="col-12 mt-4">
            <a class="btn btn-info btn-lg btn_generate_control_code text-white">
              Generar codigo
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>