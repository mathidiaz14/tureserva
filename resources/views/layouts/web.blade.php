<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{$business->name}}</title>

  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  @if($business->logo != null)
    <link rel="shortcut icon" type="image/png" href="{{asset($business->logo)}}">
  @else
    <link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
  @endif

</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="{{url('web', $business->code)}}">{{$business->name}}</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        @yield('nav')
      </div>
    </div>
  </nav>

  <!-- Header - set the background image for the header in the line below -->
    <header class="py-5 bg-image-full" style="
      @if($business->reservation->banner == null)
        background-image: url({{asset('images/banner.jpg')}});
      @else
        background-image: url({{asset($business->reservation->banner)}});
      @endif
    ">
    @if($business->logo != null)
      <img class="img-fluid d-block mx-auto" src="{{asset($business->logo)}}" width="200px" height="200px" alt="">
    @else
      <div class="mx-auto logo_null">
            <h3><b>@php echo strtoupper(substr($business->name, 0, 1)); @endphp</b></h3>
          </div>
    @endif
  </header>


  @yield('content')
  
  
  <section class="py-5 bg-image-full" style="
     @if($business->reservation->footer == null)
        background-image: url({{asset('images/footer.jpg')}});
      @else
        background-image: url({{asset($business->reservation->footer)}});
      @endif
  ">
    <div style="height: 100px;"></div>
  </section>

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <a href="{{config('app.url')}}"><p class="m-0 text-center text-white">{{config('app.name')}}</p></a>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
  @yield('scripts')
</body>

</html>
