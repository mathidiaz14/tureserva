<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>@yield('title') - {{env('APP_NAME')}}</title>

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/fontawesome-free/css/all.min.css')}}">
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/jqvmap/jqvmap.min.css')}}">
		<link rel="stylesheet" href="{{asset('dashboard/css/adminlte.min.css')}}">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/daterangepicker/daterangepicker.css')}}">
		<link rel="stylesheet" href="{{asset('dashboard/plugins/summernote/summernote-bs4.min.css')}}">
		<link href="{{asset('css/style.css')}}" rel="stylesheet">
		<link rel="shortcut icon" type="image/png" href="{{asset('images/icon2.png')}}">
		
		<script src="{{asset('dashboard/plugins/jquery/jquery.min.js')}}"></script>
		
		@yield('css')
	</head>
	<body class="hold-transition sidebar-mini layout-fixed">
		<div class="wrapper">
			
			<!-- Ayudas de funcionamiento -->
			
			@include('helpers.alert')
			@include('helpers.loading')

			<!-- Fin Ayudas -->

		  	<!-- Preloader -->
		  	<div class="preloader flex-column justify-content-center align-items-center">
		    	<img class="animation__shake" src="{{asset('images/icon.png')}}" alt="" height="60" width="60">
		  	</div>

		  	<!-- Navbar -->
	 		@include('layouts.parts.nav')
		 	

		  	<!-- Main Sidebar Container -->
		  	@include('layouts.parts.sidebar')

		  	<!-- Content Wrapper. Contains page content -->
		  	<div class="content-wrapper">
		  		<section class="content">
		  			<div class="container-fluid py-4">
		  				@yield('content')
		  			</div>
		  		</section>
		 	</div>

		  	<!-- /.content-wrapper -->
		  	@include('layouts.parts.footer')

		</div>
	<!-- ./wrapper -->

		<script src="{{asset('dashboard/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
		<script>
		  $.widget.bridge('uibutton', $.ui.button)
		</script>
		<script src="{{asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
		<script src="{{asset('dashboard/plugins/chart.js/Chart.min.js')}}"></script>
		<script src="{{asset('dashboard/plugins/sparklines/sparkline.js')}}"></script>
		<script src="{{asset('dashboard/plugins/moment/moment.min.js')}}"></script>
		<script src="{{asset('dashboard/plugins/daterangepicker/daterangepicker.js')}}"></script>
		<script src="{{asset('dashboard/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
		<script src="{{asset('dashboard/plugins/summernote/summernote-bs4.min.js')}}"></script>
		<script src="{{asset('dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
		<script src="{{asset('dashboard/js/adminlte.js')}}"></script>

		<script async src="https://www.googletagmanager.com/gtag/js?id=G-1VT242W818"></script>
	    <script>
	      window.dataLayer = window.dataLayer || [];
	      function gtag(){dataLayer.push(arguments);}
	      gtag('js', new Date());

	      gtag('config', 'G-1VT242W818');
	    </script>

		<script>
			$(document).ready(function()
			{
				$(document).on("keydown", function(e) 
		        {
		            var code = e.keyCode || e.which;
		            if(code == 27) 
		                $('.modal').modal('hide');
		        });

      			var refreshId = setInterval(function(){ $('#contacts').load("{{url('view/contact')}}"); }, 30000);

        		var refreshId = setInterval(function(){ $('#notifications').load("{{url('view/notification')}}"); }, 30000);
			});
		</script>

		@yield('scripts')
	</body>
</html>
