<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title') - {{env('APP_NAME')}}</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/jqvmap/jqvmap.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/css/adminlte.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/daterangepicker/daterangepicker.css')}}">
        <link rel="stylesheet" href="{{asset('dashboard/plugins/summernote/summernote-bs4.min.css')}}">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <link rel="shortcut icon" type="image/png" href="{{asset('images/icon2.png')}}">
        <script src="{{asset('dashboard/plugins/jquery/jquery.min.js')}}"></script>
        
        @yield('css')
    </head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            
            <!-- Ayudas de funcionamiento -->
            
            @include('helpers.alert')
            @include('helpers.loading')

            <!-- Fin Ayudas -->

            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="{{asset('images/icon.png')}}" alt="AdminLTELogo" height="60" width="60">
            </div>

            <!-- Navbar -->
             <nav class="main-header navbar navbar-expand navbar-info navbar-dark">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                  </li>
                  <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{url('/')}}" class="nav-link">Home</a>
                  </li>
                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">
                  <!-- Navbar Search -->
                  <li class="nav-item">
                    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                      <i class="fas fa-search"></i>
                    </a>
                    <div class="navbar-search-block">
                      <form class="form-inline">
                        <div class="input-group input-group-sm">
                          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                          <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                              <i class="fas fa-search"></i>
                            </button>
                            <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                              <i class="fas fa-times"></i>
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                      <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                  </li>

                  <!-- User Dropdown Menu -->
                  <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                      <span>
                        {{user()->name}}
                        <i class="fa fa-chevron-down ml-1"></i>
                      </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                      <a href="{{url('profile')}}" class="dropdown-item">
                        <i class="fa fa-user"></i>
                        Mi perfil
                      </a>
                      <div class="dropdown-divider"></div>
                      <a href="" class="dropdown-item" data-toggle="modal" data-target="#modalCerrarSesion">
                          <i class="fa fa-sign-out-alt"></i>
                          Cerrar Sesión
                      </a>
                    </div>
                  </li>
                </ul>
              </nav>

              <div class="modal fade" id="modalCerrarSesion" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
              <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                  <div class="modal-header bg-gradient-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body text-center text-secondary">
                      <p><i class="fa fa-question-circle fa-3x"></i></p>
                        <h4>¿Seguro que desea cerrar la sesión?</h4>
                        <br>
                        <hr>
                        <div class="row">
                          <div class="col">
                            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                              NO
                            </button>
                          </div>
                          <div class="col">
                            <form action="{{ url('logout') }}" method="POST">
                                @csrf
                                <button class="btn btn-info btn-block">
                                    SI
                                </button>
                            </form>
                          </div>
                        </div>
                    </div>
                  </div>
              </div>
            </div>

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-info elevation-4">
              <!-- Brand Logo -->
              <a href="@if(user()->type != 'root'){{url(business()->code)}} @endif" class="brand-link" target="_blank">
                <img src="{{asset('images/icon.png')}}" alt="{{env('APP_NAME')}}" class="brand-image" style="opacity: .8">
                <span class="brand-text font-weight-light ml-3">{{env('APP_NAME')}}</span>
              </a>

              <!-- Sidebar -->
              <div class="sidebar">
                
                  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    @if(user()->avatar != null)
                      <div class="image">
                          <img src="{{user()->avatar}}" class="img-circle elevation-2" alt="User Image">
                      </div>
                    @endif
                    <div class="info">
                      <span class="text-white">@if(Auth::user()->gender == "male")
                              {{__('message.welcome')}}
                          @elseif(Auth::user()->gender == "female")
                              {{__('message.welcome1')}}
                          @else
                              {{__('message.welcome2')}}
                          @endif</span>
                        <a href="#" class="d-block">{{user()->name}}</a>
                    </div>
                  </div>

                  <!-- Sidebar Menu -->
                  <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                      <li class="nav-item">
                        <a href="{{url('home')}}" class="nav-link {{$menu == 'home' ? 'active' : ''}}">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>
                              Inicio
                            </p>
                        </a>
                      </li>
                        <li class="nav-item">
                            <a href="{{url('root/business')}}" class="nav-link {{$menu == 'business' ? 'active' : ''}}">
                                <i style="margin-right:10px;" class="fas fa-building"></i> Empresas 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('root/user')}}" class="nav-link {{$menu == 'user' ? 'active' : ''}}">
                                <i style="margin-right:10px;" class="fas fa-users"></i> Usuarios 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('root/email')}}" class="nav-link {{$menu == 'notification' ? 'active' : ''}}">
                                <i style="margin-right:10px;" class="fas fa-envelope"></i> Probar notificaciones
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('root/category/help')}}" class="nav-link {{$menu == 'help' ? 'active' : ''}}">
                                <i style="margin-right:10px;" class="fas fa-question-circle"></i> Ayuda 
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('root/admin')}}" class="nav-link {{$menu == 'manage' ? 'active' : ''}}">
                                <i style="margin-right:10px;" class="fas fa-toolbox"></i> Administrar
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('root/cron')}}" class="nav-link {{$menu == 'cron' ? 'active' : ''}}">
                                <i style="margin-right:10px;" class="fas fa-calendar"></i> Registros CRON
                            </a>
                        </li>
                    </ul>
                  </nav>
                <!-- /.sidebar-menu -->
              </div>
              <!-- /.sidebar -->
          </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content">
                    <div class="container-fluid py-4">
                        @yield('content')
                    </div>
                </section>
            </div>

            <!-- /.content-wrapper -->
            @include('layouts.parts.footer')

        </div>
    <!-- ./wrapper -->

        <script src="{{asset('dashboard/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
        <script>
          $.widget.bridge('uibutton', $.ui.button)
        </script>
        <script src="{{asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('dashboard/plugins/chart.js/Chart.min.js')}}"></script>
        <script src="{{asset('dashboard/plugins/sparklines/sparkline.js')}}"></script>
        <script src="{{asset('dashboard/plugins/moment/moment.min.js')}}"></script>
        <script src="{{asset('dashboard/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{asset('dashboard/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
        <script src="{{asset('dashboard/plugins/summernote/summernote-bs4.min.js')}}"></script>
        <script src="{{asset('dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
        <script src="{{asset('dashboard/js/adminlte.js')}}"></script>

        <script>
            $(document).ready(function()
            {
                $(document).on("keydown", function(e) 
                {
                    var code = e.keyCode || e.which;
                    if(code == 27) 
                        $('.modal').modal('hide');
                });
            });
        </script>
        @yield('scripts')
    </body>
</html>
