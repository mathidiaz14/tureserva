<!DOCTYPE HTML>

<html>
	<head>
		<title>{{env('APP_NAME')}} - Ayuda</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="{{asset('help/assets/css/main.css')}}" />
		<noscript><link rel="stylesheet" href="{{asset('help/assets/css/noscript.css')}}" /></noscript>
		<link rel="icon" type="image/png" href="{{asset('images/icon.png')}}">

	</head>
	<body class="is-preload">
		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<div class="inner">
							<!-- Logo -->
							<a href="{{url('ayuda')}}" class="logo">
								<span class="symbol"><img src="{{asset('images/icon.png')}}" alt="" /></span><span class="title">{{env('APP_NAME')}}</span>
							</a>
						</div>
					</header>


					@yield('contenido')

				<!-- Footer -->
					<footer id="footer">
						<div class="inner">
							<section></section>
							<section>
								<ul class="icons">
									<li><a href="https://www.instagram.com/{{env('APP_NAME')}}/" target="_blank" class="icon brands style2 fa-instagram"><span class="label">Instagram</span></a></li>
									<li><a href="mailto:admin@{{env('APP_NAME')}}" class="icon solid style2 fa-envelope"><span class="label">Email</span></a></li>
								</ul>
							</section>
							<ul class="copyright">
								<li>Desarrollado por <a href="http://tutiendafacil.uy">{{env('APP_NAME')}}</a></li>
							</ul>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="{{asset('help/assets/js/jquery.min.js')}}"></script>
			<script src="{{asset('help/assets/js/browser.min.js')}}"></script>
			<script src="{{asset('help/assets/js/breakpoints.min.js')}}"></script>
			<script src="{{asset('help/assets/js/util.js')}}"></script>xº
			<script src="{{asset('help/assets/js/main.js')}}"></script>

	</body>
</html>