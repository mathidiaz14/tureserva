@extends('layouts.help')

@section('contenido')
<div id="main">
	<div class="inner">
		<header>
			<div class="row">
				<div class="col-6">
					<h1>{{$category->titulo}}</h1>
				</div>
				<div class="col-6" style="text-align:right; padding-top:20px;" >
					<h2><a href="{{url('ayuda')}}">Atras</a></h2>
				</div>
			</div>
		</header>
		<hr>
		<section class="tiles">
			@foreach($category->posts as $post)
				<article class="style{{rand(0,5)}}">
					<span class="image">
						<img src="{{asset('help/images/pic03.jpg')}}" alt="" />
					</span>
					<a href="{{url('ayuda', $post->id)}}">
						<h2>{{$post->title}}</h2>
					</a>
				</article>
			@endforeach
		</section>
	</div>
</div>
@endsection

@section('scripts')
	
@endsection