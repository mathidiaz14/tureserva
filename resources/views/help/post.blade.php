@extends('layouts.help')

@section('contenido')
<div id="main">
	<div class="inner">
		<header>
			<div class="row">
				<div class="col-6">
					<h1>{{$post->title}}</h1>
				</div>
				<div class="col-6" style="text-align:right; padding-top:20px;" >
					<h2><a href="{{url('ayuda/categoria', $post->category_id)}}">Atras</a></h2>
				</div>
			</div>
		</header>
		
		<p style="text-align:justify;">
			{!!$post->body!!}
		</p>
	</div>
</div>
@endsection

@section('scripts')
	
@endsection