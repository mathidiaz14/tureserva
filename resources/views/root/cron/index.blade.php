@extends('layouts.root', ['menu' => 'cron'])

@section('title')
    Trabajos CRON
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3>Reporte de trabajos CRON</h3>
                    </div>
                    <div class="col-6 text-right">
                    </div>
                </div>
            </div>
            <div class="card-body">
            	<div class="table table-responsive">
            		<table class="table table-striped">
            			<tr>
            				<th>Comando</th>	
            				<th>Retorno</th>
            				<th>Fecha</th>
            			</tr>
            			@foreach($crons as $cron)
            				<tr>
            					<td>{{$cron->command}}</td>
            					<td>{{$cron->return}}</td>
            					<td>{{$cron->created_at->format('d/m/Y H:i')}}</td>
            				</tr>
            			@endforeach
            		</table>
            	</div>	
            	@include('helpers.paginate', ['link' => $crons])
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
