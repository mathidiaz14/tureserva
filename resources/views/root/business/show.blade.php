@extends('layouts.root', ['menu' => 'business'])

@section('title')
	Empresa
@endsection

@section('content')

<div class="row">
	<div class="col-12">
		<h4>
			#{{$business->id}} {{$business->name}}
		</h4>
	</div>
</div>

<div class="row mt-4">
	<div class="col-6 col-md-3">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>
                    {{$business->users->count()}}
                </h3>
                <p>
                    Usuarios
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-users"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>
                    {{$business->tasks->count()}}
                </h3>
                <p>
                    Reservas
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-dark">
            <div class="inner">
                <h3>
                    {{$business->tasks->where('online', '!=', null)->count()}}
                </h3>
                <p>
                    Reservas online
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar-alt"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>
                    {{$business->logs->count()}}
                </h3>
                <p>
                    Logs
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-list"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card collapsed-card">
            <div class="card-header">
                <h3 class="card-title">Usuarios</h3>
                <div class="card-tools">
					<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
            			<i class="fas fa-plus"></i>
          			</button>
				</div>
            </div>
            <div class="card-body">
                <div class="col-xs 12 ">
                	<div class="table table-responsive">
	                	<table class="table table-striped">
	                		<tr>
	                			<th>Nombre</th>
	                			<th>Email</th>
	                			<th>Editar</th>
	                			<th>Eliminar</th>
	                		</tr>
	                		@foreach($business->users as $user)
								<tr>
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									<td>
										<a href="{{url('root/user', $user->id)}}/edit" class="btn btn-info">
											<i class="fa fa-edit"></i>
										</a>
									</td>
									<td>
										@include('helpers.delete_modal', ['delete_id' => $user->id, 'delete_route' => url('root/user', $user->id)])
									</td>
								</tr>
	                		@endforeach
	                	</table>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card collapsed-card">
            <div class="card-header">
                <h3 class="card-title">
                    Logs
                </h3>
                <div class="card-tools">
					<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
            			<i class="fas fa-plus"></i>
          			</button>
				</div>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Fecha</th>
                            <th>Empresa</th>
                            <th>Usuario</th>
                            <th>Sección</th>
                            <th>Acción</th>
                            <th>Mensaje</th>
                        </tr>

                        @foreach($logs as $log)
                            <tr>
                                <td>
                                    {{$log->created_at->format('d/m/Y H:i')}}
                                </td>
                                <td>
                                    <a href="{{url('root/business', $log->business_id)}}">
                                        {{$log->business->name}}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{url('root/user', $log->user_id)}}">
                                        {{$log->user->name}}
                                    </a>
                                </td>
                                <td>
                                    {{$log->section}}
                                </td>
                                <td>
                                    {{$log->action}}
                                </td>
                                <td>
                                    {{$log->message}}
                                </td>
                            </tr>
                        @endforeach
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-12">
        <div class="card collapsed-card">
            <div class="card-header">
                <h3 class="card-title">
                    SMS logs
                </h3>
                <div class="card-tools">
					<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
            			<i class="fas fa-plus"></i>
          			</button>
				</div>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Fecha</th>
                            <th>Destinatario</th>
                            <th>Contenido</th>
                        </tr>

                        @foreach($sms_logs as $log)
                            <tr>
                                <td>
                                    {{$log->created_at->format('d/m/Y H:i')}}
                                </td>
                                <td>
                                    {{$log->to}}
                                </td>
                                <td>
                                    {{$log->body}}
                                </td>
                            </tr>
                        @endforeach
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection