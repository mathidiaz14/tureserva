@extends('layouts.root', ['menu' => 'business'])

@section('title')
	Empresa
@endsection

@section('content')

<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3>Editar Empresa</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{URL::Previous()}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					Volver
          				</a>
          			</div>
          		</div>
          		<div class="clearfix"></div>
        	</div>
        	<div class="card-body">
          		<form action="{{url('root/business', $business->id)}}" method="POST" class="form-horizontal">
          			@csrf
          			<input type="hidden" name="_method" value="PATCH">
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Nombre</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="name" class="form-control" value="{{$business->name}}" required autofocus="">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Email</label>
	      				<div class="col-12 col-md-7">
	      					<input type="email" name="email" class="form-control" value="{{$business->email}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Telefono</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="phone" class="form-control" value="{{$business->phone}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">RUT</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="rut" class="form-control" value="{{$business->rut}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Dirección</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="address" class="form-control" value="{{$business->address}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Codigo</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="code" class="form-control" value="{{$business->code}}">
	      				</div>
	      			</div>
					<div class="form-group row">
						<label for="" class="control-label text-right col-12 col-md-3">Estado</label>
						<div class="col-12 col-md-7">
							<select name="status" class="form-control">
								@if($business->status == "enabled")
									<option value="enabled" selected="">Habilitado</option>
									<option value="disabled">Deshabilitado</option>
									<option value="pending">Pendiente</option>
								@elseif($business->status == "pending")
									<option value="enabled">Habilitado</option>
									<option value="disabled">Deshabilitado</option>
									<option value="pending" selected="">Pendiente</option>
								@else
									<option value="enabled">Habilitado</option>
									<option value="disabled" selected="">Deshabilitado</option>
									<option value="pending">Pendiente</option>
								@endif
							</select>
						</div>
	                </div>
	                <div class="form-group row">
						<label for="" class="control-label text-right col-12 col-md-3">Plan</label>
						<div class="col-12 col-md-7">
							<select name="plan" class="form-control">
								@if($business->plan == "plan1")
									<option value="plan1" selected="">Plan 1</option>
									<option value="plan2">Plan 2</option>
									<option value="plan3">Plan 3</option>
								@elseif($business->plan == "plan2")
									<option value="plan1">Plan 1</option>
									<option value="plan2" selected="">Plan 2</option>
									<option value="plan3">Plan 3</option>
								@else
									<option value="plan1">Plan 1</option>
									<option value="plan2">Plan 2</option>
									<option value="plan3" selected="">Plan 3</option>
								@endif
							</select>
						</div>
	                </div>
	                <div class="form-group row">
						<label for="" class="control-label text-right col-12 col-md-3">Pago</label>
						<div class="col-12 col-md-7">
							<select name="pay" class="form-control">
								@if($business->pay == "yes")
									<option value="yes" selected="">Pago</option>
									<option value="free">Gratis</option>
								@else
									<option value="yes">Pago</option>
									<option value="free" selected="">Gratis</option>
								@endif
							</select>
						</div>
	                </div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Contador de SMS</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="sms_count" class="form-control" value="{{$business->sms_count}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Limite de SMS</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="sms_limit" class="form-control" value="{{$business->sms_limit}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">API key</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="api_key" class="form-control" value="{{$business->api_key}}">
	      				</div>
	      			</div>
	                <div class="form-group row">
						<label for="" class="control-label text-right col-12 col-md-3">APP</label>
						<div class="col-12 col-md-7">
							<select name="app" class="form-control">
								@if($business->app == "enabled")
									<option value="enabled" selected="">Habilitada</option>
									<option value="disabled">Deshabilitada</option>
								@else
									<option value="enabled">Habilitada</option>
									<option value="disabled" selected="">Deshabilitada</option>
								@endif
							</select>
						</div>
	                </div>
	                <div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">Carpeta landing</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="folder" class="form-control" value="{{$business->folder}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">URL</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="url" class="form-control" value="{{$business->url}}">
	      				</div>
	      			</div>
	      			<div class="form-group row">
	      				<label for="" class="control-label text-right col-12 col-md-3">URL2</label>
	      				<div class="col-12 col-md-7">
	      					<input type="text" name="url2" class="form-control" value="{{$business->url2}}">
	      				</div>
	      			</div>
	      			<hr>
          			<div class="form-group row">
          				<div class="col-12 col-md-7 offset-md-3 text-right">
          					<button class="btn btn-primary">
	          					<i class="fa fa-save"></i>
	          					Guardar
	          				</button>
          				</div>
          			</div>
          		</form>
          	</div>
        </div>
  	</div>
</div>
@endsection