@extends('layouts.root', ['menu' => 'business'])

@section('title')
    Empresas
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Empresas</h3>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                	<table class="table table-striped">
                		<tr>
                			<th>#</th>
                			<th>Nombre</th>
                			<th>Email</th>
                			<th>Telefono</th>
                			<th>Estado</th>
                			<th class="text-center">Ver</th>
                			<th class="text-center">Deshabilitar</th>
                			<th class="text-center">Reinstalar landing</th>
                			<th class="text-center">Tomar control</th>
                			<th class="text-center">Editar</th>
                			<th class="text-center">Eliminar</th>
                		</tr>
                		@foreach($businesses as $business)
							<tr>
								<td>{{$business->id}}</td>
								<td>{{$business->name}}</td>
								<td>{{$business->email}}</td>
								<td>{{$business->phone}}</td>
								<td>
									@if($business->status == "enabled")
										Habilitada
									@elseif($business->status == "pending")
										Pendiente
									@else
										Deshabilitada
									@endif
								</td>
								<td class="text-center">
									<a href="{{url('root/business', $business->id)}}" class="btn btn-success">
										<i class="fa fa-eye"></i>
									</a>
								</td>
								<td class="text-center">
									<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#disabled_{{$business->id}}">
										<i class="fas fa-exchange-alt"></i>
									</button>
									<!-- Modal -->

									<div class="modal fade deleteModal" id="disabled_{{$business->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-center" role="document">
											<div class="modal-content">
												<div class="modal-header bg-warning">
													<div class="col-12 text-right">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>
												</div>
												<div class="modal-body text-center">
													<div class="row">
														<div class="col-12 text-dark">
															<i class="fa fa-exchange-alt fa-4x"></i>
															<br>
															<h3>¿Cambiar estado de la empresa?</h3>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-6">
															<button type="button" class="btn btn-default btn-block" data-dismiss="modal">
																{{__('message.no')}}
															</button>
														</div>
														<div class="col-6">
															<a href="{{url('root/business/status', $business->id)}}" class="btn btn-warning btn-block">
																{{__('message.yes')}}
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<button type="button" class="btn btn-info" data-toggle="modal" data-target="#landing_{{$business->id}}">
										<i class="fas fa-palette"></i>
									</button>
									<!-- Modal -->

									<div class="modal fade deleteModal" id="landing_{{$business->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-center" role="document">
											<div class="modal-content">
												<div class="modal-header bg-info">
													<div class="col-12 text-right">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>
												</div>
												<div class="modal-body text-center">
													<div class="row">
														<div class="col-12 text-dark">
															<i class="fas fa-palette fa-4x"></i>
															<br>
															<h3>¿Reinstalar landing?</h3>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-6">
															<button type="button" class="btn btn-default btn-block" data-dismiss="modal">
																{{__('message.no')}}
															</button>
														</div>
														<div class="col-6">
															<a href="{{url('root/business/landing', $business->id)}}" class="btn btn-info btn-block">
																{{__('message.yes')}}
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</td>
								<td class="text-center">
									<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#control_{{$business->id}}">
										<i class="fas fa-plug"></i>
									</button>
									<!-- Modal -->

									<div class="modal fade deleteModal" id="control_{{$business->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog modal-center" role="document">
											<div class="modal-content">
												<div class="modal-header bg-dark">
													<div class="col-6">
														<h5>
															Tomar control
														</h5>
													</div>
													<div class="col-6 text-right">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>
												</div>
												<div class="modal-body text-left">
													<div class="row">
														<div class="col-12 text-dark">
															<form action="{{url('root/business/control', $business->id)}}" class="form-horizontal" method="post">
																@csrf
																<div class="form-group">
																	<label for="">Codigo para conexión</label>
																	<input type="number" class="form-control" id="code_{{$business->id}}" name="code" placeholder="######">
																</div>
																<hr>
																<div class="row">
																	<div class="col-6">
																		<button type="button" class="btn btn-default btn-block" data-dismiss="modal">
																			{{__('message.no')}}
																		</button>
																	</div>
																	<div class="col-6">
																		<button class="btn btn-dark btn-block">
																			<i class="fa fa-plug"></i>
																			Conectar
																		</button>	
																	</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</td>	
								<td class="text-center">
									<a href="{{route('business.edit', $business->id)}}" class="btn btn-primary">
										<i class="fa fa-edit"></i>
									</a>
								</td>
								<td class="text-center">
									@include('helpers.delete_modal', ['delete_id' => $business->id, 'delete_route' => url('root/business', $business->id)])
								</td>
							</tr>
                		@endforeach
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			@foreach($businesses as $business)
				$('#control_{{$business->id}}').on('shown.bs.modal', function () {
				  	$('#code_{{$business->id}}').trigger('focus')
				});
			@endforeach
		});
	</script>
@endsection
