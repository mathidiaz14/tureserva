@extends('layouts.root', ['menu' => 'manage'])

@section('title')
    Usuarios
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3>Administrar</h3>
                    </div>
                    <div class="col-6 text-right">
                    </div>
                </div>
            </div>
            <div class="card-body">
            	<div class="form-group">
            		<label for="">Git status</label>
            		<textarea name="" id="" cols="30" rows="10" class="form-control" readonly>{{$git}}</textarea>
            	</div>
                <div class="form-group">
                	<div class="row">
                		<div class="col">
                			<label for="">Git PULL</label>
                		</div>
                		<div class="col text-right">
		                	<a href="{{url('root/admin/git/pull')}}" class="btn btn-info">
		                		GIT PULL
		                	</a>
                		</div>
                	</div>
                </div>
                <hr>
                <form action="{{url('root/update/env')}}" method="post" class="form-horizontal">
                	@csrf
                	<div class="form-group">
	                	<label for="">Archivo .env</label>
	                	<textarea name="env" id="" cols="30" rows="10" class="form-control">{{$env}}</textarea>
	                </div>
	                <div class="form-group text-right">
	                	<button class="btn btn-info">
	                		<i class="fa fa-save"></i>
	                		Guardar
	                	</button>
	                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
