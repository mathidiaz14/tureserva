@extends('layouts.root', ['menu' => 'home'])

@section('title')
    Root
@endsection

@section('content')

<div class="row mt-4">
    <div class="col-6 col-md-3">
        <div class="small-box bg-info">
            <div class="inner">
                <h3>
                    {{App\Models\Business::count()}}
                </h3>
                <p>
                    Empresas
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-building"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-dark">
            <div class="inner">
                <h3>
                    {{App\Models\User::count()}}
                </h3>
                <p>
                    Usuarios
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-user"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-success">
            <div class="inner">
                <h3>
                    {{App\Models\Calendar::where('online','!=' ,null)->count()}}
                </h3>
                <p>
                    Reservas online
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-warning">
            <div class="inner">
                <h3>
                    {{App\Models\Calendar::count()}}
                </h3>
                <p>Total de reservas</p>
            </div>
            <div class="icon">
                <i class="fa fa-calendar-minus"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-primary">
            <div class="inner">
                <h3>
                    {{App\Models\Log::count()}}    
                </h3>
                <p>
                    Total de logs
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-list"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
    <div class="col-6 col-md-3">
        <div class="small-box bg-danger">
            <div class="inner">
                <h3>
                    {{App\Models\Incident::count()}}    
                </h3>
                <p>
                    Incidencias registradas
                </p>
            </div>
            <div class="icon">
                <i class="fa fa-exclamation-triangle"></i>
            </div>
            <a class="small-box-footer">
                <p></p>
            </a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Ultimos 50 logs
                </h3>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Fecha</th>
                            <th>Empresa</th>
                            <th>Usuario</th>
                            <th>Sección</th>
                            <th>Acción</th>
                            <th>Mensaje</th>
                        </tr>

                        @foreach(App\Models\Log::all()->take(50) as $log)
                            <tr>
                                <td>
                                    {{$log->created_at->format('d/m/Y H:i')}}
                                </td>
                                <td>
                                    <a href="{{url('root/business', $log->business_id)}}">
                                        {{$log->business->name}}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{url('root/user', $log->user_id)}}">
                                        {{$log->user->name}}
                                    </a>
                                </td>
                                <td>
                                    {{$log->section}}
                                </td>
                                <td>
                                    {{$log->action}}
                                </td>
                                <td>
                                    {{$log->message}}
                                </td>
                            </tr>
                        @endforeach
                    </table>    
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection

@section('scripts')

@endsection
