@extends('layouts.root', ['menu' => 'help'])

@section('title')
    Ayuda
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3>Contenido de {{$category->title}}</h3>
                    </div>
                    <div class="col-6 text-right">
                      	<a href="{{url('root/post/help', $category->id)}}" class="btn btn-success">
                      		<i class="fa fa-plus"></i>
                      		Nuevo post
                      	</a>
                      	<a href="{{url('root/category/help')}}" class="btn btn-default">
                      		<i class="fa fa-chevron-left"></i>
                      		Atras
                      	</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($category->posts->count() == 0)
                	@include('helpers.registry_null')
                @else
                	<div class="row">
                		<div class="col-12">
                			<div class="table table-responsive">
			                	<table class="table table-stripped">
			                		<tr>
			                			<th>Titulo</th>
			                			<th>Ver</th>
			                			<th>Editar</th>
			                			<th>Eliminar</th>
			                		</tr>
			                		@foreach($category->posts as $post)
			                			<tr>
			                				<td>{{$post->title}}</td>
			                				<td>
			                					<a href="{{url('ayuda', $post->id)}}" class="btn btn-success" target="_blank">
			                						<i class="fa fa-eye"></i>
			                					</a>
			                				</td>
			                				<td>
			                					<a href="{{url('root/post/help', $post->id)}}/edit" class="btn btn-primary">
			                						<i class="fa fa-edit"></i>
			                					</a>
			                				</td>
			                				<td>
			                					@include('helpers.delete_modal', ['delete_id' => $post->id, 'delete_route' => url('root/post/help', $post->id)])
			                				</td>
			                			</tr>	
			                		@endforeach
			                	</table>
			                </div>	
                		</div>
                	</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
