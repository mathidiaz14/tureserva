@extends('layouts.root', ['menu' => 'help'])

@section('title')
    Ayuda
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3>Categorias Ayuda</h3>
                    </div>
                    <div class="col-6 text-right">
                       <button type="button" class="btn btn-info" data-toggle="modal" data-target="#add_category">
							<i class="fa fa-plus"></i>
							Agregar categoria
						</button>
						<!-- Modal -->

						<div class="modal fade deleteModal" id="add_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content text-left">
									<div class="modal-header">
										<div class="col-6">
											Agregar categoria
										</div>
										<div class="col-6 text-right">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										</div>
									</div>
									<div class="modal-body">
										<form action="{{url('root/category/help')}}" method="post" class="form-horizontal">
						                	@csrf
						                	<div class="form-group">
						                		<label for="">Titulo</label>
						                		<input type="text" class="form-control" name="title" placeholder="Titulo">
						                	</div>
						                	<div class="form-group text-right">
						                		<button class="btn btn-primary">
						                			<i class="fa fa-save"></i>
						                			Guardar
						                		</button>
						                	</div>
						                </form>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                @if($categories->count() == 0)
                	@include('helpers.registry_null')
                @else
                	<div class="row">
                		<div class="col-12">
                			<div class="table table-responsive">
			                	<table class="table table-stripped">
			                		<tr>
			                			<th>Titulo</th>
			                			<th>Ver</th>
			                			<th>Editar</th>
			                			<th>Eliminar</th>
			                		</tr>
			                		@foreach($categories as $category)
			                			<tr>
			                				<td>{{$category->title}}</td>
			                				<td>
			                					<a href="{{url('root/category/help', $category->id)}}" class="btn btn-success">
			                						<i class="fa fa-eye"></i>
			                					</a>
			                				</td>
			                				<td>
			                					<button type="button" class="btn btn-info" data-toggle="modal" data-target="#edit_category_{{$category->id}}">
													<i class="fa fa-edit"></i>
												</button>
												<!-- Modal -->

												<div class="modal fade deleteModal" id="edit_category_{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
													<div class="modal-dialog" role="document">
														<div class="modal-content text-left">
															<div class="modal-header">
																<div class="col-6">
																	Editar categoria
																</div>
																<div class="col-6 text-right">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																</div>
															</div>
															<div class="modal-body">
																<form action="{{url('root/category/help', $category->id)}}" method="post" class="form-horizontal">
												                	@csrf
												                	@method('PATCH')
												                	<div class="form-group">
												                		<label for="">Titulo</label>
												                		<input type="text" class="form-control" name="title" placeholder="Titulo" value="{{$category->title}}">
												                	</div>
												                	<div class="form-group text-right">
												                		<button class="btn btn-primary">
												                			<i class="fa fa-save"></i>
												                			Guardar
												                		</button>
												                	</div>
												                </form>
															</div>
														</div>
													</div>
												</div>
			                				</td>
			                				<td>
			                					@include('helpers.delete_modal', ['delete_id' => $category->id, 'delete_route' => url('root/category/help', $category->id)])
			                				</td>
			                			</tr>	
			                		@endforeach
			                	</table>
			                </div>	
                		</div>
                	</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
