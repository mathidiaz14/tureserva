@extends('layouts.root', ['menu' => 'help'])

@section('title')
    Ayuda
@endsection

@section('css')
	<link rel="stylesheet" href="{{asset('dashboard/vendors/summernote/summernote-bs4.min.css')}}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3>Nuevo post</h3>
                    </div>
                    <div class="col-6 text-right">
                      	<a href="{{url('root/category/help', $category->id)}}" class="btn btn-default">
                      		<i class="fa fa-chevron-left"></i>
                      		Atras
                      	</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <div class="row">
                	<div class="col-12">
                         <form action="{{url('root/post/help')}}" method="post" class="form-horizontal">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="">Titulo</label>
                                        <input type="text" class="form-control" name="title" placeholder="Titulo" required autofocus>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="">Categoria</label>
                                        <select class="form-control" name="category">
                                            @foreach($categories as $cat)
                                                @if($cat->id == $category->id)
                                                    <option value="{{$cat->id}}" selected>{{$cat->title}}</option>
                                                @else
                                                    <option value="{{$cat->id}}">{{$cat->title}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>  
                                </div>  
                            </div>
                            
                            <div class="form-group">
                                <textarea name="body" id="editor" rows="10" class="summernote form-control" required=""></textarea>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Guardar
                                </button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <script src="{{asset('dashboard/vendors/summernote/summernote-bs4.min.js')}}"></script>
  <script>
    $(document).ready(function()
    {
      $('.summernote').summernote({
        height: 200,   //set editable area's height
        codemirror: { // codemirror options
          theme: 'monokai'
        }
      });

    });
  </script>
@endsection