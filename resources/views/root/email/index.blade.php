@extends('layouts.root', ["menu" => "notification"])

@section('title')
    Notificaciones
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h4>Probar envio de email</h4>
                    </div>
                    <div class="col-6 text-right">
                    	
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
            	<div class="col-12 col-md-8 offset-md-2">
      				@if(session()->has('success_email'))
						<div class="alert alert-success" role="alert">
						  	{{session('success_email')}}
							{{session()->forget('success_email')}}
						</div>
					@endif
      			</div>
                <form action="{{url('root/email')}}" method="post" class="form-horizontal col-12 col-md-8 offset-md-2">
                	@csrf
                	<div class="form-group">
                		<label for="" class="control-label">
                			Email que recibe
                		</label>
                		<input type="email" class="form-control" name="email" autofocus="" required="">
                	</div>
                	<div class="form-group">
                		<label for="" class="control-label">
                			Contenido del mensaje
                		</label>
                		<input type="text" class="form-control" name="content" required="" value="Prueba desde {{config('app.name')}}">
                	</div>
                	<div class="form-group text-right">
                		<button class="btn btn-info">
                			<i class="fa fa-send"></i>
                			Enviar
                		</button>
                	</div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h4>Probar envio de SMS</h4>
                    </div>
                    <div class="col-6 text-right">
                    	
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
            	<div class="col-12 col-md-8 offset-md-2">
      				@if(session()->has('success_sms'))
						<div class="alert alert-success" role="alert">
						  	{{session('success_sms')}}
							{{session()->forget('success_sms')}}
						</div>
					@endif
                    @if(session()->has('error_sms'))
                        <div class="alert alert-danger" role="alert">
                            {{session('error_sms')}}
                            {{session()->forget('error_sms')}}
                        </div>
                    @endif
      			</div>
                <form action="{{url('root/sms')}}" method="post" class="form-horizontal col-12 col-md-8 offset-md-2">
                	@csrf
                	<div class="form-group">
                		<label for="" class="control-label">
                			Numero que recibe
                		</label>
                		<input type="phone" class="form-control" name="number" autofocus="" required="">
                	</div>
                	<div class="form-group">
                		<label for="" class="control-label">
                			Contenido del mensaje
                		</label>
                		<input type="text" class="form-control" name="content" required="" value="Prueba desde {{config('app.name')}}">
                	</div>
                	<div class="form-group text-right">
                		<button class="btn btn-info">
                			<i class="fa fa-send"></i>
                			Enviar
                		</button>
                	</div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h4>Probar envio de WPP</h4>
                    </div>
                    <div class="col-6 text-right">
                    	
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
            	<div class="col-12 col-md-8 offset-md-2">
      				@if(session()->has('success_wpp'))
						<div class="alert alert-success" role="alert">
						  	{{session('success_wpp')}}
							{{session()->forget('success_wpp')}}
						</div>
					@endif
      			</div>
                <form action="{{url('root/wpp')}}" method="post" class="form-horizontal col-12 col-md-8 offset-md-2">
                	@csrf
                	<div class="form-group">
                		<label for="" class="control-label">
                			Numero que recibe
                		</label>
                		<input type="phone" class="form-control" name="number" autofocus="" required="">
                	</div>
                	<div class="form-group">
                		<label for="" class="control-label">
                			Contenido del mensaje
                		</label>
                		<input type="text" class="form-control" name="content" required="" value="Prueba desde {{config('app.name')}}">
                	</div>
                	<div class="form-group text-right">
                		<button class="btn btn-info">
                			<i class="fa fa-send"></i>
                			Enviar
                		</button>
                	</div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
