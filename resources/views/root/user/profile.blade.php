@extends('layouts.root', ['menu' => 'user'])

@section('title')
	Usuarios
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
<div class="row">
	<div class="col-12">
      	<div class="card">
        	<div class="card-header">
          		<div class="row">
          			<div class="col-6">
          				<h3>Editar Perfil</h3>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{url('/home')}}" class="btn btn-default">
          					<i class="fa fa-chevron-left"></i>
          					Volver
          				</a>
          			</div>
          		</div>
        	</div>
        	<div class="card-body">
          		<div class="row">
          			<div class="col-12">
          				<form action="{{url('profile')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
		          			@csrf
			                <div class="form-group row">
			                  	<label for="" class="control-label col-12 col-md-3">Avatar</label>
			                  	<div class="col-12 col-md-7">
				                    <div class="row">
				                    	<div class="col-8 col-md-9 text-center">
					                    	@if(user()->avatar == null)
					                      		<img src="{{asset('images/profile.png')}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
					                    	@else
					                      		<img src="{{asset(user()->avatar)}}" class="avatar" alt="" style="width: 200px!important; height: 200px!important;">
					                    	@endif
					                    </div>
					                    <div class="col-4 col-md-3">
					                      	@if(user()->avatar == null)
												<a class="btn btn-danger btn-block disabled">
						                        	<i class="fa fa-trash"></i>
						                        	Eliminar avatar
						                      	</a>
					                      	@else
												<a href="{{url('user/delete/avatar', user()->id)}}" class="btn btn-danger btn-block">
						                        	<i class="fa fa-trash"></i>
						                        	Eliminar avatar
						                      	</a>
					                      	@endif
					                      	<br>
					                      	<div class="upload-btn-wrapper" style="width: 100%;">
											  	<button class="btn btn-info btn-block" style="margin-bottom: 0;">
											  		<i class="fa fa-arrow-up"></i>
											  		Subir archivo
											  	</button>
											  	<input type="file" name="avatar" />
											</div>
					                    </div>
				                    </div>
			                  	</div>
			                </div>
			      			<div class="form-group row">
			      				<label for="" class="control-label text-center col-12 col-md-3">Nombre</label>
			      				<div class="col-12 col-md-7">
			      					<input type="text" name="name" class="form-control" value="{{user()->name}}" required autofocus="">
			      				</div>
			      			</div>
			      			<div class="form-group row">
			      				<label for="" class="control-label text-center col-12 col-md-3">Email</label>
			      				<div class="col-12 col-md-7">
			      					<input type="email" name="email" class="form-control" value="{{user()->email}}" required="">
			      				</div>
			      			</div>
			      			<div class="form-group row">
			      				<label for="" class="control-label text-center col-12 col-md-3">Genero</label>
			      				<div class="col-12 col-md-7">
			      					<select name="gender" id="" class="form-control">
			      						@if(user()->gender == "female")
				      						<option value="female" selected="">Femenino</option>
				      						<option value="male">Masculino</option>
				      						<option value="other">Otro</option>
				      					@elseif(user()->gender == "male")
				      						<option value="female">Femenino</option>
				      						<option value="male" selected="">Masculino</option>
				      						<option value="other">Otro</option>
				      					@else
				      						<option value="female">Femenino</option>
				      						<option value="male">Masculino</option>
				      						<option value="other" selected="">Otro</option>
				      					@endif
			      					</select>
			      				</div>
			      			</div>
			      			<div class="form-group row">
			      				<label for="" class="control-label text-center col-12 col-md-3">Telefono</label>
			      				<div class="col-12 col-md-7">
			      					<input type="phone" name="phone" class="form-control" value="{{user()->phone}}" placeholder="Telefono">
			      				</div>
			      			</div>
			      			<div class="form-group row">
			      				<label for="" class="control-label text-center col-12 col-md-3">Dirección</label>
			      				<div class="col-12 col-md-7">
			      					<input type="address" name="address" class="form-control" value="{{user()->address}}" placeholder="Dirección">
			      				</div>
			      			</div>
			      			<div class="form-group row">
			      				<label for="" class="control-label text-center col-12 col-md-3">Cumpleaños</label>
			      				<div class="col-12 col-md-7">
			      					<input type="date" name="birthday" class="form-control" value="{{user()->birthday != null ? user()->birthday->format('Y-m-d') : ''}}">
			      				</div>
			      			</div>
		          			<hr>
			                <div class="form-group row">
			                	<label for="" class="col-12 text-center">Cambiar la contraseña</label>
			                  	<label for="" class="control-label text-center col-12 col-md-3">Contraseña nueva</label>
			                  	<div class="col-12 col-md-7">
			                    	<input type="password" name="password" id="password" class="form-control" placeholder="Contraseña nueva">
			                  	</div>
			                </div>
			                <div class="form-group row">
			                  	<label for="" class="control-label text-center col-12 col-md-3">Repita la contraseña</label>
			                  	<div class="col-12 col-md-7">
			                    	<input type="password" name="password_re" id="password" class="form-control" placeholder="Repita la contraseña">
			                  	</div>
			                </div>
			                <hr>
		          			<div class="form-group row">
		          				<div class="col-12 col-md-7 offset-md-3 text-right">
		          					<button class="btn btn-primary">
			          					<i class="fa fa-save"></i>
			          					Guardar
			          				</button>
		          				</div>
		          			</div>
		          		</form>
          			</div>
          		</div>
          	</div>
        </div>
  	</div>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('#view_pass').click(function()
			{
				if($('#password').get(0).type == "password")
				{
					$('#password').get(0).type = "text";
				}
				else
				{
					$('#password').get(0).type = "password";
				}
			});
		});
	</script>
@endsection