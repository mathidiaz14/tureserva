@extends('layouts.root', ['menu' => 'user'])

@section('title')
    Usuarios
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <h3>Usuario</h3>
                    </div>
                    <div class="col-6 text-right">
                    	<a href="{{url('root/user/create')}}" class="btn btn-success">
                    		<i class="fa fa-plus"></i>
                    		Nuevo usuario
                    	</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-body">
                <div class="table table-responsive">
                	<table class="table table-striped">
                		<tr>
                			<th>#</th>
                			<th>Nombre</th>
                			<th>Email</th>
                			<th>Telefono</th>
                			<th>Reserva online</th>
                			<th>Acceso a la web</th>
                			<th>Confirmado?</th>
                			<th>Empresa</th>
                			<th>Editar</th>
                			<th>Eliminar</th>
                		</tr>
                		@foreach($users as $user)
							<tr>
								<td>{{$user->id}}</td>
								<td>{{$user->name}}</td>
								<td>{{$user->email}}</td>
								<td>{{$user->phone}}</td>
								<td>
									@if($user->online == "on")
										Si
									@else
										No
									@endif
								</td>
								<td>
									@if($user->access == "on")
										Si
									@else
										No
									@endif
								</td>
								<td>
									@if($user->confirmed == 1)
										Si
									@else
										No
									@endif
								</td>
								<td>
									@if($user->business != null)
										{{$user->business->name}}
									@endif
								</td>
								<td>
									<a href="{{route('user.edit', $user->id)}}" class="btn btn-primary">
										<i class="fa fa-pencil"></i>
									</a>
								</td>
								<td>
									@include('helpers.delete_modal', ['delete_id' => $user->id, 'delete_route' => url('root/user', $user->id)])
								</td>
							</tr>
                		@endforeach
                	</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
