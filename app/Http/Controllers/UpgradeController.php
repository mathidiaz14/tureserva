<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MercadoPago;
use Http;
use Carbon\Carbon;

class UpgradeController extends Controller
{
    private $path = "admin.upgrade.";
    protected $headers;
    protected $url;

    public function __construct()
    {
        $this->headers = [
            'Authorization' => 'Bearer '.env("MP_ACCESS_TOKEN"),
        ];

        $this->url = [
            "plan"       => "https://api.mercadopago.com/preapproval",
        ];
    }

    public function index()
    {
        return view($this->path."index");
    }

    public function show($plan)
    {   
        $plan = strtoupper($plan);

        //Guardo las variables
        $amount     = env($plan);
        $sms_limit  = env('SMS_LIMIT_'.$plan);
        
        session(['plan_upgrade' => $plan, 'sms_limit' => $sms_limit, 'amount' => $amount]);

        return view($this->path."pay", compact('amount', 'plan'));
    }

    public function store(Request $request)
    {        
        $body = [
            'preapproval_plan_id'           => env('MP_'.strtoupper(session('plan_upgrade')).'_ID'),
            'reason'                        => __('message.'.session('plan_upgrade')),
            'external_reference'            => session('plan_upgrade'),
            'payer_email'                   => $request->payer['email'],
            'card_token_id'                 => $request->token,
            'back_url'                      => url('upgrade'),
            'status'                        => "authorized",
        ];
        
        //Guardo el plan
        $response = Http::withHeaders($this->headers)
                            ->post($this->url['plan'], $body);

        //Si el pago es aprobado entro al if
        if($response->json()['status'] === "authorized")
        {
            //Guardo los datos del plan
            
            $business                       = business();
            $business->pay                  = "pay";
            $business->plan_id              = $response->json()['id'];
            $business->plan                 = session('plan_upgrade');
            $business->sms_limit            = session('sms_limit');
            $business->save();

            success('Su suscripción se aprobo correctamente');

            return [
                "status"    => true,
                "message"   => "",
            ];

        }
        
        //Si el pago no fue aprobado muestro el error que devuelve mercadopago
        return [
            "status"    => false,
            "message"   => 'Pago rechazado, error: '.$response->json()['message'],
        ];
    }

    public function cancel(Request $request)
    {
        $body = [
            'status'           => "cancelled",
        ];

        $response   = Http::withHeaders($this->headers)
                                ->put($this->url['plan']."/".business()->plan_id, $body);
        
        if($response->successful())
        {
            $business = business();
            $business->plan                 = "none";
            $business->pay                  = "no";
            $business->sms_limit            = 0;
            $business->amount               = 0;
            $business->plan_id              = null;
            $business->save();

            success("Suscripción cancelada correctamente.");
            return redirect('upgrade');
        }

        error("No se pudo modificar la suscripción.");
        
        return redirect('upgrade');
    }

    public function details()
    {
        $response   = Http::withHeaders($this->headers)
                            ->get($this->url['plan']."/".business()->plan_id);

        $details    = $response->json();
        
        if($response->successful())
            return view($this->path."show", compact('details'));

        return redirect('home');
    }

    public function get_change_card()
    {
        $amount = business()->amount;
        $plan   = business()->plan;

        return view($this->path.'edit_card', compact('amount', 'plan'));
    }

    public function set_change_card(Request $request)
    {
        $body = [
            'card_token_id'           => $request->token,
        ];

        $response   = Http::withHeaders($this->headers)
                                ->put($this->url['plan']."/".business()->plan_id, $body);
        
        if($response->successful())
            success("Suscripción modificada correctamente.");

        error("No se pudo modificar la suscripción.");
    }
}