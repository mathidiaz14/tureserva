<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use Auth;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note           = new Note();
        $note->text     = $request->note;
        $note->user_id  = user()->id;

        if ($request->business == "on")
            $note->business_id = business()->id;
        
        $note->save();

        session(__('message.create_info'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $note = Note::findOrFail($id);

        if(($note == null) or (!check_business($note)))
        {
            error(__('errors.update'));
            return back();
        }
        
        $note->text = $request->note;

        if ($request->business == "on")
            $note->business_id = Auth::user()->business_id;
        else
            $note->business_id = null;

        $note->save();

        success(__('message.update_info'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $note = Note::findOrFail($id);

        if($note == null)
        {
            error(__('errors.delete'));
            return back();
        }

        $note->delete();

        success(__('message.delete_info'));
        return back();
    }
}
