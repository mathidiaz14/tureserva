<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;
use App\Models\Business;
use App\Models\Service;
use App\Models\Calendar;
use App\Models\Notification;
use App\Models\User;
use App\Models\Client;
use App\Models\TaskTmp;
use App\Models\Contact;
use Carbon\Carbon;
use MercadoPago;
use Auth;
use Storage;
use File;

class ReservationController extends Controller
{

    private $path = "admin.reservation.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservation = business()->reservation;

        return view($this->path."index", compact('reservation'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $reservation                    = business()->reservation;
        $reservation->access            = $request->access;
        $reservation->auto              = $request->auto;
        $reservation->service_price     = $request->service_price;
        $reservation->today             = $request->today;
        $reservation->days_cancel       = $request->days_cancel;
        $reservation->days_reserve      = $request->days_reserve;
        $reservation->interval          = $request->interval;
        $reservation->nobody            = $request->nobody;
        $reservation->table_hours       = $request->table_hours;
        $reservation->save();

        success(__('message.update_info'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function header()
    {
        $reservation = business()->reservation;

        if(file_exists($reservation->header))
            unlink($reservation->header);

        $reservation->header = null;
        $reservation->save();

        success(__('message.delete_info'));
        return back();
    }

    public function footer()
    {
        $reservation = business()->reservation;


        if(file_exists($reservation->footer))
            unlink($reservation->footer);

        $reservation->footer = null;
        $reservation->save();

        success(__('message.delete_info'));
        return back();
        
    }
}
