<?php

namespace App\Http\Controllers\root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Models\CronInfo;
use Validator;
use Mail;
use Auth;
use DB;
use Schema;

class RootHomeController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses =  \App\Models\Business::all();

        return view('root.index', compact('businesses'));
    }

    public function get_email()
    {
        return view('root.email.index');
    }

    public function send_email(Request $request)
    {

        $data = array(
            'content' => $request->content,
        );

        send_email('emails.test', $data, $request->email, "Prueba de envio desde ".config('app.name'));

        Session(['success_email' => "El email se envio correctamente"]);
        return back();
    }

    public function send_sms(Request $request)
    {
        if ((strpos($request->number, '+598') === false) and (strpos($request->number, '00598') === false))
            $request->number = "+598".$request->number;

        $sid    = env( 'TWILIO_SID' );
        $token  = env( 'TWILIO_TOKEN' );

        $client = new \Twilio\Rest\Client( $sid, $token );

        $client->messages->create(
            $request->number,
            array(
                'from' => env('TWILIO_FROM'),
                'body' => $request->content,
            )
        );

        Session(['success_sms' => "El sms se envio correctamente"]);

        return back();
    }

    public function send_wpp(Request $request)
    {
        if ((strpos($request->number, '+598') === false) and (strpos($request->number, '00598') === false))
            $request->number = "+598".$request->number;

        $sid    = env( 'TWILIO_SID' );
        $token  = env( 'TWILIO_TOKEN' );
        
        $twilio = new \Twilio\Rest\Client( $sid, $token );

        $message = $twilio->messages
                  ->create("whatsapp:".$request->number, // to
                           [
                               "from" => "whatsapp:".env('TWILIO_FROM_WPP'),
                               "body" => $request->content
                           ]
                  );

        Session(['success_wpp' => "El mensaje se envio correctamente"]);
        return back();
    }

    public function get_cron()
    {
        $crons = CronInfo::paginate(50);

        return view('root.cron.index', compact('crons'));
    }
}
