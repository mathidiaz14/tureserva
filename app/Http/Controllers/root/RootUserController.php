<?php

namespace App\Http\Controllers\root;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Business;
use Hash;

class RootUserController extends Controller
{
    private $path = "root.user.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view($this->path."index", compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     $old = User::where('email', $request->email)->first();
        
        if($old != null)
        {
            session(['error' => 'El email ya esta registrado en nuestra base de datos, pruebe con otro']);
            return back()->withInput();
        } else
        {
            $user                   = new User();
            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->password         = "none";
            $user->type             = $request->type;
            $user->access           = $request->access;
            $user->online           = $request->online;
            $user->color            = $request->color;
            $user->phone            = $request->phone;
            $user->address          = $request->address;
            $user->birthday         = $request->birthday;
            $user->business_hour    = $request->business_hour;
            $user->contact_number   = $request->contact_number;
            $user->coment           = $request->coment;
            $user->business_id      = $request->business;
            $user->save();

            if($request->online == "on")
            {
                $user->services()->detach();

                foreach($user->business->services as $service)
                {
                    if (($request['service_'.$service->id] == "on") or ($request->service_all == "on"))
                        $user->services()->attach($service);
                }
            }

            $week               = new WeekHourUser();
            $week->mon_start    = $request->mon_start;
            $week->mon_end      = $request->mon_end;
            $week->mon_close    = $request->mon_close;
            $week->tue_start    = $request->tue_start;
            $week->tue_end      = $request->tue_end;
            $week->tue_close    = $request->tue_close;
            $week->wed_start    = $request->wed_start;
            $week->wed_end      = $request->wed_end;
            $week->wed_close    = $request->wed_close;
            $week->thu_start    = $request->thu_start;
            $week->thu_end      = $request->thu_end;
            $week->thu_close    = $request->thu_close;
            $week->fri_start    = $request->fri_start;
            $week->fri_end      = $request->fri_end;
            $week->fri_close    = $request->fri_close;
            $week->sat_start    = $request->sat_start;
            $week->sat_end      = $request->sat_end;
            $week->sat_close    = $request->sat_close;
            $week->sun_start    = $request->sun_start;
            $week->sun_end      = $request->sun_end;
            $week->sun_close    = $request->sun_close;
            $week->user_id      = $user->id;
            $week->save();

            if($request->access == "on")
            {
                $invitation_code        = Random_String(25);
                $user->invitation_code  = $invitation_code;
                $user->invitation_hour  = Carbon::now();
                $user->save();

                $data = array(
                    'business' => $user->business->name,
                    'invitation_code' => $invitation_code,
                );

                send_email('emails.new_user', $data, $user->email, 'Invitación a '.config('app.name'));

                Session(['success' => "El usuario se creo correctamente. Se envio un email para que ingrese al sistema."]);

                return redirect('root/user');
            }
            
            Session(['success' => "El usuario se creo correctamente."]);
            
            return redirect('root/user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view($this->path."show", compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user       = User::findOrFail($id);
        $businesses = Business::all();

        return view($this->path."edit", compact('user', 'businesses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old  = User::where('email', $request->email)->where('id', '!=', $id)->first();

        if($old != null)
        {
            session(['error' => 'El email ya esta registrado en nuestra base de datos, pruebe con otro']);
            return back()->withInput();

        } else
        {            
            $user                   = User::findOrFail($id);
            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->phone            = $request->phone;
            $user->address          = $request->address;
            $user->birthday         = $request->birthday;
            $user->type             = $request->type;
            $user->access           = $request->access;
            $user->online           = $request->online;
            $user->color            = $request->color;
            $user->business_hour    = $request->business_hour;
            
            if($request->confirmed == "on")
                $user->confirmed        = 1;
            
            $user->save();
            
            if($request->access == "on")
            {
                if($request->password != null)
                {
                    $user->password  = Hash::make($request->password);
                    $user->save();
                }
            }
            
            success('El usuario se modifico correctamente');
            return redirect('root/user');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        session(['success' => 'Se elimino el usuario '.$user->email]);
        return back();
    }
}
