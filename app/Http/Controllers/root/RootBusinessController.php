<?php

namespace App\Http\Controllers\root;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Business;
use App\Models\Log;
use App\Models\SmsLog;
use App\Models\User;
use Carbon\Carbon;
use Auth;
use Str;
use Hash;

class RootBusinessController extends Controller
{
    private $path = "root.business.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businesses = Business::all();

        return view($this->path."index", compact('businesses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $business   = Business::findOrFail($id);
        $logs       = Log::where('business_id', $business->id)->paginate(20);
        $sms_logs   = SmsLog::where('business_id', $business->id)->paginate(20);
        
        return view($this->path."show", compact('business', 'logs', 'sms_logs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $business = Business::findOrFail($id);

        return view($this->path."edit", compact('business'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $business = Business::findOrFail($id);

        if($business == null)
        {
            error(__('errors.update'));
            return back();
        }

        $business->name         = $request->name;
        $business->email        = $request->email;
        $business->phone        = $request->phone;
        $business->rut          = $request->rut;
        $business->address      = $request->address;
        $business->code         = $request->code;
        $business->status       = $request->status;
        $business->plan         = $request->plan;
        $business->pay          = $request->pay;
        $business->sms_count    = $request->sms_count;
        $business->sms_limit    = $request->sms_limit;
        $business->api_key      = $request->api_key;
        $business->app          = $request->app;
        $business->folder       = $request->folder;
        $business->url          = $request->url;
        $business->url2         = $request->url2;
        $business->save();

        success(__('message.update_info'));
        return redirect('root/business');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $business = Business::findOrFail($id);

        $business->users()->delete();
        $business->notifications()->delete();
        $business->tasks()->delete();
        $business->clients()->delete();
        $business->services()->delete();
        $business->medias()->delete();
        $business->days()->delete();
        $business->resources()->delete();
        $business->contacts()->delete();
        $business->opinions()->delete();
        $business->reservation()->delete();        
        $business->notes()->delete();
        $business->absences()->delete();
        $business->tips()->delete();
        $business->waitings()->delete();
        $business->landings()->delete();
        $business->visits()->delete();
        $business->messages()->delete();
        $business->sms_logs()->delete();        
        $business->logs()->delete();        
        $business->incidents()->delete();        
        
        $business->delete();

        success("La empresa se elimino correctamente");
        return back();
    }

    public function change_status($id)
    {
        $business = Business::findOrFail($id);

        if($business == null)
        {
            error("No se pudo cambiar el estado de la empresa");
            return back();
        }

        if($business->status == "enabled")
            $business->status = "disabled";
        else
            $business->status = "enabled";

        $business->save();

        success('Se cambio el estado de la empresa');
        return back();
    }

    public function reinstall_landing($id)
    {
        $business = Business::findOrFail($id);

        if($business == null)
        {
            error("No se pudo cambiar el estado de la empresa");
            return back();
        }

        $business->landings()->delete();
        
        install_theme($business->folder, $business);

        success(__('message.update_info'));
        return back();
    }

    public function control(Request $request, $id)
    {
        $business = Business::findOrFail($id);

        if(($business == null) or ($business->control_code == null))
        {
            error('Error al conectar con la empresa');
            return back();
        }

        $now = Carbon::now();

        if(($business->control_code == $request->code) and ($now->diffInMinutes($business->control_date) <= 30))
        {
            $user = User::find($business->control_user);

            $business->control_code = null;
            $business->control_date = null;
            $business->control_user = null;
            $business->save();

            Auth::login($user, true);
            return redirect('home');
        }else
        {
            error('Error al conectar con la empresa');
            return back();
        }
    }
}
