<?php

namespace App\Http\Controllers\root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RootAdminController extends Controller
{
    public function index()
    {
        $git    = shell_exec('git show');
        $env    = file_get_contents(base_path('.env'));

        return view('root.admin.index', compact('env', 'git'));
    }

    public function git_pull()
    {
        $respuesta  = shell_exec('cd /home/lrlc0h5gdvkf/apps/tureserva && git pull');

        success('Respuesta desde el servidor: '.$respuesta);

        return back();
    }

    public function env_update(Request $request)
    {
        file_put_contents(base_path('.env'), $request->env);

        success('Se modifico archivo .env');

        return back();
    }
}
