<?php

namespace App\Http\Controllers\root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HelpCategory;
use App\Models\HelpPost;

class RootHelpPostController extends Controller
{
    private $path = "root.help.post.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post               = new HelpPost();
        $post->title        = $request->title;
        $post->category_id  = $request->category;
        $post->body         = $request->body;
        $post->save();

        success('Se creo la entrda en la categoria '.$post->category->title);
        return redirect('root/category/help/'.$post->category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category   = HelpCategory::findOrFail($id);
        $categories = HelpCategory::all();
        
        return view($this->path."create", compact('category', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post       = HelpPost::findOrFail($id);
        $categories = HelpCategory::all();

        return view($this->path."edit", compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post               = HelpPost::findOrFail($id);
        $post->title        = $request->title;
        $post->category_id  = $request->category;
        $post->body         = $request->body;
        $post->save();

        success('Se modifico la entrda '.$post->title);
        return redirect('root/category/help/'.$post->category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post               = HelpPost::findOrFail($id);
        $post->delete();

        success('Se elimino la entrda '.$post->title);
        return redirect('root/category/help/'.$post->category->id);
    }
}
