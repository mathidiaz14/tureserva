<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DaysResource;

class DaysResourceController extends Controller
{
    public function store(Request $request)
    {
        $day                = new DaysResource();
        $day->day           = $request->day;
        $day->start         = $request->start;
        $day->end           = $request->end;
        $day->resource_id   = $request->resource;
        $day->save();

        return $day;
    }

    public function delete($id)
    {
        $day = DaysResource::findOrFail($id);
        
        $day->delete();
        return true;
    }
}
