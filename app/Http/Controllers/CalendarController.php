<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calendar;
use App\Models\Business;
use App\Models\Client;
use App\Models\Service;
use App\Models\User;
use App\Models\Resource;
use Carbon\Carbon;
use Auth;

class CalendarController extends Controller
{

    private $path = "admin.calendar.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $tasks      = business()->tasks;
        $user       = null;
        $resource   = null;

        return view($this->path.'index', compact('tasks', 'resource', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $calendar = new Calendar();
        
        if($request->client != null)
        {
            $client = Client::find($request->client);

            $calendar->client_id    = $client->id;
            $calendar->client_name  = $client->name;
            $calendar->client_phone = $client->phone;
        }else
        {
            $calendar->client_name  = $request->client_name;
        }

        //completo los datos del calendario
        $calendar->title        = $request->title;
        $calendar->sms          = $request->sms;
        $calendar->start        = Carbon::create($request->start_date." ".$request->start_hour);
        $calendar->end          = Carbon::create($request->end_date." ".$request->end_hour);
        $calendar->business_id  = Auth::user()->business_id;
        $calendar->status       = "enabled";
        $calendar->code         = calendar_code();
        $calendar->save();

        //flag para comprobar si este calendario tiene usuarios
        $flag_user = false;

        //registra los usuarios que se seleccionaron en el model
        if($request->employees != null)
        {
            foreach($request->employees as $employee_name)
            {
                $user = business()->users->where('name', $employee_name)->first();
                
                if($user != null)
                    $calendar->users()->attach($user);
                        
            }
        }

        //Si tiene al menos un usuario registrado le pone el color de este
        if($calendar->users->count() > 0)
            $calendar->color = $calendar->users()->first()->color;    
        else
            $calendar->color = "#26B99A";

        $calendar->save();


        //registra los recursos que se seleccionaron en el model
        if($request->services != null)
        {
            foreach($request->services as $service_name)
            {
                $service = business()->services->where('name', $service_name)->first();

                if($service != null)
                {
                    $calendar->services()->attach($service);

                    foreach($service->resources as $res)
                        $calendar->resources()->attach($res);
                }
            }
        }

        //registra los recursos que se seleccionaron en el model
        if($request->resources != null)
        {
            foreach($request->resources as $resource_name)
            {
                $resource = business()->resources->where('name', $resource_name)->first();

                if($resource != null)
                    $calendar->resources()->attach($resource);
            }
        }

        //vuelvo a la pantalla de agenda
        $title = "";

        if($calendar->title == null)
        {
            if($calendar->client_name != null)
                $title = $calendar->client_name;
        }
        else
        {
            $title = $calendar->title;
        }

        $result = [
            'id'        => $calendar->id,
            'title'     => $title,
            'start'     => $calendar->start->subHours(3),
            'end'       => $calendar->end->subHours(3),
            'color'     => $calendar->color,
        ];

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $task = Calendar::findOrFail($id);

        if(($task != null) and (check_business($task)))
            return view($this->path.('modal_view_content'), compact('task'));

        return false;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Calendar::findOrFail($id);

        if(($task != null) and (check_business($task)))
            return view($this->path.('modal_edit_content'), compact('task'));

        return false;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $calendar  = Calendar::findOrFail($id);
        $old       = Calendar::findOrFail($id);

        if(($calendar == null) or (!check_business($calendar)))
            return false;

        //Si el cliente se selecciono lo pongo en el evento
        if($request->client != null)
        {
            $client = Client::find($request->client);

            if($client != null)
            {
                $calendar->client_id    = $client->id;
                $calendar->client_name  = $client->name;
                $calendar->client_phone = $client->phone;
            }
        }else
        {
            //pongo este nombre en el calendario
            $calendar->client_name  = $request->client_name;
        }

        //completo los datos del calendario
        $calendar->title        = $request->title;
        $calendar->sms          = $request->sms;
        $calendar->title        = $request->title;
        $calendar->start        = Carbon::create($request->start_date." ".$request->start_hour);
        $calendar->end          = Carbon::create($request->end_date." ".$request->end_hour);
        $calendar->save();

        //registra los usuarios que se seleccionaron en el model
        
        //Saco todos los usuarios para que no se dupliquen
        $calendar->users()->detach(); 

        //registra los usuarios que se seleccionaron en el model
        if($request->employees != null)
        {
            foreach($request->employees as $employee_name)
            {
                $user = business()->users->where('name', $employee_name);
                
                if($user != null)
                    $calendar->users()->attach($user);
                        
            }
        }

        //Si tiene al menos un usuario registrado le pone el color de este
        if($calendar->users->count() > 0)
            $calendar->color = $calendar->users()->first()->color;    
        else
            $calendar->color = "#26B99A";

        $calendar->save();

        $calendar->services()->detach();

        //registra los recursos que se seleccionaron en el model
        if($request->services != null)
        {
            foreach($request->services as $service_name)
            {
                $service = business()->services->where('name', $service_name)->first();

                if($service != null)
                {
                    $calendar->services()->attach($service);

                    foreach($service->resources as $res)
                        $calendar->resources()->attach($res);
                }
            }
        }

        //registra los recursos que se seleccionaron en el model
        $calendar->resources()->detach();

        //registra los recursos que se seleccionaron en el model
        if($request->resources != null)
        {
            foreach($request->resources as $resource_name)
            {
                $resource = business()->resources->where('name', $resource_name);

                if($resource != null)
                    $calendar->resources()->attach($resource);
            }
        }

        if(($old->start != $calendar->start) or ($old->end != $calendar->end))
        {
            if($calendar->client_id != null) 
            {
                $services   = "--";
                $users      = "--";

                if($calendar->services->count() > 0)
                {
                    $services = "";

                    foreach($calendar->services as $service)
                        $services .= $service->name.", ";
                    
                    $services = substr($services, 0, -2);
                }

                if($calendar->users->count() > 0)
                {
                    $users = "";

                    foreach($calendar->users as $user)
                        $users .= $user->name.", ";
                    
                    $users = substr($users, 0, -2);
                }

                $data = [
                    'business' => $calendar->business->name,
                    'service' => $services,
                    'user' => $users,
                    'date' => $calendar->date,
                    'hour' => $calendar->hour,
                    'name' => $calendar->client->name ?? '--',
                    'phone' => $calendar->client->phone ?? '--',
                    'email' => $calendar->client->email ?? '--',
                    'comment' => $calendar->description ?? '--',
                    'code' => $calendar->code,
                    'status' => 'Modificado',
                ];

                $array = [
                        'business'  => $calendar->business->name,
                        'day'       => $calendar->start->format('d/m/Y'), 
                        'hour'      => $calendar->start->format('H:i')
                    ];

                if($calendar->client->email != null)
                    send_email('emails.task_updated', $data, $calendar->client->email, "Reserva modificada");
                
                if(($calendar->sms == "on") and ($calendar->client->phone != null))
                {
                    send_sms(
                        $calendar->client_phone, 
                        replace_messages($calendar->business_id, 'message_modify_task', $array), 
                        $calendar->business_id
                    );
                }
            }
        }

        //vuelvo a la pantalla de agenda
        
        $title = "";

        if($calendar->title == null)
        {
            if($calendar->client_name != null)
                $title = $calendar->client_name;
        }
        else
        {
            $title = $calendar->title;
        }

        $result = [
            'id'        => $calendar->id,
            'title'     => $title,
            'start'     => $calendar->start->subHours(3),
            'end'       => $calendar->end->subHours(3),
            'color'     => $calendar->color,
            'status'    => $calendar->status
        ];

        return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $calendar = Calendar::findOrFail($id);

        if(($calendar == null) or (!check_business($calendar)))
            return false;

        if($request->message != null)
        {       
            if($calendar->client_id != null) 
            {
                $data = array(
                    'business'  => $calendar->business->name,
                    'date'      => $calendar->start->format('d/m/Y'),
                    'hour'      => $calendar->start->format('H:i'),
                    'comment'   => $request->message,
                );
                
                if($calendar->client->email != null)
                    send_email('emails.task_delete', $data, $calendar->client->email, "Reserva eliminada");
            }

            if(($calendar->sms == "on") and ($calendar->client_phone != null))
            {
                $array = [
                    'business'  => $calendar->business->name,
                    'day'       => $calendar->start->format('d/m/Y'),
                    'hour'      => $calendar->start->format('H:i'),
                    'comment'   => $request->message,
                ];
                
                send_sms(
                    $calendar->client_phone, 
                    replace_messages($calendar->business_id, 'message_delete_task', $array), 
                    $calendar->business_id
                );
            }
        }

        $calendar->users()->detach();
        $calendar->resources()->detach();
        $calendar->services()->detach();
        $calendar->delete();

        return true;
    }

    public function accept(Request $request)
    {
        $calendar           = Calendar::find($request->id);

        if(($calendar == null) or (!check_business($calendar)))
        {
            error(__('errors.update'));
            return back();
        }

        $calendar->status   = "enabled";
        $calendar->save();

        $services   = "--";
        $users      = "--";

        if($calendar->services->count() > 0)
        {
            $services = "";

            foreach($calendar->services as $service)
                $services .= $service->name.", ";
            
            $services = substr($services, 0, -2);
        }

        if($calendar->users->count() > 0)
        {
            $users = "";

            foreach($calendar->users as $user)
                $users .= $user->name.", ";
            
            $users = substr($users, 0, -2);
        }

        $data = [
            'business' => $calendar->business->name,
            'service' => $services,
            'user' => $users,
            'date' => $calendar->date,
            'hour' => $calendar->hour,
            'name' => $calendar->client->name ?? '--',
            'phone' => $calendar->client->phone ?? '--',
            'email' => $calendar->client->email ?? '--',
            'comment' => $calendar->description ?? '--',
            'code' => $calendar->code,
            'status' => $calendar->status == 'enabled' ? 'Aprobado' : 'Pendiente',
        ];

        send_email('emails.task_approved', $data, $calendar->client->email, "Reserva confirmada");
        
        if(($calendar->sms == "on") and isset($calendar->client->phone))
        {
            $array = [
                'business'  => $calendar->business->name,
                'day'       => $calendar->start->format('d/m/Y'), 
                'hour'      => $calendar->start->format('H:i'), 
                'code'      => $calendar->code
            ];
            
            send_sms(
                $calendar->client_phone, 
                replace_messages($calendar->business_id, 'message_confirm_task', $array), 
                $calendar->business_id
            );
        }

        success(__('message.task_approved'));
        return back();
    }

    public function refuse(Request $request)
    {
        $task           = Calendar::find($request->id);

        if(($task == null) or (!check_business($task)))
        {
            error(__('errors.update'));
            return back();
        }

        $data = array(
            'business'  => $task->business->name,
            'date'      => $task->start->format('d/m/Y'),
            'hour'      => $task->start->format('H:i'),
            'comment'   => $request->comment,
        );

        send_email('emails.task_refuse', $data, $task->client->email, "Reserva rechazada");
        
        if(($task->sms == "on") and isset($task->client->phone))
        {
            $array = [
                'business'  => $task->business->name,
                'day'       => $task->start->format('d/m/Y'), 
                'hour'      => $task->start->format('H:i'), 
                'comment'   => $request->comment,
            ];
            
            send_sms(
                $task->client_phone, 
                replace_messages($task->business_id, 'message_refuse_task', $array), 
                $task->business_id
            );
        }

        $task->delete();

        success(__('message.task_refuse'));
        return back();
    }

    public function users($id)
    {       
        $user       = User::findOrFail($id);
        $resource   = null; 

        if(($user == null) or (!check_business($user)))
        {
            error(__('errors.open'));
            return back();
        }

        $tasks  = $user->tasks;
        return view($this->path.'index', compact('tasks', 'user', 'resource'));
    }

    public function resources($id)
    {
        $resource   = Resource::findOrFail($id);
        $user       = null;

        if(($resource == null) or (!check_business($resource)))
        {
            error(__('errors.open'));
            return back();
        }

        $tasks  = $resource->tasks;
        
        return view($this->path.'index', compact('tasks', 'user', 'resource'));
    }

    public function faild($id)
    {
        $calendar = Calendar::findOrFail($id);

        if(($calendar == null) or (!check_business($calendar)))
            return false;
    
        if ($calendar->status == "faild")
            $calendar->status = "enabled";
        else 
            $calendar->status = "faild";

        $calendar->save();

        $result = [
            'id'        => $calendar->id,
            'title'     => $calendar->title ?? $calendar->client_name,
            'start'     => $calendar->start->subHours(3),
            'end'       => $calendar->end->subHours(3),
            'color'     => $calendar->color,
            'status'    => $calendar->status
        ];

        return $result;
    }

    public function reload(Request $request)
    {
        if(Auth::check())
        {
            $url = url()->previous();
            
            if($url == url('calendar'))
            {
                $tasks      = business()->tasks;
                $user       = null;
                $resource   = null;

                return view('admin.calendar.calendar', compact('tasks', 'user', 'resource'));
            
            }elseif(str_contains($url, 'users/calendar'))
            {
                $split      = explode('/', $url);
                $user       = business()->users->where('id', $split[count($split) - 1])->first();
                $resource   = null;

                if($user != null)
                {
                    $tasks = $user->tasks;
                    return view('admin.calendar.calendar', compact('tasks', 'user', 'resource'));        
                }
            }elseif(str_contains($url, 'resources/calendar'))
            {
                $split      = explode('/', $url);
                $resource   = business()->resources->where('id', $split[count($split) - 1])->first();
                $user       = null;

                if($resource != null)
                {
                    $tasks = $resource->tasks;
                    return view('admin.calendar.calendar', compact('tasks', 'user', 'resource'));        
                }
            }
        }
    }

    public function count()
    {
        if(Auth::check())
            return count(business()->tasks);
    }
}
