<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Business;
use App\Models\Days;
use App\Models\DaysUsers;
use Carbon\Carbon;
use Storage;
use File;
use Auth;

class BusinessController extends Controller
{
    private $path = "admin.business.";
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business = Auth::user()->business;    
        return view($this->path."index", compact('business'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $old_short          = Business::where('code', $request->code)->first();
        $old_email          = Business::where('email', $request->email)->first();
        $business           = business();

        if(($old_short != null) and ($old_short->id != $business->id))
        {
            error(__('errors.repeat_code'));
            return back();
        }

        if(($old_email != null) and ($old_email->id != $business->id))
        {
            error(__('errors.repeat_user'));
            return back();
        }

        $file               = $request->file('logo');

        if($file != null)
        {
            if(file_exists($business->logo))
                unlink($business->logo);

            $nameFile           = "logo_".$business->id;
            Storage::disk('business')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));
            
            $business->logo     = "storage/business/".$nameFile.".".$file->getClientOriginalExtension();
        }

        $business->name     = $request->name;
        $business->phone    = $request->phone;
        $business->email    = $request->email;
        $business->code     = $request->code;
        $business->rut      = $request->rut;
        $business->address  = $request->address;
        $business->save();

        foreach($business->users->where('business_hour', 'on') as $user)
        {
            $user->days()->delete();

            foreach(business()->days as $day)
            {
                $user_day                = new DaysUsers();
                $user_day->day           = $day->day;
                $user_day->start         = $day->start;
                $user_day->end           = $day->end;
                $user_day->status        = $day->status;
                $user_day->user_id       = $user->id;
                $user_day->save();
            }
        }

        l(
            user()->id,
            business()->id,
            "Empresa",
            "Editar",
            "Se editan los datos de la empreasa"
        );

        success(__('message.update_info'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete_logo($id)
    {
        $business = Business::findOrFail($id);

        if($business->id != Auth::user()->business_id)
        {
            error(__('errors.delete_logo'));
            return back();
        }

        if(file_exists($business->logo))
            unlink($business->logo);
        
        $business->logo = null; 
        $business->save();
        
        l(
            user()->id,
            business()->id,
            "Empresa",
            "Logo",
            "Se elimina logo"
        );
        
        success(__('message.delete_logo_info'));
        return back();
    }

    public function get_hours()
    {
        return view('admin.business.hour_table');
    }

    public function set_hours(Request $request)
    {
        $explode    = explode("_", $request->id);
        $time       = $explode[1];
        $id         = $explode[2];
        
        $day        = Days::findOrFail($id);
        $day[$time] = $request->val;
        $day->save();

        return true;
    }

    public function set_status(Request $request)
    {
        $day        = Days::find($request->id);
        
        if($day == null)
            return false;

        $days       = $day->business->days->where('day', $day->day);

        $status     = "on";
        if($request->status == "on")
            $status = "off";

        foreach($days as $day)
        {
            $day->status = $status;
            $day->save();
        }

        return true;
    }

    public function generate_code()
    {
        do {

            $code   = Random_Code(6);
            $exist  = Business::where('control_code', $code)->first();

        } while ($exist != null);

        $business   = business();
        $now        = Carbon::now();

        if(($business->control_date == null) or ($now->diffInMinutes($business->control_date) >= 30))
        {
            $business->control_code = $code;
            $business->control_user = user()->id;
            $business->control_date = Carbon::now();
            $business->save();
            
            return $code;
        }
        
        return $business->control_code;
    }
}
