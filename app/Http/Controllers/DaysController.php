<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DaysUsers;
use App\Models\Days;

class DaysController extends Controller
{

    public function store(Request $request)
    {
        //Guardo el horario que se envia por el request
        $day                = new Days();
        $day->day           = $request->day;
        $day->start         = $request->start;
        $day->end           = $request->end;
        $day->business_id   = business()->id;
        $day->save();

        //Tomo todos los horarios del dia que se modifico para copiarlo a los usuarios
        $days               = business()->days->where('day', $request->day);

        //Recorro todos los usuarios que tengan el mismo horario que la empresa
        foreach(business()->users->where('business_hour', 'on') as $user)
        {
            //Selecciono todos los horarios del usuario del dia que se envio
            $days_user = $user->days->where('day', $request->day);

            //Se borran los dias del usuario
            foreach($days_user as $d)
                $d->delete();

            foreach($days as $d)
            {
                $day                = new DaysUsers();
                $day->day           = $request->day;
                $day->start         = $request->start;
                $day->end           = $request->end;
                $day->user_id       = $user->id;
                $day->save();
            }
        }

        return $day;
    }

    public function delete($id)
    {
        $day = Days::findOrFail($id);

        if(!check_business($day))
        {
            error(__('errors.delete'));
            return false;
        }
        
        $day->delete();
        return true;
    }
}
