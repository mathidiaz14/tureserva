<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FirstController extends Controller
{
    private $path = "admin.first.";

    public function index()
    {
        return view($this->path."step1");   
    }

    public function step2()
    {   
        return view($this->path."step2");        
    }

    public function step3()
    {
        return view($this->path."step3");        
    }

    public function finish()
    {
        $business               = business();
        $business->first_login  = "complete";
        $business->save();  

        success(__('message.fist_steps_complete'));
        return redirect('calendar');
    }
}
