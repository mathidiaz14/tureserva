<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Business;
use App\Models\Service;
use App\Models\User;
use App\Models\Client;
use App\Models\TaskTmp;
use App\Models\Contact;
use App\Models\Calendar;
use App\Models\Waiting;
use App\Models\WaitingDay;
use App\Models\Opinion;
use App\Models\Visit;
use Carbon\Carbon;
use Jenssegers\Agent\Agent;
use MercadoPago;

class WebController extends Controller
{
    public function show($id)
    {
        $business = Business::where('code', $id)->first();
        
        if($business != null)
        {
            if($business->reservation->access == 'on')
                return view("landings.".$business->folder.".index", compact('business'));
            else
                abort(400);
        }
        
        abort(401);
    }

    public function store(Request $request)
    {
        $business   = Business::find($request->business);
        $service    = Service::find($request->service);

        //Si el usuario seleccionado es "Nadie en particular", elijo un usuario cualquiera y compruebo si a esa hora tiene una cita
        //si no la tiene uso ese usuario, si tiene una cita elijo otro
        
        if($request['user'] == "nobody")
        {    
            $users          = $service->users->where('online', 'on');
            $array_users    = array();

            //paso una collecion a un array para poder operarlo mas facil
            foreach($users as $user)
                array_push($array_users, $user);

            do
            {
                //Selecciono un numero entre 0 y el limite del array
                $number             = rand(0, count($array_users)-1);

                //Selecciono un usuario random con el numero anterior
                $user               = $array_users[$number];
                $request['user']    = $user->id;

                //elimino a ese usuario del array por si tengo que repetir el while no seleccione el mismo
                unset($array_users[$number]);

            }while(!$this->task_check($request));
            //Chequeo si se puede realizar la reserva con ese usuario 
        }

        if(!$this->task_check($request))
        {
            error(__('errors.not_hour_reservation'));
            return redirect($business->code);
        }
        
        //si esta habilitado el pago con MP
        if($business->return_status == "on")
        {
            $service    = Service::find($request->service);

            if(($service->price == '0') or ($service->price == null))
            {
                $code = $this->task_create($request->all());
        
                session(['confirmation_code' => $code]);
                return back();    
            }

            $id         = $this->task_create_tmp($request->all());

            MercadoPago\SDK::configure(['ACCESS_TOKEN' => $business->mp_access_token]);

            $item                               = new MercadoPago\Item();
            $item->title                        = $service->name." - ".$business->name;
            $item->quantity                     = 1;
            $item->currency_id                  = "UYU";
            $item->unit_price                   = $service->price;

            $preference                         = new MercadoPago\Preference();
            $preference->items                  = array($item);
            $preference->back_urls              = array(
                                                    "success" => url('task/success'),
                                                    "failure" => url('task/failed')
                                                    );
            $preference->auto_return            = "approved"; 
            $preference->binary_mode            = true;
            $preference->payment_methods        = array(
                                                        "excluded_payment_methods" => array(),
                                                        "excluded_payment_types" => array(
                                                            array("id" => "ticket")
                                                        ),
                                                    );
            $preference->external_reference     = $id;
            $preference->save();

            return view("landings.".$service->business->folder.".pay", compact('preference', 'business'));
        }

        $code = $this->task_create($request->all());
        
        session(['confirmation_code' => $code]);
        return back();
    }

    public function task_check($request)
    {
        $service    = Service::find($request['service']);
        $user       = User::find($request['user']);

        $start      = Carbon::create($request['date']." ".$request['hour']);
        $end        = Carbon::create($request['date']." ".$request['hour'])->addMinutes($service->duration);

        $flag       = true;

        do
        {
            $calendars = $user->tasks->where('start', $start)->where('status', 'enabled');

            foreach($calendars as $calendar)
            {
                foreach($calendar->users as $user)
                {
                    if($user->pivot->user_id == $request['user'])
                        $flag = false;
                }
            }

            $start->addMinutes($service->business->reservation->interval);

        }while(($flag) and ($start->diffInMinutes($end, false) > 0));

        return $flag;
    }

    public function task_success(Request $request)
    {
        $tmp            = TaskTmp::find($request->external_reference);
        $business       = Business::find($tmp->business);
        
        if(!$this->task_check($tmp))
        {
            error(__('errors.not_hour_reservation'));
            return redirect($business->code);
        }

        //Controlo que el pago realmente se haya realizado
        MercadoPago\SDK::configure(['ACCESS_TOKEN' => $business->mp_access_token]);
        
        $payment = MercadoPago\Payment::find_by_id($request->payment_id);

        if(($payment == null) or ($payment->status != "approved"))
        {
            error(__('errors.pay_not_check'));
            return redirect($business->code);
        }

        $code = $this->task_create($tmp, $request->payment_id);

        Session(['confirmation_code' => $code]);

        return redirect($business->code);
    }

    public function task_failed(Request $request)
    {
        $tmp            = TaskTmp::find($request->external_reference);
        $business       = Business::find($tmp->business);

        $tmp->delete();

        error(__('errors.pay_not_process'));
        return redirect($business->code);
        
    }

    public function task_create($request, $mp_id = null)
    {
        //Variables
        $service    = Service::find($request['service']);
        $business   = Business::find($request['business']);
        $user       = User::find($request['user']);
        $start      = Carbon::create($request['date']." ".$request['hour']);
        $end        = Carbon::create($request['date']." ".$request['hour']);;
        
        //Le sumo la duracion del servicio a la variable end
        $end->addMinutes($service->duration);

        //Busco el cliente para esta empresa
        $client = $business->clients->where('email', $request['email'])->first();
                        
        //Si el cliente no existe lo creo
        if($client == null)
        {
            $client                 = new Client();
            $client->name           = $request['name'];
            $client->phone          = $request['phone'];
            $client->email          = $request['email'];
            $client->business_id    = $request['business'];
            $client->save(); 
        }

        //codigo aleatorio de la tarea
        $code = calendar_code();

        //Creo el evento
        $calendar               = new Calendar();
        $calendar->client_id    = $client->id;
        $calendar->client_name  = $client->name;
        $calendar->client_phone = $client->phone;
        $calendar->start        = $start;
        $calendar->end          = $end;
        $calendar->description  = $request['description'];
        $calendar->online       = "yes";
        $calendar->service_id   = $request['service'];
        $calendar->service_name = $service->name;
        $calendar->business_id  = $request['business'];    
        $calendar->code         = $code;
        $calendar->amount       = $service->price;
        $calendar->mp_id        = $mp_id;
        
        //Reservo automaticamente si esta opcion esta habilitada en las reservas
        if($business->reservation->auto == "on")
            $calendar->status   = "enabled";
        else
            $calendar->status   = "pending";
        
        $calendar->save();

        //Asocio el usuario a el evento
        if(($business->reservation->view_users != "on") and ($request['user'] != "nobody"))
        {
            $calendar->color        = $user->color;
            $calendar->users()->attach($user);
        }else
        {
            $calendar->color        = "#26B99A";
        }

        $calendar->save();

        notification(
            __('message.new_reservation'), 
            __('message.new_reservation_info'), 
            url('home'), 
            $business->id
        );
        
        if(($business->reservation->view_users != "on") and ($request['user'] != "nobody"))
            $tmp_user = $user->name;
        else
            $tmp_user = "--";

        if($request['description'] == "")
            $tmp_comment = "--";
        else
            $tmp_comment = $request['description'];

        if($business->reservation->auto == "on")
            $tmp_status = 'Aprobado';
        else
            $tmp_status = 'Pendiente';

        $data = array(
            'business'  => $business->name,
            'service'   => $service->name,
            'user'      => $tmp_user,
            'date'      => $request['date'],
            'hour'      => $request['hour'],
            'name'      => $request['name'],
            'phone'     => $request['phone'],
            'email'     => $request['email'],
            'comment'   => $tmp_comment,
            'code'      => $code,
            'status'    => $tmp_status,
        );

        if($business->reservation->auto == "on")
        {
            send_email('emails.task_approved', $data, $request['email'], "Reserva online");
            
            $array = [
                'business'  => $business->name,
                'day'       => $request['date'], 
                'hour'      => $request['hour'], 
                'code'      => $code
            ];
            
            send_sms(
                $request['phone'], 
                replace_messages($calendar->business_id, 'message_autoconfirm_reserve', $array), 
                $business->id
            );
        }
        else    
        {
            send_email('emails.task_waiting_for_approval', $data, $request['email'], "Reserva online");

            $array = [
                'business'  => $business->name,
                'day'       => $request['date'], 
                'hour'      => $request['hour'], 
                'code'      => $code
            ];
            
            send_sms(
                $request['phone'], 
                replace_messages($calendar->business_id, 'message_confirm_reserve', $array), 
                $business->id
            );
        }

        return $code;
    }

    public function task_create_tmp($request)
    {
        $task               = new TaskTmp();
        $task->service      = $request['service'];
        $task->user         = $request['user'];
        $task->date         = $request['date'];
        $task->hour         = $request['hour'];
        $task->business     = $request['business'];
        $task->name         = $request['name'];
        $task->email        = $request['email'];
        $task->phone        = $request['phone'];
        $task->description  = $request['description'];
        $task->save();

        return $task->id;
    }

    public function body($id)
    {
        $business = Business::where('code', $id)->first();
        
        if($business != null)
        {
            if($business->reservation->access == 'on')
                return view("landings.".$business->folder.".content", compact('business'));
            else
                abort(400);
        }
        
        abort(401);   
    }

    public function setContact(Request $request)
    {
        $business = Business::find($request->business);

        if($business == null)
        {
            error(__('message.error_contact'));
            return back();
        }

        //Creo el mensaje en la base de datos
        $contact                = new Contact();
        $contact->business_id   = $request->business;
        $contact->name          = $request->name;
        $contact->email         = $request->email;
        $contact->message       = $request->message;
        $contact->status        = "enabled";
        $contact->save();

        //Creo una notificacion
        notification(
            __('message.web_contact'), 
            __('message.web_contact_info'), 
            url('contact'), 
            $business->id
        );

        $data = array(
            'name'      => $request->name,
            'email'     => $request->email,
            'body'      => $request->message,
        );

        send_email('emails.web_contact', $data, $business->email, __('message.web_contact_reservation'));

        success(__('message.message_send'));
        return redirect($contact->business->code."#contact");
    }

    public function get_cancel()
    {
        $code = "";
        return view('landings.cancel', compact('code'));
    }

    public function get_cancel_id($code)
    {
        return view('landings.cancel', compact('code'));
    }

    public function set_cancel(Request $request)
    {
        $task   = Calendar::where('code', $request->code)->first();
        $money  = 0;

        if(($task == null) or ($task->status != 'enabled'))
        {
            error('<i class="fa fa-exclamation-triangle fa-3x" style="margin:20px"></i> <br> '.__('errors.not_reservation'));
            return back()->withInput();
        }

        if(($task->mp_id != null) and ($task->business->return_option != "no")) 
        {
            MercadoPago\SDK::configure(['ACCESS_TOKEN' => $task->business->mp_access_token]);
            
            $payment = MercadoPago\Payment::find_by_id($task->mp_id);
            
            if($payment->status == "approved")
            {
                if($task->business->return_option == "all")
                    $money = $task->amount;
                elseif($task->business->return_option == "partial")
                    $money = ($task->amount * $task->business->return_percentage) / 100;
                
                $payment->refund($money);   
            }
        }

        $task->status = "cancel";
        $task->save();

        notification(
            __('web.reservation_cancel'), 
            __('web.reservation_cancel_number', [
                                        'day'   => $task->start->format('d/m/Y'),
                                        'hour'  => $task->start->format('H:i')
                                        ]), 
            url('calendar'), 
            $task->business->id
        );

        if($money == 0)
            success(__('web.reservation_cancel_success'));
        else
            success(__('web.reservation_cancel_success_money', ['money' => $money]));

        return back();
    }

    public function tips($id)
    {
        $business = Business::where('code', $id)->first();

        if($business == null)
            return redirect('/');

        return view("landings.".$business->folder.".tips", compact('business'));
    }

    public function opinions($id)
    {
        $business = Business::where('code', $id)->first();

        if($business == null)
            return redirect('/');

        return view("landings.".$business->folder.".opinions", compact('business'));      
    }

    public function gallery($id)
    {
        $business = Business::where('code', $id)->first();

        if($business == null)
            return redirect('/');

        return view("landings.".$business->folder.".gallery", compact('business'));      
    }

    public function map($id)
    {
        $business = Business::where('code', $id)->first();

        if($business == null)
            return redirect('/');

        return view("landings.".$business->folder.".map", compact('business'));      
    }

    public function getContact($id)
    {
        $business = Business::where('code', $id)->first();

        if($business == null)
            return redirect('/');

        return view("landings.".$business->folder.".contact", compact('business'));      
    }

    public function hours($id)
    {
        $business = Business::where('code', $id)->first();

        if($business == null)
            return redirect('/');

        return view("landings.".$business->folder.".hours", compact('business'));      
    }


    public function set_waiting(Request $request)
    {
        $waiting                = new Waiting();
        $waiting->business_id   = $request->business;
        $waiting->service_id    = $request->service;
        $waiting->user_id       = $request->user;
        $waiting->start         = $request->start;
        $waiting->end           = $request->end;
        $waiting->name          = $request->name;
        $waiting->email         = $request->email;
        $waiting->phone         = $request->phone;
        $waiting->comment       = $request->comment;
        $waiting->save();

        for($i = 1; $i < $waiting->business->reservation->days_reserve; $i++)
        {
            $day = Carbon::now()->addDay($i);
            
            if($request[$day->format('d-m-Y')] == "on")
            {
                $waitingDay                = new WaitingDay();
                $waitingDay->waiting_id    = $waiting->id;
                $waitingDay->date          = $day->format('d-m-Y');
                $waitingDay->save();
            }
        }

        if($request->user == "nobody")
        {
            $users = $waiting->service->users->where('online', 'on');

            do
            {
                $number = rand(0, count($users)-1);

                //Selecciono un usuario cualquiera
                $user = $users[$number];

                unset($users[$number]);
                $date = check_waiting($waiting->id, $user->id);

            }while(($date == null) and (count($users) > 0));

        }else
        {
            $user = User::find($request->user);
            $date = check_waiting($waiting->id, $user->id);
        }

        
        if($date != null)
        {
            $waiting->days()->delete();
            $waiting->delete();

            session(['waiting' => __('message.waiting_list_exist', ['date' => $date, 'user' => $user->name])]);
        }
        else
        {
            //send email
            session(['waiting' => __('message.waiting_list')]);
        }
        
        return back();
    }

    public function get_opinion($id)
    {
        $task   = Calendar::where('code', $id)->first();
        $now    = now();

        if(($task == null) or ($now->isAfter($task)) or ($task->opinion != null))
            return abort(404);

        $business = $task->business;

        //pagina fija para dar opinion
        return view("landings.opinions", compact('task', 'business'));
    }

    public function set_opinion(Request $request)
    {  
        $opinion = new Opinion();

        $opinion->business_id   = $request->business;
        $opinion->calendar_id   = $request->calendar;
        $opinion->star          = $request->stars;
        $opinion->comment       = $request->comment;
        $opinion->save();

        session(['opinion' => __('web.opinion')]);
        return redirect($opinion->business->code);
    }

    public function visit(Request $request)
    {
        $old = Visit::where('ip', $request->ip)
                    ->where('url', $request->url)
                    ->where('business_id', $request->business)
                    ->orderBy('created_at', 'desc')
                    ->first();

        if(($old == null) or (Carbon::now()->diffInMinutes($old->created_at) >= 30))
        {
            $visit              = new Visit();
            $visit->ip          = $request->ip;
            $visit->business_id = $request->business;
            $visit->device      = $request->device;
            $visit->url         = $request->url;
            $visit->city        = $request->city;
            $visit->country     = $request->country;
            $visit->save();
        }

        return true;
    }

    public function qr($id)
    {
        $business = Business::where('api_key', $id)->first();

        if($business != null)
            return redirect(url($business->code));
        
        abort(401);
    }
}
