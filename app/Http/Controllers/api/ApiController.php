<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Business;

class ApiController extends Controller
{
    public function index($api)
    {
        $business = $this->check($api);

        if($business == false)
            return abort(401);

        return view("landings.".$business->folder.".index", compact('business'));
    }

    private function check($api)
    {
        $business = Business::where('api_key', $api)->first();

        if($business == null)
            return false;

        if($business->status == "dasabled")
            return false;

        if($business->app != "enabled")
            return false;

        if($business->reservation->access != "on")
            return false;

        return $business;
    }
}
