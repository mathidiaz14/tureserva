<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Resource;
use Auth;

class ResourceController extends Controller
{
    private $path = "admin.resource.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Auth::user()->business->resources;

        return view($this->path."index", compact('resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resource                   = new Resource();
        $resource->name             = $request->name;
        $resource->type             = $request->type;
        $resource->capacity         = $request->capacity;
        $resource->business_id      = Auth::user()->business->id;
        $resource->business_hour    = "on";
        $resource->save();

        foreach(business()->services as $service)
        {
            if($request['service_'.$service->id] == "on")
                $resource->services()->attach($service);
        }

        foreach(business()->days as $day)
        {
            $resource_day                = new DaysResource();
            $resource_day->day           = $day->day;
            $resource_day->start         = $day->start;
            $resource_day->end           = $day->end;
            $resource_day->status        = $day->status;
            $resource_day->user_id       = $user->id;
            $resource_day->save();
        }

        success(__('message.create_info'));
        return redirect('resource');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resource = Resource::findOrFail($id);

        if(($resource == null) or (!check_business($resource)))
        {
            error(__('errors.open'));
            return back();
        }

        return view($this->path."edit", compact('resource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resource               = Resource::findOrFail($id);

        if(($resource == null) or (!check_business($resource)))
        {
            error(__('errors.update'));
            return back();
        }

        $resource->name             = $request->name;
        $resource->type             = $request->type;
        $resource->capacity         = $request->capacity;
        $resource->business_hour    = $request->business_hour;
        $resource->save();

        $resource->services()->detach();

        foreach(business()->services as $service)
        {
            if($request['service_'.$service->id] == "on")
                $resource->services()->attach($service);
        }

        success(__('message.update_info'));
        return redirect('resource');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource  = Resource::findOrFail($id);

        if(($resource == null) or (!check_business($resource)))
        {
            error(__('errors.delete'));
            return back();
        }      

        $resource->tasks()->detach();
        $resource->delete();

        success(__('message.delete_info'));
        return back();
    }


    public function get_hours($id)
    {
        $resource = Resource::findOrFail($id);
        return view('admin.resource.hour_table', compact('resource'));
    }

    public function set_hours(Request $request)
    {
        $explode    = explode("_", $request->id);
        $time       = $explode[1];
        $id         = $explode[2];
        
        $day        = DaysResource::findOrFail($id);
        $day[$time] = $request->val;
        $day->save();

        return true;
    }

    public function set_status(Request $request)
    {
        $day        = DaysResource::find($request->id);
        
        if($day == null)
            return false;

        $days       = $day->resource->days->where('day', $day->day);

        $status     = "on";
        if($request->status == "on")
            $status = "off";

        foreach($days as $day)
        {
            $day->status = $status;
            $day->save();
        }

        return true;
    }
}
