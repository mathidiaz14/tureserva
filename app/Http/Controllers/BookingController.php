<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\Service;
use App\Models\User;
use App\Models\Resource;
use App\Models\Days;
use App\Models\ServiceDay;
use App\Models\UserDay;
use App\Models\ResourceDay;
use App\Models\Calendar;
use App\Models\Absence;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BookingController extends Controller
{
    public function getServices(Request $request)
    {
        $ids = explode(',', $request->services);

        $services_array = Service::whereIn('id', $ids)
                                 ->with('users.services:id')
                                 ->get()
                                 ->pluck('users.*.services.*.id')
                                 ->flatten()
                                 ->unique()
                                 ->values()
                                 ->all();

        return $services_array;
    }

    public function getUsers(Request $request)
    {
        $ids = explode(',', $request->services);

        $users_array = Service::whereIn('id', $ids)
                                ->with('users:id,name')
                                ->get()
                                ->pluck('users')
                                ->flatten()
                                ->unique('id')
                                ->values()
                                ->map(function($user) {
                                    return [
                                        'id' => $user->id,
                                        'name' => $user->name
                                    ];
                                })
                                ->all();

        return $users_array;
    }


    /**
     * Obtiene los días disponibles dentro de un rango de fechas específico para realizar reservas,
     * considerando horarios de empresa, servicio, usuario y recursos, y verificando las ausencias y reservas.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableDays(Request $request)
    {
        $serviceIds = explode(',', $request->input('services'));
        $userId = $request->input('user_id');

        $services = Service::whereIn('id', $serviceIds)->with('business')->get();
        $business = $services->first()->business;
        $dayReserve = $business->reservation->days_reserve;

        // Inicializar fechas
        $startDate = Carbon::now();
        $endDate = Carbon::now()->addDays($dayReserve + 15); // Considera un periodo mayor para encontrar 15 días disponibles
        $availableDays = [];
        $includeCurrentDay = $business->reservation->today == "on" ? true : false;

        // Iterar sobre un rango de fechas hasta encontrar 15 días disponibles
        $currentDate = $startDate;
        while (count($availableDays) < $dayReserve && $currentDate->lte($endDate)) 
        {
            // Verificar si se debe incluir el día actual
            if (($includeCurrentDay && $currentDate->isSameDay($startDate)) || (!$includeCurrentDay && !$currentDate->isToday())) 
            {
                $dayName = strtolower($currentDate->format('l'));

                $isServiceDayAvailable = $services->every(function ($service) use ($dayName, $currentDate, $business) {
                    return $this->isDayAvailable(Service::class, $service->id, $dayName, $currentDate, $business->id);
                });

                $isBusinessDayAvailable = $this->isDayAvailable(Business::class, $business->id, $dayName, $currentDate);

                if ($userId === 'any') {
                    // Verificar si hay algún usuario disponible para los servicios
                    $users = User::whereHas('services', function ($query) use ($serviceIds) {
                        $query->whereIn('service_id', $serviceIds);
                    })->get();

                    $isUserDayAvailable = $users->contains(function ($user) use ($currentDate, $dayName, $business) {
                        return $this->isDayAvailable(User::class, $user->id, $dayName, $currentDate, $business->id);
                    });
                } else {
                    $isUserDayAvailable = $this->isDayAvailable(User::class, $userId, $dayName, $currentDate, $business->id);
                }

                // Comprobar disponibilidad de recursos asociados a los servicios
                $resourceIds = $services->pluck('resources.*.id')->flatten();
                $areResourcesAvailable = $resourceIds->every(function ($resourceId) use ($currentDate, $dayName, $business) {
                    return $this->isDayAvailable(Resource::class, $resourceId, $dayName, $currentDate, $business->id);
                });

                // Si todos los componentes están disponibles, añadir el día al array de días disponibles
                if ($isBusinessDayAvailable && $isServiceDayAvailable && $isUserDayAvailable && $areResourcesAvailable) {
                    $availableDays[] = $currentDate->toDateString();
                }
            }

            // Avanzar al siguiente día
            $currentDate->addDay();
        }

        return response()->json($availableDays);
    }


    /**
     * Verifica si un día específico está disponible para realizar reservas.
     *
     * @param  string  $model
     * @param  int|string  $modelId
     * @param  string  $dayOfWeek
     * @param  \Carbon\Carbon  $date
     * @param  int  $businessId
     * @return bool
     */
    private function isDayAvailable($model, $modelId, $dayOfWeek, $date, $businessId = null)
    {
        // Obtener la instancia del modelo correspondiente
        $modelInstance = $model::with('days')->find($modelId);

        // Verificar ausencias del modelo en la fecha dada.
        $absences = Absence::where('model_id', $modelId)
                           ->where('model', $model)
                           ->where(function ($query) use ($date) {
                               $query->whereDate('start', '<=', $date)
                                     ->whereDate('end', '>=', $date);
                           })->get();

        // Si hay ausencias registradas, verificar si son parciales o completas
        foreach ($absences as $absence) {
            if ($absence->start->format('Y-m-d') == $date->format('Y-m-d') && $absence->end->format('Y-m-d') == $date->format('Y-m-d')) {
                // Si la ausencia cubre todo el día
                if ($absence->start->format('H:i:s') == '00:00:00' && $absence->end->format('H:i:s') == '23:59:59') {
                    return false;
                }
            }
        }

        // Verificar si el día de la semana está disponible en los días de operación del modelo.
        $availableDays = $modelInstance->days->where('day', $dayOfWeek);

        // Si hay algún día con estado "off", el día no está disponible.
        if ($availableDays->contains('status', 'off')) {
            return false;
        }
        
        // Si el modelo no es Business y no hay días específicos para el modelo o usa horas de la empresa, verificar los días de la empresa.
        if ($model !== "App\Models\Business" && ($availableDays->isEmpty() || $modelInstance->business_hour === 'on' || $modelInstance->business_hour === null)) 
        {
            $businessDays = Days::where('business_id', $businessId)->where('day', $dayOfWeek)->get();

            if ($businessDays->contains('status', 'off')) 
                return false;
            
            return $businessDays->isNotEmpty();
        }

        // Si hay días disponibles y ninguno está en estado "off", el día está disponible.
        return true;
    }

    /**
     * Obtiene las horas disponibles en un día específico para realizar reservas.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableHours(Request $request)
    {
        $businessId = $request->input('business_id');
        $serviceIds = explode(',', $request->input('services'));
        $userId = $request->input('user_id');
        $allHoursQuery = $request->input('allHours');
        $date = Carbon::parse($request->input('date'));
        $business = Business::findOrFail($businessId);
        $interval = $business->reservation->interval;

        // Obtener la duración total de los servicios
        $services = Service::whereIn('id', $serviceIds)->get();
        $serviceDuration = $services->sum('duration'); // Sumar las duraciones de todos los servicios

        $businessHours = $this->getHoursForModel(Business::class, $businessId, $date, $interval);
        $serviceHours = $this->getCombinedServiceHours($services, $date, $interval, $businessId);
        $userHours = $userId === 'any' ? $this->getAnyUserAvailableHours($serviceIds, $date, $interval, $businessId) : $this->getHoursForModel(User::class, $userId, $date, $interval, $businessId);

        // Obtener horas de los recursos asociados a los servicios
        $resourceHours = $businessHours;
        foreach ($services as $service) {
            foreach ($service->resources as $resource) {
                $resourceHoursTmp = $this->getHoursForModel(Resource::class, $resource->id, $date, $interval, $businessId);
                $resourceHours = array_intersect($resourceHours, $resourceHoursTmp);
            }
        }

        // Unir todas las horas para encontrar la más temprana y la más tardía
        $allHours = array_merge($businessHours, $serviceHours, $userHours, $resourceHours);
        sort($allHours);
        $startOfDay = Carbon::createFromFormat('H:i', $allHours[0], $date->timezone)->setDateFrom($date);
        $endOfDay = Carbon::createFromFormat('H:i', end($allHours), $date->timezone)->setDateFrom($date)->addMinutes($interval);

        // Intersección de todas las horas disponibles
        $availableHours = array_intersect($businessHours, $serviceHours, $resourceHours, $userHours);

        $bookedHoursUser = [];
        
        if ($userId === 'any') {
            $users = User::whereHas('services', function ($query) use ($serviceIds) {
                $query->whereIn('service_id', $serviceIds);
            })->get();

            $bookedHoursUser = [];
            foreach ($users as $user) 
            {
                $bookedUser = $user->tasks->where('start', '>=', $date->startOfDay())->where('start', '<', $date->endOfDay());

                foreach ($bookedUser as $calendar) {

                    $end = Carbon::create($calendar->end);
                    $start = Carbon::create($calendar->start);

                    // Asegurarse de redondear correctamente al intervalo más cercano
                    $start = $start->copy()->floorMinutes($interval);

                    while ($start->lte($end)) {
                        $bookedHoursUser[] = $start->format('H:i');
                        $start->addMinutes($interval);
                    }
                }
            }
            
            $bookedHoursUser =array_values(array_diff_assoc($bookedHoursUser, array_unique($bookedHoursUser)));

        } else {
            // Filtrar horas que ya están reservadas
            $user = User::with('tasks')->findOrFail($userId);
            $bookedUser = $user->tasks->where('start', '>=', $date->startOfDay())->where('start', '<', $date->endOfDay());

            foreach ($bookedUser as $calendar) {
                $end = Carbon::create($calendar->end);
                $start = Carbon::create($calendar->start);

                // Asegurarse de redondear correctamente al intervalo más cercano
                $start = $start->copy()->floorMinutes($interval);

                while ($start->lte($end)) {
                    $bookedHoursUser[] = $start->format('H:i');
                    $start->addMinutes($interval);
                }
            }
        }

        $bookedHoursResource = [];

        foreach ($services as $service) {
            foreach ($service->resources as $resource) {
                // Obtener las tareas del recurso para el día específico
                $bookedResource = $resource->tasks->where('start', '>=', $date->startOfDay())->where('start', '<', $date->endOfDay());

                // Contador de reservas por intervalo de tiempo
                $reservationCount = [];

                foreach ($bookedResource as $calendar) {
                    $end = Carbon::create($calendar->end);
                    $start = Carbon::create($calendar->start);

                    // Asegurarse de redondear correctamente al intervalo más cercano
                    $start = $start->copy()->floorMinutes($interval);

                    while ($start->lte($end)) {
                        $timeSlot = $start->format('H:i');

                        if (!isset($reservationCount[$timeSlot])) {
                            $reservationCount[$timeSlot] = 0;
                        }
                        $reservationCount[$timeSlot]++;

                        // Si el número de reservas para este intervalo alcanza o supera la capacidad, marcarlo como reservado
                        if ($reservationCount[$timeSlot] >= $resource->capacity) {
                            $bookedHoursResource[] = $timeSlot;
                        }

                        $start->addMinutes($interval);
                    }
                }
            }
        }

        // Eliminar duplicados en bookedHoursResource
        $bookedHoursResource = array_unique($bookedHoursResource);

        $bookedHours = array_unique(array_merge($bookedHoursUser, $bookedHoursResource));

        // Obtener la última hora disponible del día
        $lastAvailableHour = end($availableHours);
        $lastAvailableHourCarbon = Carbon::createFromFormat('H:i', $lastAvailableHour, $date->timezone)->setDateFrom($date);

        // Obtener la hora y fecha actuales
        $now = Carbon::now($date->timezone);

        if (empty($allHoursQuery)) {
            // Filtrar las horas disponibles tomando en cuenta la duración del servicio y la hora actual
            $filteredAvailableHours = [];
            foreach ($availableHours as $hour) {
                $hourCarbon = Carbon::createFromFormat('H:i', $hour, $date->timezone)->setDateFrom($date);

                // Verificar que la hora sea mayor a la hora actual
                if ($date->isToday() && $hourCarbon->lte($now)) {
                    continue;
                }

                // Verificar que la hora + duración del servicio no choque con una cita existente o el final del día
                $isValid = true;
                foreach ($bookedHours as $bookedHour) {
                    $bookedHourCarbon = Carbon::createFromFormat('H:i', $bookedHour, $date->timezone)->setDateFrom($date);
                    if ($hourCarbon->copy()->addMinutes($serviceDuration)->gt($bookedHourCarbon) &&
                        $hourCarbon->copy()->lt($bookedHourCarbon)) {
                        $isValid = false;
                        break;
                    }
                }

                // Verificar que la hora + duración del servicio no exceda la última hora disponible
                if ($hourCarbon->copy()->addMinutes($serviceDuration - $interval)->gt($lastAvailableHourCarbon)) {
                    $isValid = false;
                }

                if ($isValid) {
                    $filteredAvailableHours[] = $hour;
                }
            }

            return array_values($filteredAvailableHours);
        } else {
            // Crear el array de horas del día con disponibilidad
            $dailyAvailability = [];
            for ($time = $startOfDay; $time->lt($endOfDay); $time->addMinutes($interval)) {
                $hour = $time->format('H:i');

                // Verificar que la hora sea mayor a la hora actual
                if ($date->isToday() && $time->lte($now)) {
                    $isAvailable = false;
                } else {
                    $isAvailable = in_array($hour, $availableHours) && !$this->isHourConflicting($time, $serviceDuration, $bookedHours, $endOfDay);
                }

                $dailyAvailability[] = [$hour, $isAvailable];
            }

            $dayName = $date->locale('es')->dayName;

            return [$date->format('d-m-Y'), ucfirst($dayName), $dailyAvailability];
        }
    }


    private function getCombinedServiceHours($services, $date, $interval, $businessId)
    {
        $combinedHours = null;

        foreach ($services as $service) {
            $serviceHours = $this->getHoursForModel(Service::class, $service->id, $date, $interval, $businessId);
            if (is_null($combinedHours)) {
                $combinedHours = $serviceHours;
            } else {
                $combinedHours = array_intersect($combinedHours, $serviceHours);
            }
        }

        return $combinedHours ?? [];
    }


    /**
     * Obtiene las horas disponibles para cualquier usuario habilitado para el servicio en un día específico.
     *
     * @param  int  $serviceId
     * @param  \Carbon\Carbon  $date
     * @param  int  $interval
     * @param  int  $businessId
     * @return array
     */
    private function getAnyUserAvailableHours($serviceId, $date, $interval, $businessId)
    {
        $users = User::whereHas('services', function ($query) use ($serviceId) {
            $query->where('service_id', $serviceId);
        })->get();

        // Obtener horas de los recursos asociados al servicio
        $allUsersHours = [];
        foreach ($users as $user) {
            $allUsersHoursTmp = $this->getHoursForModel(User::class, $user->id, $date, $interval, $businessId);
            $allUsersHours = array_merge($allUsersHours, $allUsersHoursTmp);
        }

        // Intersección de todas las horas disponibles
        return array_unique($allUsersHours);
    }

    /**
     * Obtiene las horas de disponibilidad para un modelo en un día específico.
     *
     * @param  string  $model
     * @param  int|string  $modelId
     * @param  \Carbon\Carbon  $date
     * @param  int  $interval
     * @param  int  $businessId
     * @return array
     */
    private function getHoursForModel($model, $modelId, $date, $interval, $businessId = null)
    {
        $dayOfWeek = strtolower($date->format('l'));
        $modelInstance = $model::with('days')->findOrFail($modelId);

        // Inicializar variables de horas
        $hours = [];

        // Si el modelo usa horas de la empresa, obtener horas de la empresa
        if ($model !== "App\Models\Business" && ($modelInstance->business_hour === 'on' || $modelInstance->business_hour === null)) {
            $businessDays = Days::where('business_id', $businessId)->where('day', $dayOfWeek)->get();
            foreach ($businessDays as $businessDay) {
                $start = Carbon::parse($businessDay->start);
                $end = Carbon::parse($businessDay->end);
                for ($time = $start; $time->lt($end); $time->addMinutes($interval)) {
                    $hours[] = $time->format('H:i');
                }
            }
        } else {
            // Obtener horarios específicos del modelo
            $days = $modelInstance->days->where('day', $dayOfWeek);
            foreach ($days as $day) {
                $start = Carbon::parse($day->start);
                $end = Carbon::parse($day->end);
                for ($time = $start; $time->lt($end); $time->addMinutes($interval)) {
                    $hours[] = $time->format('H:i');
                }
            }
        }

        return $hours;
    }

    /**
     * Verifica si una hora específica causa conflicto con las reservas existentes o el final del día.
     *
     * @param  \Carbon\Carbon  $time
     * @param  int  $serviceDuration
     * @param  array  $bookedHours
     * @param  \Carbon\Carbon  $lastAvailableHourCarbon
     * @return bool
     */
    private function isHourConflicting($time, $serviceDuration, $bookedHours, $lastAvailableHourCarbon)
    {
        $endTime = $time->copy()->addMinutes($serviceDuration);

        // Verificar conflicto con horas reservadas
        foreach ($bookedHours as $bookedHour) {
            $bookedHourCarbon = Carbon::createFromFormat('H:i', $bookedHour, $time->timezone)->setDateFrom($time);
            if ($time->lt($bookedHourCarbon) && $endTime->gt($bookedHourCarbon)) {
                return true;
            }
        }

        // Verificar que la hora + duración del servicio no exceda la última hora disponible
        if ($endTime->gt($lastAvailableHourCarbon)) {
            return true;
        }

        return false;
    }

    public function store(Request $request)
    {
        $business = Business::findOrFail($request->business);

        // Seleccionar un usuario aleatorio si se selecciona "nobody"
        if ($request['user'] == "any")
            $request['user'] = $this->getRandomUser($request);
        
        if (($request['user'] == null) or (!$this->taskCheck($request))) 
        {
            return [
                'status' => 'error',
                'message' => __('errors.not_hour_reservation')
            ];
        }

        // Si está habilitado el pago con MercadoPago
        if ($business->return_status == "on") {
            return $this->processPayment($request, $business);
        }

        $code = $this->taskCreate($request->all());
        
        return [
                'status' => 'success',
                'message' => __('web.confirm', ['code' => $code])
            ];
    }

    private function getRandomUser($request)
    {
        $ids = explode(',', $request->services);

        $arrayUsers = Service::whereIn('id', $ids)
                                ->with('users')
                                ->get()
                                ->pluck('users')
                                ->flatten()
                                ->unique('id')
                                ->values()
                                ->all();

        while (count($arrayUsers) > 0) 
        {
            $number             = rand(0, count($arrayUsers) - 1);

            $user               = $arrayUsers[$number];
            $request['user']    = $user['id'];

            unset($arrayUsers[$number]);

            if ($this->taskCheck($request))
                return $user['id'];
        }

        return null; // En caso de no encontrar un usuario disponible
    }

    private function processPayment($request, $business)
    {
        $service = Service::findOrFail($request->service);
        if (($service->price == '0') || ($service->price == null)) {
            $code = $this->taskCreate($request->all());
            session(['confirmation_code' => $code]);
            return back();
        }

        $id = $this->taskCreateTmp($request->all());
        SDK::configure(['ACCESS_TOKEN' => $business->mp_access_token]);

        $item = new Item();
        $item->title = $service->name . " - " . $business->name;
        $item->quantity = 1;
        $item->currency_id = "UYU";
        $item->unit_price = $service->price;

        $preference = new Preference();
        $preference->items = [$item];
        $preference->back_urls = [
            "success" => url('task/success'),
            "failure" => url('task/failed'),
        ];
        $preference->auto_return = "approved";
        $preference->binary_mode = true;
        $preference->payment_methods = [
            "excluded_payment_methods" => [],
            "excluded_payment_types" => [["id" => "ticket"]],
        ];
        $preference->external_reference = $id;
        $preference->save();

        return view("landings." . $service->business->folder . ".pay", compact('preference', 'business'));
    }

    private function taskCheck($request)
    {        
        $array = new \Illuminate\Http\Request([
            "business_id" => $request['business'],
            "services" => $request['services'],
            "user_id" => $request['user'],
            "date" => $request['date'],
        ]);

        $hours = $this->getAvailableHours($array);
        
        if(in_array($request['hour'], $hours))
            return true;
        
        return false;
    }

    private function taskCreateTmp($request)
    {
        $task = new TaskTmp();
        $task->fill($request);
        $task->save();
        return $task->id;
    }

    private function taskCreate($request, $mp_id = null)
    {
        $business = Business::findOrFail($request['business']);
        $user = User::findOrFail($request['user']);

        // Buscar o crear cliente
        $client = $business->clients()->firstOrCreate(
            ['email' => $request['email']],
            ['name' => $request['name'], 'phone' => $request['phone'], 'business_id' => $request['business']]
        );

        $services = explode(',', $request['services']);

        $start = Carbon::create($request['date'] . " " . $request['hour']);

        $end        = $start->copy();   

        foreach($services as $serviceId)
        {
            $service    = Service::findOrFail($serviceId);
            $end->addMinutes($service->duration);
        }
       
        // Crear el evento
        $code = calendar_code();
        $calendar = new Calendar();
        $calendar->fill([
            'client_id' => $client->id,
            'client_name' => $client->name,
            'client_phone' => $client->phone,
            'start' => $start,
            'end' => $end,
            'description' => $request['description'],
            'online' => "yes",
            'business_id' => $request['business'],
            'code' => $code,
            'amount' => $service->price,
            'mp_id' => $mp_id,
            'status' => $business->reservation->auto == "on" ? "enabled" : "pending",
            'sms'   => 'on',
        ]);
        
        $calendar->save();

        if (($business->reservation->view_users != "on") && ($request['user'] != "nobody")) {
            $calendar->color = $user->color;
            $calendar->users()->attach($user);
        } else {
            $calendar->color = "#26B99A";
        }

        $calendar->save();

        foreach($services as $serviceId)
        {
            $service    = Service::findOrFail($serviceId);
            $calendar->services()->attach($service);
            
            foreach($service->resources as $res)
                $calendar->resources()->attach($res);
        }

        $this->sendNotificationsCalendar($calendar);

        return $code;
    }

    private function sendNotificationsCalendar($calendar)
    {
        notification(__('message.new_reservation'), __('message.new_reservation_info'), url('home'), $calendar->business->id);

        $services   = "--";
        $users      = "--";

        if($calendar->services->count() > 0)
        {
            $services = "";

            foreach($calendar->services as $service)
                $services .= $service->name.", ";
            
            $services = substr($services, 0, -2);
        }

        if($calendar->users->count() > 0)
        {
            $users = "";

            foreach($calendar->users as $user)
                $users .= $user->name.", ";
            
            $users = substr($users, 0, -2);
        }

        $data = [
            'business' => $calendar->business->name,
            'service' => $services,
            'user' => $users,
            'date' => $calendar->date,
            'hour' => $calendar->hour,
            'name' => $calendar->client->name ?? '--',
            'phone' => $calendar->client->phone ?? '--',
            'email' => $calendar->client->email ?? '--',
            'comment' => $calendar->description ?? '--',
            'code' => $calendar->code,
            'status' => $calendar->status == 'enabled' ? 'Aprobado' : 'Pendiente',
        ];

        $array = [
            'business'  => $calendar->business->name,
            'day'       => $calendar->date, 
            'hour'      => $calendar->hour, 
            'code'      => $calendar->code
        ];

        if ($calendar->status == 'enabled') 
        {   
            if(isset($calendar->client->email)) 
                send_email('emails.task_approved', $data, $calendar->client->email, "Reserva online");
            
            if(isset($calendar->client->phone)) 
            {
                send_sms(
                    $calendar->client->phone, 
                    replace_messages($calendar->business_id, 'message_autoconfirm_reserve', $array), 
                    $calendar->business->id
                );
            }
        } else {
            if(isset($calendar->client->email)) 
                send_email('emails.task_waiting_for_approval', $data, $calendar->client->email, "Reserva online");
            
            if(isset($calendar->client->phone)) 
            {
                send_sms(
                    $calendar->client->phone, 
                    replace_messages($calendar->business_id, 'message_confirm_reserve', $array), 
                    $calendar->business->id
                );
            }
        }
    }

    public function task_success(Request $request)
    {
        $tmp = TaskTmp::findOrFail($request->external_reference);
        $business = Business::findOrFail($tmp->business);

        if (!$this->taskCheck($tmp)) {
            error(__('errors.not_hour_reservation'));
            return redirect($business->code);
        }

        // Controlar que el pago realmente se haya realizado
        SDK::configure(['ACCESS_TOKEN' => $business->mp_access_token]);
        $payment = \MercadoPago\Payment::find_by_id($request->payment_id);

        if (!$payment || $payment->status != "approved") {
            error(__('errors.pay_not_check'));
            return redirect($business->code);
        }

        $code = $this->taskCreate($tmp, $request->payment_id);
        session(['confirmation_code' => $code]);
        return redirect($business->code);
    }

    public function task_failed(Request $request)
    {
        $tmp = TaskTmp::findOrFail($request->external_reference);
        $business = Business::findOrFail($tmp->business);
        $tmp->delete();

        error(__('errors.pay_not_process'));
        return redirect($business->code);
    }

}
