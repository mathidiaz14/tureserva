<?php

namespace App\Http\Controllers\help;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HelpCategory;
use App\Models\HelpPost;

class HelpController extends Controller
{
    private $path = "help.";

    public function index()
    {
        $categories = HelpCategory::all();
        return view($this->path."index", compact('categories'));
    }

    public function category($id)
    {
        $category = HelpCategory::findOrFail($id);

        if($category == null)
            return redirect('ayuda');

        return view($this->path."category", compact('category'));
    }

    public function post($id)
    {
        $post = HelpPost::findOrFail($id);

        if($post == null)
            return redirect('ayuda');

        return view($this->path."post", compact('post'));   
    }
}
