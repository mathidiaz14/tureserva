<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;
use Auth;

class ClientController extends Controller
{

    private $path = "admin.client.";
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::where('business_id', business()->id)
                            ->orderBy('created_at', 'desc')
                            ->paginate(30);

        return view($this->path.'index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client                 = new Client();
        $client->name           = $request->name;
        $client->address        = $request->address;
        $client->email          = $request->email;
        $client->phone          = $request->phone;
        $client->birthday       = $request->birthday;
        $client->description    = $request->description;
        $client->business_id    = business()->id;
        $client->save();

        success(__('message.create_info'));
        return redirect('client');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);

        if(($client == null) or (!check_business($client)))
        {
            error(__('errors.open'));
            return back();
        }

        return view($this->path.'show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        if(($client == null) or (!check_business($client)))
        {
            error(__('errors.open'));
            return back();
        }

        return view($this->path.'edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);

        if(($client == null) or (!check_business($client)))
        {
            error(__('errors.update'));
            return back();
        }

        $client->name           = $request->name;
        $client->address        = $request->address;
        $client->email          = $request->email;
        $client->phone          = $request->phone;
        $client->birthday       = $request->birthday;
        $client->description    = $request->description;
        $client->save();

        success(__('message.update_info'));
        return redirect('client');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::findOrFail($id);
        
        if(($client == null) or (!check_business($client)))
        {
            error(__('errors.delete'));
            return back();
        }

        foreach($client->tasks as $task)
        {
            $task->client_id = null;
            $task->save();
        }
        
        $client->delete();
 
        success(__('message.delete_info'));
        return back();
    }

    public function create_jquery(Request $request)
    {
        $client                 = new Client();
        $client->name           = $request->name;
        $client->address        = $request->address;
        $client->email          = $request->email;
        $client->phone          = $request->phone;
        $client->description    = $request->description;
        $client->business_id    = business()->id;
        $client->save();

        return $client->id;
    }
}
