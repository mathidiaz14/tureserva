<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DaysService;

class DaysServiceController extends Controller
{
    public function store(Request $request)
    {
        $day                = new DaysService();
        $day->day           = $request->day;
        $day->start         = $request->start;
        $day->end           = $request->end;
        $day->service_id    = $request->service;
        $day->save();

        return $day;
    }

    public function delete($id)
    {
        $day = DaysService::findOrFail($id);
        
        $day->delete();
        return true;
    }
}
