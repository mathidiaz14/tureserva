<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notification;
use Auth;

class NotificationController extends Controller
{
    private $path = "admin.notification.";
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::where('business_id', business()->id)->paginate(20);

        foreach($notifications as $notification)
        {
            $notification->status = "disabled";
            $notification->save();
        }

        return view($this->path.'index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = Notification::findOrFail($id);

        if(($notification == null) or (!check_business($notification)))
        {
            error(__('errors.open'));
            return redirect('home');
        }
        
        $notification->status = "disabled";
        $notification->save();

        return redirect($notification->link);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::findOrFail($id);

        if(($notification == null) or (!check_business($notification)))
        {
            error(__('errors.delete'));
            return back();
        }
        
        $notification->delete();

        success(__('message.delete_info'));
        return back();
    }

    public function view()
    {
        if(Auth::check())
            return view('layouts.parts.notifications');
    }

    public function read()
    {
        $notifications = business()->notifications->where('status', 'pending');

        foreach($notifications as $notification)
        {
            $notification->status = "enabled";
            $notification->save();
        }

        return true;
    }
}
