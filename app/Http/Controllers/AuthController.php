<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CalendarOption;
use App\Models\WeekHourUser;
use App\Models\Reservation;
use App\Models\Business;
use App\Models\WeekHour;
use App\Models\User;
use App\Models\Days;
use App\Models\DaysUsers;
use App\Models\Code;
use Carbon\Carbon;
use MercadoPago;
use Auth;
use Hash;

class AuthController extends Controller
{
    private $path = "auth.";


    public function showRegistrationForm()
    {
        if(!Auth::check())
            return view($this->path."register");

        return redirect('home');
    }

    public function showLoginForm(Request $request)
    {
        if(!Auth::check())
            return view($this->path."login", compact('request'));   

        return redirect('home');
    }

    public function get_password_request()
    {
        return view('auth.passwords.email');
    }

    public function set_password_request(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if(!$user)
        {
            error(__('errors.not_user'));
            return back();
        }

        $invitation_code        = Random_String(10);
        $user->invitation_code  = $invitation_code;
        $user->invitation_hour  = Carbon::now();
        $user->save();

        $data = array(
            'invitation_code'   => $invitation_code,
        );

        send_email('emails.password.reset', $data, $user->email, 'Reseteo de contraseña');

        l(
            $user->id,
            $user->business_id,
            "Contraseña",
            "Olvido de contraseña",
            "Se envia email de olvido de contraseña"
        );
        
        success(__('message.password_link'));
        return redirect('login');
    }

    public function get_password_email($code)
    {
        $user = User::where('invitation_code', $code)->first();

        if ($user == null)
        {
            error(__('error.password_reset'));
            return redirect('login');
        }

        $now = Carbon::now();

        if (($user->invitation_hour == null) or ($user->invitation_hour->diffInHours($now) >= 48))
            return view('auth.invitation.invitation_expired');   

        return view('auth.passwords.reset', compact('user'));
    }

    public function set_password_email(Request $request, $code)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user                   = User::where('invitation_code', $code)->first();

        if ($user == null)
        {
            error(__('error.password_reset'));
            return redirect('login');
        }

        $user->password         = Hash::make($request->password);
        $user->invitation_code  = NULL;
        $user->invitation_hour  = NULL;
        $user->confirmed        = 1;
        $user->save();

        l(
            $user->id,
            $user->business_id,
            "Contraseña",
            "Cambio de contraseña",
            "Se cambia contraseña"
        );

        if (Auth::login($user))
            return redirect('home');
        
        return redirect('login');
    }

    public function get_invitation($code)
    {
        $user = User::where('invitation_code', $code)->first();

        if ($user == null)
        {
            error(__('errors.password_invitation'));
            return redirect('login');
        }

        $now = Carbon::now();

        if (($user->invitation_hour == null) or ($user->invitation_hour->diffInHours($now) >= 48))
            return view('auth.invitation.invitation_expired');   

        return view('auth.invitation.invitation', compact('user'));   
    }

    public function set_invitation($code, Request $request)
    {
        $request->validate([
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user                   = User::where('invitation_code', $code)->first();
        
        if ($user == null)
        {
            error(__('errors.password_invitation'));
            return redirect('login');
        }

        $user->password         = Hash::make($request->password);
        $user->invitation_code  = NULL;
        $user->invitation_hour  = NULL;
        $user->confirmed        = 1;
        $user->save();

        l(
            $user->id,
            $user->business_id,
            "Invitación",
            "Se crea usuario",
            "Se crea usuario desde invitación"
        );

        if (Auth::attempt(['email' => $user->email, 'password' => $request->password]))
            return redirect('home');

        return redirect('login');
    }

    public function create(Request $request)
    {
        $old = User::where('email', $request->email)->first();

        if($old != null)
        {
            error(__('errors.repeat_user'));
            return back()->withInput();   
        }

        if($request->password != $request->password_confirmation)
        {
            error(__('errors.password_not_match'));
            return back()->withInput();

        }elseif(strlen($request->password) < 8)
        {
            error(__('errors.password_not_length'));
            return back()->withInput();
        }

        $number = 0;
        
        do{
            $code   = normalize(str_replace(' ', '_', strtolower($request->business_name)));
            
            if($number != 0)
                $code .= $number;

            $number ++;

        }while(Business::where('code', $code)->first() != null);

        $data_business = [
            'name'                  => $request->business_name,
            'email'                 => $request->email,
            'phone'                 => $request->phone,
            'status'                => 'enabled',
            'pay'                   => 'free',
            'code'                  => $code,
            'plan'                  => 'free',
            'sms_limit'             => 60,
            'amount'                => '0',
            'mp_state'              => null,
            'mp_access_token'       => null,
            'mp_public_key'         => null,
            'mp_refresh_token'      => null,
            'mp_user_id'            => null,
            'mp_expires_in'         => null,
            'api_key'               => Random_string(20),
            'app'                   => null,
            'first_login'           => null,
        ];

        $business = create_business($data_business);

        install_theme('default', $business);

        install_messages($business->id);

        $confirmation_code          = Random_String(25);

        $data_user = [
            'name'              => $request->name,
            'email'             => $request->email,
            'phone'             => $request->phone,
            'address'           => null,
            'gender'            => $request->gender,
            'type'              => "admin",
            'color'             => Random_color(),
            'online'            => 'on',
            'access'            => 'on',
            'confirmed'         => 0,
            'birthday'          => null,
            'password'          => Hash::make($request->password),
            'confirmation_code' => $confirmation_code,
            'business_id'       => $business->id,
        ];

        $user = create_user($data_user, $business);

        //envio confirmación de usuario por mail
        $data = array(
            'confirmation_code' => $confirmation_code,
            'user'              => $user,
        );

        send_email('emails.verify', $data, $user->email, "Confirmación de usuario");
        Auth::login($user);
        
        l(
            $user->id,
            $user->business_id,
            "Empresa",
            "Registro",
            "Se registra empresa"
        );

        l(
            $user->id,
            $user->business_id,
            "Usuario",
            "Registro",
            "Se registra usuario"
        );

        //aviso al root que se creo una nueva empresa
        $data = array(
            'name'  => $business->name,
            'email' => $business->email,
            'phone' => $business->phone,
            'user'  => $user->name,
            'api'   => $business->api,
        );

        send_email('emails.root.new_business', $data, "admin@tureserva.io", "Nueva empresa registrada");
        
        return view($this->path."verify");
    }

    public function login(Request $request)
    {

        $user = User::where('email', $request->email)->first();

        if($user == null)
        {
            error(__('auth.failed'));
            return redirect('login')->withInput();   
        }
     
        if($user->access != "on")
        {
            error(__('errors.user_disabled'));
            return back()->withInput();
        }
            
        if (Auth::attempt($request->only('email', 'password')))
        {
            if(Auth::user()->type != "root")
            {
                l(
                    user()->id,
                    business()->id,
                    "Login",
                    "Inicio de sesión",
                    "Se inicia sesión"
                );
            }
            
            if($request->redirect != null)
                return redirect($request->redirect);
            
            return redirect('home');
        }
        
        error(__('auth.failed'));
        return redirect('login')->withInput();   

    }

    public function logout(Request $request) 
    {
        Auth::logout();
        return redirect('/login');
    }

    public function get_disabled()
    {
        return view("auth.disabled");
    }
}
