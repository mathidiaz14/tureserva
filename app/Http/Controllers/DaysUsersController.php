<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DaysUsers;

class DaysUsersController extends Controller
{
    public function store(Request $request)
    {
        $day                = new DaysUsers();
        $day->day           = $request->day;
        $day->start         = $request->start;
        $day->end           = $request->end;
        $day->user_id       = $request->user;
        $day->save();

        return $day;
    }

    public function delete($id)
    {
        $day = DaysUsers::findOrFail($id);
        
        $day->delete();
        return true;
    }
}
