<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Notification;
use Auth;

class ContactController extends Controller
{
    private $path = "admin.contact.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = Contact::where('business_id', business()->id)->paginate(20);;

        return view($this->path."index", compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message            = Contact::findOrFail($id);
        $message->status    = "read";
        $message->save();

        return view($this->path.'show', compact('message'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact                = new Contact();
        $contact->business_id   = business()->id;
        $contact->name          = user()->name;
        $contact->email         = user()->email;
        $contact->message       = $request->message;
        $contact->status        = "answer";
        $contact->contact_id    = $id;
        $contact->save();

        $old                    = Contact::findOrFail($id);
        $old->status            = "answered";
        $old->save();

        //Envio el email
        $data = array(
            'business'  => business()->name,
            'body'      => $request->message,
        );

        send_email('emails.web_contact_response', $data, $old->email, "Respuesta a tu consulta");
        
        success(__('message.message_send'));
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);

        if(($contact == null) or (!check_business($contact)))
        {
            error(__('errors.delete'));
            return back();
        }

        $contact->childrens()->delete();
        $contact->delete();

        success(__('message.delete'));
        return back();
    }

    public function view()
    {
        if(Auth::check())
            return view('layouts.parts.messages');
    }

    public function read()
    {
        $contacts = business()->contacts->where('status', 'pending');

        foreach($contacts as $contact)
        {
            $contact->status = "enabled";
            $contact->save();
        }

        return true;
    }
}
