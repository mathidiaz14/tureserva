<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tips;
use Storage;
use File;   

class TipController extends Controller
{
    private $path = "admin.tips.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tips = business()->tips;

        return view($this->path."index", compact('tips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tip = new Tips();

        $file                   = $request->file('image');

        if($file != null)
        {
            $nameFile       = Random_String(10);
            Storage::disk('tips')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));
            $tip->image     = "storage/tips/".$nameFile.".".$file->getClientOriginalExtension();
        }

        $tip->title         = $request->title;
        $tip->body          = $request->body;
        $tip->business_id   = business()->id;
        $tip->save();

        success(__('message.create_info'));
        return redirect('tips');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tip = Tips::findOrFail($id);

        if(!check_business($tip))
        {
            error(__('errors.update'));
            return back();
        }

        return view($this->path."edit", compact('tip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tip = Tips::findOrFail($id);

        if(!check_business($tip))
        {
            error(__('errors.update'));
            return back();
        }

        $file                   = $request->file('image');

        if($file != null)
        {
            if(file_exists($tip->image))
                unlink($tip->image);

            $nameFile       = Random_String(10);
            Storage::disk('tips')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));
            $tip->image     = "storage/tips/".$nameFile.".".$file->getClientOriginalExtension();
        }

        $tip->title         = $request->title;
        $tip->body          = $request->body;
        $tip->save();

        success(__('message.update_info'));
        return redirect('tips');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tip = Tips::findOrFail($id);

        if(!check_business($tip))
        {
            error(__('errors.delete'));
            return back();
        }

        $tip->delete();

        success(__('message.delete_info'));
        return back();
    }
}
