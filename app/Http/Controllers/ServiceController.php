<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Models\DaysService;
use Auth;
use Storage;
use File;

class ServiceController extends Controller
{
    
    private $path = "admin.service.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Auth::user()->business->services;

        return view($this->path.'index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((business()->plan == "plan1") and (business()->users->count() >= 1))
        {
            error(__('errors.limit_service_plan1'));
            return back();

        }elseif((business()->plan == "plan2") and (business()->users->count() >= 3))
        {
            error(__('errors.limit_service_plan2'));
            return back();
        }

        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if((business()->plan == "plan1") and (business()->users->count() >= 1))
        {
            error(__('errors.limit_service_plan1'));
            return back();

        }elseif((business()->plan == "plan2") and (business()->users->count() >= 3))
        {
            error(__('errors.limit_service_plan2'));
            return back();
        }

        $service                = new Service();
        $service->name          = $request->name;
        $service->description   = $request->description;
        $service->price         = $request->price;
        $service->duration      = $request->duration;
        $service->online        = $request->online;
        $service->business_id   = business()->id;
        $service->business_hour = "on";
        $service->save();

        foreach(business()->users as $user)
        {
            if($request['user_'.$user->id] == "on")
                $service->users()->attach($user);
        }

        foreach(business()->resources as $resource)
        {
            if($request['resource_'.$resource->id] == "on")
                $service->resources()->attach($resource);
        }

        //Se ingresan los datos de horarios en la tabla de horarios

        foreach(business()->days as $day)
        {
            $service_day                = new DaysService();
            $service_day->day           = $day->day;
            $service_day->start         = $day->start;
            $service_day->end           = $day->end;
            $service_day->status        = $day->status;
            $service_day->user_id       = $user->id;
            $service_day->save();
        }

        success(__('message.create_info'));
        return redirect('service');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        if(($service == null) or (!check_business($service)))
        {
            error(__('errors.open'));
            return back();
        }
        
        return view($this->path.'show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);

        if(($service == null) or (!check_business($service)))
        {
            error(__('errors.open'));
            return back();
        }
        
        return view($this->path.'edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);

        if(($service == null) or (!check_business($service)))
        {
            error(__('errors.update'));
            return back();
        }

        $service->name          = $request->name;
        $service->description   = $request->description;
        $service->price         = $request->price;
        $service->duration      = $request->duration;
        $service->online        = $request->online;
        $service->business_hour = $request->business_hour;
        $service->save();

        $service->users()->detach();

        foreach(business()->users as $user)
        {
            if($request['user_'.$user->id] == "on")
                $service->users()->attach($user);
        }

        $service->resources()->detach();

        foreach(business()->resources as $resource)
        {
            if($request['resource_'.$resource->id] == "on")
                $service->resources()->attach($resource);
        }

        success(__('message.update_info'));
        return redirect('service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);

        if(!check_business($service))
        {
            error(__('errors.delete'));
            return back();
        }

        $service->users()->detach();
        
        foreach($service->tasks as $task)
        {
            $task->service_id = null;
            $task->save();
        }
        
        $service->delete();

        success(__('message.delete_info'));
        return back();
    }

    public function get_hours($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.service.hour_table', compact('service'));
    }

    public function set_hours(Request $request)
    {
        $explode    = explode("_", $request->id);
        $time       = $explode[1];
        $id         = $explode[2];
        
        $day        = DaysService::findOrFail($id);
        $day[$time] = $request->val;
        $day->save();

        return true;
    }

    public function set_status(Request $request)
    {
        $day        = DaysService::find($request->id);
        
        if($day == null)
            return false;

        $days       = $day->service->days->where('day', $day->day);

        $status     = "on";
        if($request->status == "on")
            $status = "off";

        foreach($days as $day)
        {
            $day->status = $status;
            $day->save();
        }

        return true;
    }
}
