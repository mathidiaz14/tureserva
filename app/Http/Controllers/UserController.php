<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CalendarOption;
use App\Models\DaysUsers;
use Carbon\Carbon;
use Auth;
use Hash;
use Storage;
use File;

class UserController extends Controller
{

    private $path = "admin.user.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = business()->users;

        return view($this->path.'index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if((business()->plan == "plan1") and (business()->users->count() >= 1))
        {
            error(__('errors.limit_user_plan1'));
            return back();

        }elseif((business()->plan == "plan2") and (business()->users->count() >= 3))
        {
            error(__('errors.limit_user_plan2'));
            return back();
        }
        
        return view($this->path."create"); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Controlo el limite de pleados que tiene el plan de la empresa

        if((business()->plan == "plan1") and (business()->users->count() >= 1))
        {
            error(__('errors.limit_user_plan1'));
            return back();

        }elseif((business()->plan == "plan2") and (business()->users->count() >= 3))
        {
            error(__('errors.limit_user_plan2'));
            return back();
        }

        //Reviso que ese email no este registrado
        
        if(($request->email != null) and (User::where('email', $request->email)->first() != null))
        {
            error(__('errors.repeat_user'));
            return back()->withInput();
        } 

        //Se crea el usuario

        $user                   = new User();
        $user->name             = $request->name;
        $user->email            = $request->email;
        $user->password         = "";
        $user->type             = $request->type;
        $user->access           = $request->access;
        $user->online           = $request->online;
        $user->color            = $request->color;
        $user->phone            = $request->phone;
        $user->address          = $request->address;
        $user->birthday         = $request->birthday;
        $user->gender           = $request->gender;
        $user->business_hour    = "on";
        $user->contact_number   = $request->contact_number;
        $user->coment           = $request->coment;
        $user->business_id      = business()->id;
        $user->save();

        if($request->online == "on")
        {
            foreach(business()->services as $service)
            {
                if (($request['service_'.$service->id] == "on") or ($request->service_all == "on"))
                    $user->services()->attach($service);
            }
        }

        //Se ingresan los datos de horarios en la tabla de horarios

        foreach(business()->days as $day)
        {
            $user_day                = new DaysUsers();
            $user_day->day           = $day->day;
            $user_day->start         = $day->start;
            $user_day->end           = $day->end;
            $user_day->status        = $day->status;
            $user_day->user_id       = $user->id;
            $user_day->save();
        }

        //Se crea la entrada en la tabla CalendarOption para los datos del calendario en caso que utilice la agenda

        $option             = new CalendarOption;
        $option->user_id    = $user->id;
        $option->save();
        
        // Si se le da acceso a la plataforma se le envia una invitación
        if($request->access == "on")
        {
            $invitation_code        = Random_String(10);
            $user->invitation_code  = $invitation_code;
            $user->invitation_hour  = Carbon::now();
            $user->save();

            $data = array(
                'business'          => business()->name,
                'invitation_code'   => $invitation_code,
                'user'              => $user,
            );

            send_email('emails.new_user', $data, $user->email, 'Invitación a '.config('app.name'));

            success(__('message.invitation'));
            return redirect('user');
        }
        
        success(__('message.create_info'));
        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        if(!check_business($user))
        {
            error(__('errors.update'));
            return back();
        }   

        return view($this->path."edit", compact('user'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $old  = User::where('email', $request->email)->where('id', '!=', $id)->first();

        if(($old != null) and ($request->email != null))
        {
            error(__('errors.repeat_user'));
            return back()->withInput();

        } 
            
        $user                   = User::findOrFail($id);

        if(!check_business($user))
        {
            error(__('errors.update'));
            return back();
        }

        $user->name             = $request->name;
        $user->email            = $request->email;
        $user->phone            = $request->phone;
        $user->address          = $request->address;
        $user->birthday         = $request->birthday;
        $user->type             = $request->type;
        $user->access           = $request->access;
        $user->online           = $request->online;
        $user->color            = $request->color;
        $user->business_hour    = $request->business_hour;
        $user->gender           = $request->gender;
        $user->save();

        if($request->online == "on")
        {

            if ($request->service_all == "on")
            {
                $user->services()->detach();
                $user->services()->attach($service);
            }else
            {
                foreach($user->business->services as $service)
                {
                    if($request['service_'.$service->id] == "on")
                    {
                        $user->services()->detach($service);
                        $user->services()->attach($service);
                    }
                    else
                    {
                        $user->services()->detach($service);
                    }
                }
            }
        }else
        {
            $user->services()->detach();
        }


        foreach($user->days as $day)
        {
            $day->start     = $request[$day->day.'_start_'.$day->id];
            $day->end       = $request[$day->day.'_end_'.$day->id];
            $day->status    = $request[$day->day.'_status_'.$day->id];
            $day->save();
        }

        $file                   = $request->file('avatar');
        
        if($file != null)
        {
            $nameFile   = "avatar_".$user->id;
            Storage::disk('users')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));
            $user->avatar       = "storage/users/".$nameFile.".".$file->getClientOriginalExtension();
            $user->save();
        }
        
        if($request->access == "on")
        {
            if($request->password != null)
            {
                $user->password  = Hash::make($request->password);
                $user->save();
            }
        }

        if($request->business_hour == "on")
        {
            $user->days()->delete();

            foreach(business()->days as $day)
            {
                $user_day                = new DaysUsers();
                $user_day->day           = $day->day;
                $user_day->start         = $day->start;
                $user_day->end           = $day->end;
                $user_day->status        = $day->status;
                $user_day->user_id       = $user->id;
                $user_day->save();
            }

        }
        
        success(__('message.update_info'));
        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if(!check_business($user))
        {
            error(__('errors.delete'));
            return back();
        }

        if(file_exists($user->avatar))
            unlink($user->avatar);

        $user->days()->delete();
        $user->services()->detach();
        $user->tasks()->detach();
        $user->calendar_options()->delete();
        $user->notes()->delete();
        $user->delete();

        success(__('message.delete_info'));
        return back();
    }

    public function delete_avatar($id)
    {
        $user = User::findOrFail($id);

        if(!check_business($user))
        {
            error(__('errors.delete'));
            return back();
        }
     
        if ((Auth::user()->type == "admin") or ($user->id == Auth::user()->id))
        {
            if(file_exists($user->avatar))
                unlink($user->avatar);
            
            $user->avatar = null; 
            $user->save();

            success(__('message.delete_info'));
            return back();
        }

        error(__('errors.delete'));
        return back();
    }

    public function profile()
    {
        if(user()->type == "root")
            return view("root.user.profile");
        else
            return view($this->path."profile");
    }

    public function set_profile(Request $request)
    {
        $old  = User::where('email', $request->email)->where('id', '!=', user()->id)->first();

        if($old != null)
        {
            error(__('errors.repeat_user'));
            return back()->withInput();

        } else
        {
            $user                   = user();
            $file                   = $request->file('avatar');

            if($file != null)
            {
                if(file_exists($user->avatar))
                    unlink($user->avatar);

                $nameFile   = "avatar_".$user->id;
                Storage::disk('users')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));
                $user->avatar       = "storage/users/".$nameFile.".".$file->getClientOriginalExtension();
            }

            if($request->password != null)
            {
                if($request->password !== $request->password_re)
                {
                    error(__('errors.password_not_match'));
                    return back();
                }

                if(strlen($request->password) < 8)
                {
                    error(__('errors.password_not_length'));
                    return back();
                }

                $user->password = Hash::make($request->password);
            }

            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->phone            = $request->phone;
            $user->gender           = $request->gender;
            $user->address          = $request->address;
            $user->birthday         = $request->birthday;
            $user->color            = $request->color;
            $user->save();
            
            success(__('message.update_info'));
            return redirect('profile');
        }
    }

    public function get_verify()
    {
        if(Auth::user()->confirmed == 1)
            return redirect('home');

        return view('auth.verify');
    }

    public function set_verify($code)
    {
        $user = User::where('confirmation_code', $code)->first();

        if (! $user)
            return view('auth.verify_error');

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();
        
        Auth::login($user, true);
        return view('auth.verify_success');
    }

    public function send_verify()
    {
        $user = user();

        if($user->confirmation == 1)
            return redirect('home');

        if($user->confirmation_code == null)
        {
            $user->confirmation_code = Random_String(25);
            $user->save();
        }
        
        $data = array(
            'confirmation_code' => $user->confirmation_code,
            'user'              => $user,
        );

        send_email('emails.verify', $data, $user->email, "Confirmación de usuario");
        
        success(__('message.send_verify'));
        return back();
    }

    public function get_hours($id)
    {
        $user = User::findOrFail($id);
        return view('admin.user.hour_table', compact('user'));
    }

    public function set_hours(Request $request)
    {
        $explode    = explode("_", $request->id);
        $time       = $explode[1];
        $id         = $explode[2];
        
        $day        = DaysUsers::findOrFail($id);
        $day[$time] = $request->val;
        $day->save();

        return true;
    }

    public function set_status(Request $request)
    {
        $day        = DaysUsers::find($request->id);
        
        if($day == null)
            return false;

        $days       = $day->user->days->where('day', $day->day);

        foreach($days as $day)
        {
            $day->status = $request->status;
            $day->save();
        }

        return true;
    }

    public function get_random_color()
    {
        return Random_color();
    }
}
