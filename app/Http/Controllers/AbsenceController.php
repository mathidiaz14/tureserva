<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Absence;
use Carbon\Carbon;

class AbsenceController extends Controller
{

    private $path = "admin.absence.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $absences = business()->absences;

        return view($this->path."index", compact('absences'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $absence                = new Absence();
        $absence->start         = Carbon::create($request->start);
        $absence->end           = Carbon::create($request->end);
        $absence->model         = $request->model;
        $absence->model_id      = $request->model_id;
        $absence->business_id   = business()->id;
        $absence->save();

        l(
            user()->id,
            business()->id,
            "Ausencias",
            "Crear",
            "Se crea ausencia ".$absence->id
        );

        success(__('message.create_info'));
        
        return redirect('absence');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $absence = Absence::findOrFail($id);
        return view($this->path."edit", compact('absence'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $absence                = Absence::findOrFail($id);

        if(($absence == null) or (!check_business($absence)))
        {
            error(__('errors.open'));
            return back();
        }

        $absence->start         = Carbon::create($request->start);
        $absence->end           = Carbon::create($request->end);
        $absence->model         = $resource->model;
        $absence->model_id      = $resource->model_id;
        $absence->save();

        l(
            user()->id,
            business()->id,
            "Ausencias",
            "Actualizar",
            "Se actualiza ausencia ".$absence->id
        );

        success(__('message.update_info'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $absence = Absence::findOrFail($id);

        if(($absence == null) or (!check_business($absence)))
        {
            error(__('errors.delete'));
            return back();
        }

        $absence->delete();

        l(
            user()->id,
            business()->id,
            "Ausencias",
            "Eliminar",
            "Se elimina ausencia ".$absence->id
        );

        success(__('message.delete_info'));
        return back();
    }
}
