<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Incident;

class IncidentController extends Controller
{
    private $path = "admin.incident.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidents = Incident::where('business_id', business()->id)->paginate(10);

        return view($this->path."index", compact('incidents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $incident               = new Incident();
        $incident->user_id      = user()->id;
        $incident->business_id  = business()->id;
        $incident->screen       = $request->screen;

        $file                   = $request->file('arrached');

        if($file != null)
        {
            $nameFile = "incident_".Random_string(10).".".$file->getClientOriginalExtension();
            
            Storage::disk('incident')->put($nameFile, File::get($file));
            $incident->attached     = "storage/incident/".$nameFile;
        }

        $incident->content      = $request->content;
        $incident->url          = $request->url;
        $incident->status       = "pending";
        $incident->save();

        success(__('message.incident_create'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incident = Incident::findOrFail($id);
        
        if(!check_business($incident))
        {
            error(__('errors.open'));
            return back();
        }

        return view($this->path."show", compact('incident'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view_screen($id)
    {
        $incident   = Incident::findOrFail($id);
        $content    = $incident->screen;

        return view($this->path."screen", compact('content'));
    }

    public function view_attach($id)
    {
        $incident = Incident::findOrFail($id);


    }
}
