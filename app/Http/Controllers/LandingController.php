<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Landing;
use Storage;
use File;

class LandingController extends Controller
{
    private $path = "admin.landing.";
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $landings = business()->landings;

        if($landings == null)
        {
            error(__('errors.update'));
            return back;
        }

        foreach($landings as $landing)
        {
            if($landing->type == "image")
            {
                $file       = $request->file($landing->key);

                if($file != null)
                {

                    if(file_exists($landing->value))
                        unlink($landing->value);
                    
                    $name       = $file->getClientOriginalName();
                    $nameFile   = Random_String(15);

                    Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));

                    $landing->value = "storage/".$nameFile.".".$file->getClientOriginalExtension();
                }
            }else
            {
                $landing->value = $request[$landing->key];
            }

            $landing->save();
        }

        success(__('message.update_info'));
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path   = resource_path()."/views/landings/";
        $dir    = opendir($path);   
        
        while ($folder = readdir($dir))
        {
            if( $folder != "." && $folder != "..")
            {
                if(str_replace(' ', '', $folder) == $id)
                {
                    business()->landings()->delete();

                    install_theme($folder, business());

                    business()->folder = $folder;
                    business()->save();

                    success(__('message.update_info'));
                    return redirect('theme/1/edit');
                }
            }
        }

        error(__('errors.update'));
        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view($this->path."edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        business()->landings()->delete();
        
        install_theme(business()->folder, business());

        success(__('message.update_info'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function delete($id)
    {
        $landing = Landing::findOrFail($id);

        $landing->value = null;
        $landing->save();

        success(__('message.delete_info'));
        return back();
    }
}

