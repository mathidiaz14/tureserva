<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Http;
use Auth;

class MercadoPagoController extends Controller
{
    private $path = "admin.mercadopago.";

    public function index()
    {
        return view($this->path."index");
    }

    public function connect(Request $request)
    {
        $response = Http::post('https://api.mercadopago.com/oauth/token', [
            'grant_type'    => "authorization_code", 
            'client_secret' => env('MP_CLIENT_SECRET'),
            'client_id'     => env('MP_CLIENT_ID'), 
            'code'          => $request->code,
            'redirect_uri'  => env("MP_REDIRECT_URI")
        ]);
        
        if($response == null)
        {
            error(__('errors.mercadopago_connect'));
            return back();
        }

        $business                      = business();
        $business->mp_state            = "connect";
        $business->mp_access_token     = $response->json()['access_token'];
        $business->mp_public_key       = $response->json()['public_key'];
        $business->mp_refresh_token    = $response->json()['refresh_token'];
        $business->mp_user_id          = $response->json()['user_id'];
        $business->mp_expires_in       = $response->json()['expires_in'];
        $business->save();

        success(__('message.mercadopago_connect'));
        
        return view($this->path."index");
    }

    public function disconnect()
    {
        $business                       = business();
        $business->mp_estado            = null;
        $business->mp_access_token      = null;
        $business->mp_public_key        = null;
        $business->mp_refresh_token     = null;
        $business->mp_user_id           = null;
        $business->mp_expires_in        = null;
        $business->save();

        success(__('message.mercadopago_disconnect'));
        return back();
    }

    public function store(Request $request)
    {
        $business = business();

        if($request->status == "on")
        {
            $business->return_status = "on";
            $business->return_option = $request->return;

            if($request->return == "partial")
                $business->return_percentage = $request->return_percentage;
        }else
        {
            $business->return_status = null;
        }

        $business->save();

        success(__('message.update_info'));
        return back();
    }
}
