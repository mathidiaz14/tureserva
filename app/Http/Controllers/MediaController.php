<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Media;
use Auth;
use File;
use Storage;

class MediaController extends Controller
{
    private $path = "admin.media.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file               = $request->file('file');
        
        $name               = $file->getClientOriginalName();
        $nameFile           = Random_String(10).".".$file->getClientOriginalExtension();

        Storage::disk('public')->put($nameFile, File::get($file));

        $media              = new Media();
        $media->url         = "storage/".$nameFile;
        $media->name        = $name;
        $media->type        = $file->getClientOriginalExtension();
        $media->size        = $file->getSize();
        $media->user_id     = user()->id;
        $media->business_id = business()->id;
        $media->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $media                  = Media::findOrFail($id);

        if(($media == null) or (!check_business($media)))
        {
            error(__('errors.update'));
            return back();
        }

        $media->name            = $request->name;
        $media->alternative     = $request->alternative;
        $media->web             = $request->web;
        $media->save();

        success(__('message.update_info'));
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::findOrFail($id);

        if(($media == null) or (!check_business($media)))
        {
            error(__('errors.delete'));
            return back();
        }

        if(file_exists($media->url))
            unlink($media->url);

        $media->delete();

        success(__('message.delete_info'));
        return back();
    }

}
