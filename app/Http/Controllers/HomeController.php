<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calendar;
use App\Models\Business;
use App\Models\User;
use App\Models\Contact;
use Carbon\Carbon;
use Auth;
use MercadoPago;

class HomeController extends Controller
{

    public function landing()
    {
        $url = str_replace("http://", "", url('/'));
        $url = str_replace("https://", "", $url);

        if(str_contains(env('APP_URL'), $url))
            return view('welcome');
        
        $business = Business::where('url', $url)->first();

        if($business == null)
            $business = Business::where('url2', $url)->first();

        if($business == null)
            return abort(401);

        if($business->reservation->access != "on")
            return abort(400);

        return view("landings.".$business->folder.".index", compact('business'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(business()->first_login == null)
            return redirect('first/login');
        
        $tasks = business()->tasks->where('status', 'pending')->where('start', '>=', Carbon::now());

        return view('admin.home', compact('tasks'));
    }

    public function contact_root(Request $request)
    {
        if($request->contact == "advice")
        {
            $data = array(
                'name'      => $request->name,
                'email'     => $request->email,
                'phone'     => $request->phone,
            );
            
            $contact                = new Contact();
            $contact->business_id   = "root";
            $contact->name          = $request->name;
            $contact->email         = $request->email;
            $contact->phone         = $request->phone;
            $contact->status        = "enabled";
            $contact->root          = "yes";
            $contact->save();

            send_email(
                'emails.contact_root_advice', 
                $data, 
                'admin@tureserva.io', 
                "Contacto desde TuReserva.io"
            );

            session(['form_advice' => 'Su contacto fue enviado correctamente.']);
            
            return redirect(url('/')."#request");
        }else
        {
            $data = array(
                'name'      => $request->name,
                'email'     => $request->email,
                'message'   => $request->message,
            );
            
            $contact                = new Contact();
            $contact->business_id   = "root";
            $contact->name          = $request->name;
            $contact->email         = $request->email;
            $contact->message       = $request->message;
            $contact->status        = "enabled";
            $contact->root          = "yes";
            $contact->save();

            send_email(
                'emails.contact_root_form', 
                $data, 
                'admin@tureserva.io', 
                "Contacto desde TuReserva.io"
            );

            session(['form' => 'Su contacto fue enviado correctamente.']);
            return redirect(url('/')."#contact");
        }
    }
}
