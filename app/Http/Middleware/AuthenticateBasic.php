<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Auth;

class AuthenticateBasic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if(!Auth::check())
            return redirect('login?redirect='.$request->url());

        if (user()->type == "root")
            return redirect('root');

        if(business()->status == "disabled")
            return redirect('disabled');

        if((business()->plan == "free") and (business()->created_at->diffInDays(Carbon::now()) > 30))
            return redirect('upgrade');

        if(user()->confirmed == 0)
            return redirect('verify');
        
        Auth::user()->update(['last_login' => now()]);
        
        return $next($request);
    }
}
