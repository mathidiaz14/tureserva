<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class AuthenticateRoot
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        if(!Auth::check())
            return redirect('login');

        if (Auth::user()->type != "root")
            return redirect('home');
        
        Auth::user()->update(['last_login' => now()]);
        
        return $next($request);
    }
}
