<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Business extends Model
{
	protected $fillable = [
        'name', 'email', 'pay', 'end', 'code'
    ];

    protected $dates = [
        'control_date'
    ];

    public function users()
    {
    	return $this->hasMany('App\Models\User');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\Notification')->orderBy('created_at', 'desc');
    }

    public function tasks()
    {
        return $this->hasMany('App\Models\Calendar');
    }

    public function clients()
    {
        return $this->hasMany('App\Models\Client');
    }

    public function services()
    {
        return $this->hasMany('App\Models\Service');
    }

    public function medias()
    {
        return $this->hasMany('App\Models\Media');
    }

    public function days()
    {
        return $this->hasMany('App\Models\Days');
    }

    public function resources()
    {
        return $this->hasMany('App\Models\Resource');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

    public function opinions()
    {
        return $this->hasMany('App\Models\Opinion');
    }

    public function reservation()
    {
        return $this->hasOne('App\Models\Reservation');
    }
    
    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }

    public function absences()
    {
        return $this->hasMany('App\Models\Absence');
    }

    public function tips()
    {
        return $this->hasMany('App\Models\Tips');
    }

    public function waitings()
    {
        return $this->hasMany('App\Models\Waiting');
    }

    public function landings()
    {
        return $this->hasMany('App\Models\Landing');
    }

    public function visits()
    {
        return $this->hasMany('App\Models\Visit');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    public function sms_logs()
    {
        return $this->hasMany('App\Models\SmsLog');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\Log');
    }

    public function incidents()
    {
        return $this->hasMany('App\Models\Incident');
    }
}
