<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    use HasFactory;

    protected $dates = ['start', 'end'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function resource()
    {
        return $this->belongsTo('App\Models\Resource');
    }
}
