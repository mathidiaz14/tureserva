<?php

//Helper que guarda contenido en session para mostrar cartel de success
function success($contenido)
{
    session(['success' => $contenido]);
}

//Helper que guarda contenido en session para mostrar cartel de error
function error($contenido)
{
    session(['error' => $contenido]);
}

//Helper que guarda contenido en session para mostrar cartel de warning
function warning($contenido)
{
    session(['warning' => $contenido]);
}


//Helper que guarda contenido en session para mostrar cartel de info
function info_alert($contenido)
{
    session(['info' => $contenido]);
}

//Helper para envio de emails
function send_email($template, $data, $to, $subject, $resend = null, $name = "TuReserva.io", $business = null)
{
    if($business != null)
        $name = $business->name;

    $mj = new \Mailjet\Client(
                            env('MAIL_USERNAME'), 
                            env('MAIL_PASSWORD'), 
                            true, 
                            ['version' => 'v3.1']
                        );

    $body = [
        'Messages' => [
            [
                'From' => [
                    'Email' => env('MAIL_FROM_ADDRESS'),
                    'Name' => env('MAIL_FROM_NAME'),
                ],
                'To' => [
                    [
                        'Email' => $to
                    ]
                ],
                'Subject' => $subject,
                'HTMLPart' => view($template)->with('data', $data)->render()
            ]
        ]
    ];

    $log                = new \App\Models\EmailLog();

    if($business == null)
        $log->business_id = "root";
    else
        $log->business_id = $business->id;

    $log->subject       = $subject;
    $log->name          = $name;
    $log->template      = $template;
    $log->body          = json_encode($data);
    $log->to            = $to;
    $log->status        = "pending";
    $log->save();

    $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);

    if($response->success())
    {
        $log->status        = "send";
        $log->status_body   = "correct send";
        $log->save();

        return true;
    }

    $log->status        = "error";
    $log->status_body   = $response->getMessage()['body']['ErrorMessage'];
    $log->save();
    
    return false;
}

//Helper para envio de sms
function send_sms($number, $content, $business_id)
{
    try 
    {
        $business = App\Models\Business::find($business_id);
    
        if(($business == null) or ($business->sms_count >= $business->sms_limit))
            return false;
        
        if ((strpos($number, '+598') === false) and (strpos($number, '00598') === false))
            $number = "+598".$number;

        $client = new \Twilio\Rest\Client(env( 'TWILIO_SID' ), env( 'TWILIO_TOKEN' ));

        $client->messages->create(
            $number,
            array(
                'from' => env( 'TWILIO_FROM' ),
                'body' => $content,
            )
        );    

        $business->sms_count += 1;
        $business->save();

        //Guardo el log de envio de SMS
        $log                = new App\Models\SmsLog();
        $log->business_id   = $business_id;
        $log->body          = $content;
        $log->to            = $number;
        $log->save();

        l(
            "", 
            $business_id, 
            "SMS", 
            "Envio", 
            "Se envio sms a: ".$number
        ); 

        return true;

    } catch (Exception $e) 
    {
        l(
            "", 
            $business->id, 
            "SMS", 
            "Envio", 
            $e->getMessage()
        );       

        return false;
    }
}

//Helper para crear notificaciones
function notification($title, $content, $url, $business)
{
    $notification               = new App\Models\Notification();
    $notification->title        = $title;
    $notification->body         = $content;
    $notification->link         = $url;
    $notification->status       = "new";
    $notification->type         = "web";
    $notification->business_id  = $business;
    $notification->save();

    return true;
}

//Helper que genera un string random de la longitud que quiera, predeterminadamente 10
function Random_String($length = 10) 
{ 
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
} 

//Helper que genera un integer random de la longitud que quiera, predeterminadamente 10
function Random_Code($length = 10) 
{ 
    return substr(str_shuffle("0123456789"), 0, $length); 
} 

//Helper que devuelve la inicial del nombre que se le pasa, se usa para crear el logo de la empresa si no tiene uno
function LogoLetter($name)
{
	return strtoupper(substr($name, 0, 1));
}

//Devuelve el nombre en español del dia que se le pasa en ingles
function day_name($day)
{
	$name = "";

	switch ($day) {
        case 'Monday':
            $name = "Lunes";
            break;
        case 'Tuesday':
            $name = "Martes";
            break;
        case 'Wednesday':
            $name = "Miercoles";
            break;
        case 'Thursday':
            $name = "Jueves";
            break;
        case 'Friday':
            $name = "Viernes";
            break;
        case 'Saturday':
            $name = "Sabado";
            break;
        case 'Sunday':
            $name = "Domingo";
            break;
        case 'monday':
            $name = "Lunes";
            break;
        case 'tuesday':
            $name = "Martes";
            break;
        case 'wednesday':
            $name = "Miercoles";
            break;
        case 'thursday':
            $name = "Jueves";
            break;
        case 'friday':
            $name = "Viernes";
            break;
        case 'saturday':
            $name = "Sabado";
            break;
        case 'sunday':
            $name = "Domingo";
            break;
        default:
            # code...
            break;
    }

    return $name;
}

//devuelve el nombre en español del mes que se le pasa en ingles en formato de 3 letras
function month_name($month)
{
    $name = "";

    switch (strtolower($month)) {
        case 'jan':
            $name = "Ene";
            break;
        case 'feb':
            $name = "Feb";
            break;
        case 'mar':
            $name = "Mar";
            break;
        case 'apr':
            $name = "Abr";
            break;
        case 'may':
            $name = "May";
            break;
        case 'jun':
            $name = "Jun";
            break;
        case 'jul':
            $name = "Jul";
            break;
        case 'aug':
            $name = "Ago";
            break;
        case 'sep':
            $name = "Set";
            break;
        case 'oct':
            $name = "Oct";
            break;
        case 'nov':
            $name = "Nov";
            break;
        case 'dec':
            $name = "Dic";
            break;
        default:
            # code...
            break;
    }

    return $name;
}

//Devuelve los datos de la empresa del usuario actual
function business()
{
    return Auth::user()->business;
}

//Devuelve los datos del usuario actual
function user()
{
    return Auth::user();
}

//Comprueba si el modelo que le paso corresponde a la empresa del usuario actual
function check_business($model)
{
    if(($model != null) and ($model->business_id == Auth::user()->business_id))
        return true;

    return false;
}

//Normaliza contenido para prevenir errores con caracteres incorrectos
function normalize($var)
{
    $original   = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modify     = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $var        = utf8_decode($var);
    $var        = strtr($var, utf8_decode($original), $modify);
    $var        = strtolower($var);
    
    return utf8_encode($var);
}

//Guarda log
function l($user, $business, $section, $action, $message)
{
    $log                = new App\Models\Log();
    $log->user_id       = $user;
    $log->business_id   = $business;
    $log->section       = $section;
    $log->action        = $action;
    $log->message       = $message;
    $log->save();
}

//Devuelve un color Random de los que estan en el string
function Random_color()
{
   $colors = [
      "#FF99FF",   "#FF66FF",   "#FF33FF",   "#FF00FF",  "#FF99CC",   "#FF66CC",   "#FF33CC",   "#FF00CC",
      "#FF9999",   "#FF6699",   "#FF3399",   "#FF0099",  "#FF9966",   "#FF6666",   "#FF3366",   "#FF0066",
      "#FF9933",   "#FF6633",   "#FF3333",   "#FF0033",  "#FF9900",   "#FF6600",   "#FF3300",   "#FF0000",
      "#CC99FF",   "#CC66FF",   "#CC33FF",   "#CC00FF",  "#CC99CC",   "#CC66CC",   "#CC33CC",   "#CC00CC",
      "#CC9999",   "#CC6699",   "#CC3399",   "#CC0099",  "#CC9966",   "#CC6666",   "#CC3366",   "#CC0066",
      "#CC9933",   "#CC6633",   "#CC3333",   "#CC0033",  "#CC9900",   "#CC6600",   "#CC3300",   "#CC0000",
      "#9999FF",   "#9966FF",   "#9933FF",   "#9900FF",  "#9999CC",   "#9966CC",   "#9933CC",   "#9900CC",
      "#999999",   "#996699",   "#993399",   "#990099",  "#999966",   "#996666",   "#993366",   "#990066",
      "#999933",   "#996633",   "#993333",   "#990033",  "#999900",   "#996600",   "#993300",   "#990000",
      "#6699FF",   "#6666FF",   "#6633FF",   "#6600FF",  "#6699CC",   "#6666CC",   "#6633CC",   "#6600CC",
      "#669999",   "#666699",   "#663399",   "#660099",  "#669966",   "#666666",   "#663366",   "#660066",
      "#669933",   "#666633",   "#663333",   "#660033",  "#669900",   "#666600",   "#663300",   "#660000",
      "#3399FF",   "#3366FF",   "#3333FF",   "#3300FF",  "#3399CC",   "#3366CC",   "#3333CC",   "#3300CC",
      "#339999",   "#336699",   "#333399",   "#330099",  "#339966",   "#336666",   "#333366",   "#330066",
      "#339933",   "#336633",   "#333333",   "#330033",  "#339900",   "#336600",   "#333300",   "#330000",
      "#0099FF",   "#0066FF",   "#0033FF",   "#0000FF",  "#0099CC",   "#0066CC",   "#0033CC",   "#0000CC",
      "#009999",   "#006699",   "#003399",   "#000099",  "#009966",   "#006666",   "#003366",   "#000066",
      "#009933",   "#006633",   "#003333",   "#000033",  "#009900",   "#006600",   "#003300",   "#000000"
   ];

   return $colors[rand(0, count($colors) -1)];
}

//Me devuelve la hora en la que abre la empresa un dia determinado
function check_day_start($business, $day)
{
    $dates = $business->days->where('day', $day);
    $today = \Carbon\Carbon::today();
    $start = null;

    foreach ($dates as $date) 
    {
        $start2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->start);
        
        if($start == null)
        {
            $start = $start2;
        }else
        {
            if($start2->diffInMinutes($start, false) > 0)
                $start = $start2;
        }
    }

    return $start;
}

//Me devuelve la hora en la que abre la empresa en la semana
function check_day_start_week($business)
{
    $dates = $business->days;
    $today = \Carbon\Carbon::today();
    $start = null;

    foreach ($dates as $date) 
    {
        $start2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->start);
        
        if($start == null)
        {
            $start = $start2;
        }else
        {
            if($start2->diffInMinutes($start, false) > 0)
                $start = $start2;
        }
    }

    return $start;
}

//Me devuelve la hora en la que cierra la empresa un dia determinado
function check_day_end($business, $day)
{
    $dates = $business->days->where('day', $day);
    $today = \Carbon\Carbon::today();
    $end = null;

    foreach ($dates as $date) 
    {
        $end2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->end);
        
        if($end == null)
        {
            $end = $end2;
        }else
        {
            if($end2->diffInMinutes($end, false) < 0)
                $end = $end2;
        }
    }

    return $end;
}

//Me devuelve la hora en la que cierra la empresa en la semana
function check_day_end_week($business)
{
    $dates = $business->days;
    $today = \Carbon\Carbon::today();
    $end = null;

    foreach ($dates as $date) 
    {
        $end2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->end);
        
        if($end == null)
        {
            $end = $end2;
        }else
        {
            if($end2->diffInMinutes($end, false) < 0)
                $end = $end2;
        }
    }

    return $end;
}

//Devuelve el estado del dia que paso
function check_day_status($business, $day)
{
    $day = $business->days->where('day', $day)->first();

    if($day->status == "on")
        return true;

    return false;
}

//Me devuelve la hora en la que un usuario empieza a trabajar un dia determinado
function check_day_start_user($user_id, $day)
{
    $user   = \App\Models\User::find($user_id);
    $dates  = $user->days->where('day', $day);
    $today  = \Carbon\Carbon::today();
    $start  = null;

    foreach ($dates as $date) 
    {
        $start2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->start);
        
        if($start == null)
        {
            $start = $start2;
        }else
        {
            if($start2->diffInMinutes($start, false) > 0)
                $start = $start2;
        }
    }

    return $start;
}

//Me devuelve la hora en la que un usuario termina de trabajar un dia determinado
function check_day_end_user($user_id, $day)
{
    $user   = \App\Models\User::find($user_id);
    $dates  = $user->days->where('day', $day);
    $today  = \Carbon\Carbon::today();
    $end = null;

    foreach ($dates as $date) 
    {
        $end2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->end);
        
        if($end == null)
        {
            $end = $end2;
        }else
        {
            if($end2->diffInMinutes($end, false) < 0)
                $end = $end2;
        }
    }

    return $end;
}

//Me devuelve la hora en la que un usuario termina de trabajar un dia determinado
function check_day_end_service($service_id, $day)
{
    $service    = \App\Models\Service::find($service_id);
    $dates      = $service->days->where('day', $day);
    $today      = \Carbon\Carbon::today();
    $end        = null;

    foreach ($dates as $date) 
    {
        $end2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->end);
        
        if($end == null)
        {
            $end = $end2;
        }else
        {
            if($end2->diffInMinutes($end, false) < 0)
                $end = $end2;
        }
    }

    return $end;
}

function check_day_end_resource($resource_id, $day)
{
    $resource   = \App\Models\Resource::find($resource_id);
    $dates      = $resource->days->where('day', $day);
    $today      = \Carbon\Carbon::today();
    $end        = null;

    foreach ($dates as $date) 
    {
        $end2 = \Carbon\Carbon::create($today->format('Y-m-d').' '.$date->end);
        
        if($end == null)
        {
            $end = $end2;
        }else
        {
            if($end2->diffInMinutes($end, false) < 0)
                $end = $end2;
        }
    }

    return $end;
}

//indica si existe una hora en un array de horas
function search_in_array_date($array, $hour)
{
    foreach($array as $value)
    {
        if($value == $hour)
            return true;
    }

    return false;
}

//indica si existe una fecha y una hora en un array
function search_in_array_date_nobody($array, $date, $hour)
{
    $flag = false;

    foreach($array as $dates)
    {
        if($dates[0] == $date)
        {
            if($dates[1] == null)
            {
                $flag = false;
            }else
            {
                foreach($dates[1] as $hours)
                {
                    if($hours == $hour)
                        $flag = true;
                }
            }
        }
    }

    return $flag;
}

//Devuelve lista de dias
function days_list()
{
    $days = [
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
    ];

    return $days;
}

//Devuelve la lista de mensajes
function messages_list()
{
    $messages = [
        'message_confirm_reserve',
        'message_autoconfirm_reserve',
        'message_confirm_task',
        'message_refuse_task',
        'message_remember_task',
        'message_modify_task',
        'message_delete_task',
        'message_birthday',
        'message_waiting_list',
    ];

    return $messages;
}

//Compruebo si hay una fecha disponible para un cliente en la lista de espera y un usuario
/*
function check_waiting($waiting_id, $user_id)
{
    $waiting    = App\Models\Waiting::find($waiting_id);
    $user       = App\Models\User::find($user_id);
    $first_free = null;                 //Variable que va a mostrar la primera fecha disponible

    foreach($waiting->days as $day) //Recorro todos los dias que se guardaron en la lista
    {
        if(check_absences($user->id, $day->date)) //Chequeo que ese dia no haya ausencia
        {
            if($first_free == null) //Chequeo que no haya una fecha guardada para no repetir el proceso
            {

                //Fecha y hora de inicio que guardo el usuario
                $start          = \Carbon\Carbon::create($day->date." ".$waiting->start); 
                
                //Fecha y hora de finalizar que guardo el usuario
                $end            = \Carbon\Carbon::create($day->date." ".$waiting->end);

                //Fecha y hora en la que el la empresa abre
                $startBusiness  = \Carbon\Carbon::create(
                                    $day->date." ".check_day_start_user(
                                                            $user->id, 
                                                            strtolower($start->format('l'))
                                                        )->format('H:i')
                                    );

                //Fecha y hora en la que el la empresa cierra
                $endBusiness    = \Carbon\Carbon::create(
                                    $day->date." ".check_day_end_user(
                                                            $user->id, 
                                                            strtolower($end->format('l'))
                                                        )->format('H:i')
                                    );
                
                //Controlo si el usuario puso un horario que la empresa aun no abrio
                if(!$start->gt($startBusiness))
                    $start = $startBusiness;
                
                //Controlo si el usuario puso un horario que la empresa ya cerro 
                if(!$end->lt($endBusiness))
                    $end = \Carbon\Carbon::create($endBusiness);

                //Variable que guarda la hora en que terminaria el servicio selecionado
                $endService     = \Carbon\Carbon::create($start)->addMinutes($waiting->service->duration);
                    
                //Consulto las horas disponibles para ese usuario, ese servicio y ese dia
                $hours  = hours($waiting->service, $user, $day->date);
                
                //Mientras no se haya guardado una fecha y mientras el horario de iniciono supere el final del dia busco
                do
                {    
                    foreach($hours as $hour)
                    {
                        if($hour == $start->format('H:i'))
                            $first_free = $hour;
                    }

                    //Si no hay horario disponible agrego 10 min
                    if($first_free == null)
                        $start->addMinutes($user->business->reservation->interval);

                }while(($first_free == null) and ($start->lt($end)));
            }
        }
    }
    
    //devuelvo la fecha
    if ($first_free != null)
        return $day->date." ".$first_free;

    return null;
}
*/

//Compruebo si hay aucencias para un usuario y un dia
/*
function check_absences($user_id, $var_date)
{   
    $user = App\Models\User::find($user_id);
    $date = \Carbon\Carbon::create($var_date);
    $day  = strtolower(date('l', strtotime($date))); //Guardo el nombre del dia de la fecha
    $flag = true;

    //consulto si ese dia trabaja la empresa y trabaja el empleado seleccionado
    if(($user->days->where('day', $day)->first()->status == "off") or ($user->business->days->where('day', $day)->first()->status == "off")) 
    {
        $flag = false;  
    }
 
    //Consulto las ausencias de la empresa y veo si la fecha esta en esa ausencia
    foreach($user->business->absences->where('user_id', null) as $absence) 
    {
        if($date->betweenIncluded($absence->start, $absence->end->addDay()))
            $flag = false;
    }
    
    //Consulto las ausencias del usuario y veo si la fecha esta en esa ausencia
    foreach($user->absences as $absence) 
    {
        if($date->betweenIncluded($absence->start, $absence->end->addDay()))
            $flag = false;
    }
    
    return $flag;
}
*/

//Devuelve las horas de un usuario, servicio y fecha determinados
/*
function hours($service, $user, $date, $business_id = null)
{
    $alt        = Collect();
    $hours2     = Collect();
    $hours      = Collect();
    $day_name   = strtolower(date('l', strtotime(\Carbon\Carbon::create($date))));
    
    if($business_id == null)
    {
        $days       = $user->days->where('day', $day_name);
    }
    else
    {
        $business   = App\Models\Business::find($business_id);
        $days       = $business->days->where('day', $day_name);
    }
    

    //Recorro los horarios que tenga el usuario para ese dia, ya que puede tener horario cortado
    foreach($days as $day)
    {   
        $start      = \Carbon\Carbon::create($date.' '.$day->start);
        $end        = \Carbon\Carbon::create($date.' '.$day->end);
           
        //Reviso que no haya ninguna cita para todas las horas entre el inicio y el fin del dia
        do{
            $task = $user->tasks()->where('start', '<=', $start)->where('end', '>', $start)->where('status', '!=', 'cancel')->first();
    
            if($task == null) 
                $alt->add($start->format('H:i'));        
    
            $start->addMinutes($user->business->reservation->interval);

        }while($start->diffInMinutes($end) > 5);

        //Reviso que no haya ninguna cita para el horario de la cita mas la hora del servicio
        foreach($alt as $a)
        {
            $hour   = \Carbon\Carbon::create($date." ".$a);
            $end    = \Carbon\Carbon::create($date." ".$a);

            $end->addMinutes($service->duration);

            $task = $user->tasks()->whereBetween('start', [$hour, $end])->where('status', '!=', 'cancel')->first();

            if($task == null)
                $hours2->add($hour->format('H:i'));
        }
    }
    
    //unifico todas las horas ya que se repiten al recorrer varios horarios por dia
    $hours2 = $hours2->unique();

    //Si el dia seleccionado es hoy, reviso que la hora no sea anterior a la hora actual
    if(\Carbon\Carbon::create($date)->isToday())
    {
        foreach($hours2 as $hour)
        {
            if(\Carbon\Carbon::now()->roundMinutes(10) < \Carbon\Carbon::create($date." ".$hour))
                $hours->add($hour);
        }
    }
    else
    {
        //Si el dia no es hoy, paso las horas que ya guarde anteriormente recorriendo los horarios del dia
        $hours = $hours2;
    }

    return $hours;
}
*/

//Instalo un tema visual determinado en una empresa
function install_theme($folder, $business)
{
    if(file_exists(resource_path()."/views/landings/".$folder."/install.xml"))
    {
        $install = simplexml_load_file(resource_path()."/views/landings/".$folder."/install.xml");

        foreach($install as $element)
        {
            $landing                = new App\Models\Landing();
            $landing->business_id   = $business->id;
            $landing->title         = $element->title;
            $landing->key           = $element->key;
            $landing->value         = $element->value;
            $landing->type          = $element->type;
            $landing->description   = $element->description;
            $landing->save();
        }
    }
}

//Devuelvo una entrada de la tabla landing para una empresa determinada, se utiliza en los views del landing
function landing($key, $business)
{
    $business = App\Models\Business::find($business);

    if($business == null)
        return false;

    $landing = $business->landings->where('key', $key)->first();
    
    if($landing != null)
        return $landing->value;
    
    return null;
}

//checqueo las horas de todos los usuarios para saber si hay al menos una disponible
function check_hours_from_dates($dates, $business)
{
    $return_hours   = collect();
    $day_name       = strtolower(date('l', strtotime(\Carbon\Carbon::create($dates->first()['date']))));
    $start          = \Carbon\Carbon::create($dates->first()['date'].' '.check_day_start($business, $day_name)->format('H:i'));
    $end            = \Carbon\Carbon::create($dates->first()['date'].' '.check_day_end($business, $day_name)->format('H:i'));
    
    while($start->lt($end))
    {
        $flag = false;

        foreach($dates as $date)
        {
            foreach($date['hours'] as $hour)
            {
                if($hour == $start->format("H:i"))
                    $flag = true;
            }   
        }

        if($flag)
            $return_hours->add($start->format("H:i"));

        $start->addMinutes($business->reservation->interval);
    }

    return $return_hours;
}

//instala los mensajes base de la empresa
function install_messages($business_id)
{
    $messages = messages_list();

    foreach($messages as $message)
    {
        \DB::table('messages')->insert([
            "key"           => $message,
            "value"         => __('sms.'.$message),
            "description"   => __('sms.'.$message.'_des'),
            "restrictions"  => __('sms.'.$message.'_res'),
            "business_id"   => $business_id
        ]);
    }

    return true;
}

//crea un usuario
function create_user($data, $business)
{
    $days = days_list();

    $user = new \App\Models\User();

    $user->name              = $data['name'];
    $user->email             = $data['email'];
    $user->phone             = $data['phone'];
    $user->address           = $data['address'];
    $user->gender            = $data['gender'];
    $user->type              = $data['type'];
    $user->color             = $data['color'];
    $user->online            = $data['online'];
    $user->access            = $data['access'];
    $user->confirmed         = $data['confirmed'];
    $user->birthday          = $data['birthday'];
    $user->password          = $data['password'];
    $user->confirmation_code = $data['confirmation_code'];
    $user->business_hour     = "on";
    $user->business_id       = $business->id;
    $user->save();


    for($i = 0; $i < 7; $i++)
    {
        \DB::table('days_users')->insert([
            'day'           => $days[$i],
            'start'         => check_day_start($business, $days[$i])->format('H:i'),
            'end'           => check_day_end($business, $days[$i])->format('H:i'),
            'status'        => check_day_status($business, $days[$i]),
            'user_id'       => $user->id,
        ]);
    }

    \DB::table('calendar_options')->insert([
        'user_id'    => $user->id,
    ]);

    return $user;
}

//Crea una empresa
function create_business($data)
{
    $business                       = new \App\Models\Business();
    $business->name                 = $data['name'];
    $business->email                = $data['email'];
    $business->phone                = $data['phone'];
    $business->status               = $data['status'];
    $business->pay                  = $data['pay'];
    $business->code                 = $data['code'];
    $business->plan                 = $data['plan'];
    $business->amount               = $data['amount'];
    $business->sms_limit            = $data['sms_limit'];
    $business->mp_state             = $data['mp_state'];
    $business->mp_access_token      = $data['mp_access_token'];
    $business->mp_public_key        = $data['mp_public_key'];
    $business->mp_refresh_token     = $data['mp_refresh_token'];
    $business->mp_user_id           = $data['mp_user_id'];
    $business->mp_expires_in        = $data['mp_expires_in'];
    $business->api_key              = $data['api_key'];
    $business->app                  = $data['app'];
    $business->first_login          = $data['first_login'];
    $business->save();

    $days = days_list();

    for($i = 0; $i < 7; $i++)
    {
        $flag = 'on';
        
        if($days[$i] == "sunday")
            $flag = 'off';

        \DB::table('days')->insert([
            'day'           => $days[$i],
            'start'         => '09:00',
            'end'           => '18:00',
            'status'        => $flag,
            'business_id'   => $business->id,
        ]);
    }

    \DB::table('reservations')->insert([
        'business_id' => $business->id,
    ]);

    return $business;
}

function convert_restrictions($data)
{
    $result = str_replace("[", "", $data);
    $result = str_replace("]", "", $result);
    $result = explode(",", $result);

    return $result;
}

function check_message_restrictions($message, $restrictions)
{
    $flag = true;

    foreach(convert_restrictions($restrictions) as $restriction)
    {
        if(!str_contains($message, "*".$restriction."*"))
            $flag = false;
    }

    return $flag;
}

function replace_messages($business_id, $key, $array)
{
    $business   = App\Models\Business::find($business_id);
    $message    = $business->messages->where('key', $key)->first();
    $text       = $message->value;

    foreach(convert_restrictions($message->restrictions) as $restriction)
        $text = str_replace("*".$restriction."*", $array[$restriction], $text);

    return $text;
}

function calendar_code()
{
    do
    {
        $code = Random_Code(5);
        $flag = App\Models\Calendar::where('code', $code)->first();
    
    }while($flag != null);

    return $code;
}

function cron_info($command, $return)
{
    $cron = new App\Models\CronInfo();

    $cron->command  = $command;
    $cron->return   = $return;
    $cron->save();

    return true;
}

function delete_folder($folder)
{
    foreach(glob($folder . "/*") as $file)
    {             
        if (is_dir($file))
            delete_folder($file);
        else
            unlink($file);
    }
     
    rmdir($folder);
}
