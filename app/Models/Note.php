<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public function businsess()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
