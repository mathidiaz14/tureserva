<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Models\Calendar');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function waitings()
    {
        return $this->hasMany('App\Models\Waiting');
    }
    
    public function days()
    {
        return $this->hasMany('App\Models\DaysService');
    }

    public function resources()
    {
        return $this->belongsToMany('App\Models\Resource');
    }
    
}
