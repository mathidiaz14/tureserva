<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Waiting extends Model
{
    use HasFactory;

    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function days()
    {
        return $this->hasMany('App\Models\WaitingDay');
    }
}
