<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Opinion extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function task()
    {
        return $this->hasOne('App\Models\Calendar');
    }
}
