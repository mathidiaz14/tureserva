<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
	protected $fillable = [
        'business_id', 
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }
}
