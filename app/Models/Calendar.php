<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $guarded = [];

    protected $dates = ['start', 'end'];


    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function users()
    {
    	return $this->belongsToMany('App\Models\User');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'calendar_service');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function resources()
    {
        return $this->belongsToMany('App\Models\Resource');
    }

    public function opinion()
    {
        return $this->hasOne('App\Models\Opinion');
    }
}
