<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Models\Calendar');
    }

    public function days()
    {
        return $this->hasMany('App\Models\DaysResource');
    }

    public function absences()
    {
        return $this->hasMany('App\Models\Absence');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service');
    }
}
