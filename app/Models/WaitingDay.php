<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaitingDay extends Model
{
    use HasFactory;

    public function waiting()
    {
        return $this->belongsTo('App\Models\Waiting');
    }
}
