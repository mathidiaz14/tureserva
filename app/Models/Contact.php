<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Contact', 'contact_id');
    }

    public function childrens()
    {
        return $this->hasMany('App\Models\Contact', 'contact_id');
    }

}
