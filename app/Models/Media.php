<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function posts()
    {
    	return $this->hasMany('App\Models\Post');
    }

    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }
}
