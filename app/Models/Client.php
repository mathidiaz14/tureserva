<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{

    protected $dates = ['birthday'];

    protected $guarded = [];

    public function business()
    {
    	return $this->belongsTo('App\Models\Business');
    }

    public function tasks()
    {
    	return $this->hasMany('App\Models\Calendar');
    }

    public function notes()
    {
        return $this->hasMany('App\Models\ClientNote');
    }
}
