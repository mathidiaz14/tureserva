<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HelpPost extends Model
{
    use HasFactory;

    public function category()
    {
        return $this->belongsTo('App\Models\HelpCategory', 'category_id');
    }
}
