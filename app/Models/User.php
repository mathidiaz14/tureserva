<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'business_id', 'online', 'last_login',
    ];

    protected $dates = ['invitation_hour', 'birthday'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function business()
    {
        return $this->belongsTo('App\Models\Business');
    }

    public function tasks()
    {
        return $this->belongsToMany('App\Models\Calendar');
    }

    public function days()
    {
        return $this->hasMany('App\Models\DaysUsers');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service');
    }

    public function calendar_options()
    {
        return $this->hasOne('App\Models\CalendarOption');
    }

    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }

    public function absences()
    {
        return $this->hasMany('App\Models\Absence');
    }

    public function clientNotes()
    {
        return $this->hasMany('App\Models\ClientNote');
    }

    public function waitings()
    {
        return $this->hasMany('App\Models\Waiting');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\Log');
    }

    public function incidents()
    {
        return $this->hasMany('App\Models\Incident');
    }
}
