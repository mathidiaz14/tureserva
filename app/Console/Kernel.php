<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Cron;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('tureserva:backup')->cron('0 0 * * 0'); //Se ejecuta los domingos
        $schedule->command('tureserva:birthday')->cron('0 9 * * *'); //Se ejecuta una vez al día a las 09:00
        $schedule->command('tureserva:suscription')->cron('0 */6 * * *'); //Se ejecuta cada 6 horas
        $schedule->command('tureserva:waiting')->cron('0 */6 * * *'); //Se ejecuta cada 6 horas
        $schedule->command('tureserva:absence')->cron('0 0 * * *'); //Se ejecuta una vez al día
        $schedule->command('tureserva:opinion')->cron('0 */6 * * *'); //Se ejecuta cada 6 horas
        $schedule->command('tureserva:task')->cron('0 * * * *'); //Se ejecuta cada hora
        $schedule->command('tureserva:reset_sms')->cron('0 0 1 * *'); //Se ejecuta el primer dia del mes a las 00:00
        $schedule->command('tureserva:delete_tmp_folder')->cron('0 0 * * *'); //Se ejecuta una vez al día
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
