<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Absence;

class DeleteAbscenceOld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:absence';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina las ausencias que ya pasaron';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $yesterday  = Carbon::now()->subDay();
        $absences   = Absence::all();
        $count      = 0;

        foreach($absences as $absence)
        {
            if($absence->end <= $yesterday )
            {
                $absence->delete();
                $count ++;
            }
        }

        cron_info($this->signature, 'Se eliminaron '.$count.' ausencias.');
    }
}
