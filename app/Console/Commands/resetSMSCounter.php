<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Business;

class resetSMSCounter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:reset_sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reseteo de contador de sms mensual';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $businesses = Business::all();

        foreach($businesses as $business)
        {
            $business->sms_count = 0;
            $business->save();
        }

        cron_info($this->signature, 'Se resetearon los contadores de sms');
    }
}
