<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Business;
use MercadoPago;
use Http;

class CheckPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:suscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que comprueba las suscripciones y si no estan habilitadas cambia el estado del negocio';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $businesses = Business::where('plan_id', '!=', null)->get();
        $count      = 0;

        foreach($businesses as $business)
        {
            $headers = [
                'Authorization' => 'Bearer '.env("MP_ACCESS_TOKEN"),
            ];

            $response   = Http::withHeaders($headers)->get("https://api.mercadopago.com/preapproval/".$business->plan_id);

            $details    = $response->json();
            
            $flag = false;

            if($response->successful())
            {
                if($details['status'] == "authorized")
                    $flag = true;
            }

            if(!$flag)
            {
                $business->plan                 = "plan1";
                $business->pay                  = "yes";
                $business->amount               = 0;
                $business->plan_id              = null;
                $business->save();

                notification(
                    "Suscripción cancelada", 
                    "Su suscripción fue cancelada", 
                    url('details/upgrade'), 
                    $business->id
                );  

                $count ++;
            }
        }

        cron_info($this->signature, "Se comprobaron ".$businesses->count()." empresas y se cancelaron ".$count);
    }
}
