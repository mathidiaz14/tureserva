<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Calendar;
use Carbon\Carbon;

class OpinionsRemember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:opinion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para enviar un email para que el cliente de su opinion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $yesterday  = Carbon::yesterday(); 
        $tasks      = Calendar::whereBetween('start', [
                                                    $yesterday->format('Y-m-d')." 00:00:00", 
                                                    $yesterday->format('Y-m-d')." 23:59:59"
                                                ])
                                ->where('status', '!=', 'cancel')
                                ->get();
        $count = 0;

        foreach($tasks as $task)
        {
            if($task->opinion == null)
            {
                if(($task->client_id != null) and ($task->client->email != null))
                {
                    $data = array(
                        'task' => $task,
                    );
                    
                    send_email('emails.task_opinion', $data, $task->client->email, "Danos tu opinion");

                    $count ++;
                }
            }
        }

        cron_info($this->signature, 'Se enviaron '.$count.' emails.');
    }
}
