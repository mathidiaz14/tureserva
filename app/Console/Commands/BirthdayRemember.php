<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Client;
use Carbon\Carbon;

class BirthdayRemember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio de mensaje para recordar cumpleaños.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today      = Carbon::today();
        $clients    = Client::where('birthday', $today)->get();
        $count      = 0;

        foreach($clients as $client)
        {
            if($client->phone != null)
            {
                $array = [
                    'business'  => $client->business->name,
                ];
                
                $return = send_sms(
                    $client->phone, 
                    replace_messages($client->business_id, 'message_birthday', $array), 
                    $client->business_id
                );

                if($return == true)
                    $count ++;
            }
        }

        cron_info($this->signature, 'Se envian '.$count.' SMS por cumpleaños');
    }
}
