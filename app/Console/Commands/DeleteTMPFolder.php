<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class DeleteTMPFolder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:delete_tmp_folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elimina el contenido de la carpeta temporal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('*****************************');
        $this->info('**** Comienza el proceso ****');
        $this->info('*****************************');

        $files = glob(storage_path().'/app/public/tmp/*'); //obtenemos todos los nombres de los ficheros

        $count = 0;

        foreach($files as $file){
            if(is_file($file))
            {
                unlink($file); //elimino el fichero
                $count ++;
            }
        }

        $this->newLine(2);
        $this->info('-> Se eliminaron '.$count.' archivos');
        $this->info('*****************************************');
        $this->info('*****************************************');
    }
}
