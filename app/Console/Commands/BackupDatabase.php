<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PhpZip\ZipFile;
use DB;
use Storage;

class BackupDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respaldo de base de datos y carpeta de storage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('*****************************************');
        $this->info('*** Comienza el respaldo de los datos ***');
        $this->info('*****************************************');

        $db_name            = 'database_'.date('d_m_Y_H_i').'.sql';
        $storage_name       = 'storage_'.date('d_m_Y_H_i').'.zip';
        
        $db_estado          = false;
        $storage_estado     = false;

        //Respaldo de la base de datos
        try 
        {
            $this->info("---------------------------");
            $this->info("Comenzando respaldo de la base de datos");
            $this->info("---------------------------");

            $command = "mysqldump --user=" . env('DB_USERNAME') ." --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . " > " . storage_path("backups/" . $db_name);

            exec($command);
            
            $this->info("---------------------------");
            $this->info("Guardando en S3 respaldo de la base de datos");            
            $this->info("---------------------------");

            Storage::disk('s3')->put($db_name, fopen(storage_path("backups/".$db_name), 'r+'));

            unlink(storage_path("backups/".$db_name));
            
            $db_estado = true;

            $this->info("---------------------------");
            $this->info("Respaldo de la base de datos completo");
            $this->info("---------------------------");

        } catch (Exception $e) 
        {
            $db_estado = $e->getMessage();

            $this->error("---------------------------");
            $this->error("Hubo un error al respaldar la base de datos: ".$e->getMessage());
            $this->error("---------------------------");
        }

        //Respaldo de la carpeta storage
        try 
        {
            $this->info("---------------------------");
            $this->info("Comenzando respaldo de la carpeta storage");
            $this->info("---------------------------");

            $zip_storage    = new ZipFile();
        
            $zip_storage
                ->addDirRecursive(storage_path('app/public'))
                ->saveAsFile(storage_path("backups/".$storage_name))
                ->close(); 
            
            $this->info("---------------------------");
            $this->info("Guardando en S3 el respaldo de la carpeta storage");
            $this->info("---------------------------");

            Storage::disk('s3')->put($storage_name, fopen(storage_path("backups/".$storage_name), 'r+'));

            unlink(storage_path("backups/".$storage_name));

            $storage_estado = true;

            $this->info("---------------------------");
            $this->info("Respaldo de la carpeta storage completo");
            $this->info("---------------------------");

        } catch (Exception $e) 
        {
            $storage_estado = $e->getMessage();
            
            $this->error("---------------------------");
            $this->error("Hubo un error al respaldar la carpeta storage: ".$e->getMessage());
            $this->error("---------------------------");
        }

        $this->info("---------------------------");
        $this->info("Se envia email con informe");
        $this->info("---------------------------");

        $data = [
            'database'  => $db_estado == 1 ? "Completo" : $db_estado,
            'storage'   => $storage_estado == 1 ? "Completo" : $storage_estado,
        ];
        
        send_email('emails.root.database_backup', $data, 'admin@tureserva.io', "Respaldo de base de datos");

        cron_info($this->signature, "Se realiza el respaldo de la base de datos");
        
        $this->newLine();
        $this->info("*****************************************");
        $this->info("*** Finaliza el respaldo de los datos ***");
        $this->info("*****************************************");
    }
}
