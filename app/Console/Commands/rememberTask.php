<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Calendar;
use Carbon\Carbon;

class rememberTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar aviso a las personas por cita proxima';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $last   = \Carbon\Carbon::now()->addDay();

        $tasks  = Calendar::whereBetween('start', [
                                                    $last->format('Y-m-d')." 00:00:00", 
                                                    $last->format('Y-m-d')." 23:59:59"
                                                ])
                            ->where('sms', 'on')
                            ->where('reminder_send', null)
                            ->get();

        $count  = 0;
        
        foreach($tasks as $task)
        {
            if($task->client_phone != null)
            {
                $array = [
                    'business'      => $task->business->name,
                    'hour'          => $task->start->format('H:i'),
                    'url'           => url('cancelar', $task->code),
                ];
                
                $return = send_sms(
                    $task->client_phone, 
                    replace_messages($task->business_id, 'message_remember_task', $array), 
                    $task->business_id
                );
                
                if($return == true)
                    $count ++;

                $task->reminder_send = "send";
            }
            
            if(($task->client_id != null) and ($task->client->email != null))
            {
                $array = [
                    'business'      => $task->business->name,
                    'hour'          => $task->start->format('H:i'),
                    'url'           => url('cancelar', $task->code)
                ];

                $data = array(
                    'content' => replace_messages($task->business_id, 'message_remember_task', $array),
                );
                
                send_email('emails.alert_task', $data, $task->client->email, "Recordatorio de cita");
            }

            $task->save();
        }

        cron_info($this->signature, "Se enviaron ".$count." mensajes.");
    }
}
