<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Waiting;
use App\Models\User;
use Carbon\Carbon;

class CheckWaitingList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tureserva:waiting';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Controlo las listas de espera para ver si hay disponibilidad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $waitings       = Waiting::all(); //guardo toda la lista
        $tomorrow       = Carbon::tomorrow(); //guardo fecha de mañana
        $waitings_count = 0; //Guardo variable para contar

        //Recorro toda la lista
        foreach($waitings as $waiting)
        {
            //Si el usuario seleccionado fue ninguno en particular
            if($waiting->user == "nobody")
            {
                //Guardo todos los usuarios habilitados para ese servicio
                $users = $waiting->service->users->where('online', 'on')->get();

                //Continuo mientras no haya una fecha disponible y mientras haya usuarios
                do
                {
                    //Numero random dentro del array
                    $number = rand(0, count($users)-1);

                    //Selecciono el usuario que esta en el numero random
                    $user = $users[$number];

                    //Elimino ese usuario de la lista de usuarios
                    unset($users[$number]);

                    //Controlo si hay disponibilidad para ese usuario
                    $date = check_waiting($waiting->id, $user->id);

                }while(($date == null) and (count($users) > 0));

            }else //Si hay un usuario seleccionado
            {
                //Busco el usuario en la base de datos
                $user = User::find($waiting->user_id);

                //Consulto si hay una fecha disponible para ese usuario
                $date = check_waiting($waiting->id, $user->id);
            }

            //Si hay una fecha disponible
            if($date != null)
            {
                //Sumo uno al contador
                $waitings_count ++;

                //Si guardo el telefono envio SMS
                if($waiting->phone != null)
                {
                    $array = [
                        'business'  => $waiting->business->name,
                        'date'      => $date,
                        'user'      => $user->name,
                    ];

                    send_sms(
                        $waiting->phone, 
                        replace_messages($client->business_id, 'waiting_list_aviable', $array), 
                        $waiting->business->id
                    );
                }

                //Si guardo email envio mail
                if($waiting->email != null)
                {
                    $data = array(
                        'business'  => $waiting->business,
                        'service'   => $waiting->service,
                        'user'      => $waiting->user,
                        'date'      => $date
                    );
                    
                    send_email(
                        'emails.alert_waiting', 
                        $data, 
                        $waiting->email, 
                        "Horario disponible"
                    );
                }
                
                //Doy formato de fecha al string que contiene la fecha
                $date = Carbon::create($date);
                
                //Busco el dia en la lista de dias de la reserva y lo elimino
                foreach($waiting->days as $day)
                {
                    if($day->date == $date->format('d-m-Y'))
                        $day->delete();
                }

            }

            //Controlo si la reserva tiene algun dia esperando, sino la borro
            if($waiting->days->count() == 0)
                $waiting->delete(); 
            
            //Selecciono el ultimo dia de la reserva
            $last = $waiting->days()->orderBy('date', 'desc')->first();

            //Si hay un dia y ese dia es mañana, aviso que no hay reserva disponible
            if(($last != null) and ($last->date == $tomorrow->format('d-m-Y')))
            {   
                $waiting->days()->delete();
                $waiting->delete();

                $data = array(
                    'business'  => $waiting->business,
                );
                
                send_email(
                    'emails.alert_not_waiting', 
                    $data, 
                    $waiting->email, 
                    "No hay horario disponible"
                );
            }
        }
    
        cron_info($this->signature, 'Se aviso a '.$waitings_count.' clientes');
    }
}
