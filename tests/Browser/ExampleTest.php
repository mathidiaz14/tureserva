<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->type('business_name', "Demo")
                    ->type('name', "Nombre usuario")
                    ->type('email', "demo@tureserva.uy")
                    ->type('phone', "123456789")
                    ->select('gender', 'other')
                    ->type('password', 'password')
                    ->type('password_confirmation', 'password')
                    ->click('@register-button');
        });

        $this->assertDatabaseHas('users', [
            'email' => 'demo@tureserva.uy'
        ]);

        $this->assertDatabaseHas('businesses', [
            'name' => 'Demo'
        ]);
        
    }
}
