<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('rut')->nullable();
            $table->string('address')->nullable();
            $table->string('logo')->nullable();
            $table->string('status')->default('enabled');
            $table->string('plan')->nullable();
            $table->string('pay')->nullable();
            $table->string('amount')->nullable();
            $table->string('plan_id')->nullable();
            $table->string('code')->nullable();
            $table->string('api_key')->nullable();
            $table->string('url')->nullable();
            $table->string('url2')->nullable();
            $table->string('first_login')->nullable();
            $table->string('folder')->default('default');
            $table->integer('sms_count')->default(0);
            $table->integer('sms_limit')->nullable();
            $table->string('mp_state')->nullable();
            $table->string('mp_access_token')->nullable();
            $table->string('mp_public_key')->nullable();
            $table->string('mp_refresh_token')->nullable();
            $table->string('mp_user_id')->nullable();
            $table->string('mp_expires_in')->nullable();
            $table->string('return_status')->nullable();
            $table->string('return_option')->nullable();
            $table->string('return_percentage')->nullable();
            $table->string('control_code')->nullable();
            $table->string('control_user')->nullable();
            $table->string('control_date')->nullable();
            $table->string('app')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businesses');
    }
}
