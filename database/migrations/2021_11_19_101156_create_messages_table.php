<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Tabla que se usa para modificar los mensajes enviados por sms para cada empresa
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('value');
            $table->text('description');
            $table->string('restrictions');
            $table->string('business_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
