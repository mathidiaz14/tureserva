<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('business_id');
            $table->string('access')->nullable()->default('on');
            $table->string('auto')->nullable();
            $table->string('service_price')->nullable();
            $table->string('today')->nullable();
            $table->string('nobody')->nullable();
            $table->string('table_hours')->nullable();
            $table->string('days_cancel')->default('5');
            $table->string('days_reserve')->default('14');
            $table->string('interval')->default('10');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
