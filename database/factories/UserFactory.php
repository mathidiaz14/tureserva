<?php

namespace Database\Factories;

use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = Factory::create();

        return [
            'name'              => $faker->name,
            'email'             => $faker->unique()->safeEmail,
            'email_verified_at' => now(),
            'type'              => 'admin',  
            'color'             => $faker->hexcolor,
            'online'            => $faker->randomElement($array = array ('on', null)),
            'phone'             => $faker->phoneNumber,
            'address'           => $faker->streetAddress,
            'birthday'          => $faker->date($format = 'Y-m-d', $max = 'now'),
            'password'          => bcrypt('password'),
            'remember_token'    => Str::random(10),
            'business_id'       => '1',
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
