<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class businessUserServiceSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 6; $i++) 
        { 
            for ($x=3; $x <= 4; $x++) 
            { 
                \DB::table('service_user')->insert([
                    'service_id'    => $i,
                    'user_id'       => $x,
                ]);
            }
        }
    }
}
