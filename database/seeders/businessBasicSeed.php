<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class businessBasicSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_business = [
            'name'                  => "Empresa de prueba",
            'email'                 => 'admin@admin.com',
            'phone'                 => null,
            'status'                => 'enabled',
            'pay'                   => 'yes',
            'code'                  => 'prueba',
            'plan'                  => 'plan3',
            'sms_limit'             => '1000',
            'amount'                => '999',
            'mp_state'              => 'connect',
            'mp_access_token'       => 'APP_USR-5676611826474974-092303-af701c66daec850679c57d0b25e6fb70-161286386',
            'mp_public_key'         => 'APP_USR-96dad562-13ad-4b96-a338-f763b86b9634',
            'mp_refresh_token'      => 'TG-614bf1fe4eaadd00091a822b-161286386',
            'mp_user_id'            => '161286386',
            'mp_expires_in'         => '15552000',
            'api_key'               => Random_string(20),
            'app'                   => 'enabled',
            'first_login'           => 'complete',
        ];

        $data_user = [
            'name'              => "Administrator",
            'email'             => "admin@admin.com",
            'phone'             => null,
            'address'           => null,
            'type'              => "admin",
            'color'             => Random_color(),
            'online'            => null,
            'access'            => 'on',
            'confirmed'         => 1,
            'birthday'          => null,
            'password'          => bcrypt('123456'),
            'confirmation_code' => null,
            'business_id'       => '1',
            'gender'            => 'other',
        ];

        $business   = create_business($data_business);
        $user       = create_user($data_user, $business);

        install_messages($business->id);
    }
}
