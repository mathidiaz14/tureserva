<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class rootSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name'          => "ROOT",
            'email'         => 'hola@tureserva.io',
            'password'      => bcrypt('123456'),
            'type'          => 'root',  
            'confirmed'     => '1',
            'access'        => 'on', 
            'business_id'   => 'root',
        ]);
    }
}
