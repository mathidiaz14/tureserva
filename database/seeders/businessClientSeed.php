<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory;

class businessClientSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for($i = 0; $i < 50; $i++)
        {
            \DB::table('clients')->insert([
                'name'          => $faker->name,
                'email'         => $faker->email,
                'phone'         => $faker->phoneNumber,
                'address'       => $faker->streetAddress,
                'birthday'      => $faker->date($format = 'Y-m-d', $max = 'now'),
                'business_id'   => '1',
                'created_at'    => now(),
            ]);
        }
    }
}
