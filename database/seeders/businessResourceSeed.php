<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class businessResourceSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('resources')->insert([
            'name'          => 'Recurso 1',
            'business_id'   => '1',
            'type'          => 'other'
        ]);

        \DB::table('resources')->insert([
            'name'          => 'Recurso 2',
            'business_id'   => '1',
            'type'          => 'other'
        ]);

        \DB::table('resources')->insert([
            'name'          => 'Recurso 3',
            'business_id'   => '1',
            'type'          => 'other'
        ]);

        \DB::table('resources')->insert([
            'name'          => 'Recurso 4',
            'business_id'   => '1',
            'type'          => 'other'
        ]);

        \DB::table('resources')->insert([
            'name'          => 'Recurso 5',
            'business_id'   => '1',
            'type'          => 'other'
        ]);

        $days = days_list();
        $business = \App\Models\Business::find(1);

        for ($x=1; $x < 6; $x++) 
        { 
            for($i = 0; $i < 7; $i++)
            {
                \DB::table('days_resources')->insert([
                    'day'           => $days[$i],
                    'start'         => check_day_start($business, $days[$i])->format('H:i'),
                    'end'           => check_day_end($business, $days[$i])->format('H:i'),
                    'status'        => check_day_status($business, $days[$i]) == true ? "on" : "off",
                    'resource_id'    => $x,
                ]);
            }
        }
    }
}
