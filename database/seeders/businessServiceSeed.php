<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class businessServiceSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('services')->insert([
            'name'          => 'Servicio 1',
            'description'   => 'Descripcion del servicio 1',
            'price'         => '100',
            'duration'      => '30',
            'online'        => 'on',
            'business_id'   => '1',
        ]);

        \DB::table('services')->insert([
            'name'          => 'Servicio 2',
            'description'   => 'Descripcion del servicio 2',
            'price'         => '150',
            'duration'      => '60',
            'online'        => 'on',
            'business_id'   => '1',
        ]);

        \DB::table('services')->insert([
            'name'          => 'Servicio 3',
            'description'   => 'Descripcion del servicio 3',
            'price'         => '200',
            'duration'      => '75',
            'online'        => 'on',
            'business_id'   => '1',
        ]);

        \DB::table('services')->insert([
            'name'          => 'Servicio 4',
            'description'   => 'Descripcion del servicio 4',
            'price'         => '100',
            'duration'      => '90',
            'online'        => 'on',
            'business_id'   => '1',
        ]);

        \DB::table('services')->insert([
            'name'          => 'Servicio 5',
            'description'   => 'Descripcion del servicio 5',
            'price'         => '500',
            'duration'      => '120',
            'online'        => 'on',
            'business_id'   => '1',
        ]);

        $days = days_list();
        $business = \App\Models\Business::find(1);

        for ($x=1; $x < 6; $x++) 
        { 
            for($i = 0; $i < 7; $i++)
            {
                \DB::table('days_services')->insert([
                    'day'           => $days[$i],
                    'start'         => check_day_start($business, $days[$i])->format('H:i'),
                    'end'           => check_day_end($business, $days[$i])->format('H:i'),
                    'status'        => check_day_status($business, $days[$i]) == true ? "on" : "off",
                    'service_id'    => $x,
                ]);
            }
        }
    }
}
