<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
                rootSeed::class,
                businessBasicSeed::class,
                businessUserSeed::class,
                businessClientSeed::class,
                businessResourceSeed::class,
                businessServiceSeed::class,
                businessUserServiceSeed::class,
            ]);
    }
}
