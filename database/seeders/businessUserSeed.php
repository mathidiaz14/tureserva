<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\User;
use App\Models\WeekHourUser;
use App\Models\CalendarOption;
use Str;

class businessUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker      = Factory::create();
        $business   = \App\Models\Business::find(1);

        for ($i=0; $i < 5; $i++) 
        { 
            $data_user = [
                'name'              => $faker->name,
                'email'             => $faker->unique()->safeEmail,
                'type'              => 'admin',  
                'color'             => Random_color(),
                'online'            => 'on',
                'confirmed'         => 0,
                'access'            => null,
                'phone'             => $faker->phoneNumber,
                'address'           => $faker->streetAddress,
                'birthday'          => $faker->date($format = 'Y-m-d', $max = '2002-9-6'),
                'password'          => "",
                'confirmation_code' => null,
                'business_id'       => '1',
                'gender'            => 'other',
            ];

            $user       = create_user($data_user, $business);
        }
    }
}
