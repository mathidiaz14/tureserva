@if(session()->has('confirmation_code'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-success">
				<h5>{{trans('web.confirm', ['code' => Session('confirmation_code')])}}</h5>
				
				{{session()->forget('confirmation_code')}}
			</div>
		</div>
	</div>

@elseif(session()->has('error'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-danger">
				{{session('error')}}
				{{session()->forget('error')}}
			</div>
		</div>
	</div>

@elseif(session()->has('waiting'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-success">
				<h5>{{session('waiting')}}</h5>
				
				{{session()->forget('waiting')}}
			</div>
		</div>
	</div>

@elseif(session()->has('opinion'))
	<div class="row">
		<div class="col text-center">
			<div class="alert alert-success">
				<h5>{{session('opinion')}}</h5>
				
				{{session()->forget('opinion')}}
			</div>
		</div>
	</div>
@endif