<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>{{$business->name}}</title>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

	<link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
	
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
	
	<link rel="stylesheet" href="{{asset('landings/default/assets/style.css')}}">

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
	<meta name="csrf-token" content="{{ csrf_token() }}">
  	
  	@if($business->logo != null)
    	<link rel="shortcut icon" type="image" href="{{asset($business->logo)}}">
  	@else
    	<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
  	@endif

  	<style>
  		body
  		{
  			background: {{landing('background', $business->id)}}!important;
  			color: {{landing('text_color', $business->id)}}!important;
  		}

  		nav, footer
  		{
  			background: {{landing('header_color', $business->id)}}!important;
  		}

  		.btn-default, .btn-primary
  		{
  			background: {{landing('primary_button', $business->id)}}!important;	
  			border:none;
  		}

  		.btn-default:hover, .btn-primary:hover
  		{
  			background: {{landing('primary_button', $business->id)}}d1!important;	
  		}

  		.btn-back
  		{
  			background: {{landing('back_button', $business->id)}}!important;	
  			border:none;
  		}

  		.btn-back:hover
  		{
  			background: {{landing('back_button', $business->id)}}d1!important;	
  		}

  		.btn-service:hover, .btn-user:hover, .timelapse.active, .hour:hover
  		{
  			background: {{landing('principal_back_color', $business->id)}};
  		}

  		.btn-service, .btn-user, .timelapse
  		{
  			border: solid 1px {{landing('principal_back_color', $business->id)}};
  		}

  		.logo_null
  		{
  			background: {{landing('principal_back_color', $business->id)}};
  		}
  	</style>

</head>
<body>
    <header class="py-5 bg-image-full" style="
			@if($business->reservation->header == null)
				background-image: url({{asset('images/header.jpg')}});
			@else
				background-image: url({{asset(landing('header', $business->id))}});
			@endif
    ">

	    @if($business->logo != null)
	      	<img class="img-fluid d-block mx-auto" src="{{asset($business->logo)}}" width="200px" height="200px" alt="">
	    @else
	      	<div class="mx-auto logo_null">
	            <h3><b>{{LogoLetter($business->name)}}</b></h3>
	      	</div>
	    @endif
  	</header>

  	@include('landings.default.parts.menu')
	
	@include('landings.default.parts.thanks')

	@if(landing('client_menssage', $business->id) != "")
		<div class="alert alert-warning text-center pt-4">
			<p>{{landing('client_menssage', $business->id)}}</p>
		</div>	
	@endif

  	<section class="py-5" id="top">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<p class="lead"><b>{{__('web.welcome')}}</b></p>
					<br>
				</div>		
			</div>
			<div class="row">
				<div class="col-12 timelapse_all mt-3" style="display:none;">
					<hr>
					<div class="row">
						<div class="col-md col-5 timelapse timelapse_service active">
							<b>{{__('web.service')}}</b>
						</div>
						
						<div class="col-md col-2 pt-2"><hr></div>
						
						<div class="col-md col-5 timelapse timelapse_user">
							<b>{{__('web.user')}}</b>
						</div>
						
						<div class="col-md d-none d-sm-none d-md-block pt-2"><hr></div>
						
						<div class="col-md col-5 timelapse timelapse_date">
							<b>{{__('web.date_hour')}}</b>
						</div>
						
						<div class="col-md col-2 pt-2"><hr></div>
						
						<div class="col-md col-5 timelapse timelapse_finish">
							<b>{{__('web.finish')}}</b>
						</div>
					</div>
					<div class="row">
						<div class="col-12 text-center">
							<p class="time_count_content pt-4" style="display:none;">
								{{trans('web.details_time')}} 
								<b class="time_count"></b>
							</p>
						</div>
					</div>
					<hr>
				</div>
			</div>

			<div class="row">
				<div class="col-12 body">
					<div class="service_section">
						@include('landings.default.service')
					</div>

					<div class="user_section" style="display:none;">
					</div>

					<div class="date_section" style="display:none;">
					</div>

					<div class="form_section" style="display:none;">
						@include('landings.default.form')
					</div>

					@include('helpers.loading')
				</div>
			</div>
		</div>
	</section>	

	<section>
		<div class="container">
			<div class="row">
				@if(landing('gallery', $business->id) == "on")
					@include('landings.default.parts.gallery')
				@endif

				@if(landing('map', $business->id) == "on")
					@include('landings.default.parts.map')
				@endif	
			</div>

			<div class="row">
				@if(landing('contact', $business->id) == "on")
					@include('landings.default.parts.contact')
				@endif

				@if(landing('hours', $business->id) == "on")
					@include('landings.default.parts.hours')
				@endif	
			</div>
		</div>
	</section>
  
	<section class="py-5 bg-image-full" style="
		@if($business->reservation->footer == null)
			background-image: url({{asset('images/footer.jpg')}});
		@else
			background-image: url({{asset(landing('footer', $business->id))}});
		@endif
	">
	</section>

	<!-- Footer -->
	<footer class="py-5">
		<div class="container">
			<a href="{{url('/')}}" target="_blank">
				<p class="m-0 text-center text-white">
					<img src="{{asset('images/icon.png')}}" alt="" width="30px">
					{{config('app.name')}}
				</p>
			</a>
		</div>
	</footer>
  
	<script>
		
		$(document).ready(function()
		{
    		$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });    		
		});
	</script>

	@include('helpers.footer')
</body>

</html>
