<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">

<section class="content style3" id="gallery">
	<div class="inner">
		<h4>{{__('web.gallery')}}</h4>
		<div class="tz-gallery">
			@if(count($business->medias) == 0)
				<div class="row">
					<div class="col-sm-12 text-center">
						<i class="fa fa-images fa-3x"></i>
						<br><br>
						<p>{{trans('web.no_photos')}}</p>
					</div>
				</div>
			@else
				<div class="row">
					@foreach($business->medias as $media)
			            <div class="col-6 col-md-4 col-lg-3">
							@if($loop->iteration < 5)
				                <a class="lightbox" href="{{asset($media->url)}}">
				                    <img src="{{asset($media->url)}}" alt="Park">
				                </a>
				            @else
								<a class="lightbox" href="{{asset($media->url)}}" style="display: none;">
				                    <img src="{{asset($media->url)}}" alt="Park">
				                </a>
				            @endif
			            </div>
					@endforeach
			    </div>
			    <small>{{count($business->medias)}} {{trans('web.photos')}}</small>
			@endif
		</div>

		<script>
			$(document).ready(function()
			{
				baguetteBox.run('.tz-gallery');
			});
		</script>
	</div>
</section>