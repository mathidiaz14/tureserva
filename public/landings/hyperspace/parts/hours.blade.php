<section class="content style2" id="hours">
	<div class="inner">
		<h4>{{__('web.hours')}}</h4>
		<br><br>
		<div class="row">
			<div class="col-12">
				<div class="table table-responsive">
					<table class="table">
						@foreach(days_list() as $list)
							@if($business->days->where('day', $list)->first()->status != "off")
								<tr>
									<td>
										<b>{{__('message.'.$list)}}: </b>
									</td>
							
									@foreach($business->days->where('day', $list) as $day)
										<td>
											{{$day->start}} - {{$day->end}}
										</td>
									@endforeach
								</tr>	
							@endif
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</section>