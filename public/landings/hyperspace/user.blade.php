@if($users->count() == 0)
	<div class="row">
		<div class="col-12 text-center">
			<i class="fa fa-exclamation-triangle fa-3x"></i>
			<br><br>
			<p>{{__('web.not_users')}}</p>
		</div>
	</div>
@else
	<div class="row">
		<h4>{{__('web.users')}}</h4>
		<br><br>
	</div>
	<div class="row">
		@foreach($users as $user)
			<div class="col-12 col-md-6 col-lg-4">
				<div class="btn_user" user_id="{{$user->id}}" user_name="{{$user->name}}">
					<div class="row">
			      		<div class="col text-center">
			      			<h5>
			      				{{$user->name}}
			      			</h5>
			      		</div>
			      	</div>
				</div>
			</div>
		@endforeach

		@if($users->count() > 1)
		    <div class="col-12 col-md-6 col-lg-4">
				<div class="btn_user" user_id="nobody" user_name="{{__('web.nobody')}}">
			      	<div class="row">
			      		<div class="col text-center">
			      			<h5>
			      				{{__('web.nobody')}}
			      			</h5>
			      		</div>
			      	</div>
				</div>
			</div>
		@endif
	</div>
@endif

<div class="row mt-4">
	<div class="col-12">
		<a href="{{url($business->code)}}">
			<i class="fa fa-angle-left"></i>
			Volver a empezar
		</a>
	</div>
</div>

<script>
	$(document).ready(function()
	{
		$('.btn_user').click(function()
		{
			$('.loading').fadeIn();
			$('.user_section').hide();
			$('.timelapse_all').hide();

			var user 	= $(this).attr('user_id');
			var name 	= $(this).attr('user_name');

			$('.date_section').load("{{url('web/dates')}}/{{$service->id}}/"+user, function()
			{
				$('.timelapse_user').html('<b>'+name+'</b>');
				$('.timelapse_user').removeClass('active');
				$('.timelapse_date').addClass('active');
				
				$('#form #user_id').val(user);
				$('.date_section').fadeIn();
				$('.timelapse_all').fadeIn();

				$('.time_count_content').fadeIn();
				$(".time_count").timer(
				{
					duration:'5m',
					callback: function(){
						window.location.reload();
					},
				});

				$('.loading').fadeOut();
			});
			
		});
	});
</script>