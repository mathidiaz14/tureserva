<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{$business->name}}</title>
	
	<link rel="stylesheet" href="{{asset('landings/hyperspace/assets/style.css')}}">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="{{asset('landings/hyperspace/assets/style.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>

	@if($business->logo != null)
		<link rel="shortcut icon" type="image" href="{{asset($business->logo)}}">
	@else
		<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
	@endif

	<style>
		.navigate
		{
			background: {{landing('sidebar_background', $business->id)}}!important;
		}

		body
		{
			background: {{landing('style1', $business->id)}};	
		}

		.style2
		{
			background: {{landing('style2', $business->id)}};	
		}

		.style3
		{
			background: {{landing('style3', $business->id)}};	
		}

		.style4
		{
			background: {{landing('style4', $business->id)}};	
		}

		.mercadopago-button
		{
			-moz-appearance: none!important;
		    -webkit-appearance: none!important;
		    -ms-appearance: none!important;
		    appearance: none!important;
		    -moz-transition: border-color 0.2s ease!important;
		    -webkit-transition: border-color 0.2s ease!important;
		    -ms-transition: border-color 0.2s ease!important;
		    transition: border-color 0.2s ease!important;
		    background-color: transparent!important;
		    border: solid 1px !important;
		    border-color: rgba(255, 255, 255, 0.15) !important;
		    border-radius: 3em!important;
		    color: #ffffff !important;
		    cursor: pointer!important;
		    display: inline-block!important;
		    font-size: 1em!important;
		    font-weight: bold!important;
		    height: calc(4.75em + 2px)!important;
		    letter-spacing: 0.25em!important;
		    line-height: 4.75em!important;
		    outline: 0!important;
		    padding: 0 3.75em!important;
		    position: relative!important;
		    text-align: center!important;
		    text-decoration: none!important;
		    text-transform: uppercase!important;
		    white-space: nowrap!important;
		}

		.mercadopago-button:hover
		{
			background: #00000014!important;
    		transition: .2s!important;
		}
	</style>

</head>
<body>
	
	@include('helpers.loading')

	@include('landings.hyperspace.parts.menu')

	<div class="container-fluid">
		@include('landings.hyperspace.parts.alerts')

		<section class="content style1 intro" id="one">
			<div class="inner">
				<div class="row">
					<div class="col-12 text-center">
						<h1>{{$business->name}}</h1>
						<br><br>
						<h3>{{trans('web.pay_info')}}</h3>
			      		<b class="time_count" style="font-size:20px;"></b>
			      		<hr>
					</div>
					<div class="col-12 col-md-6 text-center">
	    				<script
							src="https://www.mercadopago.com.uy/integrations/v1/web-payment-checkout.js"
							data-preference-id="<?php echo $preference->id; ?>"
							data-button-label="{{trans('web.pay')}}">
						</script>
	    			</div>
	    			<br><br>
	    			<div class="col-12 col-md-6 text-center">
	    				<a href="{{url('web', $business->code)}}" class="button">
	    					<i class="fa fa-times" style="padding-right: 10px;"></i>
	    					{{trans('web.cancel')}}
	    				</a>
	    			</div>
				</div>
			</div>
		</section>
	</div>

	<footer>
		<div class="content-alt style1">
			<div class="inner">
				<p>
					Design by <b class="me-3"><a href="{{url('/')}}">{{env('APP_NAME')}}</a></b> | 

					<b class="ms-3"><a href="{{url('home')}}">Administrar</a></b>
				</p>
			</div>
		</div>
	</footer>

	<script>
		$(".time_count").timer(
		{
			duration:'2m',
			callback: function(){
				window.location.replace("{{url('web/'.$business->code)}}");
			},
		});
	</script>
</body>
</html>