@php
	$services = $business->services->where('online', 'on');
@endphp

@if($services->count() == 0)
	<div class="row">
		<div class="col-12 text-center">
			<i class="fa fa-exclamation-triangle fa-3x"></i>
			<br><br>
			<p>{{__('web.not_services')}}</p>
		</div>
	</div>
@else
	<div class="row">
		<h4>{{__('web.services')}}</h4>
		<br><br>
	</div>
	<div class="row">
		@foreach($business->services->where('online', 'on') as $service)
			<div class="col-12 col-md-6 ">
				<div class="btn_service" service_id="{{$service->id}}" service_name="{{$service->name}}">
					<div class="row">
						<div class="col-xs-12 col-md-8 col-lg-9">
							<h3>{{$service->name}}</h3>
							<p>{{$service->description}}</p>
						</div>
						<div class="col-xs-12 col-md-4 col-lg-3 text-end">
							<p>{{$service->duration}} min</p>
				  			
				  			@if($service->business->reservation->service_price == "on") 
								<p><b>${{$service->price}}</b></p>
					      	@endif
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
@endif

<script>
	$(document).ready(function()
	{
		$('.btn_service').click(function()
		{
			$('.loading').fadeIn();
			$('.service_section').hide();
			$('.timelapse_all').hide();

			var service = $(this).attr('service_id');
			var name 	= $(this).attr('service_name');

			
			$('.user_section').load("{{url('web/users')}}/"+service, function()
			{
				$('#form #service_id').val(service);
				$('.timelapse_service').html('<b>'+name+'</b>');
				
				$('.timelapse_all').fadeIn();
				$('.timelapse.active').removeClass('active');
				$('.timelapse_user').addClass('active');
				
				$('.user_section').fadeIn();
				$('.timelapse_all').fadeIn();
				$('.loading').fadeOut();
			});
		});
	});
</script>