<!-- Date and Time Accordion -->
<div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
        <button id="collapseDateButton" class="accordion-button collapsed" type="button"  data-bs-target="#collapseDate" aria-expanded="false" aria-controls="collapseDate">
            <i class="far fa-clock" style="margin-right:2em;"></i> <b>{{__('web.date_hour')}}</b>
        </button>
    </h2>
    <div id="collapseDate" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#acordionBooking">
        <div class="accordion-body">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="custom-calendar-wrap">
                        <div id="custom-inner" class="custom-inner">
                            <div class="custom-header clearfix">
                                <nav>
                                    <span id="custom-prev" class="custom-prev">&#9664;</span>
                                    <span id="custom-next" class="custom-next">&#9654;</span>
                                </nav>
                                <h2 id="custom-month" class="custom-month"></h2>
                                <h3 id="custom-year" class="custom-year"></h3>
                            </div>
                            <div id="calendar" class="fc-calendar-container"></div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="row hours">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $(document).on('click', '.fc-day', function () 
        {
            if(!$(this).hasClass('disabled'))
            {
                $('.loading').fadeIn();

                var date = $(this).attr('attr-date');
                var text = $(this).attr('attr-date-text');
                var services = $('#services').val();
                var user = $('#user').val();


                $('#date').val(date);

                $('.fc-day').removeClass('select');
                $(this).addClass('select');

                $.get("{{url('booking/aviable-hours')}}?business_id={{$business->id}}&services=" + services + "&user_id=" + user + "&date=" + date, function (response) {
                    
                    $('.hours').html("");
                    
                    var hour = "<div class='col-12 text-center hours-title mb-4'><p>"+text+"</p></div>";
                    
                    if(response.length == 0)
                    {
                        var none = '<div class="row"><div class="col-12 text-center py-5 text-secondary  "><i class="fa fa-exclamation-circle fa-3x"></i><br><br><p>{{__("web.not_hours")}}</p></div></div>';

                        $('.hours').append(none);
                        
                    }else
                    {
                        $.each(response, function (index, element) {
                            hour += "<div class='col-4'><div class='row rounded-2 btn-hour text-start m-2 p-2' attr-date='" + date + "' attr-hour='" + element + "'><div class='col-12 text-center'>" + element + "</div></div></div>";

                        });
                        
                        $('.hours').append(hour);
                    }

                }).done(function () {
                    $('.loading').fadeOut();
                });
            }
        });

        $(document).on('click', '.btn-hour', function () 
        {
            var hour        = $(this).attr('attr-hour');
            var date        = $(this).attr('attr-date');
            var services    = $('#services').val();
            var user        = $('#user').val();

            $('#hour').val(hour);

            $('.loading').fadeIn();

            $('#collapseDate').removeClass('show');
            $('#collapseDateButton').addClass('collapsed');
            $('#collapseDateButton').html('<i class="fa fa-check-circle text-success" style="margin-right:2em;"></i> <b>{{__("web.date_hour")}}: </b> &nbsp;'+date+" "+hour);

            $('#collapseFinish').addClass('show');
            $('#collapseFinishButton').removeClass('collapsed');
            $('#collapseFinishButton').attr('aria-expanded', 'true');

            var details = '<div class="col-12"><p><b>{{__("web.date_hour")}}: </b> '+date+" "+hour+'</p></div>';

            $('.details').append(details);

            $('.loading').fadeOut();

        });

        $("#custom-next").on("click", nextMonth);
        $("#custom-prev").on("click", prevMonth);
    });
</script>
