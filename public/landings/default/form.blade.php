<div class="accordion-item">
    <h2 class="accordion-header" id="headingFour">
        <button id="collapseFinishButton" class="accordion-button collapsed" type="button"  data-bs-target="#collapseFinish" aria-expanded="false" aria-controls="collapseFinish">
            <i class="far fa-clock" style="margin-right:2em;"></i> <b>{{__('web.finish')}}</b>
        </button>
    </h2>
    <div id="collapseFinish" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#acordionBooking">
        <div class="accordion-body">
        	 <div class="row my-1">
                <div class="col-12 text-secondary">
                    <p>{{__('web.form')}}</p>
                </div>
            </div>
        	<div class="row">
				<input type="hidden" id="services" name="services">
			    <input type="hidden" id="user" name="user">
			    <input type="hidden" id="date" name="date">
			    <input type="hidden" id="hour" name="hour">
			    <input type="hidden" id="dates" name="dates">

				<div class="col-12 mt-2">
					<div class="form-group">
						<label for="" class="form-label">{{trans('web.name')}} *</label>
						<input type="text" class="form-control" name="name" id="name" required placeholder="Escribe tu nombre">
					</div>
				</div>
				
				<div class="col-12 col-md-6 mt-2">
					<div class="form-group">
						<label for="" class="form-label">{{trans('web.email')}} *</label>
						<input type="email" class="form-control" name="email" id="email" required="" placeholder="Escibe tu email">
					</div>	
				</div>
					
				<div class="col-12 col-md-6 mt-2">
					<div class="form-group">
						<label for="" class="form-label">{{trans('web.phone')}} *</label>
						<input type="phone" class="form-control" name="phone" id="phone" required="" placeholder="Escibe tu telefono">
					</div>
				</div>

				<div class="col-12 mt-2">
					<div class="form-group">
						<label for="" class="form-label">{{trans('web.comment')}}</label>
						<textarea id="description" name="description" cols="30" rows="3" class="form-control" placeholder="¿Quieres agregar algun comentario a la reserva?"></textarea>
					</div>
				</div>
				
				<div class="col-12 mt-2">
					<div class="form-group">
						<small>{{trans('web.required')}}</small>
					</div>
				</div>
				<div class="col-12 d-grid gap-2">
					<a class="btn btn-info p-3 btn-send-form mt-3 text-white">
						<i class="fa fa-paper-plane"></i>
						{{trans('web.send')}}
					</a>
				</div>
			</div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function()
	{
		$('.btn-send-form').click(function()
		{
			$('.loading').fadeIn();

			$.ajax({
				type: "POST",
				url: "{{url('booking/store')}}",
				dataType: "JSON",
				data: {
					"_token" : "{{csrf_token()}}",
					"business" : "{{$business->id}}",
					"services" : $('#services').val(),
					"user" : $('#user').val(),
					"date" : $('#date').val(),
					"hour" : $('#hour').val(),
					"name" : $('#name').val(),
					"email" : $('#email').val(),
					"phone" : $('#phone').val(),
					"description" : $('#description').val(),
				},
				success : function(result)
				{
					var content = '<div class="row"><div class="col-12 text-center py-4 text-secondary  "><p style="font-size:1.5em;"><i class="fa fa-exclamation-circle fa-3x"></i><br><br>'+result.message+'</p></div></div>';
					
					if(result.status == "error")
						$('#modalMessage > .modal-dialog > .modal-content > .modal-header').addClass('bg-danger');	
					else
						$('#modalMessage > .modal-dialog > .modal-content > .modal-header').addClass('bg-success');

					$('#modalMessage > .modal-dialog > .modal-content > .modal-body').html(content);
					$('#modalMessage').modal('toggle');

					$(".body").load("{{url($business->code)}}/body");
				}
			}).done(function()
			{
				$('.loading').fadeOut();
			});
		});
	});
</script>