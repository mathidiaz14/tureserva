<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{$business->name}}</title>

    <!-- Optimized CSS loading -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">

    <!-- Inline critical CSS -->
    <style>

    </style>

    <!-- External CSS -->
    <link rel="stylesheet" href="{{asset('landings/default/assets/style.css')}}">
    <link rel="stylesheet" href="{{asset('landings/default/assets/calendar.css')}}">
    <link rel="shortcut icon" type="image" href="{{$business->logo != null ? asset($business->logo) : asset('images/icon2.png')}}">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
    <script src="{{asset('landings/default/assets/calendar.js')}}"></script>
</head>

<body>
    @include('helpers.loading')

    <header class="container-xxl">
        <div class="row mt-5">
            <div class="col-12 col-md-6 text-md-end text-center">
                <img class="img-fluid logo" src="{{$business->logo != null ? asset($business->logo) : asset('images/icon.png')}}" width="150" height="150" alt="">
            </div>
            <div class="col-12 col-md-6 text-md-start text-center">
                <h4 class="mt-5">{{$business->name}}</h4>
                <p>{{$business->address}}</p>
            </div>
        </div>
    </header>

    <div class="container-xxl">
    	@include('landings.default.parts.messages')
    </div>

    <div class="container-xxl my-3 container_body">
    	@if(landing('client_menssage', $business->id) != "")
     <div class="row">
        <div class="col-12">
           <div class="alert alert-warning text-center pt-4">
              <p>{{landing('client_menssage', $business->id)}}</p>
          </div>	
      </div>
  </div>
  @endif

  <div class="row">
     <div class="col-12">
        @include('landings.default.parts.menu')
    </div>
</div>

<div class="row body">
    @include('landings.default.content')
</div>

<div class="row">
   <div class="col-12 text-center mt-3 py-4 text-secondary">
      <p>TuReserva.io</p>
  </div>
</div>
</div>

<div class="modal fade" id="modalMessage" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">

    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-12">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script type="text/javascript">
   $(document).ready(function()
   {
      var width 			= $('#details').css('width');
      var margin 			= $('.container_body').css('margin-right');

      window.onscroll = function() {myFunction()};

      var div = document.getElementById("details");
      var sticky = div.offsetTop;

      function myFunction() 
      {
       if ((window.pageYOffset + 20) > sticky)
       {
           div.classList.add("sticky");
           $('.sticky').css('width', width);
           $('.sticky').css('right', margin);
       }
       else 
       {
           div.classList.remove("sticky");
       }

   }
})
</script>

</body>

</html>
