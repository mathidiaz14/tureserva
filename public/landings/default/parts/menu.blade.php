<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="container-fluid">
		<a class="navbar-brand" href="{{url($business->code)}}">{{$business->name}}</a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}">{{__('web.home')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>

				@if(landing('gallery', $business->id) == "on")
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}/gallery">{{__('web.gallery')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
				@endif

				@if(landing('map', $business->id) == "on")
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}/map">{{__('web.map')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
				@endif

				@if(landing('tips', $business->id) == "on")
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}/tips">{{__('web.tips')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
				@endif

				@if(landing('opinions', $business->id) == "on")
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}/opinions">{{__('web.opinions')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
				@endif

				@if(landing('contact', $business->id) == "on")
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}/contact">{{__('message.contact')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
				@endif

				@if(landing('hours', $business->id) == "on")
				<li class="nav-item active">
					<a class="nav-link" href="{{url($business->code)}}/hours">{{__('web.hours')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
				@endif

				<li class="nav-item">
					<a class="nav-link" href="{{url('cancel')}}">{{__('web.cancel_task')}}
						<span class="sr-only">(current)</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>