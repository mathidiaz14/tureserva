<div class="col-12 col-md-8 mt-3">
    <div class="accordion border-0" id="acordionBooking">
        @include('landings.default.service')
        
        @include('landings.default.user')
        
        @include('landings.default.dates')
        
        @include('landings.default.form')
    </div>
</div>
<div class="col-12 col-md-4 mt-3" id="details">
	@include('landings.default.details')
</div>