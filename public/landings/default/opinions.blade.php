<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>{{$business->name}}</title>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

	<link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
	
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
	
	<link rel="stylesheet" href="{{asset('landings/default/assets/style.css')}}">

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
	<meta name="csrf-token" content="{{ csrf_token() }}">
  	
  	@if($business->logo != null)
    	<link rel="shortcut icon" type="image" href="{{asset($business->logo)}}">
  	@else
    	<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
  	@endif

  	<style>
  		body
  		{
  			background: {{landing('background', $business->id)}}!important;
  			color: {{landing('text_color', $business->id)}}!important;
  		}

  		nav, footer
  		{
  			background: {{landing('header_color', $business->id)}}!important;
  		}

  		.opinion
  		{
  			background: {{landing('header_color', $business->id)}}!important;
  			color:white;
  			padding:20px;
  		}
  	</style>

</head>
<body>
    <header class="py-5 bg-image-full" style="
			@if($business->reservation->header == null)
				background-image: url({{asset('images/header.jpg')}});
			@else
				background-image: url({{asset(landing('header', $business->id))}});
			@endif
    ">

	    @if($business->logo != null)
	      	<img class="img-fluid d-block mx-auto" src="{{asset($business->logo)}}" width="200px" height="200px" alt="">
	    @else
	      	<div class="mx-auto logo_null">
	            <h3><b>{{LogoLetter($business->name)}}</b></h3>
	      	</div>
	    @endif
  	</header>

  	@include('landings.default.parts.menu')
	

	@if(landing('text', $business->id) != "")
		<div class="alert alert-warning">
			<p>{{landing('text', $business->id)}}</p>
		</div>	
	@endif

	<section class="py-5" id="top">
	    <div class="container text-center">
	    	<p class="lead">{{__('web.all_opinions')}}</p>
	    	<hr>
		</div>
	</section>	

	<section class="pb-5">
		<div class="container">
			@if($business->opinions->count() > 0)
				<div class="col-12">
					<div class="row">
						@foreach($business->opinions->take(5) as $opinion)
							<div class="col-12 p-4 m-3 opinion">
								<div class="row">
									<div class="col">
										<p>{{$opinion->star}} <i class="fa fa-star"></i></p>
									</div>
									<div class="col text-end">
										<p>{{$opinion->created_at->format('d/m/Y')}}</p>
									</div>
								</div>
								{{$opinion->comment}}
							</div>
						@endforeach
					</div>
				</div>
			@endif
		</div>
	</section>
  
	<section class="py-5 bg-image-full" style="
		@if($business->reservation->footer == null)
			background-image: url({{asset('images/footer.jpg')}});
		@else
			background-image: url({{asset(landing('footer', $business->id))}});
		@endif
	">
	</section>

	<!-- Footer -->
	<footer class="py-5">
		<div class="container">
			<a href="{{url('/')}}" target="_blank">
				<p class="m-0 text-center text-white">
					<img src="{{asset('images/icon.png')}}" alt="" width="30px">
					{{config('app.name')}}
				</p>
			</a>
		</div>
	</footer>
  
	<script>
		
		$(document).ready(function()
		{
    		$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });    		
		});
	</script>
</body>

</html>
