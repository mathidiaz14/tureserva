<!-- Users Accordion -->
<div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
        <button id="collapseUsersButton" class="accordion-button collapsed" type="button"  data-bs-target="#collapseUsers" aria-expanded="false" aria-controls="collapseUsers">
            <i class="far fa-clock" style="margin-right:2em;"></i> <b>{{__('web.user')}}</b>
        </button>
    </h2>
    <div id="collapseUsers" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#acordionBooking">
        <div class="accordion-body">
             <div class="row my-1">
                <div class="col-12 text-secondary">
                    <p>{{__('web.users')}}</p>
                </div>
            </div>
            <div class="row users">
               
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $(document).on('click', '.btn-user', function () 
        {
            $('.loading').fadeIn();

            var user = $(this).attr('attr-id');
            var name = $(this).attr('attr-name');
            
            var services = $('#services').val();

            $('#user').val(user);

            $.get("{{url('booking/aviable-dates')}}?business_id={{$business->id}}&services=" + services + "&user_id=" + user, function (response) {
                $('#dates').val(response);
                $('#date').val(response[0]);

                renderCalendar(currentMonth, currentYear, response);

            }).done(function () {
                var currentDate = $('#date').val();

                var text = $('#day_'+currentDate).attr('attr-date-text');

                $.get("{{url('booking/aviable-hours')}}?business_id={{$business->id}}&services=" + services + "&user_id=" + user + "&date=" + currentDate, function (response) {
                    
                    var hour = "<div class='col-12 text-center hours-title mb-4'><p>"+text+"</p></div>";

                    if(response.length == 0)
                    {
                        var none = '<div class="row"><div class="col-12 text-center py-5 text-secondary  "><i class="fa fa-exclamation-circle fa-3x"></i><br><br><p>{{__("web.not_hours")}}</p></div></div>';

                        $('.hours').append(none);
                        
                    }else
                    {
                        $.each(response, function (index, element) {
                            hour += "<div class='col-4'><div class='row rounded-2 btn-hour text-start m-2 p-2' attr-date='" + currentDate + "' attr-hour='" + element + "'><div class='col-12 text-center'>" + element + "</div></div></div>";

                        });
                        
                        $('.hours').append(hour);
                    }

                }).done(function(){
                    
                    $('#collapseUsers').removeClass('show');
                    $('#collapseUsersButton').addClass('collapsed');
                    $('#collapseUsersButton').html('<i class="fa fa-check-circle text-success" style="margin-right:2em;"></i> <b>{{__("web.user")}}: </b> &nbsp;'+name);

                    $('#collapseDate').addClass('show');
                    $('#collapseDateButton').removeClass('collapsed');
                    $('#collapseDateButton').attr('aria-expanded', 'true');

                    var details = '<div class="col-12"><p><b>{{__("web.user")}}:</b> '+name+'</p></div>';

                    $('.details').append(details);

                    var details = '<br><div class="col-12 text-danger"><p><small>{{__("web.details_time")}}</small></p></div>';
                    $('#details').append(details);

                    $('#timer-content').fadeIn();
                    $("#timer").timer(
                    {
                        duration:'300m',
                        callback: function()
                        {
                            $(".body").load("{{url($business->code)}}/body");
                        },
                    });

                    $('.loading').fadeOut();
                });
            });

        });
    })
</script>