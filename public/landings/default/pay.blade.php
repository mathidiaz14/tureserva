<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>{{$business->name}}</title>

	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
	<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">
	
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/PrintArea.js')}}"></script>
	<script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
  	
  	@if($business->logo != null)
    	<link rel="shortcut icon" type="image/png" href="{{asset($business->logo)}}">
  	@else
    	<link rel="shortcut icon" type="image/png" href="{{asset('images/icon.png')}}">
  	@endif

  	<style>
  		.mercadopago-button
			{
				display: -webkit-box!important;
				display: -webkit-flex!important;
				display: -moz-box!important;
				display: -ms-flexbox!important;
				display: flex!important;
				justify-content: center!important;
				align-items: center!important;
				padding: 20 50px!important;
				border-radius: 5px!important;
				background: #009ee3!important;
				height: 70px;
				font-size: 20px!important;
				color: #fff!important;
				line-height: 1.2!important;
				text-transform: uppercase!important;
				letter-spacing: 1px!important;

				-webkit-transition: all 0.4s!important;
				-o-transition: all 0.4s!important;
				-moz-transition: all 0.4s!important;
				transition: all 0.4s!important;
				width: 100%;
				box-shadow: 0 5px 5px rgb(0 0 0 / 10%), 0 -3px 0 rgb(0 0 0 / 30%) inset;
			}

			.mercadopago-button:hover
			{
				background: #333333!important;
			}
  	</style>
</head>
<body>
  
  	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    	<div class="container">
      		<a class="navbar-brand" href="{{url('web', $business->code)}}">{{$business->name}}</a>
  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    			<span class="navbar-toggler-icon"></span>
  			</button>
      		<div class="collapse navbar-collapse" id="navbarResponsive">
        		<ul class="navbar-nav ml-auto">
	      			<li class="nav-item active">
	        			<a class="nav-link" href="{{url('web', $business->code)}}">{{trans('web.home')}}
	          				<span class="sr-only">(current)</span>
	        			</a>
	      			</li>
			      	
			      	<li class="nav-item">
	        			<a class="nav-link" href="{{url('web/cancel')}}">{{trans('web.cancel_task')}}
	          				<span class="sr-only">(current)</span>
	        			</a>
	      			</li>
	    		</ul>
      		</div>
    	</div>
  	</nav>

  <!-- Header - set the background image for the header in the line below -->
    <header class="py-5 bg-image-full" style="
		@if($business->reservation->header == null)
			background-image: url({{asset('images/header.jpg')}});
		@else
			background-image: url({{asset($business->reservation->header)}});
		@endif
    ">

	    @if($business->logo != null)
	      	<img class="img-fluid d-block mx-auto" src="{{asset($business->logo)}}" width="200px" height="200px" alt="">
	    @else
	      	<div class="mx-auto logo_null">
	            <h3><b>{{LogoLetter($business->name)}}</b></h3>
	      	</div>
	    @endif
  	</header>
	
		@if(($business->reservation->text != NULL) and ($business->reservation->text != ""))
			<div class="alert alert-warning">
				<p>{{$business->reservation->text}}</p>
			</div>	
		@endif
  
  	<section class="py-5" id="top">
	    <div class="container" style="max-width: 1300px;">
      		<h1>{{$business->name}}</h1>
	      	
	      	<p class="lead">{{trans('web.pay_info')}}</p>
	      	
	      	<b class="time_count" style="font-size:20px;"></b>
	      	<hr>
	    		<div class="row">
	    			<div class="col-xs-12 col-md-6 text-center">
	    				<script
								src="https://www.mercadopago.com.uy/integrations/v1/web-payment-checkout.js"
								data-preference-id="<?php echo $preference->id; ?>"
								data-button-label="{{trans('web.mercadopago')}}">
							</script>
	    			</div>
	    			<div class="col-xs-12 col-md-6">
	    				<a href="{{url('web', $business->code)}}" class="btn mercadopago-button btn-home" style="background: #333333!important;">
	    					<i class="fa fa-times" style="padding-right: 10px;"></i>
	    					{{trans('web.cancel')}}
	    				</a>
	    			</div>
	    		</div>	
			</div>
  	</section>
		<section class="py-5 bg-image-full" style="
			@if($business->reservation->footer == null)
				background-image: url({{asset('images/footer.jpg')}});
			@else
				background-image: url({{asset($business->reservation->footer)}});
			@endif
		">
  	</section>

	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<a href="{{config('app.url')}}" target="_blank">
				<p class="m-0 text-center text-white">
					<img src="{{asset('images/icon.png')}}" alt="" width="30px">
					{{config('app.name')}}
				</p>
			</a>
		</div>
	</footer>
  
	<script>
		$(".time_count").timer(
		{
			duration:'2m',
			callback: function(){
				window.location.replace("{{url('web/'.$business->code)}}");
			},
		});
	</script>
</body>

</html>
