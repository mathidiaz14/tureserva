<footer id="footer">
	<section>
		<p>{{landing('text_footer', $business->id)}}</p>
	</section>
	<section>
		<h2>Datos de contacto</h2>
		<dl class="alt">
			<dt>{{__('web.address')}}</dt>
			<dd>{{landing('address', $business->id)}}</dd>
			<dt>{{__('web.phone')}}</dt>
			<dd>{{landing('phone', $business->id)}}</dd>
			<dt>{{__('web.email')}}</dt>
			<dd><a href="mailto:{{landing('email', $business->id)}}">{{landing('email', $business->id)}}</a></dd>
		</dl>
		<ul class="icons">
			@if(landing('twitter', $business->id) != "")
				<li><a href="https://twitter.com/{{landing('twitter', $business->id)}}" class="icon brands fa-twitter alt"><span class="label">Twitter</span></a></li>
			@endif

			@if(landing('facebook', $business->id) != "")
				<li><a href="https://facebook.com/{{landing('facebook', $business->id)}}" class="icon brands fa-facebook-f alt"><span class="label">Facebook</span></a></li>
			@endif

			@if(landing('instagram', $business->id) != "")
				<li><a href="https://instagram.com/{{landing('instagram', $business->id)}}" class="icon brands fa-instagram alt"><span class="label">Instagram</span></a></li>
			@endif
		</ul>
	</section>
	<p class="copyright">Design by: <a href="https://{{env('APP_NAME')}}">{{env('APP_NAME')}}</a>.</p>
</footer>