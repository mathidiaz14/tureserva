<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
<link href="{{asset('css/full-width-pics.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>

<section id="gallery" class="main special">
	<header class="major">
		<h2>{{__('web.gallery')}}</h2>
	</header>
	<div class="tz-gallery">
		@if(count($business->medias) == 0)
			<div class="row" style="color:gray!important;">
				<div class="col-12 text-center pt-5">
					<br><br>
					<i class="fa fa-images fa-3x"></i>
					<br><br>
					<p>{{trans('web.no_photos')}}</p>
				</div>
			</div>
		@else
			<div class="row">
				@foreach($business->medias as $media)
					
			            <div class="col-6 col-md-4 col-3-large">
							@if($loop->iteration < 4)
				                <a class="lightbox" href="{{asset($media->url)}}">
				                    <img src="{{asset($media->url)}}" alt="Park">
				                </a>
				            @else
								<a class="lightbox" href="{{asset($media->url)}}" style="display: none;">
				                    <img src="{{asset($media->url)}}" alt="Park">
				                </a>
				            @endif
			            </div>
				@endforeach
		    </div>
		    <small>{{count($business->medias)}} {{trans('web.photos')}}</small>
		@endif
	</div>
</section>

<script>
	$(document).ready(function()
	{
		baguetteBox.run('.tz-gallery');
	});
</script>

