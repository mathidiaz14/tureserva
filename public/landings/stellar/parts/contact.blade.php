<section id="contact" class="main special">
	<header class="major">
		<h2>{{__('web.contact')}}</h2>
	</header>	
	@include('helpers.alert_login')
	<form action="{{url('web/contact')}}" method="post">
		@csrf
		<input type="hidden" name="business" value="{{$business->id}}">
		<div class="row gtr-uniform">
			<div class="col-md-6 col-xs-12 mb-3">
				<input type="text" name="name" required placeholder="{{trans('web.name_write')}}">		
			</div>
			<div class="col-md-6 col-xs-12 mb-3">
				<input type="email" name="email" required placeholder="{{trans('web.email_write')}}">		
			</div>
			<div class="col-12">
				<textarea name="message" placeholder="{{__('web.message_write')}}" rows="6"></textarea>
			</div>
			<div class="col-12 mt-3">
				<ul class="actions">
					<li><input type="submit" value="{{__('web.send')}}" class="primary"></li>
				</ul>
			</div>
		</div>
	</form>
</section>