<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">

<div class="tz-gallery">
	<hr>
	@if(count($business->medias) == 0)
		<div class="row" style="color:gray!important;">
			<div class="col-sm-12 text-center pt-5">
				<br><br>
				<i class="fa fa-images fa-3x"></i>
				<br><br>
				<p>{{trans('web.no_photos')}}</p>
			</div>
		</div>
	@else
		<div class="row">
			@foreach($business->medias as $media)
				@if($media->web == "on")
		            <div class="col-xs-6 col-md-4 col-lg-3">
						@if($loop->iteration < 4)
			                <a class="lightbox" href="{{asset($media->url)}}">
			                    <img src="{{asset($media->url)}}" alt="Park">
			                </a>
			            @else
							<a class="lightbox" href="{{asset($media->url)}}" style="display: none;">
			                    <img src="{{asset($media->url)}}" alt="Park">
			                </a>
			            @endif
		            </div>
				@endif
			@endforeach
	    </div>
	    <small>{{count($business->medias->where('web', 'on'))}} {{trans('web.photos')}}</small>
	@endif
	<br><br>
	<hr>
</div>

<script>
	$(document).ready(function()
	{
		baguetteBox.run('.tz-gallery');
	});
</script>