<div class="col-12 col-md mb-3">
	<hr>
	<p class="lead">{{__('web.contact')}}</p>
	@include('helpers.alert_login')
	<form action="{{url('web/contact')}}" method="post" class="form-horizontal">
		@csrf
		<input type="hidden" name="business" value="{{$business->id}}">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="" class="form-label">{{trans('web.name')}}</label>
					<input type="text" class="form-control" name="name" required placeholder="{{trans('web.name_write')}}">		
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="form-group">
					<label for="" class="form-label">{{trans('web.email')}}</label>
					<input type="email" class="form-control" name="email" required="" placeholder="{{trans('web.email_write')}}">
				</div>	
			</div>
		</div>
			
		<div class="form-group">
			<label for="" class="form-label">{{trans('web.message')}}</label>
			<textarea name="message" cols="30" rows="5" class="form-control" placeholder="{{trans('web.message_write')}}"></textarea>
		</div>
		<div class="form-group text-end my-3">
			<button class="btn btn-primary btn-lg">
				{{trans('web.send')}}
			</button>
		</div>
	</form>
</div>