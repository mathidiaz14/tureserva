<section id="aboutUs"><!--Aboutus-->
	<div class="inner_wrapper aboutUs-container fadeInLeft animated wow">
		<div class="container">
			<h2>Sobre nosotros</h2>
			<div class="inner_section">
				<div class="row">
				<div class="col-md-6"> 
					<img class="img-responsive" src="{{landing('about_image', $business->id)}}" align=""> 
				</div>
				<div class="col-md-6">
					<p>{{landing('about_menssage', $business->id)}}</p>				 
				</div>
				</div>
			</div>
		</div> 
	</div>
</section>