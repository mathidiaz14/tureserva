@if($users->count() == 0)
	<div class="row">
		<div class="col-sm-12 text-center text-secondary">
			<br><br>
			<i class="fa fa-exclamation-triangle fa-3x"></i>
			<br><br>
			<p>{{trans('web.not_users')}}</p>
		</div>
	</div>
@else
	<p class="lead">{{trans('web.users')}}</p>
	<div class="row">
	    @foreach($users as $user)
			<div class="col-sm col-md-4">
				<div class="btn-user" user_id="{{$user->id}}" user_name="{{$user->name}}">
			      	<div class="row">
			      		<div class="col text-center">
			      			<h5>
			      				{{$user->name}}
			      			</h5>
			      		</div>
			      	</div>
				</div>
			</div>
	    @endforeach

	    @if($users->count() > 1)
		    <div class="col-sm col-md-4">
				<div class="btn-user" user_id="nobody" user_name="{{__('web.nobody')}}">
			      	<div class="row">
			      		<div class="col text-center">
			      			<h5>
			      				{{__('web.nobody')}}
			      			</h5>
			      		</div>
			      	</div>
				</div>
			</div>
		@endif

	</div>
	<script>
		$(document).ready(function()
		{
			$('.btn-user').click(function()
			{
				$('.loading').fadeIn();
				$('.user_section').hide();
				$('.timelapse_all').hide();

				var user 	= $(this).attr('user_id');
				var name 	= $(this).attr('user_name');

				$('.date_section').load("{{url('web/dates')}}/{{$service->id}}/"+user, function()
				{
					$('.timelapse_user').html('<b>'+name+'</b>');
					$('.timelapse_user').removeClass('active');
					$('.timelapse_date').addClass('active');
					
					$('#form #user_id').val(user);
					$('.date_section').fadeIn();
					$('.timelapse_all').fadeIn();

					$('.time_count_content').fadeIn();
					$(".time_count").timer(
					{
						duration:'5m',
						callback: function(){
							window.location.reload();
						},
					});

					$('.loading').fadeOut();
				});
				
			});
		});
	</script>
@endif

<div class="row">
	<div class="col">
		<a href="{{url($business->code)}}">
			<i class="fa fa-angle-left"></i>
			Volver a empezar
		</a>
	</div>
</div>
<!-- FIN USUARIOS -->