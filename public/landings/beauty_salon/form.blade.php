<div class="col-12 text-center">
	<h4>{{trans('web.form')}}</h4>
</div>
<div class="col-12">
	<form id="form" method="post" action="{{url('web')}}" class="form-horizontal">
		@csrf
		<div class="row">
			<input type="hidden" id="service_id" class="service_id" name="service" >
			<input type="hidden" id="user_id" class="user_id" name="user" >
			<input type="hidden" id="date" name="date" >
			<input type="hidden" id="hour" name="hour" >
			<input type="hidden" id="business" name="business" value="{{$business->id}}">

			<div class="col-12 mt-2">
				<div class="form-group">
					<label for="" class="form-label">{{trans('web.name')}} *</label>
					<input type="text" class="form-control" name="name" id="name" required placeholder="Escribe tu nombre">
				</div>
			</div>
			
			<div class="col-12 col-md-6 mt-2">
				<div class="form-group">
					<label for="" class="form-label">{{trans('web.email')}} *</label>
					<input type="email" class="form-control" name="email" id="email" required="" placeholder="Escibe tu email">
				</div>	
			</div>
				
			<div class="col-12 col-md-6 mt-2">
				<div class="form-group">
					<label for="" class="form-label">{{trans('web.phone')}} *</label>
					<input type="phone" class="form-control" name="phone" id="phone" required="" placeholder="Escibe tu telefono">
				</div>
			</div>

			<div class="col-12 mt-2">
				<div class="form-group">
					<label for="" class="form-label">{{trans('web.comment')}}</label>
					<textarea id="description" name="description" cols="30" rows="3" class="form-control" placeholder="¿Quieres agregar algun comentario a la reserva?"></textarea>
				</div>
			</div>
			
			<div class="col-12 mt-2">
				<div class="form-group">
					<small>{{trans('web.required')}}</small>
				</div>
			</div>
			<div class="col-12">
				<div class="form-group text-end">
					<button class="btn btn-primary btn-lg">
						<i class="fa fa-paper-plane"></i>
						{{trans('web.send')}}
					</button>
				</div>
			</div>
		</div>
	</form>
</div>

<script>
	$(document).ready(function()
	{
		$('.timelapse_date').click(function()
		{
			if($(this).hasClass('pointer'))
			{
				$('.loading').fadeIn();
				$('.form').fadeOut();
				
				$('form input').val("");
				$('form textarea').val("");

				$('.timelapse_date').html('<b>{{__("web.date_hour")}}</b>');
				
				$('.timelapse .active').removeClass('active');
				$('.timelapse_date').addClass('active');
				$('.timelapse_date').removeClass('pointer');

				$('.dates').fadeIn();
				$('.loading').fadeOut();
			}
		});
	});
</script>