<?php

use App\Http\Controllers\ReservationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CalendarOptionController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\DaysServiceController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\DaysResourceController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\OpinionController;
use App\Http\Controllers\StatisticsController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\MediaController;
use App\Http\Controllers\MercadoPagoController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UpgradeController;
use App\Http\Controllers\DaysController;
use App\Http\Controllers\DaysUsersController;
use App\Http\Controllers\AbsenceController;
use App\Http\Controllers\TipController;
use App\Http\Controllers\WebController;
use App\Http\Controllers\FirstController;
use App\Http\Controllers\NoteClientController;
use App\Http\Controllers\WaitingController;
use App\Http\Controllers\VisitController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\IncidentController;
use App\Http\Controllers\BookingController;

use App\Http\Controllers\help\HelpController;

use App\Http\Controllers\root\RootHelpCategoryController;
use App\Http\Controllers\root\RootHelpPostController;
use App\Http\Controllers\root\RootHomeController;
use App\Http\Controllers\root\RootUserController;
use App\Http\Controllers\root\RootBusinessController;
use App\Http\Controllers\root\RootCodeController;
use App\Http\Controllers\root\RootAdminController;

Route::get('/'									, [HomeController::class, 'landing']);

Route::get('null', function()
{
	return "";
});

Route::get('comenzar'							, [AuthController::class, 'showRegistrationForm']);
Route::get('empezar'							, [AuthController::class, 'showRegistrationForm']);
Route::get('register'							, [AuthController::class, 'showRegistrationForm']);
Route::post('register'							, [AuthController::class, 'create']);
Route::get('ingresar'							, [AuthController::class, 'showLoginForm']);
Route::get('login'								, [AuthController::class, 'showLoginForm'])->name('login');
Route::post('login'								, [AuthController::class, 'login']);

Route::get('home'								, [HomeController::class, 'index'])->middleware('auth.basic');
Route::get('help'								, [HomeController::class, 'help'])->middleware('auth.basic');

Route::get('password/request'					, [AuthController::class, 'get_password_request']);
Route::post('password/request'					, [AuthController::class, 'set_password_request']);
Route::get('password/email/{ID}'				, [AuthController::class, 'get_password_email']);
Route::post('password/email/{ID}'				, [AuthController::class, 'set_password_email']);

Route::post('logout'							, [AuthController::class, 'logout']);

Route::get('disabled'							, [AuthController::class, 'get_disabled']);
Route::get('pending'							, [AuthController::class, 'get_pending']);
Route::get('expired'							, [AuthController::class, 'get_expired']);
Route::post('expired'							, [AuthController::class, 'set_expired']);
Route::get('invitation/{code}'					, [AuthController::class, 'get_invitation']);
Route::post('invitation/{code}'					, [AuthController::class, 'set_invitation']);

Route::get('ayuda'								, [HelpController::class, 'index']);
Route::get('ayuda/{slug}'						, [HelpController::class, 'post']);
Route::get('ayuda/categoria/{slug}'				, [HelpController::class, 'category']);
Route::get('ayuda/buscar/{slug}'				, [HelpController::class, 'search']);


Route::get('first/login'						, [FirstController::class, 'index'])->middleware('auth.admin');
Route::get('first/step1'						, [FirstController::class, 'index'])->middleware('auth.admin');
Route::get('first/step2'						, [FirstController::class, 'step2'])->middleware('auth.admin');
Route::get('first/step3'						, [FirstController::class, 'step3'])->middleware('auth.admin');
Route::get('first/finish'						, [FirstController::class, 'finish'])->middleware('auth.admin');

Route::resource('reservation'					, ReservationController::class)->middleware('auth.admin');
Route::get('delete/header'						, [ReservationController::class, 'header'])->middleware('auth.admin');
Route::get('delete/footer'						, [ReservationController::class, 'footer'])->middleware('auth.admin');

Route::resource('business'						, BusinessController::class)->middleware('auth.admin');
Route::get('business/delete/logo/{id}'			, [BusinessController::class, 'delete_logo'])->middleware('auth.admin');
Route::get('business/get/hours'					, [BusinessController::class, 'get_hours'])->middleware('auth.admin');
Route::post('business/set/hours'				, [BusinessController::class, 'set_hours'])->middleware('auth.admin');
Route::post('business/set/status'				, [BusinessController::class, 'set_status'])->middleware('auth.admin');
Route::get('business/generate/code'				, [BusinessController::class, 'generate_code'])->middleware('auth.admin');

Route::get('days/delete/{ID}'					, [DaysController::class, 'delete'])->middleware('auth.only');
Route::post('days/add'							, [DaysController::class, 'store'])->middleware('auth.only');

Route::resource('user'							, UserController::class)->middleware('auth.admin');
Route::get('user/delete/avatar/{id}'			, [UserController::class, 'delete_avatar'])->middleware('auth.basic');
Route::get('profile'							, [UserController::class, 'profile'])->middleware('auth');
Route::post('profile'							, [UserController::class, 'set_profile'])->middleware('auth');
Route::get('user/random/color'					, [UserController::class, 'get_random_color'])->middleware('auth.admin');
Route::get('user/get/hours/{ID}'				, [UserController::class, 'get_hours'])->middleware('auth.admin');
Route::post('user/set/hours'					, [UserController::class, 'set_hours'])->middleware('auth.admin');
Route::post('user/set/status'					, [UserController::class, 'set_status'])->middleware('auth.admin');

Route::get('daysUser/delete/{ID}'				, [DaysUsersController::class, 'delete'])->middleware('auth.admin');
Route::post('daysUser/add'						, [DaysUsersController::class, 'store'])->middleware('auth.admin');

Route::get('verify'								, [UserController::class, 'get_verify'])->middleware('auth.only');
Route::get('verify/{code}'						, [UserController::class, 'set_verify']);
Route::get('send/verify'						, [UserController::class, 'send_verify']);

Route::resource('notification'					, NotificationController::class)->middleware('auth.basic');
Route::get('view/notification'					, [NotificationController::class, 'view'])->middleware('auth.notification');
Route::get('read/notification'					, [NotificationController::class, 'read'])->middleware('auth.notification');

Route::resource('calendar'						, CalendarController::class)->middleware('auth.basic');
Route::post('manual/calendar'					, [CalendarController::class ,'manual'])->middleware('auth.basic');
Route::get('users/calendar/{ID}'				, [CalendarController::class ,'users'])->middleware('auth.basic');
Route::get('resources/calendar/{ID}'			, [CalendarController::class ,'resources'])->middleware('auth.basic');
Route::post('accept/calendar'					, [CalendarController::class ,'accept'])->middleware('auth.basic');
Route::post('refuse/calendar'					, [CalendarController::class ,'refuse'])->middleware('auth.basic');
Route::get('faild/calendar/{id}'				, [CalendarController::class ,'faild'])->middleware('auth.basic');
Route::get('reload/calendar'					, [CalendarController::class ,'reload']);
Route::get('count/calendar'						, [CalendarController::class ,'count']);

Route::resource('client'						, ClientController::class)->middleware('auth.basic');
Route::post('client/create/jquery'				, [ClientController::class, 'create_jquery'])->middleware('auth.basic');
Route::resource('note/client'					, NoteClientController::class)->middleware('auth.basic');
Route::resource('calendar_option'				, CalendarOptionController::class)->middleware('auth.basic');
Route::resource('statistics'					, StatisticsController::class)->middleware('auth.admin');
Route::resource('note'							, NoteController::class)->middleware('auth.basic');
Route::resource('media'							, MediaController::class)->middleware('auth.basic');
Route::resource('opinion'						, OpinionController::class)->middleware('auth.admin');
Route::resource('absence'						, AbsenceController::class)->middleware('auth.admin');
Route::resource('tips'							, TipController::class)->middleware('auth.admin');
Route::resource('waiting'						, WaitingController::class)->middleware('auth.admin');
Route::resource('visit'							, VisitController::class)->middleware('auth.admin');

Route::resource('service'						, ServiceController::class)->middleware('auth.admin');
Route::get('service/get/hours/{ID}'				, [ServiceController::class, 'get_hours'])->middleware('auth.admin');
Route::post('service/set/hours'					, [ServiceController::class, 'set_hours'])->middleware('auth.admin');
Route::post('service/set/status'				, [ServiceController::class, 'set_status'])->middleware('auth.admin');

Route::get('daysService/delete/{ID}'			, [DaysServiceController::class, 'delete'])->middleware('auth.admin');
Route::post('daysService/add'					, [DaysServiceController::class, 'store'])->middleware('auth.admin');

Route::resource('resource'						, ResourceController::class)->middleware('auth.admin');
Route::get('resource/get/hours/{ID}'			, [ResourceController::class, 'get_hours'])->middleware('auth.admin');
Route::post('resource/set/hours'				, [ResourceController::class, 'set_hours'])->middleware('auth.admin');
Route::post('resource/set/status'				, [ResourceController::class, 'set_status'])->middleware('auth.admin');

Route::get('daysResource/delete/{ID}'			, [DaysResourceController::class, 'delete'])->middleware('auth.admin');
Route::post('daysResource/add'					, [DaysResourceController::class, 'store'])->middleware('auth.admin');

Route::resource('message'						, MessageController::class)->middleware('auth.admin');
Route::post('check/message'						, [MessageController::class, 'check_messages'])->middleware('auth.admin');

Route::resource('incident'						, IncidentController::class)->middleware('auth.admin');
Route::get('incident/screen/{ID}'				, [IncidentController::class, 'view_screen'])->middleware('auth.admin');
Route::get('incident/attach/{ID}'				, [IncidentController::class, 'view_attach'])->middleware('auth.admin');

Route::resource('upgrade'						, UpgradeController::class)->middleware('auth');
Route::post('cancel/upgrade'					, [UpgradeController::class, 'cancel'])->middleware('auth');
Route::get('details/upgrade'					, [UpgradeController::class, 'details'])->middleware('auth');
Route::get('change/card/upgrade'				, [UpgradeController::class, 'get_change_card'])->middleware('auth');
Route::post('change/card/upgrade'				, [UpgradeController::class, 'set_change_card'])->middleware('auth');

Route::resource('theme'							, LandingController::class)->middleware('auth.admin');
Route::get('theme/delete/{id}'					, [LandingController::class, 'delete'])->middleware('auth.admin');

Route::resource('contact'						, ContactController::class)->middleware('auth.basic');
Route::get('view/contact'						, [ContactController::class, 'view'])->middleware('auth.notification');
Route::get('read/contact'						, [ContactController::class, 'read'])->middleware('auth.notification');

Route::post('star'								, [OpinionController::class, 'store']);

Route::get('mercadopago' 						, [MercadoPagoController::class, 'index'])->middleware('auth.admin');
Route::post('mercadopago' 						, [MercadoPagoController::class, 'store'])->middleware('auth.admin');
Route::get('mercadopago/connect' 				, [MercadoPagoController::class, 'connect'])->middleware('auth.admin');

Route::post('whatsapp'							, [HomeController::class, 'whatsapp']);

Route::get('root'								, [RootHomeController::class, 'index'])->middleware('auth.root');
Route::resource('root/business'					, RootBusinessController::class)->middleware('auth.root');
Route::get('root/business/status/{ID}'			, [RootBusinessController::class, 'change_status'])->middleware('auth.root');
Route::get('root/business/landing/{ID}'			, [RootBusinessController::class, 'reinstall_landing'])->middleware('auth.root');
Route::post('root/business/control/{ID}'		, [RootBusinessController::class, 'control'])->middleware('auth.root');

Route::resource('root/user'						, RootUserController::class)->middleware('auth.root');
Route::resource('root/category/help'			, RootHelpCategoryController::class)->middleware('auth.root');
Route::resource('root/post/help'				, RootHelpPostController::class)->middleware('auth.root');

Route::get('root/email'							, [RootHomeController::class, 'get_email'])->middleware('auth.root');
Route::post('root/email'						, [RootHomeController::class, 'send_email'])->middleware('auth.root');
Route::post('root/sms'							, [RootHomeController::class, 'send_sms'])->middleware('auth.root');
Route::post('root/wpp'							, [RootHomeController::class, 'send_wpp'])->middleware('auth.root');
Route::get('root/cron'							, [RootHomeController::class, 'get_cron'])->middleware('auth.root');

Route::get('root/admin'							, [RootAdminController::class, 'index'])->middleware('auth.root');
Route::get('root/admin/git/pull'				, [RootAdminController::class, 'git_pull'])->middleware('auth.root');
Route::post('root/update/env'					, [RootAdminController::class, 'env_update'])->middleware('auth.root');

Route::get('load/images' 								, function()
{
	return view('admin.media.images');
})->middleware('auth.basic');

Route::post('contact_root'						, [HomeController::class, 'contact_root']);

Route::post('booking/store'						, [BookingController::class, 'store']);
Route::get('booking/services'					, [BookingController::class, 'getServices']);
Route::get('booking/users'						, [BookingController::class, 'getUsers']);
Route::get('booking/aviable-dates'				, [BookingController::class, 'getAvailableDays']);
Route::get('booking/aviable-hours'				, [BookingController::class, 'getAvailableHours']);
Route::get('booking/success'					, [BookingController::class, 'task_success']);
Route::get('booking/failed'						, [BookingController::class, 'task_failed']);

Route::get('{ID}/tips'							, [WebController::class, 'tips']);
Route::get('{ID}/gallery'						, [WebController::class, 'gallery']);
Route::get('{ID}/map'							, [WebController::class, 'map']);
Route::get('{ID}/opinions'						, [WebController::class, 'opinions']);
Route::get('{ID}/contact'						, [WebController::class, 'getContact']);
Route::post('{ID}/contact'						, [WebController::class, 'setContact']);
Route::get('{ID}/hours'							, [WebController::class, 'hours']);
Route::get('{ID}/body'							, [WebController::class, 'body']);
Route::get('{ID}'								, [WebController::class, 'show']);

Route::get('cancelar'							, [WebController::class, 'get_cancel']);
Route::get('cancelar/{ID}'						, [WebController::class, 'get_cancel_id']);

Route::post('web/waiting'						, [WebController::class, 'set_waiting']);

Route::post('visit'								, [WebController::class, 'visit']);
Route::get('qr/{ID}'							, [WebController::class, 'qr']);
